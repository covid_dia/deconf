import json
import subprocess
import sys

if __name__=="__main__":
    with open(sys.argv[1]) as fp:
        config = json.load(fp)

    processes = []
    ports = {}#one assumes that if it's the same port, it's the same server, so no need to relaunch
    for key,model in config['models'].items():
        if model["port"] not in ports:
            ports[model["port"]]=key
            cmd = """source venv/bin/activate;
            PYTHONPATH+=. python src/model/%s --interface %s:%d"""%(model["cmd"],
                                                                    model["host"],
                                                                    model["port"])
        
        
            processes.append(subprocess.Popen(cmd, shell=True, executable="/bin/bash"))

            print("launched model server %s at %s:%d"%(model["cmd"],
                                                       model["host"],
                                                       model["port"]))
       
            for key2,optim in config['optimizers'].items():

                print("launching optim server %s at %s:%d"%(optim["cmd"],
                                                           optim["host"],
                                                           optim["port"]+model['index']))
                
                cmd = """source venv/bin/activate;
                PYTHONPATH+=. python src/optim/%s --interface %s:%d --modelinterface %s:%d"""%(optim["cmd"],
                                                                                               optim["host"],
                                                                                               optim["port"]+model['index'],
                                                                                               model["host"],
                                                                                               model["port"])
                #print(cmd) 
                processes.append(subprocess.Popen(cmd, shell=True, executable="/bin/bash"))
                



        else:
            print(key, ": found a model with same port (",model['port'],":",ports[model['port']],"), assuming they use the same server and not launching anything")
                
                
    while True:
        inp = input()
        if inp=="exit" or inp =="kill" or inp=="quit":
            for p in processes:
                p.kill()
            exit()
        if inp=="show" or inp=="pid":
            for p in processes:
                print(p.pid)
