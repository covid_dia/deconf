# DECOV TGCC #

## Connect to the TGCC frontend ##


```bash
      _
      (_)_______  ____  ___
     / / ___/ _ \/ __ \/ _ \
    / / /  /  __/ / / /  __/
   /_/_/   \___/_/ /_/\___/
       	       	       	 PPI
```

Load remote file systems:

```bash
$ module switch dfldatadir/tgccaid
```

Check home directories are loaded:

```bash
$ ccc_home -a
```

General information are given using command `machine.info`.


## Install Decov ##



Change PWD to working directory and create a working place:

```bash
$ cd $CCCWORKDIR
$ mkdir dev
$ cd dev
```

Copy Decov archive from project (or from you own source)

```bash
$ cp ../../tgccaid/dev/deconf.tar.gz .
$ tar xvzf deconf.tar.gz
```

Compile the `C++` version of Decov (micromodel)

```bash
cd deconf
mkdir build
cd src/model/decov
module load c++/gnu/9.3.0
make
cp build/RELEASE/decov ../../../build/
cd ../../../../
```

Create virtualenv with Python 3.7.5. In this example, `venv` is
created outside of the `deconf` folder to make future updates of
deconf easier.

```bash
$ module load python3/3.7.5
$ virtualenv venv_deconf
```

Install dependencies. For now the SALib dependency is not supported,
use the provided requirements file instead.

```bash
$ source venv_deconf/bin/activate
$ pip install --no-index --find-links ../../tgccaid/dev/decov_wheels/ -r ../../tgccaid/dev/decov_requirements_without_SALib.txt
```

At this point you can check that the environnment is properly
working. Do not execute CPU-intensive jobs as you are logged into the
master/head node.

```bash
$ cd deconf
$ PYTHONPATH+=. python3 test/test_economie.py
$ deactivate
```

## Run a first job on the TGCC ##

The purpose of this run is to deploy the Decov MPI overlay and run
some MacroModel evaluations on the remote nodes. This also shows how
to submit a Python application to the platform.

Copy the MPI job description file from the project directory.

```bash
$ cd $CCCWORKDIR/dev
$ cp ../../tgccaid/dev/decov_mpi_jobs/job_list .
```

[optional] Open and edit this `job_list` file if you want to modify
the number of MacroModel evaluations and the JSON requests. Note that
paths to the JSON requests are relative to the $CCCWORKDIR/dev
directory as we will use this path as working directory for job
submission.

```bash
emacs -nw job_list
vi job_list
```

The TGCC scheduler is invoked using the `ccc_msub` command. Parameters
to describe the submitted job are given either by command line or by
providing a shell script.


Prepare a script to submit a job to the TGCC: copy the template from
project folder.

```bash
$ cp ../../tgccaid/dev/decov_mpi_jobs/ccc_decov_mpi.sh .
$ cat ccc_decov_mpi.sh 
#!/bin/bash

#MSUB -q rome
#MSUB -A tgccaid
#MSUB -m work

#MSUB -r decov_mpi
#MSUB -n 7
#MSUB -T 20

#MSUB -o decov_mpi.%I.output
#MSUB -e decov_mpi.%I.error

set -x
module load python3/3.7.5
cd ${BRIDGE_MSUB_PWD}
source ${BRIDGE_MSUB_PWD}/venv_deconf/bin/activate
export PYTHONPATH=$PYTHONPATH:${BRIDGE_MSUB_PWD}/deconf/
ccc_mprun python3 -m mpi4py -rc thread_level=funneled ${BRIDGE_MSUB_PWD}/deconf/src/mpi/decov_mpi.py --jobfile ${BRIDGE_MSUB_PWD}/job_list
deactivate
```

This is a regular shell script that is executed on each node. `#MSUB`
directives are given to the TGCC scheduler. More information are
available with the `machine.info` command.

### General information about the run within the script ###

[mandatory] Option `#MSUB -q rome` specifies which partition is used to allocate
nodes. `rome` is the Irene Rome AMD cluster while `v100` is the hybrid
CPU+GPU partition.

[mandatory] Option `#MSUB -A tgccaid` is the project name.

[mandatory] Option `#MSUB -m work` is the file system used during the
run.

[optional] Option `#MSUB -@ your@address.email` is used to notify that
the job has ended.

### Information about the application within the script ###

Option `#MSUB -r decov_mpi` indicates the name of the application

Option `#MSUB -n 7` the number of cores to allocate (equivalent to the
`-np` parameter for MPI)

[mandatory] Option `#MSUB -T 20` is the timeout before killing jobs if
the application does not terminate properly (in seconds).

[optional] `#MSUB -o decov_mpi.%I.output` and `#MSUB -e
decov_mpi.%I.error` are used to redirect I/O. Note that the scheduler
is verbose and that these files contain more information than the
application I/O.


`set -x` to dump commands in the error log.

`${BRIDGE_MSUB_PWD}` is the environnment variable that redirects to
the working directory that issued the submission `ccc_msub` command.

`source ${BRIDGE_MSUB_PWD}/venv_deconf/bin/activate` load the
virtualenv on each node.

`export PYTHONPATH=$PYTHONPATH:${BRIDGE_MSUB_PWD}/deconf/` add the
application root folder to the Python path. It is not possible to use
the Unix-like syntax `PYTHONPATH+=deconf/ python3 foo.py`.

### Command line to run the application within the script ###

`ccc_mprun` is used to run parallel tasks. In the following example
the parallel tasks are running `python3`.

```bash
ccc_mprun python3 -m mpi4py -rc thread_level=funneled ${BRIDGE_MSUB_PWD}/deconf/src/mpi/decov_mpi.py --jobfile ${BRIDGE_MSUB_PWD}/job_list
```

`-m mpi4py` indicates that the MPI module is used. `-rc
thread_level=funneled` is needed because the mpi4py module does not
support the full thread parallelism as specified in the TGCC MPI backend.


### Submit the job and retrieve results ###

Submit the job with the following command:

```bash
$ ccc_msub ccc_decov_mpi.sh
```

Check the job status:

```bash
$ ccc_mpp | grep $USER
```

Once terminated, results for the JSON requests are stored in the
`result_<name_of_MPI_job>` files.


Check the project CPU time consumption (updated every day). Deactivate
virtualenv before running this command (`deactivate`).

```bash
$ ccc_myproject
```

