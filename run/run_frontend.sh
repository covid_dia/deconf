#!/bin/sh
# get the script directory - https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
RUNDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOTDIR="$(dirname "$RUNDIR")"
cd $ROOTDIR

if [ -e venv/bin/activate ]
then
	source venv/bin/activate
else
	echo "venv/bin/activate not found"
	echo "the script will continue without"
fi
pip -q install -r requirements.txt

#a bunch of data copies to serve files
cp data/departements.geojson  src/frontend/static
cp data/data_emploi_departement.json  src/frontend/static
cp data/data_age_departement.json  src/frontend/static
cp data/data_risques_departement.json src/frontend/static
cp data/data_age_region.json  src/frontend/static
cp data/data_lits_hopitaux_departements.json src/frontend/static
cp data/data_risques_departement.json src/frontend/static
cp data/pcr_departements.json src/frontend/static
cp doc/seair_model_param_explain.json src/frontend/static
cp doc/policy_level_explain.json src/frontend/static
cp doc/optimizer_explain.json src/frontend/static
cp doc/objectives_explain.json src/frontend/static
cp data/eye-icon.png src/frontend/static
cp data/eye-closed-icon.png src/frontend/static
cp data/icon-x-active.png src/frontend/static
cp data/icon-x-inactive.png src/frontend/static
cp data/icon-y-active.png src/frontend/static
cp data/icon-y-inactive.png src/frontend/static
cp data/icon-info.png src/frontend/static

cp doc/france_contact_matrix.csv src/frontend/static




cp data/initial_conditions_20200601.json src/frontend/static/

cp doc/optimize_req.json src/frontend/static

cp data/default_layout.json src/frontend/static
cp data/default_layout_optim.json src/frontend/static

cd src/frontend

python make_blank_eval_policy_req.py

#initialize the res data (and tests the model server)
python -c "import json, subprocess;config=json.load(open('../../"$1"'));subprocess.call('curl -s -o static/eval_policy_res_blank.json -X POST -d @static/eval_policy_req_blank.json '+config['models']['macro_Alizon20']['host']+':'+str(config['models']['macro_Alizon20']['port'])+'/eval_policy -H \"Content-Type: application/json\"', shell=True)"

#curl -s -o static/eval_policy_res_blank.json -X POST -d @static/eval_policy_req_blank.json ${model_host}:${optim_port}/eval_policy -H "Content-Type: application/json"
#echo

export FLASK_ENV=testing
export FLASK_DEBUG=1
export DECOV_CONFIG=$1


python -c "import json, subprocess;config=json.load(open('../../"$1"'));subprocess.call('export DECOV_CONFIG=../../"$1" export FLASK_APP='+config['frontend']['cmd']+'; flask run --port='+str(config['frontend']['port'])+' --host='+config['frontend']['host'],shell=True)"


