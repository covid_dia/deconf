#!/bin/sh

if [ -e venv/bin/activate ]
then
	source venv/bin/activate
else
	echo "venv/bin/activate not found"
	echo "the script will continue without"
fi

pip -q install -r requirements.txt


#Parse config file
{ IFS="," read front_dir front_cmd front_host front_port;
  IFS="," read model_dir model_cmd model_host model_port;
  IFS="," read optim_dir optim_cmd optim_host optim_port;
}  < $1

PYTHONPATH+=. python src/model/$model_cmd --interface $model_host:$model_port
