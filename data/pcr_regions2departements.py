import json

from pandas import read_csv

from data.data_lits_hopitaux_regions2departements import compute_departements2region_regions_pop
from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req

if __name__ == '__main__':
    infile = decov_path + '/data/pcr_regions.csv'
    outfile = decov_path + '/data/pcr_departements.json'

    req = default_eval_policy_req()
    departements2regions, regions_pop = compute_departements2region_regions_pop()
    pcr_regions = {row.ARS: row.capacite_1105 for _, row in read_csv(infile, skiprows=range(0, 1)).iterrows()}

    pcr_dpts = {}
    correction_factor = 1 - 415E-7  # apply correction factor to find the correct sum (145593)
    for dpt_idx, dpt in enumerate(req['zones']):
        region = departements2regions[dpt]
        pcr_dpts[dpt] = round(
            correction_factor * pcr_regions[region] * req['population_size'][dpt_idx] / regions_pop[region])
    print('total_beds', sum(pcr_dpts.values()), 'should be', sum(pcr_regions.values()))
    pcr_dpts['_comment'] = 'generated from {}. Used in eval_policy_req.'.format(infile.replace(decov_path, ''))

    with open(outfile, 'w') as f:
        json.dump(pcr_dpts, f, indent=2)
    print('Generated file', outfile, 'of size', len(pcr_dpts.values()) - 1, ', content:', pcr_dpts)
