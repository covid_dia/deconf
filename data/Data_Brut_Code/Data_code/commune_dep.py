# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

import pandas as pd

# os.chdir("C://Users/Fnac/Documents/Covid/DATA_DEP")
from src.model.policy_utils import decov_path

commune = pd.read_csv(decov_path + "/data/Data_DEP/commune_densite_dep.csv", encoding="ISO-8859-1")

commune.index = commune["dep"]

dfi = commune.set_index(["Code"])

data = {}
for i in commune['dep']:
    data[i] = {}
    table = commune[commune['dep'] == i]
    for j in table['Code']:
        data[i][j] = {}
        data[i][j] = dfi.loc[j]

df = pd.DataFrame(data)
df.head()

df.to_json(r'commune_dep.json')
