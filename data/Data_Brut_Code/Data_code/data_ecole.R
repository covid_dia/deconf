###############################"
# MATERNELLE PRIMAIRE
setwd("C:/Users/Fnac/Downloads")

data=read.csv("fr-en-effectifs-premier-degre.csv",sep=';',h=T)

data=subset(data,data$annee=="2019-2020")

###############################################################
                    #PAR REGION
###############################################################

regions=c("AUVERGNE-RHONE-ALPES","BOURGOGNE-FRANCHE-COMTE","BRETAGNE","CENTRE-VAL DE LOIRE","CORSE","GRAND EST","HAUTS-DE-FRANCE","ILE-DE-FRANCE","NORMANDIE","NOUVELLE-AQUITAINE","OCCITANIE","PAYS DE LA LOIRE","PROVENCE-ALPES-COTE D'AZUR")
list_reg=list()
for ( i in 1:length(regions)){
list_reg[[i]]=subset(data,data$region==regions[i])

}

somme=c()
for (i in 1:13){
  somme[i]=sum(list_reg[[i]]$Nombre_d_eleves)
}
somme


#************************************************







##########################################################################
                  #PAr DEPARTEMENT
#########################################################################""

depa=levels(data$dep)
list_dep=list()
for ( i in 1:length(depa)){
  list_dep[[i]]=subset(data,data$dep==depa[i])
  
}


somme_dep=c()
for (i in 1:102){
  somme_dep[i]=sum(list_dep[[i]]$Nombre_d_eleves)
}
somme_dep




###############################"
# COLLEGE LYCEE


data=read.csv("fr-en-effectifs-second-degre.csv",sep=';',h=T)

data=subset(data,data$annee=="2019-2020")

###############################################################
#PAR REGION
###############################################################

regions=levels(data$region)
list_reg=list()
for ( i in 1:length(regions)){
  list_reg[[i]]=subset(data,data$region==regions[i])
  
}

somme=c()
for (i in 1:19){
  somme[i]=sum(list_reg[[i]]$Nombre_d_eleves)
}
somme


#************************************************







##########################################################################
#PAr DEPARTEMENT
#########################################################################""

depa=levels(data$dep)
list_dep=list()
for ( i in 1:length(depa)){
  list_dep[[i]]=subset(data,data$dep==depa[i])
  
}


somme_dep=c()
for (i in 1:102){
  somme_dep[i]=sum(list_dep[[i]]$Nombre_d_eleves)
}
somme_dep



###############################"
# ETUDE SUPERIEUR


data=read.csv("fr-esr-atlas_regional-effectifs-d-etudiants-inscrits (1).csv",sep=';',h=T)
data=subset(data,data$rentree==2017)
data=data[,-c(2)]
data=subset(data,data$niveau_geographique=="Région")
###############################################################
#PAR REGION
###############################################################

regions=levels(data$reg_id)
list_reg=list()
for ( i in 1:length(regions)){
  list_reg[[i]]=subset(data,data$reg_id==regions[i])
  
}

somme=c()
for (i in 1:21){
  somme[i]=sum(list_reg[[i]]$effectif)
}
somme


#************************************************







##########################################################################
#PAr DEPARTEMENT
#########################################################################""

depa=levels(data$dep)
list_dep=list()
for ( i in 1:length(depa)){
  list_dep[[i]]=subset(data,data$dep==depa[i])
  
}


somme_dep=c()
for (i in 1:102){
  somme_dep[i]=sum(list_dep[[i]]$Nombre_d_eleves)
}
somme_dep
