import json
from copy import deepcopy

from pandas import read_csv

from src.model.policy_utils import decov_path

infile = decov_path + '/src/model/decov/data.json'
outfile = decov_path + '/src/model/decov/data_departement.json'

csv = read_csv(decov_path + '/data/departements2regions.csv')
departements2regions = {row.departement: row.region for _, row in csv.iterrows()}
with open(infile, 'r') as f:
    in_dict = json.load(f)

# create all departements in 'Regions'
out_dict = deepcopy(in_dict)
out_dict['Regions'] = []
for dpt in departements2regions.keys():
    region = departements2regions[dpt]
    # find the data of the corresponding region
    region_data = next(x for x in in_dict['Regions'] if x['Name'] == region)
    print('dpt', dpt, '-> region', region)
    dpt_data = deepcopy(region_data)
    dpt_data['Name'] = dpt
    dpt_data['Communes'] = []
    out_dict['Regions'].append(dpt_data)

# we have to iterate on all regions because some it seems some communes are not in the good region
# (we got 40025 communes instead of 40449)
for region_data in in_dict['Regions']:
    for commune in region_data['Communes']:
        dpt = commune['CodePostal'][0:2]
        if dpt == '20':
            dpt = '2A'
        dpt_data = next(x for x in out_dict['Regions'] if x['Name'] == dpt)
        dpt_data['Communes'].append(commune)

with open(outfile, 'w') as f:
    json.dump(out_dict, f, indent=2)
print('Generated file', outfile, ', content:', out_dict)
