import json

from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req

outfile = decov_path + '/data/data_emploi_departement.json'

out = {}
eval_policy_req = default_eval_policy_req()
for zone in eval_policy_req['zones']:
    out[zone] = {
        'agriculture': 100,
        'industrie': 200,
        'construction': 300,
        'tertiaire marchand': 400,
        'autre tertiaire': 500
    }
with open(outfile, 'w') as f:
    json.dump(out, f)
print('Generated file', outfile, ', content:', out)
