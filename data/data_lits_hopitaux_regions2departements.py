import json

from pandas import read_csv

from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req


def compute_departements2region_regions_pop():
    # compute region population
    csv = read_csv(decov_path + '/data/departements2regions.csv')
    departements2regions = {row.departement: row.region for _, row in csv.iterrows()}
    regions_pop = {row.region: 0 for _, row in csv.iterrows()}
    req = default_eval_policy_req()
    for dpt_idx, dpt in enumerate(req['zones']):
        regions_pop[departements2regions[dpt]] += req['population_size'][dpt_idx]
    print('regions_pop', regions_pop, ', total pop', sum(regions_pop.values()))
    return departements2regions, regions_pop


if __name__ == '__main__':
    infile = decov_path + '/data/data_lits_hopitaux_regions.json'
    outfile = decov_path + '/data/data_lits_hopitaux_departements.json'
    req = default_eval_policy_req()
    departements2regions, regions_pop = compute_departements2region_regions_pop()
    with open(infile, 'r') as f:
        lits_regions = json.load(f)
        del lits_regions['_comment']
    lits_dpts = {}
    correction_factor = 1 + 3E-4  # apply correction factor to find the correct sum (1071)
    for dpt_idx, dpt in enumerate(req['zones']):
        region = departements2regions[dpt]
        lits_dpts[dpt] = round(
            correction_factor * lits_regions[region] * req['population_size'][dpt_idx] / regions_pop[region])
    print('total_beds', sum(lits_dpts.values()), 'should be', sum(lits_regions.values()))
    lits_dpts['_comment'] = 'generated from {}. Used in eval_policy_req.'.format(infile.replace(decov_path, ''))

    with open(outfile, 'w') as f:
        json.dump(lits_dpts, f, indent=2)
    print('Generated file', outfile, 'of size', len(lits_dpts.values()) - 1, ', content:', lits_dpts)
