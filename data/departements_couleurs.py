import json

from pandas import read_csv

from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req

# https://www.gouvernement.fr/info-coronavirus/carte-et-donnees
# https://www.data.gouv.fr/fr/datasets/indicateurs-dactivite-epidemique-covid-19-par-departement/
url = 'https://www.data.gouv.fr/fr/datasets/r/f2d0f955-f9c4-43a8-b588-a03733a38921'
date = '2020-05-07'
outfile = decov_path + '/data/departements_couleurs_' + date + '.json'

data = read_csv(url, sep=";|,", engine="python")  # use , or ; as separator
req = default_eval_policy_req()
zones, labels = [], []
for zone in req['zones']:
    row_idx = (data['extract_date'] == date) & (data['departement'] == zone)
    nrows = len(data[row_idx].index)
    assert nrows == 1
    row = data[row_idx].iloc[0]
    zones.append(row.departement)
    labels.append(1 if row.indic_synthese == 'vert' else 0)
out = {'zones': zones,
       'zone_label_names': ['rouge', 'vert'],
       'zone_labels': labels}
with open(outfile, 'w') as f:
    json.dump(out, f)
print(len(labels), 'dpts,', labels.count(0), 'rouges', labels.count(1), 'verts')
print('Generated file', outfile, ', content:', out)
