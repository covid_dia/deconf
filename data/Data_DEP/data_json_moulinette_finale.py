# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

import json
import os
import pandas as pd
from glob import glob
from src.model.policy_utils import decov_path
from src.model.policy_utils import pretty_write

file_list = glob(decov_path + "/data/Data_DEP/*dep.json")
allFilesDict = {v:k for v, k in enumerate(file_list, 1)}


data = []

for k,v in allFilesDict.items():
    if 1 <= k <= 103:
        with open(v, 'r') as d:
            jdata = json.load(d)
            if jdata:
                data.append(jdata)

df = pd.DataFrame(data)
   
nom_colonne={}          
for k,v in allFilesDict.items():
    nom_colonne[k]=v.partition("_dep.")[0]
df.index=nom_colonne.values()


pretty_write(df, 'data.json')


