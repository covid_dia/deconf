import json

from pandas import read_csv

from src.model.policy_utils import decov_path

# https://www.insee.fr/fr/statistiques/1893198
infile = decov_path + '/data/departements_population_size.csv'
outfile = decov_path + '/data/departements_population_size.json'

data = read_csv(infile, sep=',')
print(data)
zones, pops = [], []
for idx, row in data.iterrows():
    if row.departement.startswith('97'):  # DOM
        continue
    zones.append(row.departement)
    pops.append(int(row.population_size))
out = {'zones': zones,
       'population_size': pops}
with open(outfile, 'w') as f:
    json.dump(out, f)
print('Generated file', outfile, ', content:', out)
