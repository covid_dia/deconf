import json

from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req

outfile = decov_path + '/data/initial_conditions.json'

initial_ratios = {}
eval_policy_req = default_eval_policy_req()
for zone in eval_policy_req['zones']:
    initial_ratios[zone] = [0.991, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3]
out = {'initial_ratios': initial_ratios}
with open(outfile, 'w') as f:
    json.dump(out, f)
print('Generated file', outfile, ', content:', out)
