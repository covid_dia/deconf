import json

from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req

outfile = decov_path + '/data/population_active_a_risque_departement.json'

eval_policy_req = default_eval_policy_req()
out = {zone: {'active': 18.98, 'all': 39.69} for zone in eval_policy_req['zones']}
with open(outfile, 'w') as f:
    json.dump(out, f)
print('Generated file', outfile, ', content:', out)
