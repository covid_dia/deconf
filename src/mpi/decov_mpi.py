#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# MPI
from mpi4py import MPI

# ----------------------------------------------------------------------------------

from src.mpi.mpi_base import *
from src.mpi.mpi_scheduler import *
from src.mpi.mpi_http import *
from src.mpi.mpi_file import *
from src.mpi.mpi_worker import *

# ----------------------------------------------------------------------------------

                
# ----------------------------------------------------------------------------------

def main (argv):

    args = argumentParse()

    if MPI.COMM_WORLD.Get_rank() < args.nbschedulers:
        mpi_decov = DecovMPIScheduler(args=args)
    elif MPI.COMM_WORLD.Get_rank() == args.nbschedulers - 1 + DECOV_MPI_RANK_HTTP:
        mpi_decov = DecovMPIHttp(args=args)
    elif MPI.COMM_WORLD.Get_rank() == args.nbschedulers - 1 + DECOV_MPI_RANK_FILE:
        mpi_decov = DecovMPIFile(args=args)
    else:
        mpi_decov = DecovMPIWorker(args=args)

    mpi_decov.serverLoop()
    
        

# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)


# ----------------------------------------------------------------------------------

