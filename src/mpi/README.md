
The following command will launch an MPI overlay of 7 processes
composed by one scheduler, one http proxy, one proxy reading from file
and 4 workers.

Non-interactive mode, using a job description file::

```bash
T1$ PYTHONPATH+=../../ mpirun --oversubscribe -np 7 python3 ./decov_mpi.py --jobfile job_list --verbose
```

Interactive mode, using the http interface:

```bash
T1$ PYTHONPATH+=../../ mpirun --oversubscribe -np 7 python3 ./decov_mpi.py --configfile config.json --verbose
T2$ curl --data @../../doc/eval_policy_req.json http://localhost:8101/
T2$ curl --data @../../doc/eval_policy_req.json http://localhost:8100/
T2$ curl --data @../../doc/optimize_req.json http://localhost:6000/
T2$ curl --data @../../doc/optimize_req.json http://localhost:6001/
```

Use the `--help` option to display the full help.
