#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# MPI
from mpi4py import MPI

# argv
import sys

# queue
import queue

# ----------------------------------------------------------------------------------

from src.mpi.eeprobe import *

# ----------------------------------------------------------------------------------

from src.mpi.mpi_base import *

# ----------------------------------------------------------------------------------


class DecovMPIScheduler(DecovMPIBase):

    def __init__(self, args=None):
        DecovMPIBase.__init__(self, args)
        self.log("start scheduler role")
        self.idle_resources = set()
        self.pending_jobs = queue.Queue()
        self.pending_help = queue.Queue()
        self.terminate = False

    def serverLoop(self):
        status = MPI.Status()
        shutdown = False
        while not shutdown:
            self.eep.probe(self.comm, source = MPI.ANY_SOURCE,
                           tag = MPI.ANY_TAG, enable = self.eep_enable)
            mpbuffer = self.comm.recv(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG,
                                      status = status)
            mptype, origin, transaction_id, mpdata = mpbuffer
            source = status.Get_source()

            if mptype == "SHUTDOWN":
                self.log("shutdown")
                shutdown = True

            elif mptype == "TERMINATE":
                self.log("terminate signal from " + str(source))
                self.terminate = True
                if self.pending_jobs.empty():
                    uptime = self.getTime() - self.start_time
                    self.log("uptime is " + str(uptime) + " (" + str(int(round(uptime / 1e6))) + " seconds)")
                    self.shutdown()
                
            elif mptype == "IDLE":
                if not self.pending_jobs.empty():
                    resource = origin
                    job = self.pending_jobs.get()
                    job_type, _, job_id, _ = job
                    self.log("assign job " + job_id + " (" + job_type + ") to worker " + str(resource))
                    self.comm.send(job, dest = resource, tag = DECOV_MPI_TAG)
                elif not self.pending_help.empty():
                    resource = origin
                    beneficiary = self.pending_help.get()
                    self.log("send idle worker " + str(resource) + " to " + str(beneficiary))
                    self.comm.send(("IDLE", resource, str(uuid.uuid4()), ""),
                                   dest = beneficiary, tag = DECOV_MPI_TAG)
                else:
                    self.log("worker " + str(origin) + " in idle resources list")
                    self.idle_resources.add(origin)



            elif mptype == "MODEL_MACRO" or mptype == "MODEL_MICRO" or mptype == "OPTIM_RANDOM_MACRO" or mptype == "OPTIM_RANDOM_MICRO":
                resource = None
                if len(self.idle_resources) > 0:
                    resource = self.idle_resources.pop()
                    self.log("assign job " + transaction_id + " (" + mptype + ") to worker " + str(resource))
                    self.comm.send(mpbuffer, dest = resource, tag = DECOV_MPI_TAG)
                else:
                    self.log("postpone job " + transaction_id + " (" + mptype + ") to pending list")
                    self.pending_jobs.put(mpbuffer)
                    for i in range(0, self.args.nbschedulers):
                        if i != self.rank:
                            self.log("send resource request to " + str(i))
                            self.comm.send(("REQUEST_RESOURCES", self.rank, str(uuid.uuid4()), ""),
                                           dest = i, tag = DECOV_MPI_TAG)

            elif mptype == "REQUEST_RESOURCES":
                if len(self.idle_resources) > 0:
                    resource = self.idle_resources.pop()
                    self.log("send idle resource " + str(resource) + " to " + str(origin))
                    self.comm.send(("IDLE", resource, str(uuid.uuid4()), ""),
                                   dest = origin, tag = DECOV_MPI_TAG)
                else:
                    self.log("postpone resource request from " + str(origin))
                    self.pending_help.put(origin)


            else:
                self.log("unknown message type in server loop " + str(mptype))

            
# ----------------------------------------------------------------------------------

def main (argv):

    mpi_scheduler = DecovMPIScheduler()
    mpi_scheduler.serverLoop()

    
# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)


# ----------------------------------------------------------------------------------

        
