#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# MPI
from mpi4py import MPI

# gethostname
import socket

# argv
import sys

# exists
from os import path

# threading
import threading

# lock
from threading import Lock

# JSON
import json

# UUID
import uuid

# ----------------------------------------------------------------------------------

from src.mpi.eeprobe import *

# ----------------------------------------------------------------------------------

from src.mpi.mpi_base import *

# ----------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------

class DecovMPIFile(DecovMPIBase):

    def __init__(self, args=None):
        DecovMPIBase.__init__(self, args)
        self.log("start file role")

        self.terminate = False

        self.job_list_lock = Lock()
        self.job_list = set()

        if self.args.jobfile is not None:
            tid = threading.Thread(target=self.threadFileServer)
            tid.daemon = False
            tid.start()


    def threadFileServer(self):
        with open(self.args.jobfile, 'r') as fjl:
            for line in fjl:
                job_id = None
                file_path = None
                linetuple = line.rstrip('\n').split(' ')

                if len(linetuple) == 3:
                    mpi_tag, job_id, file_path = linetuple
                elif len(linetuple) == 1:
                    mpi_tag = linetuple[0]
                else:
                    self.log("unknown job description " + str(linetuple))

                if mpi_tag == "TERMINATE":
                    self.comm.send((mpi_tag, self.rank,
                                    str(uuid.uuid4()), ""),
                                   dest = self.rank, tag = DECOV_MPI_TAG)
                else:
                    self.job_list_lock.acquire()
                    duplicate = (job_id in self.job_list)
                    self.job_list_lock.release()
                    if duplicate:
                        self.log("job " + job_id + " already in job list")
                    else:
                        if path.exists(file_path):
                            with open(file_path, 'r') as fj:
                                request_json = json.loads(fj.read().replace('\n', ''))
                                self.log("create job " + job_id + " mpi tag " + mpi_tag)
                                self.job_list_lock.acquire()
                                self.job_list.add(job_id)
                                self.job_list_lock.release()
                                self.comm.send((mpi_tag, self.rank,
                                                job_id, request_json),
                                               dest = self.pickScheduler(), tag = DECOV_MPI_TAG)

        
    def serverLoop(self):
        status = MPI.Status()

        shutdown = False
        while not shutdown:

            self.eep.probe(self.comm, source = MPI.ANY_SOURCE,
                           tag = MPI.ANY_TAG, enable = self.eep_enable)
            mpbuffer = self.comm.recv(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG,
                                      status = status)
            mptype, origin, transaction_id, mpdata = mpbuffer
            source = status.Get_source()

            if mptype == "SHUTDOWN":
                self.log("shutdown")
                shutdown = True

            elif mptype == "TERMINATE":
                self.log("terminate")
                self.terminate = True
                self.job_list_lock.acquire()
                if len(self.job_list) == 0:
                    self.comm.send(("TERMINATE", self.rank,
                                    str(uuid.uuid4()), ""),
                                   dest = self.pickScheduler(), tag = DECOV_MPI_TAG)
                self.job_list_lock.release()

            elif mptype == "MODEL_MACRO_RES" or mptype == "MODEL_MICRO_RES" or mptype == "OPTIM_RANDOM_MACRO_RES" or mptype == "OPTIM_RANDOM_MICRO_RES":
                self.log("result for job " + transaction_id + " (" + mptype + ") from worker " + str(source))
                self.job_list_lock.acquire()
                self.job_list.remove(transaction_id)
                if len(self.job_list) == 0:
                    self.comm.send(("TERMINATE", self.rank,
                                    str(uuid.uuid4()), ""),
                                   dest = self.pickScheduler(), tag = DECOV_MPI_TAG)
                self.job_list_lock.release()
                with open("result_" + transaction_id, 'w') as fp:
                    json.dump(mpdata, fp)
                    fp.flush()

            else:
                self.log("unknown message type in server loop " + str(mptype))


# ----------------------------------------------------------------------------------

def main (argv):

    mpi_file = DecovMPIFile()
    mpi_file.serverLoop()

    
# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)


# ----------------------------------------------------------------------------------

