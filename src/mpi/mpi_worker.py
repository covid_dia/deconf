#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# MPI
from mpi4py import MPI

# argv
import sys

# UUID
import uuid

# thread
import threading

# Lock
from threading import Lock

# JSON
import json

# ----------------------------------------------------------------------------------

from src.mpi.eeprobe import *

# ----------------------------------------------------------------------------------

from src.mpi.mpi_base import *

from src.model.economie import Economie

from src.model.macromodel import MacroModel
from src.model.micromodel import MicroModelRequestHandler

from src.optim.pareto import ParetoFront
from src.optim.random_optim import RandomOptimRequestHandler

# ----------------------------------------------------------------------------------


class DecovMPIWorker(DecovMPIBase):

    def __init__(self, args=None):
        DecovMPIBase.__init__(self, args)
        self.log("start worker role")

        self.economy = None

        self.macromodel = None
        self.micromodel = None

        self.current_model = "MODEL_MACRO"

        self.optimrandom = None

        self.model_results_lock = Lock()
        self.model_results = dict()

    def call_eval_policy_async(self, eval_policy_req: dict, activable_measures_dict: dict,
                               job_uid=None):
        if job_uid is None:
            job_uid = str(uuid.uuid4())
        self.log("create job " + job_uid + " mpi tag " + self.current_model)
        self.comm.send((self.current_model, self.rank,
                        job_uid, eval_policy_req),
                       dest = self.pickScheduler(), tag = DECOV_MPI_TAG)
        return job_uid

    def call_eval_policy_poll_job(self, eval_policy_req: dict, activable_measures_dict: dict,
                                  job_uid):
        result = None
        ok = False
        self.model_results_lock.acquire()
        if job_uid in self.model_results:
            result = self.model_results[job_uid]
            ok = True
        self.model_results_lock.release()
        return ok, result, ok


    def threadOptim(self, mpi_tag, optim_handler, optimize_req, origin, transaction_id):
        policy_common_values, activable_measures_dict = optim_handler.make_policy_common_values(optimize_req)
        ok, pareto_front = optim_handler.optimize_async_para(optimize_req, policy_common_values,
                                                             activable_measures_dict,
                                                             self, self.args.optimdepth)

        # measure_weeks, forced_telecommuting, social_containment_all, school_closures, total_containment_high_risk
        dumps = {'success': True, 'zones': optimize_req['zones'],
                 'zone_label_names': optimize_req['zone_label_names'], 'policies': []}
        for preq, pres, objs in zip(pareto_front.eval_policy_reqs,
                                    pareto_front.eval_policy_ress,
                                    pareto_front.metrics):
            # forced_telecommuting, school_closures, social_containment_all, total_containment_high_risk
            pdump = {k: preq[k] for k in optimize_req['max_measure_values'].keys()}
            for k in ['measure_weeks']:
                pdump[k] = preq[k]
            for k in [
                    'activity',
                    'cases',
                    'deaths',
                    'gdp_accumulated_loss',
                    'hospital_patients',
                    'ICU_patients',
                    'tests'
            ]:
                pdump[k] = pres[k]
            pdump['objectives'] = objs
            dumps['policies'].append(pdump)

        result = dumps

        self.comm.send((mpi_tag + "_RES", self.rank, transaction_id, result),
                       dest = origin, tag = DECOV_MPI_TAG)
        self.log("done " + mpi_tag + " job " + transaction_id)
        self.comm.send(("IDLE", self.rank, str(uuid.uuid4()), ""),
                       dest = self.pickScheduler(), tag = DECOV_MPI_TAG)



    def serverLoop(self):
        status = MPI.Status()

        self.comm.send(("IDLE", self.rank, str(uuid.uuid4()), ""),
                       dest = self.pickScheduler(), tag = DECOV_MPI_TAG)

        shutdown = False
        while not shutdown:

            self.eep.probe(self.comm, source = MPI.ANY_SOURCE,
                           tag = MPI.ANY_TAG, enable = self.eep_enable)
            mpbuffer = self.comm.recv(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG,
                                      status = status)
            mptype, origin, transaction_id, mpdata = mpbuffer
            source = status.Get_source()

            if mptype == "SHUTDOWN":
                self.log("shutdown")
                shutdown = True

            elif mptype == "MODEL_MACRO":
                self.log("accept MODEL_MACRO job " + transaction_id)
                if self.economy is None:
                    self.economy = Economie()
                if self.macromodel is None:
                    self.macromodel = MacroModel(self.economy, self.args.verbose)
                result = self.macromodel.eval_policy(mpdata)
                self.comm.send(("MODEL_MACRO_RES", self.rank, transaction_id, result),
                               dest = origin, tag = DECOV_MPI_TAG)
                self.log("done MODEL_MACRO job " + transaction_id)
                self.comm.send(("IDLE", self.rank, str(uuid.uuid4()), ""),
                               dest = self.pickScheduler(), tag = DECOV_MPI_TAG)


            elif mptype == "MODEL_MICRO":
                self.log("accept MODEL_MICRO job " + transaction_id)
                if self.economy is None:
                    self.economy = Economie()
                if self.micromodel is None:
                    self.micromodel = MicroModelRequestHandler()
                eval_raw = json.dumps(mpdata).encode()
                result = self.micromodel.eval_policy(eval_raw, mpdata, economy=self.economy)
                self.comm.send(("MODEL_MICRO_RES", self.rank, transaction_id, result),
                               dest = origin, tag = DECOV_MPI_TAG)
                self.log("done MODEL_MICRO job " + transaction_id)
                self.comm.send(("IDLE", self.rank, str(uuid.uuid4()), ""),
                               dest = self.pickScheduler(), tag = DECOV_MPI_TAG)



            elif mptype == "OPTIM_RANDOM_MACRO" or mptype == "OPTIM_RANDOM_MICRO":
                self.log("accept " + mptype + " job " + transaction_id)
                self.current_model = "MODEL_MACRO"
                if mptype == "OPTIM_RANDOM_MICRO":
                    self.current_model = "MODEL_MICRO"
                self.model_results_lock.acquire()
                self.model_results.clear()
                self.model_results_lock.release()
                if self.optimrandom is None:
                    self.optimrandom = RandomOptimRequestHandler()
                    tid = threading.Thread(target=self.threadOptim,
                                           args=(mptype, self.optimrandom,
                                                 mpdata, origin, transaction_id))
                    tid.daemon = False
                    tid.start()


            elif mptype == "MODEL_MACRO_RES" or mptype == "MODEL_MICRO_RES":
                self.log("result for job " + transaction_id + " (" + mptype + ") from worker " + str(source))
                self.model_results_lock.acquire()
                self.model_results[transaction_id] = mpdata
                self.model_results_lock.release()


            else:
                self.log("unknown message type in server loop " + str(mptype))
                
# ----------------------------------------------------------------------------------

def main (argv):

    mpi_worker = DecovMPIWorker()
    mpi_worker.serverLoop()

    
# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)


# ----------------------------------------------------------------------------------

