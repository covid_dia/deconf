#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# MPI
from mpi4py import MPI

# argv
import sys

# ArgumentParser
import argparse

# gethostname
import socket

# monotonic
from time import monotonic

# UUID
import uuid

# rand
from random import randint

# ----------------------------------------------------------------------------------

DECOV_MPI_TAG = 0

DECOV_MPI_RANK_SCHEDULER = 0
DECOV_MPI_RANK_HTTP = 1
DECOV_MPI_RANK_FILE = 2

# ----------------------------------------------------------------------------------

from eeprobe import *

# ----------------------------------------------------------------------------------

def argumentParse():
    parser = argparse.ArgumentParser(description='Python Decov MPI. An Artificial Intelligence-based tool to search and optimize tradeoffs between sanitary and economy criteria of the COVID-19-lockdown measures liftings.', usage='PYTHONPATH+=. mpirun --oversubscribe -np 7 python3 ./decov_mpi.py')
    parser.add_argument('--configfile', type=str,
                        help='path to the JSON config file describing models and optimizers. An http server is instantiated for each entry in the config file. These http servers accept optim and model requests and are submitted to the MPI overlay. The results are given back using the regular http response. Note that requests to the http servers are sequentially processed on each http server. Open more http servers in the config file, intantiate more MPI http roles or fall back to the job file option if more parallelism is needed')
    parser.add_argument('--jobfile', type=str,
                        help='path to the file containing the list of jobs, one job per line. Format of a job is space-separated and contains the following information in this order: job_type job_id path_to_file_containing_the_json_request. Example of accepted job types includes MODEL_MACRO, MODEL_MICRO, OPTIM_RANDOM_MACRO and OPTIM_RANDOM_MICRO. Results are written back into a file named result_<job_id>')
    parser.add_argument('--optimdepth', type=int, default=10,
                        help='number of solutions evaluated by the optimization algorithm. This has to be set in regards with the hardware capabilities and the time allocated for each optimization problem')
    parser.add_argument('--nbschedulers', type=int, default=1,
                        help='number of schedulers to deploy in the overlay. This allows to dispatch scheduling requests and resource management to avoid contention on a centralized scheduler')
    parser.add_argument('--disableeep', action='store_true',
                        help='disable EEProbe (Energy Efficient Probe). This might slightly improve performance if the MPI processes are not colocated onto the same processor. Do not disable if colocated as you will experience a performance drop. In all cases, using this option will increase power consumption. Visit https://github.com/lcudenne/eeprobe/ for more information')
    parser.add_argument('--verbose', action='store_true',
                        help='explain what is being done')

    args = parser.parse_args()
    if args.nbschedulers < 1:
        args.nbschedulers = 1

    return args



# ----------------------------------------------------------------------------------


class DecovMPIBase:

    def __init__(self, args=None):
        self.start_time = self.getTime()
        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.nr = self.comm.Get_size()
        self.hostname = socket.gethostname()
        self.eep = EEProbe()
        self.eep_enable = EEPROBE_Enable.EEPROBE_ENABLE

        self.args = args
        if args is None:
            self.args = argumentParse()

        if self.args.disableeep:
            self.eep_enable = EEPROBE_Enable.EEPROBE_DISABLE
            self.log("EEProbe disabled")

        
    def log(self, message):
        if self.args.verbose:
            mformat = str(self.getTime() - self.start_time) + " " \
                + self.hostname + " " + str(self.rank) + " " + message
            print(mformat)
        
    @staticmethod
    def getTime():
        return int(round(monotonic() * 1E6))

    def pickScheduler(self):
        return randint(0, self.args.nbschedulers - 1)

    def serverLoop(self):
        self.log("nothing to do")

    def shutdown(self):
        self.log("broadcast shutdown")
        for dest in range(0, self.nr):
            self.comm.send(("SHUTDOWN", self.rank, str(uuid.uuid4()), ""),
                           dest = dest, tag = DECOV_MPI_TAG)
        

# ----------------------------------------------------------------------------------

def main (argv):

    mpi_decov = DecovMPIBase()
    mpi_decov.serverLoop()
    mpi_decov.shutdown()

# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)


# ----------------------------------------------------------------------------------

        
