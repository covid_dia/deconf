#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# MPI
from mpi4py import MPI

# gethostname
import socket

# argv
import sys

# threading
import threading

# lock and event
from threading import Lock, Event

# handle http RPC
from http.server import BaseHTTPRequestHandler, HTTPServer

# JSON
import json

# UUID
import uuid


# ----------------------------------------------------------------------------------

from src.mpi.eeprobe import *

# ----------------------------------------------------------------------------------

from src.mpi.mpi_base import *

# ----------------------------------------------------------------------------------

class HttpRequestHandlerPlaceHolder(BaseHTTPRequestHandler):

    def do_POST(self):
        status = MPI.Status()
        
        decov_mpi = self.server.decov_mpi
        
        decov_mpi.log("inbound http request on proxy " + self.server.server_key + " at " + str(self.server.server_address))
        
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        request_json = json.loads(post_body)
        job_uuid = str(uuid.uuid4())
        
        self.send_response(200)
        self.end_headers()

        event = threading.Event()
        decov_mpi.requests_lock.acquire()
        decov_mpi.requests[job_uuid] = (event, None)
        decov_mpi.requests_lock.release()

        decov_mpi.log("create job " + job_uuid + " mpi tag " + self.server.mpi_tag)
        decov_mpi.comm.send((self.server.mpi_tag, decov_mpi.rank,
                             job_uuid, request_json),
                            dest = self.pickScheduler(), tag = DECOV_MPI_TAG)

        event.wait()

        decov_mpi.requests_lock.acquire()
        _, result = decov_mpi.requests[job_uuid]
        decov_mpi.requests_lock.release()

        self.wfile.write(json.dumps(result).encode())

        

        

class HttpServerPlaceHolder(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass,
                 server_key=None, decov_mpi=None, mpi_tag=None):
        self.server_address = server_address
        self.server_key = server_key
        self.decov_mpi = decov_mpi
        self.mpi_tag = mpi_tag
        HTTPServer.__init__(self, server_address, RequestHandlerClass)


# ----------------------------------------------------------------------------------

class DecovMPIHttp(DecovMPIBase):

    def __init__(self, args=None):
        DecovMPIBase.__init__(self, args)
        self.log("start http role")

        self.config = None

        self.model_servers = dict()
        self.optim_servers = dict()

        self.requests_lock = Lock()
        self.requests = dict()
        
        if self.args.configfile is not None:
            with open(self.args.configfile) as fp:
                self.config = json.load(fp)
        if self.config is not None:
            for key,model in self.config['models'].items():
                if 'mpitag' in model:
                    mpitag = model['mpitag']
                    model_address = (model['host'], model['port'])
                    self.log("http start proxy " + key + " at " + str(model_address) + " mpi tag " + mpitag)
                    server = HttpServerPlaceHolder(model_address,
                                                   HttpRequestHandlerPlaceHolder,
                                                   server_key=key,
                                                   decov_mpi=self,
                                                   mpi_tag=mpitag)
                    tid = threading.Thread(target=self.threadHttpServer,
                                           args=(server,))
                    self.model_servers[key] = (tid, server)
                    tid.daemon = True
                    tid.start()

            for key,optim in self.config['optimizers'].items():
                if 'mpitag' in optim:
                    mpitag = optim['mpitag']
                    optim_address = (optim['host'], optim['port'])
                    self.log("http start proxy " + key + " at " + str(optim_address) + " mpi tag " + mpitag)
                    server = HttpServerPlaceHolder(optim_address,
                                                   HttpRequestHandlerPlaceHolder,
                                                   server_key=key,
                                                   decov_mpi=self,
                                                   mpi_tag=mpitag)
                    tid = threading.Thread(target=self.threadHttpServer,
                                           args=(server,))
                    self.optim_servers[key] = (tid, server)
                    tid.daemon = True
                    tid.start()


    def threadHttpServer(self, server):
        while True:
            server.handle_request()
                
        
    def serverLoop(self):
        status = MPI.Status()

        shutdown = False
        while not shutdown:

            self.eep.probe(self.comm, source = MPI.ANY_SOURCE,
                           tag = MPI.ANY_TAG, enable = self.eep_enable)
            mpbuffer = self.comm.recv(source = MPI.ANY_SOURCE, tag = MPI.ANY_TAG,
                                      status = status)
            mptype, origin, transaction_id, mpdata = mpbuffer
            source = status.Get_source()

            if mptype == "SHUTDOWN":
                self.log("shutdown")
                shutdown = True

            elif mptype == "MODEL_MACRO_RES" or mptype == "MODEL_MICRO_RES" or mptype == "OPTIM_RANDOM_MACRO_RES" or mptype == "OPTIM_RANDOM_MICRO_RES":
                self.log("result for job " + transaction_id + " (" + mptype + ") from worker " + str(source))
                self.requests_lock.acquire()
                request = self.requests[transaction_id]
                event, _ = request
                self.requests[transaction_id] = (event, mpdata)
                self.requests_lock.release()

                event.set()


            else:
                self.log("unknown message type in server loop " + str(mptype))


# ----------------------------------------------------------------------------------

def main (argv):

    mpi_http = DecovMPIHttp()
    mpi_http.serverLoop()

    
# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)


# ----------------------------------------------------------------------------------

