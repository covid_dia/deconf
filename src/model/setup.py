#!/usr/bin/env python3
from distutils.core import setup, Extension


salje_ext = Extension('saljepp',
                    include_dirs = ['/usr/include', '/usr/include/python3.6'],
                    library_dirs = ['/usr/local/lib'],
                    sources = ['saljeppmodule.cpp'])



alizon_ext = Extension('alizonpp',
                    include_dirs = ['/usr/include', '/usr/include/python3.6'],
                    library_dirs = ['/usr/local/lib'],
                    sources = ['alizonppmodule.cpp'])

setup (name = 'modelpp',
       version = '1.0',
       description = 'Salje cpp code',
       author = 'Tristan Bitard-Feildel',
       author_email = '',
       url = '',
       long_description = '''

''',
       ext_modules = [salje_ext, alizon_ext])
