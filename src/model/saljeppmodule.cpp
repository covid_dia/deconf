#define PY_SSIZE_T_CLEAN 
#include <Python.h>
#include <iostream>
#include <vector>
#include <string>
#include <exception>
#include "saljeppmodule.h"

using namespace std;
        

static PyObject* 
GetList(std::vector<double> list)
{
    srand(time(NULL));
    int const N = list.size();
    PyObject* python_val = PyList_New(N);
    for (int i = 0; i < N; ++i)
    {
        PyObject* python_float = Py_BuildValue("f", list[i]);
        PyList_SetItem(python_val, i, python_float);
    }
    return python_val;
}

static PyObject* 
GetUnpackedList(std::vector<std::vector<double>> packedlist)
{
    srand(time(NULL));
    int const N = packedlist.size();
    int cnt = 0;
    for (int i = 0; i < N; ++i) {
        int const M = packedlist[i].size();
        for (int j = 0; j < M; ++j){
            cnt ++;
        }   
    }
    PyObject* python_val = PyList_New(cnt);
    cnt = 0;

    for (int i = 0; i < N; ++i) {
        int const M = packedlist[i].size();
        for (int j = 0; j < M; ++j){
            PyObject* python_float = Py_BuildValue("f", packedlist[i][j]);
            PyList_SetItem(python_val, cnt, python_float);
            cnt ++;
        }
    }
    return python_val;
}


static PyObject* 
integrate(PyObject *self, PyObject *args, PyObject *kwargs) {
    static char *kwlist[] = {"y0", "max_days", "params", NULL};
    // 1 -> y0
	// 2 -> time
	// 3 -> parameters
	int max_days;
    int numComp;
    double value;
	PyObject * listObj; /* the list of strings */
    PyObject * floatObj;
    std::vector<double> y0;

	PyObject * dictObj; /* the list of strings */
    PyObject *dkey, *dvalue, *strObj;
    Py_ssize_t pos = 0;
    std::string key;
    float R0 = -1;
    float beta = -1;
    float epsilon = -1; 
    float sigma = -1;
    float rho1 = -1;
    float rho2 = -1;
    float gamm1 = -1;
    float kappa = -1;
    float mort_sup = -1;
    double i_sat = -1;
    int size;
    int dt=1;

    std::vector<double> measures; // should be a flattened matrix
    std::vector<double> P_I1_I2; // should be a vector
    std::vector<double> P_I2_D;  // should be a vector

    /* the O! parses for a Python object (listObj) checked
       to be of type PyList_Type */
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!iO!", kwlist, 
                                                            &PyList_Type, &listObj, 
                                                            &max_days,
                                                            &PyDict_Type, &dictObj))
        return NULL;

    /* Parse y0 */

    /* get the number of compartiments */
    listObj = PySequence_Fast(listObj, "argument must be iterable");
    if(!listObj)
        return 0;

    /* prepare data as an array of doubles */
    numComp = PySequence_Fast_GET_SIZE(listObj);
    for(int i=0; i < numComp; i++) {
        PyObject *fitem;
        PyObject *item = PySequence_Fast_GET_ITEM(listObj, i);
        if(!item) {
            //Py_DECREF(listObj);
            return 0;
        }
        fitem = PyNumber_Float(item);
        if(!fitem) {
            //Py_DECREF(listObj);
            PyErr_SetString(PyExc_TypeError, "all items must be numbers");
            return 0;
        }
        value = PyFloat_AS_DOUBLE(fitem);
        y0.push_back(value);
        //std::cerr << value << std::endl;
        Py_DECREF(fitem);
    }    

    /* clean up, compute, and return result */
    //Py_DECREF(listObj);

    /* y0 done */

    /* parse parameters */
    while (PyDict_Next(dictObj, &pos, &dkey, &dvalue)) {
        /* get key */
        PyObject * temp_bytes = PyUnicode_AsEncodedString(dkey, "UTF-8", "strict"); // Owned reference
        if (temp_bytes != NULL) {
            /* make it a string */
            key = PyBytes_AS_STRING(temp_bytes); // Borrowed pointer
            Py_DECREF(temp_bytes);
            if (key == "beta"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                beta = value;
            }
            else if (key == "measures"){ // is a flatten matrix to list
                dvalue = PySequence_Fast(dvalue, "argument must be iterable");
                if(!dvalue)
                    return 0;
                /* prepare data as an array of doubles */
                size = PySequence_Fast_GET_SIZE(dvalue);
                for(int i=0; i < size; i++) {
                    PyObject *fitem;
                    PyObject *item = PySequence_Fast_GET_ITEM(dvalue, i);
                    if(!item) {
                        Py_DECREF(dvalue);
                        return 0;
                    }
                    fitem = PyNumber_Float(item);
                    if(!fitem) {
                        Py_DECREF(dvalue);
                        PyErr_SetString(PyExc_TypeError, "all items must be numbers");
                        return 0;
                    }
                    value = PyFloat_AS_DOUBLE(fitem);
                    measures.push_back(value);
                    Py_DECREF(fitem);
                }
            }
            else if (key == "epsilon"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                epsilon = value;
            }
            else if (key == "sigma"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                sigma = value;
            }
            else if (key == "rho1"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                rho1 = value;
            }
            else if (key == "rho2"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                rho2 = value;
            }
            else if (key == "gamm1"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                gamm1 = value;
            }
            else if (key == "kappa"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                kappa = value;
            }
            else if (key == "mort_sup"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                mort_sup = value;
            }
            else if (key == "i_sat"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                i_sat = value;
            }
            else if (key == "dt"){
                /* get value */
                value = PyLong_AsUnsignedLong(dvalue);
                dt = (int)value;
            }    
            else if (key == "P_I1_I2"){ // is a list
                dvalue = PySequence_Fast(dvalue, "argument must be iterable");
                if(!dvalue)
                    return 0;
                /* prepare data as an array of doubles */
                size = PySequence_Fast_GET_SIZE(dvalue);
                for(int i=0; i < size; i++) {
                    PyObject *fitem;
                    PyObject *item = PySequence_Fast_GET_ITEM(dvalue, i);
                    if(!item) {
                        Py_DECREF(dvalue);
                        return 0;
                    }
                    fitem = PyNumber_Float(item);
                    if(!fitem) {
                        Py_DECREF(dvalue);
                        PyErr_SetString(PyExc_TypeError, "all items must be numbers");
                        return 0;
                    }
                    value = PyFloat_AS_DOUBLE(fitem);
                    P_I1_I2.push_back(value);
                    Py_DECREF(fitem);
                }
            }
            else if (key == "P_I2_D"){ // is a list
                dvalue = PySequence_Fast(dvalue, "argument must be iterable");
                if(!dvalue)
                    return 0;
                /* prepare data as an array of doubles */
                size = PySequence_Fast_GET_SIZE(dvalue);
                for(int i=0; i < size; i++) {
                    PyObject *fitem;
                    PyObject *item = PySequence_Fast_GET_ITEM(dvalue, i);
                    if(!item) {
                        Py_DECREF(dvalue);
                        return 0;
                    }
                    fitem = PyNumber_Float(item);
                    if(!fitem) {
                        Py_DECREF(dvalue);
                        PyErr_SetString(PyExc_TypeError, "all items must be numbers");
                        return 0;
                    }
                    value = PyFloat_AS_DOUBLE(fitem);
                    P_I2_D.push_back(value);
                    Py_DECREF(fitem);
                }
            }
            //std::cout << key << ": " << value << std::endl;     
        } else {
            std::cerr << "Something went wrong when decoding 'parameters' arguments" << std::endl;
            throw std::exception();
        }
    }

    /* parameters Done */

    /* instanciate model and integrate */
    std::vector<SEAIRStateSalje> states;
    

    SEAIRModelSalje seair;
    /*std::vector<double> step = one_step(
            y0, beta,measures, epsilon, sigma, rho1, rho2,
            gamm1, kappa, mort_sup, i_sat, P_I1_I2, P_I2_D);*/
    seair.beta = beta;
    seair.epsilon = epsilon; 
    seair.sigma = sigma;
    seair.rho1 = rho1;
    seair.rho2 = rho2;
    seair.gamm1 = gamm1;
    seair.kappa = kappa;
    seair.mort_sup = mort_sup;
    seair.P_I1_I2 = P_I1_I2;
    seair.P_I2_D = P_I2_D;

    int nb_steps;
    for (int i = 0; i < max_days; i+=dt ){
        //std::cerr << "Compute step nb : " << i << std::endl;
        states.push_back(y0);
        nb_steps = seair.integrate(y0, measures, i_sat, dt);
        //std::cerr << "    integrate in " << nb_steps << std::endl;
    }
    //states.push_back(y0);

    //std::cerr << "end loop" << std::endl;

    PyObject * results = GetUnpackedList(states);

    //std::cerr << "end unpacking" << std::endl;

    return results; 
};

static PyMethodDef saljepp_methods[] = {
        {"integrate",  (PyCFunction)integrate, METH_VARARGS | METH_KEYWORDS, "compute Salje ODE"},
        {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef saljepp_module = {
        /* link between C code and python code
         *      */
        PyModuleDef_HEAD_INIT,
        "saljepp", /* name of module */
        "",        /* module documentation, may be NULL */
        -1,        /* size of per-interpreter state of the module, 
                    or -1 if the module keeps state in global variables.*/
        saljepp_methods
};

PyMODINIT_FUNC PyInit_saljepp(void)
{    
        //import_array();    // only for numpy
        return PyModule_Create(&saljepp_module);
};
