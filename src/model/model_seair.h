#include <vector>
#include <numeric> // for accumulate
#include <math.h> // for fabs
#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric;
typedef vector<double> SEAIRState; //!< [S, E1, A1, I1, R1, E2, A2, I2, R2, M]

//! [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
inline SEAIRState default_y0() {
  return {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
}

// https://www.boost.org/doc/libs/1_60_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/getting_started/short_example.html
struct icu_values_saver {
  vector< double > & _icus, & _times;

  icu_values_saver( vector< double > &icus, vector< double > &times )
    : _icus( icus ) , _times( times ) { }

  inline void operator()( const SEAIRState &y , double t ) {
    _icus.push_back( .3 * y[7] );
    _times.push_back( t );
  }
};

struct SEAIR {
  static const unsigned int STATESIZE = 10;
  double bb = .2, mort_sup = 1e-5, vir_sup = .01;
  double epsilon = 1, sigma = .4, CFR = .15, R0 = 2.5, gamm1 = .06, gamm2 = .05, mild = .9; //!< common
  double nu = 0, i_sat = 0.2; //!< set in constructor
  double beta = -1, virulence = -1;  //!< set in compute_virulence_beta
  double c = 0;

  SEAIR(const double & nu_ = 0) : nu(nu_) {}

  inline void compute_virulence_beta(const double & S0) {
    // calculate virulence from case fatality ratio
    virulence = CFR * gamm2 / (1. - CFR);
    // calculer Beta depuis R0
    beta = R0 * gamm1 * sigma * (virulence + gamm2);
    double denom = virulence * gamm1 + gamm1 * gamm2;
    denom += bb * gamm1 * sigma;
    denom += virulence * mild * sigma;
    denom -= bb * gamm1 * mild * sigma;
    denom += gamm2 * mild * sigma;
    beta /= (S0 * denom);
  }

  //!< [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
  inline void covid_ode(const SEAIRState & y, SEAIRState & dydt) const {
    // fonction pour le système d'ODE lui-même
    // S=y[1] ; E1=y[2] ; A1=y[3] ; I1=y[4] ; R1=y[5] ;
    // E2=y[6] ; A2=y[7] ; I2=y[8] ; R2=y[9] ; M=y[10]
    //printf("y:"); for (auto& v : y) printf("%g, ", v); printf("\n");
    // Lambda <- (1 - c) * Beta * (y[3] + y[7] + y[4] + bb * y[8])
    double Lambda = (1. - c) * beta * (y[2] + y[6] + y[3] + bb * y[7]);
    // Mu <- Mu_func(mort_sup, y[8],i.sat)
    //double Mu = mu_func(mort_sup, y[7], i_sat);
    double Mu = mort_sup * (y[7] > i_sat);
    // Alpha <- Virulence + Mu_func(vir_sup, y[8],i.sat)
    //double Alpha = virulence + mu_func(vir_sup, y[7], i_sat);
    double Alpha = virulence + vir_sup * (y[7] > i_sat);
    // Unlike in some other programming languages, when you use negative numbers for indexing in R,
    // it doesn’t mean to index backward from the end
    // Instead, it means to drop the element at that index, counting the usual way, from the beginning.
    // N_tot <- sum(y[-10]) = sum of all elements minus M
    //double N_tot = sum(y) - y[9];
    double N_tot = accumulate(y.begin(), y.end()-1, 0.);
    //print(N_tot)
    // dydt = [dS, dE1, dA1, dI1, dR1, dE2, dA2, dI2, dR2, dM];
    // dS <- - Lambda * y[1] - Mu * y[1]
    dydt[0] = - Lambda * y[0] - Mu * y[0];
    // dE1 <- Mild * Lambda * y[1] - Epsilon * y[2] + Mild * Nu - Mu * y[2]
    dydt[1] = mild * Lambda * y[0] - epsilon * y[1] + mild * nu - Mu * y[1];
    // dA1 <- Epsilon * y[2] - Sigma * y[3] - Mu * y[3]
    dydt[2] = epsilon * y[1] - sigma * y[2] - Mu * y[2];
    // dI1 <- Sigma * y[3] - (Gamm1 + Mu) * y[4]
    dydt[3] = sigma * y[2] - (gamm1 + Mu) * y[3];
    // dR1 <- Gamm1 * y[4] - Mu * y[5]
    dydt[4] = gamm1 * y[3] - Mu * y[4];
    // dE2 <- (1 - Mild) * Lambda * y[1] - Epsilon * y[6] + (1 - Mild) * Nu - Mu * y[6]
    dydt[5] = (1. - mild) * Lambda * y[0] - epsilon * y[5] + (1. - mild) * nu - Mu * y[5];
    // dA2 <- Epsilon * y[6] - Sigma * y[7] - Mu * y[7]
    dydt[6] = epsilon * y[5] - sigma * y[6] - Mu * y[6];
    // dI2 <- Sigma * y[7] - (Gamm2 + Mu + Alpha) * y[8]
    dydt[7] = sigma * y[6] - (gamm2 + Mu + Alpha) * y[7];
    // dR2 <- Gamm2 * y[8] - Mu * y[9]
    dydt[8] = gamm2 * y[7] - Mu * y[8];
    // dM <- Alpha * y[8] + Mu * (N_tot-y[8])
    dydt[9] = Alpha * y[7] + Mu * (N_tot - y[7]);
  }


  vector<double> icu_times, icu_ratios;

  inline float mean_icu_patients() const {
    // printf("icu_ratios.size():%li\n", icu_ratios.size());
    double mean_icu_patients = 0, dt = icu_times.back() - icu_times.front();
    unsigned int nval = icu_times.size();
    for (unsigned int i = 1; i < nval; ++i)
      mean_icu_patients += (icu_times[i] - icu_times[i-1]) * .5 * (icu_ratios[i] + icu_ratios[i-1]);
    return mean_icu_patients / dt;
  }

  inline void operator() ( const SEAIRState &y , SEAIRState &dydt , const double t ) {
    // printf("():t:%f\n", t);
    covid_ode(y, dydt);
  }

  inline ssize_t integrate(SEAIRState & y, const double & c,
                           const double & i_sat, unsigned int maxdays,
                           bool save_icu_values = false) {
    // printf("integrate(%i, %i)\n", maxdays, store_icu_values);
    this->c = c;
    this->i_sat = i_sat;
    double start_time = 0, end_time = maxdays, dt =1;
    if (save_icu_values) {
      icu_times.clear();
      icu_ratios.clear();
      return odeint::integrate(*this, y, start_time, end_time, dt, icu_values_saver(icu_ratios, icu_times));
    } else
      return odeint::integrate(*this, y, start_time, end_time, dt);
    // typedef odeint::runge_kutta_cash_karp54< SEAIRState > error_stepper_type;
    //    double abs_error = 1E-3, rel_error = 1E-3;
    //    try {
    //      return odeint::integrate_adaptive( odeint::make_controlled< error_stepper_type >( abs_error, rel_error ) ,
    //                                        *this, y, start_time , end_time , dt ,
    //                                        odeint::null_observer() );
    //    } catch (runtime_error& /*e*/) {
    //      //printf("error: %s\n", e.what());
    //      return -1;
    //    }
  } // end integrate()
}; // end struct SEIAR

