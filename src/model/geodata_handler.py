import json
import numpy as np


def latlong(x,y):
        lat = -y / 111.0
        lng = x / (111.0 * np.cos(lat * 3.14159265359 / 180))
        return lng, lat

def bary(a, spots, spot_ids):
        pond = 100
        ax=  ((pond-1) * spots[spot_ids[a['assigned'][0]]]['x'] + sum([spots[spot_ids[ass]]['x'] for ass in a['assigned'] if ass!=-1]))/\
                                                                       (pond-1 + len([s for s in a['assigned'] if s!=-1]))
        ay =  ((pond-1) * spots[spot_ids[a['assigned'][0]]]['y'] + sum([spots[spot_ids[ass]]['y'] for ass in a['assigned'] if ass!=-1]))/\
                                                                       (pond-1 + len([s for s in a['assigned'] if s!=-1]))    
        return ax,ay
                
def check_spots(data):
        all_spots = set([s['id'] for s in data['spots']])
        not_added = set()
        for agent in data['agents']:
                for spot in agent['assigned']:
                        if not spot in all_spots and not spot in not_added:
                                not_added.add(spot)
                                print("Error, spot",spot, "not found")

        if len(not_added)>0:
                exit(1)

def get_agents_spots_department(filename, departments):
    with open(filename) as fp:
        data = json.load(fp)
    spots = data['spots']
    agents = data['agents']
    spot_ids = {s['id']:i for i,s in enumerate(spots)}

    agent_dep = {}
    departments_set = set(departments)
    for a in agents:
        if a['assigned'][0]==-1:#homeless ?
                continue
        dep = spots[spot_ids[a['assigned'][0]]]['area']
        if dep in departments_set:
                if dep in agent_dep:
                        agent_dep[dep].append(a)
                else:
                    agent_dep[dep] = [a]
            
    
    features = []
    geodata = {"type":"FeatureCollection", "features":features}
    
    
    for area in departments:
        drawn_spots = set()
        for a in agent_dep[area]:
            ax,ay = bary(a,spots, spot_ids)
            features.append({"type":"Feature",
                                 "properties":{"type":"agent","r":3},
                                 "geometry":{"type":"Point",
                                             "coordinates": latlong(ax,ay)}}) 
            for s in a['assigned']:
                    if s!= -1:
                        spot = spots[spot_ids[s]]
                        if not s in drawn_spots:
                            features.append({"type":"Feature",
                                             "properties":{"type":"spot","kind":spot["kind"], "r":3},
                                             "geometry":{"type":"Point",
                                                         "coordinates": latlong(spot['x'],spot['y'])}})
                            drawn_spots.add(s)
                            
                        features.append({"type":"Feature",
                                         "properties":{"type":"link"},
                                         "geometry":{"type":"LineString",
                                                     "coordinates":[latlong(ax,ay),
                                                                    latlong(spot['x'],
                                                                            spot['y'])]}})
    print("dumping", len(features), "features")
    return geodata


def get_contact_and_state(filename, departments):
        with open(filename) as fp:
                data = json.load(fp)
        spots = data['spots']
        agents = data['agents']
        spot_ids = {s['id']:i for i,s in enumerate(spots)}

        for a in agents:
                lat, lng = latlong(*bary(a,spots,spot_ids))
                a['lat'] = lat
                a['lng'] = lng

        
        geodata = []
        
        departments_set = set(departments)
        added_agents = set()
        
        for w in range(len(data['weeks'])):
                features = []
                geodata.append({"type":"FeatureCollection", "features":features})
                for j,a in enumerate(data['weeks'][w]['agents']):
                        dep = spots[spot_ids[agents[j]['assigned'][0]]]['area']
                        
                        if not dep in departments_set: continue
                        
                        features.append({"type":"Feature",
                                         "properties":{"type":"agent","r":3, "stage":a['stage']},
                                         "geometry":{"type":"Point",
                                                     "coordinates": [agents[j]['lat'],agents[j]['lng']]}})
                        added_agents.add(j) 
                        
                for j,a in enumerate(data['weeks'][w]['agents']):
                        dep = spots[spot_ids[agents[j]['assigned'][0]]]['area']
                        if not dep in departments_set: continue
                        contacts = a['contacts']
                        for c in contacts:
                                cont = agents[c]
                        
                                if not c in added_agents:
                                        added_agents.add(j)
                                        features.append({"type":"Feature",
                                                         "properties":{"type":"agent","r":3, "stage":data['weeks'][w]['agents'][c]['stage']},
                                                         "geometry":{"type":"Point",
                                                                     "coordinates": [cont['lat'],cont['lng']]}}) 
                                
                              
                                features.append({"type":"Feature",
                                                 "properties":{"type":"link"},
                                                 "geometry":{"type":"LineString",
                                                              "coordinates":[[agents[j]['lat'],agents[j]['lng']],
                                                                             [cont['lat'],cont['lng']]]}})
                print("dumping", len(features), "features")
        return geodata
