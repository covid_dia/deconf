#include <vector>
#include <numeric> // for accumulate
#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric;
namespace pl = std::placeholders;
typedef vector<double> SEAIRStateSalje; //!< [S, E, A, I1, I2, R, M]

//! [S, E, A, I1, I2, R, M]
inline SEAIRStateSalje default_y0() {
  return {1, 0, 0, 0, 0, 0, 0};
}


std::vector<double> inner_vect(
                std::vector<double> a, // flattened matrix of size M x M
                std::vector<double> b, // vector of size M (n_age)
                int n_age){
    /* numpy style inner product of two flattened matrix
       only take row of the left matrix
     */
    std::vector<double> tot(n_age, 0.0);
    for(int i=0; i< n_age; ++i){
        double s = 0;
        for(int j=0; j<n_age; ++j){
            s += a[(i*n_age)+j] * b[j];
        }
        tot[i] = s;
    }
    return tot;
}

std::vector<double> compute_lambda(
            float beta,
            std::vector<double> measures,
            std::vector<double> sub_A,
            std::vector<double> sub_I1,
            std::vector<double> sub_I2,
            int n_ages){
    /* compute lambda, "infection strengh for each age category
     */
    std::vector<double> output;

    std::vector<double> measures_A = inner_vect(measures, sub_A, n_ages);
    std::vector<double> measures_I1 = inner_vect(measures, sub_I1, n_ages);
    std::vector<double> measures_I2 = inner_vect(measures, sub_I2, n_ages);

    for (int i=0; i<measures_A.size(); ++i)
        output.push_back(beta * (measures_A[i] + measures_I1[i] + measures_I2[i]));
    
    return output;
}                 


struct SEAIRModelSalje {
  // default parameter values, they will be updated in saljeppmodule.cpp
  static const unsigned int STATESIZE = 7;
  double epsilon = 0.18602296118236494, gamm1 = 0.05, kappa = 0.06244988053735144, mort_sup = 1.9247963043881915E-07;
  double R0 = 3.0455528279804147, rho1 = 0.3622625114616134, rho2 = 0.01826128053979491, sigma = 0.9057847879975816;
  double beta;
  std::vector<double> c;
  std::vector<double> P_I1_I2 = {0.001, 0.005, 0.01 , 0.015, 0.028, 0.061, 0.096, 0.217};
  std::vector<double> P_I2_D = {0.006, 0.014, 0.021, 0.036, 0.07 , 0.132, 0.232, 0.384};
  //std::vector<double p_age = {0.24388774, 0.11732615, 0.12213723, 0.13413307, 0.13018146, 0.12000388, 0.07182578, 0.0605047};
  int n_age = P_I1_I2.size();
  double i_sat = 0.2;

  //!< [S, E, A, I1, I2, R, M]
  inline void one_step(const SEAIRStateSalje & y, SEAIRStateSalje & dydt) const {
    /* one step of integration
     */

    /* the y0 matrix (N comp x M age) is flattened, quick indexing of compartiments*/
    int idx_S = 0;        // [0, n[ -> the S compartiments
    int idx_E = n_age;    // [n, n*2[ -> the S compartiments
    int idx_A = n_age*2;  // idem ...
    int idx_I1 = n_age*3;
    int idx_I2 = n_age*4;
    int idx_R = n_age*5;
    int idx_D = n_age*6;

    /* prepare sub vector corresponding to infectious people for easy handling */
    std::vector<double> sub_A( y.begin() + idx_A,  y.begin() + idx_A  + n_age);
    std::vector<double> sub_I1(y.begin() + idx_I1, y.begin() + idx_I1 + n_age);
    std::vector<double> sub_I2(y.begin() + idx_I2, y.begin() + idx_I2 + n_age);

    /* compute mort sup factor */
    double Mu = mort_sup * (accumulate(sub_I2.begin(), sub_I2.end(), 0.0) > i_sat);

    /* compute Lambda vector of size n_age */
    std::vector<double> Lambda = compute_lambda(beta, c, sub_A, sub_I1, sub_I2, n_age);

    /*std::cerr << "Lambda = " ;
    for (int i=0; i< Lambda.size(); ++i)
        std::cerr << Lambda[i] << ", ";
    std::cerr << std::endl;*/

    /* for each age class */
    for (int i=0; i<n_age; ++i){
      /* compute n_tot */
      double N_tot = y[idx_S+i] + y[idx_E+i] + y[idx_A+i] + y[idx_I1+i] + y[idx_I2+i] + y[idx_R+i];
      /* get corresponding lambda */
      double lambda = Lambda[i];
      /* integrate */
      dydt[idx_S+i] = - lambda * y[idx_S+i]  -      Mu * y[idx_S+i];
      dydt[idx_E+i] =   lambda * y[idx_S+i]  - epsilon * y[idx_E+i]   - Mu * y[idx_E+i];
      dydt[idx_A+i] =  epsilon * y[idx_E+i]  -   sigma * y[idx_A+i]   - Mu * y[idx_A+i];
      dydt[idx_I1+i] =   sigma * y[idx_A+i]  -   (rho1 * y[idx_I1+i] * (1 - P_I1_I2[i])) - (gamm1 * y[idx_I1+i] * P_I1_I2[i]) - Mu * y[idx_I1+i];
      dydt[idx_I2+i] =   gamm1 * y[idx_I1+i] * P_I1_I2[i] - (rho2 * y[idx_I2+i] * (1 - P_I2_D[i])) - (kappa * y[idx_I2+i] * P_I2_D[i]) - Mu * y[idx_I2+i];
      dydt[idx_R+i] =    (rho1 * y[idx_I1+i] * (1 - P_I1_I2[i])) + (rho2 * y[idx_I2+i] * (1 - P_I2_D[i])) - Mu * y[idx_R+i];
      dydt[idx_D+i] =    kappa * y[idx_I2+i] * P_I2_D[i] + Mu * N_tot;
    }
  }

  typedef odeint::runge_kutta_cash_karp54< SEAIRStateSalje > error_stepper_type;

  void operator() ( const SEAIRStateSalje &y , SEAIRStateSalje &dydt , const double t ) {
    one_step(y, dydt);
  }

  inline ssize_t integrate(SEAIRStateSalje & y, const std::vector<double> & c,
                           const double & i_sat, unsigned int maxdays) {
    this->c = c;
    this->i_sat = i_sat;
    double start_time = 0, end_time = maxdays, dt =1;
    //return odeint::integrate(*this, y, start_time, end_time, dt);
    double abs_error = 1E-9, rel_error = 1E-9;
    //odeint::runge_kutta4< SEAIRStateSalje > stepper;
    try {
      return odeint::integrate_adaptive(odeint::make_controlled< error_stepper_type >( abs_error, rel_error ) , // stepper,
                                        *this, y, start_time , end_time , dt ,
                                        odeint::null_observer() );
    } catch (runtime_error& /*e*/) {
      //printf("error: %s\n", e.what());
      return -1;
    }
  } // end integrate()
}; // end struct SEIARModelSalje

