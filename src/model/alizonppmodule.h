#include <vector>
#include <numeric> // for accumulate
#include <math.h> // for fabs
#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric;
namespace pl = std::placeholders;
typedef vector<double> SEAIRStateAlizon; //!< [S, E1, A1, I1, R1, E2, A2, I2, R2, M]

SEAIRStateAlizon SEAIR_ratios2y0(int npop, const SEAIRStateAlizon & in) {
  assert(in.size() == 10);
  double total = accumulate(in.begin(), in.end(), 0.);
  assert(fabs(total - 1) < 1E-4);
  double r = 1. * npop / total;
  return { r*in[0], r*in[1], r*in[2], r*in[3], r*in[4], r*in[5], r*in[6], r*in[7], r*in[8], r*in[9] };
}

//! [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
inline SEAIRStateAlizon default_y0() {
  return {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
}

struct SEAIRModelAlizon {
  static const unsigned int STATESIZE = 10;
  double bb = .2, mort_sup = 1e-5, vir_sup = .01;
  double epsilon = 1, sigma = .4, CFR = .15, R0 = 2.5, gamm1 = .06, gamm2 = .05, mild = .9; //!< common
  double nu = 0, i_sat = 0.2; //!< set in constructor
  double beta = -1, virulence = -1;  //!< set in compute_virulence_beta
  double c = 0;

  SEAIRModelAlizon(const double & nu_ = 0) : nu(nu_) {}

  inline void compute_virulence_beta(const double & S0) {
    // calculate virulence from case fatality ratio
    virulence = CFR * gamm2 / (1. - CFR);
    // calculer Beta depuis R0
    beta = R0 * gamm1 * sigma * (virulence + gamm2);
    double denom = virulence * gamm1 + gamm1 * gamm2;
    denom += bb * gamm1 * sigma;
    denom += virulence * mild * sigma;
    denom -= bb * gamm1 * mild * sigma;
    denom += gamm2 * mild * sigma;
    beta /= (S0 * denom);
  }

  //!< [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
  inline void covid_ode(const SEAIRStateAlizon & y, SEAIRStateAlizon & dydt) const {
    // fonction pour le système d'ODE lui-même
    // S=y[1] ; E1=y[2] ; A1=y[3] ; I1=y[4] ; R1=y[5] ;
    // E2=y[6] ; A2=y[7] ; I2=y[8] ; R2=y[9] ; M=y[10]
    //printf("y:"); for (auto& v : y) printf("%g, ", v); printf("\n");
    // Lambda <- (1 - c) * Beta * (y[3] + y[7] + y[4] + bb * y[8])
    double Lambda = (1. - c) * beta * (y[2] + y[6] + y[3] + bb * y[7]);
    // Mu <- Mu_func(mort_sup, y[8],i.sat)
    //double Mu = mu_func(mort_sup, y[7], i_sat);
    double Mu = mort_sup * (y[7] > i_sat);
    // Alpha <- Virulence + Mu_func(vir_sup, y[8],i.sat)
    //double Alpha = virulence + mu_func(vir_sup, y[7], i_sat);
    double Alpha = virulence + vir_sup * (y[7] > i_sat);
    // Unlike in some other programming languages, when you use negative numbers for indexing in R,
    // it doesn’t mean to index backward from the end
    // Instead, it means to drop the element at that index, counting the usual way, from the beginning.
    // N_tot <- sum(y[-10]) = sum of all elements minus M
    //double N_tot = sum(y) - y[9];
    double N_tot = accumulate(y.begin(), y.end()-1, 0.);
    // dydt = [dS, dE1, dA1, dI1, dR1, dE2, dA2, dI2, dR2, dM];
    // dS <- - Lambda * y[1] - Mu * y[1]
    dydt[0] = - Lambda * y[0] - Mu * y[0];
    // dE1 <- Mild * Lambda * y[1] - Epsilon * y[2] + Mild * Nu - Mu * y[2]
    dydt[1] = mild * Lambda * y[0] - epsilon * y[1] + mild * nu - Mu * y[1];
    // dA1 <- Epsilon * y[2] - Sigma * y[3] - Mu * y[3]
    dydt[2] = epsilon * y[1] - sigma * y[2] - Mu * y[2];
    // dI1 <- Sigma * y[3] - (Gamm1 + Mu) * y[4]
    dydt[3] = sigma * y[2] - (gamm1 + Mu) * y[3];
    // dR1 <- Gamm1 * y[4] - Mu * y[5]
    dydt[4] = gamm1 * y[3] - Mu * y[4];
    // dE2 <- (1 - Mild) * Lambda * y[1] - Epsilon * y[6] + (1 - Mild) * Nu - Mu * y[6]
    dydt[5] = (1. - mild) * Lambda * y[0] - epsilon * y[5] + (1. - mild) * nu - Mu * y[5];
    // dA2 <- Epsilon * y[6] - Sigma * y[7] - Mu * y[7]
    dydt[6] = epsilon * y[5] - sigma * y[6] - Mu * y[6];
    // dI2 <- Sigma * y[7] - (Gamm2 + Mu + Alpha) * y[8]
    dydt[7] = sigma * y[6] - (gamm2 + Mu + Alpha) * y[7];
    // dR2 <- Gamm2 * y[8] - Mu * y[9]
    dydt[8] = gamm2 * y[7] - Mu * y[8];
    // dM <- Alpha * y[8] + Mu * (N_tot-y[8])
    dydt[9] = Alpha * y[7] + Mu * (N_tot - y[7]);
  }

  typedef odeint::runge_kutta_cash_karp54< SEAIRStateAlizon > error_stepper_type;
  vector<pair<double, double>> icu_values;
  bool store_icu_values;

  void operator() ( const SEAIRStateAlizon &y , SEAIRStateAlizon &dydt , const double t ) {
    covid_ode(y, dydt);
    if (store_icu_values)
      icu_values.push_back(make_pair(t, .3 * y[7]));
  }

  inline ssize_t integrate(SEAIRStateAlizon & y, const double & c,
                           const double & i_sat, unsigned int maxdays,
                           bool store_icu_values = false) {
    this->c = c;
    this->i_sat = i_sat;
    this->store_icu_values = store_icu_values;
    if (store_icu_values)
      icu_values.clear();
    double start_time = 0, end_time = maxdays, dt=1;
    //return odeint::integrate(*this, y, start_time, end_time, dt);
    double abs_error = 1E-9, rel_error = 1E-9;
    try {
      return odeint::integrate_adaptive( odeint::make_controlled< error_stepper_type >( abs_error, rel_error ) ,
                                        *this, y, start_time , end_time , dt ,
                                        odeint::null_observer() );
    } catch (runtime_error& /*e*/) {
      //printf("error: %s\n", e.what());
      return -1;
    }
  } // end integrate()
}; // end struct SEIAR

