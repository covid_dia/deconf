# !/usr/bin/env python3

import json
import os
import random
import subprocess
from http.server import BaseHTTPRequestHandler, HTTPServer

from src.model.economie import Economie
from src.model.policy_utils import decov_path
from src.model.geodata_handler import get_agents_spots_department, get_contact_and_state
from src.model.macromodel import MacroModelServer, start_server, error_dumps

from hashlib import md5

class MicroModelRequestHandler(BaseHTTPRequestHandler):

    def __init__(self, request=None, client_address=None, server=None):
        self.server = None
        if (request, client_address, server) != (None, None, None):
            BaseHTTPRequestHandler.__init__(self, request, client_address, server)


    def eval_policy(self, post_body, eval_policy_req, economy=None):
        use_economy = economy
        if use_economy is None:
            use_economy = self.server.economy
        # sanity checks on eval_policy_req
        if 'compartmental_model_parameters' not in eval_policy_req:
            return error_dumps('Missing compartmental_model_parameters.')
        # 1. call micromodel via subprocess
        bin_path = decov_path + '/build/decov'
        # Commented because dangerous: should use the same generation system of CI to avoid unexpected behavior
        # if not os.path.isfile(bin_path):  # Makefile-style
        #     bin_path = decov_path + '/src/model/decov/build/RELEASE/decov'
        if not os.path.isfile(bin_path):  # CMake-style
            return error_dumps('Could not find "decov" C++ executable, did you compile?')
        args = [bin_path,
                '-parameters', 'stdin',
                '-report', 'stdout',
                '-verbose', 'false']
        getgeo = 'get_geo_department' in eval_policy_req and len(eval_policy_req['get_geo_department']) > 0
        if getgeo:
            tmp = "/tmp/" + str(random.randint(10056, 110311)) + ".json"
            args += ["-geo", tmp]

        """init_data_dir = decov_path + '/src/model/decov/init_data/'
        if not os.path.exists(init_data_dir):
            os.mkdir(init_data_dir)
        m = md5()
        m.update(post_body)
        checksum = m.hexdigest()
        path_to_init = init_data_dir + checksum + ".bson"
        if os.path.exists(path_to_init):
            args += ["-load", path_to_init]
        else:
            args += ["-save", path_to_init]
        """
        
        args += ['-data', decov_path + '/src/model/decov/data.json']
        print("Run:", " ".join(args))
        simout = subprocess.check_output(args, input=post_body)
        simout = str(simout)  # convert byte array -> string
        # print(simout)
        # strip output until we find the first "{" and after last "}"
        simout = simout[simout.index('{'):simout.rindex('}') + 1]

        dumps = json.loads(simout)
        if getgeo:
            if not 'getgeo_mode' in eval_policy_req or eval_policy_req['getgeo_mode'] == "spots":
                geodata = get_agents_spots_department(tmp, [eval_policy_req['zones'].index(i) for i in
                                                            eval_policy_req['get_geo_department']])
            else:
                geodata = get_contact_and_state(tmp, [eval_policy_req['zones'].index(i) for i in
                                                      eval_policy_req['get_geo_department']])
            dumps['geodata'] = geodata
            os.remove(tmp)

        # sanity checks on micromodel output
        # print(dumps)
        if 'cases' not in dumps:
            return error_dumps('The micromodel did not output expected "cases" field.')
        nweeks = dumps['max_days'] // 7  # in case of early stopping, can be < eval_policy_req['max_days']

        # 2. call economics model
        gdp_accumulated_loss = use_economy.evaluate(eval_policy_req, nweeks)
        dumps.update({'gdp_accumulated_loss': gdp_accumulated_loss})
        # print(dumps)
        return dumps

    # noinspection PyPep8Naming
    def do_POST(self):
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        self.send_response(200)
        self.end_headers()
        dumps = None
        if 'eval_policy' in self.path:
            dumps = self.eval_policy(post_body, json.loads(post_body))
        if not dumps:
            dumps = error_dumps('command ' + self.path + ' not recognized')
        self.wfile.write(json.dumps(dumps).encode())

    # disable logs "127.0.0.1 - - [12/Apr/2020 14:16:17] "POST /eval_policy HTTP/1.1" 200 -"
    def log_message(self, format_, *args):
        return


class MicroModelServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass):
        # stuff that needs to be stored here
        self.economy = Economie()
        HTTPServer.__init__(self, server_address, RequestHandlerClass)


if __name__ == '__main__':
    start_server(MacroModelServer, MicroModelRequestHandler)
