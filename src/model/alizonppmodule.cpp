#define PY_SSIZE_T_CLEAN 
#include <Python.h>
#include <iostream>
#include <vector>
#include <string>
#include <exception>
#include "alizonppmodule.h"

using namespace std;
        

static PyObject* 
GetList(std::vector<double> list)
{
    srand(time(NULL));
    int const N = list.size();
    PyObject* python_val = PyList_New(N);
    for (int i = 0; i < N; ++i)
    {
        PyObject* python_float = Py_BuildValue("f", list[i]);
        PyList_SetItem(python_val, i, python_float);
    }
    return python_val;
}

static PyObject* 
GetUnpackedList(std::vector<std::vector<double>> packedlist)
{
    srand(time(NULL));
    int const N = packedlist.size();
    int cnt = 0;
    for (int i = 0; i < N; ++i) {
        int const M = packedlist[i].size();
        for (int j = 0; j < M; ++j){
            cnt ++;
        }   
    }
    PyObject* python_val = PyList_New(cnt);
    cnt = 0;

    for (int i = 0; i < N; ++i) {
        int const M = packedlist[i].size();
        for (int j = 0; j < M; ++j){
            PyObject* python_float = Py_BuildValue("f", packedlist[i][j]);
            PyList_SetItem(python_val, cnt, python_float);
            cnt ++;
        }
    }
    return python_val;
}


static PyObject* 
integrate(PyObject *self, PyObject *args, PyObject *kwargs) {
    static char *kwlist[] = {"y0", "max_days", "params", NULL};
    // 1 -> y0
	// 2 -> time
	// 3 -> parameters
	int max_days;
    int numComp;
    double value;
	PyObject * listObj; /* the list of strings */
    PyObject * floatObj;
    std::vector<double> y0;

	PyObject * dictObj; /* the list of strings */
    PyObject *dkey, *dvalue, *strObj;
    Py_ssize_t pos = 0;
    std::string key;
    float epsilon = -1;
    float sigma= -1;
    float CFR = -1;
    float R0 = -1;
    float gamm1 = -1;
    float gamm2 = -1;
    float mild = -1;
    float nu = -1;
    float bb = -1;
    float mort_sup = -1;
    float vir_sup = -1;
    float c = -1; 
    float virulence = -1; 
    float beta = -1; 
    float i_sat = -1;
    int size;
    int dt=1;

    /* the O! parses for a Python object (listObj) checked
       to be of type PyList_Type */
	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O!iO!", kwlist, 
                                                            &PyList_Type, &listObj, 
                                                            &max_days,
                                                            &PyDict_Type, &dictObj))
        return NULL;

    /* Parse y0 */

    /* get the number of compartiments */
    listObj = PySequence_Fast(listObj, "argument must be iterable");
    if(!listObj)
        return 0;

    /* prepare data as an array of doubles */
    numComp = PySequence_Fast_GET_SIZE(listObj);
    for(int i=0; i < numComp; i++) {
        PyObject *fitem;
        PyObject *item = PySequence_Fast_GET_ITEM(listObj, i);
        if(!item) {
            Py_DECREF(listObj);
            return 0;
        }
        fitem = PyNumber_Float(item);
        if(!fitem) {
            Py_DECREF(listObj);
            PyErr_SetString(PyExc_TypeError, "all items must be numbers");
            return 0;
        }
        value = PyFloat_AS_DOUBLE(fitem);
        y0.push_back(value);
        //std::cerr << value << std::endl;
        Py_DECREF(fitem);
    }    

    /* clean up, compute, and return result */
    Py_DECREF(listObj);

    /* y0 done */

    //Py_INCREF(dictObj);
    /* parse parameters */
    while (PyDict_Next(dictObj, &pos, &dkey, &dvalue)) {
        /* get key */
        PyObject * temp_bytes = PyUnicode_AsEncodedString(dkey, "UTF-8", "strict"); // Owned reference
        if (temp_bytes != NULL) {
            /* make it a string */
            key = PyBytes_AS_STRING(temp_bytes); // Borrowed pointer
            Py_DECREF(temp_bytes);
            if (key == "c"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                c = value;
            }
            else if (key == "epsilon"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                epsilon = value;
            }
            else if (key == "beta"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                beta = value;
            }
            else if (key == "virulence"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                virulence = value;
            }
            else if (key == "sigma"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                sigma = value;
            }
            else if (key == "CFR"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                CFR = value;
            }
            else if (key == "R0"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                R0 = value;
            }
            else if (key == "gamm1"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                gamm1 = value;
            }
            else if (key == "gamm2"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                gamm2 = value;
            }
            else if (key == "mild"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                mild = value;
            }
            else if (key == "nu"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                nu = value;
            }
            else if (key == "bb"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                bb = value;
            }
            else if (key == "mort_sup"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                mort_sup = value;
            }
            else if (key == "vir_sup"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                vir_sup = value;
            }
            else if (key == "i_sat"){
                /* get value */
                value = PyFloat_AS_DOUBLE(dvalue);
                i_sat = value;
            }
            else if (key == "dt"){
                /* get value */
                value = PyLong_AsUnsignedLong(dvalue);
                dt = (int)value;
            }            
            //std::cout << key << ": " << value << std::endl;     
        } else {
            std::cerr << "Something went wrong when decoding 'parameters' arguments" << std::endl;
            throw std::exception();
        }
    }
    //Py_DECREF(dictObj);

    /* parameters Done */

    /* instanciate model and integrate */
    std::vector<SEAIRStateAlizon> states;
    

    SEAIRModelAlizon seair;
    seair.epsilon = epsilon;
    seair.sigma= sigma;
    seair.CFR = CFR;
    seair.R0 = R0;
    seair.gamm1 = gamm1;
    seair.gamm2 = gamm2;
    seair.mild = mild;
    seair.nu = nu;
    seair.bb = bb;
    seair.mort_sup = mort_sup;
    seair.vir_sup = vir_sup;
    //seair.c = c;
    seair.beta = beta;
    seair.virulence = virulence;
    //seair.compute_virulence_beta(y0[0]);


    int nb_steps=0;
    for (int i = 0; i < max_days; i+=dt ){
        states.push_back(y0);
        //std::cerr << i << " " << max_days << std::endl;
        //std::cerr << "Compute step nb : " << i << std::endl;
        nb_steps += seair.integrate(y0, c, i_sat, dt);
        //std::cerr << "    integrate in " << nb_steps << std::endl;
    }
    //states.push_back(y0); // needed?

    //std::cerr << nb_steps << std::endl;

    PyObject * results = GetUnpackedList(states);

    //std::cerr << "end unpacking" << std::endl;

    return results; 
};

static PyMethodDef alizonpp_methods[] = {
        {"integrate",  (PyCFunction)integrate, METH_VARARGS | METH_KEYWORDS, "compute Alizon ODE"},
        {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef alizonpp_module = {
        /* link between C code and python code
         *      */
        PyModuleDef_HEAD_INIT,
        "alizonpp", /* name of module */
        "",        /* module documentation, may be NULL */
        -1,        /* size of per-interpreter state of the module, 
                    or -1 if the module keeps state in global variables.*/
        alizonpp_methods
};

PyMODINIT_FUNC PyInit_alizonpp(void)
{    
        //import_array();    // only for numpy
        return PyModule_Create(&alizonpp_module);
};
