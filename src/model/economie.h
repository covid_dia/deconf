#include <fstream>
#include <iostream>
#include <map>
#include <boost/algorithm/string.hpp>
#include "decov_path.h"

using namespace  std;
typedef double GDP;
typedef unsigned char MeasureValue; //!< value of a measure, normally in [0..3]
typedef vector<MeasureValue> StepPolicy;

class Economie {
public:
  //! ctor, load CSV and JSON from data and parse them
  Economie() {
    // read CSV
    ifstream file(DECOV_PATH "/data/economie_evaluate_one.csv");
    string line = "";
    getline(file, line); // skip first line
    getline(file, line); // second line
    assert(line=="red_contact_tracing,red_forced_telecommuting,red_school_closures,red_social_containment_all,red_total_containment_high_risk,green_contact_tracing,green_forced_telecommuting,green_school_closures,green_social_containment_all,green_total_containment_high_risk,c");
    // Iterate through each line and split the content using delimeter
    StepPolicy red(5), green(5);
    vector<string> vec;
    while (getline(file, line)) {
      boost::algorithm::split(vec, line, boost::is_any_of(","));
      if (vec.size() != 11) {
        printf("line '%s' has incorrect format\n", line.c_str());
        exit(-1);
      }
      for (unsigned int i = 0; i < 5; ++i)
        red[i] = (MeasureValue) atoi(vec[i].c_str());
      for (unsigned int i = 5; i < 10; ++i)
        green[i-5] = (MeasureValue) atoi(vec[i].c_str());
      GDP gdp = atof(vec.back().c_str());
      _red_green_values[red][green] = gdp;
      if (accumulate(green.begin(), green.end(), 0) == 0)
        _red_values[red] = gdp;
      if (accumulate(red.begin(), red.end(), 0) == 0)
        _green_values[green] = gdp;
    }
    file.close();
  } // end ctor

  //! week_policy = [red_contact_tracing, red_forced_telecommuting, red_school_closures,
  //!                red_social_containment_all, red_total_containment_high_risk,
  //!                green_contact_tracing, green_forced_telecommuting, green_school_closures,
  //!                green_social_containment_all, green_total_containment_high_risk]
  inline GDP evaluate_one_week(const StepPolicy & step_policy_red,
                               const StepPolicy & step_policy_green) const {
    return _red_green_values.at(step_policy_red).at(step_policy_green);
  } // end evaluate_one_week()

  inline GDP evaluate_one_week_one_label(const unsigned int label_idx,
                                         const StepPolicy & label_step_policy) const {
    // printf("label_step_policy:"); for (const auto & v : label_step_policy) printf("%i, ", v); printf("\n");
    switch (label_idx) {
      case 0:
        return _red_values.at(label_step_policy);
      default:
        return _green_values.at(label_step_policy);
    }
  } // end evaluate_one_week_one_label()

  map<StepPolicy, map<StepPolicy, GDP> > _red_green_values;
  map<StepPolicy, GDP> _red_values;
  map<StepPolicy, GDP> _green_values;
}; // end class Economie
