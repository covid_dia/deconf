# encoding=utf8
import csv
import itertools
import json
import sys

from src.model.policy_utils import default_eval_policy_req, all_measures, decov_path


class Economie:
    def __init__(self):
        with open(decov_path + '/data/data_impact_branches.csv') as fp:
            reader = csv.DictReader(fp)
            self.impact = dict()
            for row in reader:
                key = row["branches"]
                del row["branches"]
                self.impact[key] = {k: abs(float(a)) for k, a in row.items()}  # WARNING use everything > 0

        with open(decov_path + '/data/population_active_a_risque_departement.json') as fp:
            self.risque = json.load(fp)

        with open(decov_path + '/data/data_emploi_departement.json') as fp:
            self.emploi = json.load(fp)

        emploi_national_branches = {branch: 0 for branch in self.impact}
        for reg in self.emploi:
            for branch in self.emploi[reg]:
                emploi_national_branches[branch] += self.emploi[reg][branch]

        self.part_pib_region = dict()
        for reg in self.emploi:
            self.part_pib_region[reg] = dict()
            for branch in self.impact:
                self.part_pib_region[reg][branch] = self.impact[branch]['part'] * self.emploi[reg][branch] / \
                                                    emploi_national_branches[branch]

    def evaluate_one(self, policy, hrvcr=0.8):
        zones = policy['zones']
        gdb_loss = 0
        for zone_idx, reg in enumerate(zones):

            label_idx = policy['zone_labels'][zone_idx]
            # if reg=='2A':
            #     print('evaluate_one()', reg, label_idx,
            #           policy["forced_telecommuting"][label_idx], policy["school_closures"][label_idx],
            #           policy["social_containment_all"][label_idx], policy["total_containment_high_risk"][label_idx])
            for branch in self.impact:
                gdb_branch_reg_ecoles = self.part_pib_region[reg][branch] * \
                                        self.impact[branch]['ecoles']
                gdb_branch_reg_confinement = self.part_pib_region[reg][branch] * \
                                             self.impact[branch]['confinement']
                gdb_branch_reg_teletravail = self.part_pib_region[reg][branch] * \
                                             self.impact[branch]['teletravail']
                telecommuting = policy["forced_telecommuting"][label_idx] / 2.0
                gdb_loss += telecommuting * gdb_branch_reg_teletravail / 52

                school_closed = policy["school_closures"][label_idx] >= 2
                val = school_closed * gdb_branch_reg_ecoles / 52
                gdb_loss += val / 2 if policy['school_closures'][label_idx] == 2 else val  # divide by two if half-half

                activity_less = [0, 0.2, 0.8, 1]  # social activity <10, <100 and all | todo : use data

                socially_contained = activity_less[int(policy["social_containment_all"][label_idx])]
                gdb_loss += socially_contained * gdb_branch_reg_confinement / 52

                risk_contained = hrvcr * policy["total_containment_high_risk"][label_idx]
                gdb_loss += risk_contained * self.risque[reg]["active"] * (
                        telecommuting == 0) * gdb_branch_reg_teletravail / 5200.0
                gdb_loss += risk_contained * self.risque[reg]["all"] * (
                        socially_contained == 0) * gdb_branch_reg_confinement / 5200.0

        return gdb_loss

    def evaluate(self, req, weeks):
        if isinstance(req, str):
            with open(req) as fp:
                req = json.load(fp)
        zones = req['zones']
        npolicies = len(req["zone_label_names"])
        gdb_accumulated_loss = [0]
        gdb_loss = 0
        dummy = [0] * npolicies
        for week in range(weeks):
            last_week = len([j for j, i in enumerate(req['measure_weeks']) if i <= week]) - 1
            if last_week == -1:
                policy = {"social_containment_all": [2] * npolicies,
                          "school_closures": [3] * npolicies,
                          "forced_telecommuting": [3] * npolicies,
                          "total_containment_high_risk": [1] * npolicies}
            else:
                so = dummy if req["social_containment_all"] is None else req["social_containment_all"][last_week][
                    'activated']
                sc = dummy if req["school_closures"] is None else req["school_closures"][last_week]['activated']
                te = dummy if req["forced_telecommuting"] is None else req["forced_telecommuting"][last_week][
                    'activated']
                ri = dummy if req["total_containment_high_risk"] is None else req["total_containment_high_risk"][
                    last_week]['activated']
                policy = {"social_containment_all": so, "school_closures": sc, "forced_telecommuting": te,
                          "total_containment_high_risk": ri}
            policy['zones'] = req['zones']  # To be sure that we evaluate on req's zones
            policy['zone_labels'] = req['zone_labels']  # copy labels too
            gdb_loss += self.evaluate_one(policy, req['compartmental_model_parameters']['common'][
                'high_risk_voluntary_containment_rate'])
            gdb_accumulated_loss.append(gdb_loss)
        return gdb_accumulated_loss


def simple_test():
    if len(sys.argv) == 1:
        filename = decov_path + "/doc/eval_policy_req.json"
    else:
        filename = sys.argv[1]
    economie = Economie()
    print(economie.evaluate(filename, 10))


def evaluate_one_write_csv():
    eval_policy_req = default_eval_policy_req()
    f = open(decov_path + '/data/economie_evaluate_one.csv', 'w')
    # header
    f.write('# File generated by Python test_economie_evaluate_one_write_csv(). '
            'Read by C++. This ensures evaluate_one() values match between Python and C++.\n')
    measure_names = ['red_' + m for m in all_measures().keys()]
    measure_names.extend(['green_' + m for m in all_measures().keys()])
    print(measure_names)
    f.write('{},c\n'.format(','.join(measure_names)))
    # values
    nlabels = len(eval_policy_req['zone_label_names'])
    values = [range(max + 1) for max in all_measures().values()] * nlabels
    assert 2 * 5 == len(values)
    eco = Economie()
    # iterations
    ncomb, ncomb_exp = 0, ((2 + 1) * (2 + 1) * (3 + 1) * (3 + 1) * (1 + 1)) ** 2
    policy = {'zones': eval_policy_req['zones'], 'zone_labels': eval_policy_req['zone_labels'],
              'contact_tracing': [0, 0], 'forced_telecommuting': [0, 0],
              'school_closures': [0, 0], 'social_containment_all': [0, 0],
              'total_containment_high_risk': [0, 0]}
    for ncomb, comb in enumerate(itertools.product(*values)):
        if ncomb % 5000 == 0:
            print('{}/{} done'.format(ncomb, ncomb_exp))
        policy['contact_tracing'] = [comb[0], comb[5]]
        policy['forced_telecommuting'] = [comb[1], comb[6]]
        policy['school_closures'] = [comb[2], comb[7]]
        policy['social_containment_all'] = [comb[3], comb[8]]
        policy['total_containment_high_risk'] = [comb[4], comb[9]]
        f.write('{},{},{},{},{},{},{},{},{},{},{}\n'.format(*comb, eco.evaluate_one(policy)))
    f.close()
    assert ncomb_exp == ncomb + 1  # start at 0


if __name__ == '__main__':
    # simple_test()
    evaluate_one_write_csv()
