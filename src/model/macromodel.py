# !/usr/bin/env python3
import time
import argparse
import json
import os
from copy import deepcopy
from http.server import BaseHTTPRequestHandler, HTTPServer

import numpy as np
import pandas as pd

from src.model.economie import Economie, decov_path
from src.model.model_seair_alizon import SEAIRModelAlizon, SEAIR_alizon_ratios2y0, measures2c_alizon
from src.model.model_seair_salje import SEAIRModelSaljeWithAge, measures2c_salje, get_contact_matrix, \
    SEAIR_salje_ratios2y0
from src.model.policy_utils import mklist


def error_dumps(errstr):
    print('ERROR:', errstr)
    return {'success': False, 'error': errstr}


def json2measure_days_c(model: str, eval_policy_req: dict, label_idx: int,
                        max_days: int, c_max_containment: float = 0.9):
    # compute measure days and estimate "c" according to the chosen data
    measure_days, myc = [], []
    # a few sanity checks - WARNING comment for performance
    # nweeks, nzones = len(eval_policy_req['measure_weeks']), len(eval_policy_req['zones'])
    # print('eval_policy_req:', eval_policy_req)
    # for m in all_measures():
    #     print('measure:', m)
    #     if eval_policy_req[m]:
    #         assert len(eval_policy_req[m]) == nweeks
    #         for week_idx, week_nb in enumerate(eval_policy_req['measure_weeks']):
    #             assert len(eval_policy_req[m][week_idx]['activated']) == nzones
    m2c = measures2c_alizon if model == "Alizon20" else measures2c_salje
    for step_idx, week_nb in enumerate(eval_policy_req['measure_weeks']):
        measure_days.append(7 * week_nb)
        # The '0 if not' part is to take in account that the field in data may not be available.
        tr = 0 if not eval_policy_req['contact_tracing'] else \
            eval_policy_req['contact_tracing'][step_idx]['activated'][label_idx]
        tc = 0 if not eval_policy_req['forced_telecommuting'] else \
            eval_policy_req['forced_telecommuting'][step_idx]['activated'][label_idx]
        sc = 0 if not eval_policy_req['school_closures'] else \
            eval_policy_req['school_closures'][step_idx]['activated'][label_idx]
        so = 0 if not eval_policy_req['social_containment_all'] else \
            eval_policy_req['social_containment_all'][step_idx]['activated'][label_idx]
        hr = 0 if not eval_policy_req['total_containment_high_risk'] else \
            eval_policy_req['total_containment_high_risk'][step_idx]['activated'][label_idx]
        c = m2c(contact_tracing=tr,
                forced_telecommuting=tc,
                school_closures=sc,
                social_containment_all=so,
                total_containment_high_risk=hr,
                c_max_containment=c_max_containment)
        myc.append(c)
    # ensure we start at week 0
    assert measure_days[0] == 0
    # add measure ends and measure lifting (c=0)
    measure_days.append(max_days)
    return measure_days, myc


def compute_model_parameters(model_parameters):
    """ compute model parameters
    """
    if "MinorInfectionDuration" in model_parameters:
        model_parameters["gamm1"] = 1. / model_parameters["MinorInfectionDuration"][1]
    if "MajorInfectionDuration" in model_parameters:
        model_parameters["gamm2"] = 1. / model_parameters["MajorInfectionDuration"][1]
    # Differente values in macro and micro for sigma and mild. Currently checked 20200424
    # model_parameters["sigma"] = 1. / model_parameters["AsymptomaticDuration"][1]
    # model_parameters["mild"] = 1 - ( model_parameters["AtRiskMajorInfectionRatio"] + model_parameters["OtherMajorInfectionRatio"] )


def get_compartments_tocheck(model_name):
    compartments = {"Alizon20": ["S", "E1", "A1", "I1", "R1", "E2", "A2", "I2", "R2", "M"],
                    "Salje20": ['S', 'E', 'A', 'I1', 'I2', 'R', 'M']}
    return compartments[model_name]


def add_Salje20_params(params):
    df = pd.read_json(decov_path + "/data/pasteur_data.json")

    P_I1_I2 = df["P(Host|Infected)"].values / 100
    # P_I2_C = df["P(ICU|Hosp)"].values / 100
    P_I2_D = df["P(Death|Hosp)"].values / 100

    # contacts_weekdays = get_contact_matrix("contact_matrix_weekday_all.csv")
    # contacts_weekend = get_contact_matrix("contact_matrix_weekend_all.csv")

    contact_matrix = get_contact_matrix(decov_path + "/data/contact_matrix_weekday_all.csv")

    params["contact"] = contact_matrix
    params["P_I1_I2"] = P_I1_I2
    # params["P_I2_C"] = P_I2_C
    params["P_I2_D"] = P_I2_D


def add_Salje20_age_pyramid(params, data_age, zone):
    da = data_age[zone]
    p_age = [da["0"] + da["5"] + da["10"] + da["15"],
             da["20"] + da["25"],
             da["30"] + da["35"],
             da["40"] + da["45"],
             da["50"] + da["55"],
             da["60"] + da["65"],
             da["70"] + da["75"],
             da["80"] + da["85"] + da["90"] + da["95"]]
    n_age = sum(p_age)
    params["p_age"] = np.array([p / n_age for p in p_age])
    return params["p_age"], n_age


class MacroModel:

    def __init__(self, economy_model, verbose=True):
        self._economy_model = economy_model
        self._verbose = verbose

    def eval_policy(self, eval_policy_req):
        """Evaluate the given policy, and returns eval_policy_res result as a dictionary"""
        # print('ModelRequestHandler::eval_policy()')
        # sanity checks
        if 'measure_weeks' not in eval_policy_req:
            return error_dumps('Missing weeks number for measures.')
        # check we use Alizon20 or Salje20
        if 'compartmental_model' not in eval_policy_req or eval_policy_req['compartmental_model'] not in ['Alizon20',
                                                                                                          "Salje20"]:
            return error_dumps('Comparmental model not valid, use Alizon20 or Salje20.')
        model_name = eval_policy_req['compartmental_model']
        compartments_name = 'compartments_' + model_name
        # print(eval_policy_req.keys())
        if compartments_name not in eval_policy_req:
            return error_dumps('Could not find compartments {} .'.format(compartments_name))
        if eval_policy_req[compartments_name] != get_compartments_tocheck(model_name):
            return error_dumps('Compartments not valid. Expected:' + str(get_compartments_tocheck(model_name)) +
                               'got:' + str(eval_policy_req[compartments_name]))
        # check zones are valid and that population_size has the same length
        if 'zones' not in eval_policy_req or not eval_policy_req['zones']:
            return error_dumps('No zones provided.')
        nzones = len(eval_policy_req['zones'])
        if len(eval_policy_req['population_size']) != nzones:
            return error_dumps('Zone and population_size lengths do not match.')
        if len(eval_policy_req['ICU_beds']) != nzones:
            return error_dumps('Zone and ICU_beds lengths do not match.')
        max_days = eval_policy_req['compartmental_model_parameters']['common']['max_days']
        nweeks = int(max_days / 7)
        cases, hospital_patients, ICU_patients, deaths, tests, activity = \
            mklist(nweeks), mklist(nweeks), mklist(nweeks), mklist(nweeks), mklist(nweeks), mklist(nweeks)
        ICU_ratio = eval_policy_req["compartmental_model_parameters"]["common"]["ICURatio"]
        zone_params_template = deepcopy(eval_policy_req['compartmental_model_parameters']["common"])
        param_key = 'macro_' + model_name
        zone_params_template.update(eval_policy_req['compartmental_model_parameters'][param_key])
        compute_model_parameters(zone_params_template)

        if model_name == "Alizon20":
            model = SEAIRModelAlizon(alizon20_parameters={}, y0=[], verbose=self._verbose)
        elif model_name == "Salje20":
            with open(decov_path + "/data/data_age_departement.json") as fp:
                data_age = json.load(fp)
            model = SEAIRModelSaljeWithAge(parameters={}, y0=[])

        # now create and use a SEAIR model per zone
        for zone_idx, zone_name in enumerate(eval_policy_req['zones']):
            # print(zone_name)
            npop = int(eval_policy_req['population_size'][zone_idx])
            # get initial ratios
            if zone_name not in eval_policy_req['initial_ratios_' + model_name]:
                return error_dumps('No initial ratio for zone ' + zone_name)
            # set ICU beds
            zone_params = deepcopy(zone_params_template)
            zone_params['i_sat'] = eval_policy_req['ICU_beds'][zone_idx]
            ratios = eval_policy_req['initial_ratios_' + model_name][zone_name]
            c_max_containment = zone_params["c_max_containment"]

            # create data and model
            label_idx = eval_policy_req['zone_labels'][zone_idx]
            measure_days, myc = json2measure_days_c(model_name, eval_policy_req=eval_policy_req, label_idx=label_idx,
                                                    max_days=max_days, c_max_containment=c_max_containment)
            if model_name == "Alizon20":
                if len(ratios) != 10:
                    return error_dumps('Incorrect initial ratio for zone ' + zone_name)
                y0 = SEAIR_alizon_ratios2y0(npop, *ratios)
                if not y0:
                    return error_dumps('SEAIR_ratios2y0() failed for zone ' + zone_name)
                #model = SEAIRModelAlizon(alizon20_parameters=zone_params, y0=y0, verbose=self._verbose)

            elif model_name == "Salje20":
                if len(ratios) != 7:
                    return error_dumps('Incorrect initial ratios for zone ' + zone_name)
                # issue #114, population size in data_departements.json  does not match pop size in optimize_req.json/eval_policy_req.json
                #p_age, npop = add_Salje20_age_pyramid(zone_params, data_age, zone_name)
                p_age, _ = add_Salje20_age_pyramid(zone_params, data_age, zone_name)
                y0 = np.asarray(ratios)
                if len(y0.shape) == 1:
                    # old version without age classes in initial_ratios
                    y0 = SEAIR_salje_ratios2y0(p_age, 1, *ratios)
                # print(npop * np.sum(y0, axis=0))
                if y0 is None:
                    return error_dumps('Modele Salje20 SEAIR_ratios2y0() failed for zone ' + zone_name)
                add_Salje20_params(zone_params)
                #model = SEAIRModelSaljeWithAge(zone_params, y0)

            model.update_y0(y0)
            model.update_params(zone_params)

            # use the SAIR model
            results = model.compute(c_list=myc, time_intervals=measure_days, rtol=1e-6, atol=1e-6, dt=7)
            # results = model.compute(c_list=myc, time_intervals=measure_days, rtol=1e-6, atol=1e-6, dt=7, version="python")
            # results *= npop
            if model_name == "Salje20":
                results = results.sum(axis=2)
            # append to JSON dumps
            for week in range(nweeks + 1):  # include week 0
                if model_name == "Alizon20":
                    #S, E1, A1, I1, R1, E2, A2, I2, R2, M = results[week * 7][:]  # unpack week results
                    S, E1, A1, I1, R1, E2, A2, I2, R2, M = results[week][:]  # unpack week results, dt=7
                elif model_name == "Salje20":
                    #S, E, A, I1, I2, R, M = npop * results[week * 7][:]  # unpack weeks results
                    S, E, A, I1, I2, R, M = npop * results[week][:]  # unpack weeks results, dt=7
                    # print(S, E, A, I1, I2, R, C, M)

                key = 'initial' if week == 0 else 'week'
                all_cases = I1 + I2
                cases[week][key].append(all_cases)
                deaths[week][key].append(M)

                # "We assume that 30% of those that are hospitalised will require critical care" [Alizon20]
                # + "we assume that 50% of those in critical care will die" [Ferguson20]
                # I2 = "personnes infectées nécessitant une hospitalisation" [Alizon20]
                # theta = taux de létalité des infections hospitalisées = 0.15 [Alizon20]
                # --> ICU_patients = 30% * I2
                hospital_patients[week][key].append(I2)
                ICU_patients[week][key].append(ICU_ratio * I2)
                tests[week][key].append(0)  # macromodel can't evaluate tests
                activity[week][key].append(0)  # macromodel cannot evaluate activity rate

        # use economics model
        gdp_accumulated_loss = self._economy_model.evaluate(eval_policy_req, nweeks)
        # finally prepare JSON dumps
        dumps = {'success': True,
                 'zones': eval_policy_req['zones'],
                 'compartmental_model': eval_policy_req['compartmental_model'],
                 'max_days': max_days,
                 'gdp_accumulated_loss': gdp_accumulated_loss,
                 'cases': cases,
                 'hospital_patients': hospital_patients,
                 'ICU_patients': ICU_patients,
                 'deaths': deaths,
                 'tests': tests,
                 'activity': activity}
        return dumps


class MacroModelRequestHandler(BaseHTTPRequestHandler):

    def eval_policy(self, eval_policy_req):
        macro_model = MacroModel(self.server.economy, verbose=self.server.verbose)
        return macro_model.eval_policy(eval_policy_req)

    # noinspection PyPep8Naming
    def do_POST(self):
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body.decode('utf-8'))
        self.send_response(200)
        self.end_headers()

        dumps = None
        if "eval_policy" in self.path:
            dumps = self.eval_policy(data)
        if not dumps:
            dumps = error_dumps('command ' + self.path + ' not recognized')
        self.wfile.write(json.dumps(dumps).encode())

    # disable logs "127.0.0.1 - - [12/Apr/2020 14:16:17] "POST /eval_policy HTTP/1.1" 200 -"
    def log_message(self, format_, *args):
        return


class MacroModelServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass, verbose=True):
        # stuff that needs to be stored here
        self.economy = Economie()
        self.verbose = verbose
        HTTPServer.__init__(self, server_address, RequestHandlerClass)


def start_server(Server, RequestHandler):
    host = 'localhost'
    port = 8000

    parser = argparse.ArgumentParser(description='Python ' + os.path.basename(__file__) + ' server.')
    parser.add_argument('--interface', type=str,
                        help='hostname(default=\'localhost\'):port(default=8000)')
    parser.add_argument('--singlerequest', action='store_true',
                        help='processes a single http request and terminates')
    parser.add_argument('--nbrequests', type=int,
                        help='processes a given number of http requests and terminates')
    args = parser.parse_args()

    if args.interface is not None:
        host = ':'.join(args.interface.split(':')[:-1])
        port = int(args.interface.split(':')[-1])

    verbose = True
    server = Server((host, port), RequestHandler, verbose=verbose)
    print('Starting server at http://%s:%d' % (host, port))

    if args.singlerequest:
        server.handle_request()
    elif args.nbrequests:
        for _ in range(args.nbrequests):
            server.handle_request()
    else:
        server.serve_forever()


if __name__ == '__main__':
    start_server(MacroModelServer, MacroModelRequestHandler)
