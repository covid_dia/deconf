#!/usr/bin/env python
# coding: utf-8

from copy import deepcopy

import numpy as np
from scipy.integrate import odeint


#import chart_studio.plotly as py


def set_odes_seir(y,t,p):
    """ Defines the system of differential equations describing the SEIR model 
    INPUT: p - named list of parameter values
    OUTPUT: list of derivatives of each variable
    """
    S, E0, E1, I0, I1, I2, I3, R, D = y
    #s = y[1]
    #E0 = y[2]
    #E1 = y[3]
    #I0 = y[4]
    #I1 = y[5]
    #I2 = y[6]
    #I3 = y[7]
    #R = y[8]
    #D = y[9]
    be = p["be"]
    a1 = p["a1"]
    f = p["f"]
    a0 = p["a0"]
    b0 = p["b0"]
    g0 = p["g0"]
    b1 = p["b1"]
    p1 = p["p1"]
    g1 = p["g1"]
    b2 = p["b2"]
    p2 = p["p2"]
    g2 = p["g2"]
    b3 = p["b3"]
    g3 = p["g3"]
    u = p["u"]
    
    
    dS_dt = -(be*E1+b0*I0+b1*I1+b2*I2+b3*I3)*S 
    dE0_dt = (be*E1+b0*I0+b1*I1+b2*I2+b3*I3)*S -a0*E0 
    dE1_dt = a0*E0-a1*E1
    dI0_dt = f*a1*E1-g0*I0
    dI1_dt = (1-f)*a1*E1-g1*I1-p1*I1
    dI2_dt = p1*I1-g2*I2-p2*I2
    dI3_dt = p2*I2-g3*I3-u*I3
    dR_dt = g0*I0+g1*I1+g2*I2+g3*I3
    dD_dt = u*I3
    
    return [dS_dt, dE0_dt, dE1_dt, dI0_dt, dI1_dt, dI2_dt, dI3_dt, dR_dt, dD_dt]


def get_model_params(input_params):
    """Function to take the parameters entered by the user and turn them into the rate parameters used by the model
    INPUT: input - structure containing all the user entered information
    OUTPUT: named list consisting of the population size N and another list of the model parameters, pModel
    """
  
    #Incubation period, days
    IncubPeriod = input_params["IncubPeriod"]  
    #Duration of mild infections, days
    DurMildInf = input_params["DurMildInf"] 
    #Fraction of infections that are severe
    FracSevere = input_params["FracSevere"]/100 
    #Fraction of infections that are critical
    FracCritical = input_params["FracCritical"]/100
    #Fraction of infections that are mild
    FracMild = 1-FracSevere-FracCritical  
    #Probability of dying given critical infection
    ProbDeath = input_params["ProbDeath"]  
    #Case fatality rate (fraction of infections resulting in death)
    CFR = ProbDeath*FracCritical/100 
    #Time from ICU admission to death, days
    TimeICUDeath = input_params["TimeICUDeath"] 
    #Duration of hospitalization, days
    DurHosp = input_params["DurHosp"] 

    N = input_params["N"]
  
    # The transmission rates are changed from values per time to values per capita per time
    b1 = input_params["b1"]/N
    b2 = input_params["b2"]/N
    b3 = input_params["b3"]/N

    #If asymptomatic infection is allowed
    if input_params["AllowAsym"]:
        #Fraction of all infections that are asymptomatic
        FracAsym = input_params["FracAsym"]/100 
        #Duration of asympatomatic infection
        DurAsym = input_params["DurAsym"] 
        b0 = input_params["b0"]/N
    else:
        #Fraction of all infections that are asymptomatic
        FracAsym = 0
        #Duration of asympatomatic infection
        DurAsym = 7 
        #Transmission rate (asymptomatic infections)
        b0 = 0 
    
    # If presymptomatic transmission is allowed
    if input_params["AllowPresym"]:
        #Length of infections phase of incubation period
        PresymPeriod = input_params["PresymPeriod"] 
        be = input_params["be"]/ N
    else:
        #Length of infectious phase of incubation period
        PresymPeriod = 0 
        #Transmission rate (pre-symptomatic)
        be = 0 
    
    pClin = {"IncubPeriod":IncubPeriod, 
             "DurMildInf": DurMildInf,
             "FracMild": FracMild, 
             "FracSevere": FracSevere,
             "FracCritical": FracCritical,
             "CFR": CFR,
             "TimeICUDeath": TimeICUDeath,
             "DurHosp": DurHosp,
             "FracAsym": FracAsym,
             "PresymPeriod": PresymPeriod,
             "DurAsym": DurAsym
            }

    # Turn these clinical parameters into the rate constants of the model
    pModel = get_params_seir(pClin)

    pModel["be"] = be
    pModel["b0"] = b0
    pModel["b1"] = b1
    pModel["b2"] = b2
    pModel["b3"] = b3 
    return pModel


def get_params_seir(pClin):
    """ Function to relate the clinical parameters entered by the user into the rate parameters used by the model
    INPUT: pClin - named list of the clinical parameters
    OUTPUT: named list of the model rate parameters, excluding the Betas
    """
    #presymptomatic period of transmission
    if pClin["PresymPeriod"] != 0:
        a1 = min(10**6, 1./pClin["PresymPeriod"])
    else:
        a1 = 10**6
    # true latent period, avoid infinity when no presymptomatic phase
    a0 = min(10**6, (pClin["IncubPeriod"]-pClin["PresymPeriod"])**(-1)) 
    
    f = pClin["FracAsym"]
    
    g0 = 1/pClin["DurAsym"]
    
    g1 = (1/pClin["DurMildInf"])*pClin["FracMild"]
    p1 = (1/pClin["DurMildInf"]) - g1
    
    p2 = (1/pClin["DurHosp"])*(pClin["FracCritical"]/(pClin["FracSevere"]+pClin["FracCritical"]))
    g2 = (1/pClin["DurHosp"])-p2
    
    if pClin["FracCritical"] == 0:
        u = 0
    else:
        u = (1/pClin["TimeICUDeath"])*(pClin["CFR"]/pClin["FracCritical"])
    
    
    g3 = (1/pClin["TimeICUDeath"]) - u
    
    return {"a0": a0,"a1": a1, "f": f, "g0": g0, "g1": g1, "g2": g2, "g3": g3, "p1": p1, "p2": p2, "u": u}


def set_hosp_capacity(input_params):
    """Function to determine the capacity for hospital beds and ICU beds based on total beds and availability,
    and to # get ventilator capacity
    INPUT: input - structure containing all the user entered information
    OUTPUT: named list consisting of all the healthcare capacity parameters
    """

    #Available hospital beds per 1000 ppl in US based on total beds and occupancy
    AvailHospBeds = input_params["HospBedper"] * (100-input_params["HospBedOcc"]*(1+input_params["IncFluOcc"]/100))/100 
    #Available ICU beds per 1000 ppl in US, based on total beds and occupancy. Only counts adult not neonatal/pediatric beds
    AvailICUBeds = input_params["ICUBedper"] *(100-input_params["ICUBedOcc"]*(1+input_params["IncFluOcc"]/100))/100 
    #Estimated excess # of patients who could be ventilated in US (per 1000 ppl) using conventional protocols
    ConvVentCap = input_params["ConvMVCap"]
    #Estimated excess # of patients who could be ventilated in US (per 1000 ppl) using contingency protocols
    ContVentCap = input_params["ContMVCap"]
    #Estimated excess # of patients who could be ventilated in US (per 1000 ppl) using crisis protocols
    CrisisVentCap = input_params["CrisisMVCap"] 

    capParams = {"AvailHospBeds": AvailHospBeds,
                 "AvailICUBeds": AvailICUBeds,
                 "ConvVentCap": ConvVentCap,
                 "ContVentCap": ContVentCap,
                 "CrisisVentCap": CrisisVentCap
                }

    return capParams



def default_parameters():
    """ the default parameters for Hill's model
    """
    # default parameters
    parameters = {"IncubPeriod": 5, #Duration of incubation period
                       "DurMildInf": 6, #Duration of mild infections
                       "FracSevere": 15, #% of symptomatic infections that are severe
                       "FracCritical": 5, #% of symptomatic infections that are critical
                       "ProbDeath": 40, #Death rate for critical infections
                       "DurHosp": 5, #Duration of severe infection/hospitalization
                       "TimeICUDeath": 8, #Duration critical infection/ICU stay
                       "FracAsym": 25,  #Fraction of all infections that are asymptomatic
                       "PresymPeriod": 2, #Length of infectious phase of incubation period 
                       "DurAsym": 6, #Duration of asympatomatic infection
                       "be": 0.5, #Transmission rate (pre-symptomatic)
                       "b0": 0.5, #Transmission rate (asymptomatic infections)
                       "b1": 0.5, #Transmission rate (mild infections)
                       "b2": 0.1, #Transmission rate (severe infections)
                       "b3": 0.1, #Transmission rate (critical infections)
                       "N": 1000,
                       "Tmax": 300,
                       "InitInf": 1,
                       "AllowPresym": False,
                       "AllowAsym": False,
                       "VarShowInt": "S",
                       "Tint": 30,
                       "Tend": 300,
                       "s0": 0, #Reduction in transmission from asymptomatic/presymptomatic infections 
                       "s1": 30, #Reduction in transmission from mild infections
                       "s2": 0, #Reduction in transmission from severe infections
                       "s3": 0, #Reduction in transmission rate from critical infections
                       "c": [0., 0.6], # TODO currently c is not used, the policies are applied using s0, s1, s2, s3
                       "time_intervals": [0, 30, 300],
                       "RoundOne": True
        }
    return parameters

class SEIIIRModel:
    def __init__(self, hill2020_parameters):
        self.parameters = deepcopy(hill2020_parameters)
        assert len(hill2020_parameters["time_intervals"]) - 1 == len(hill2020_parameters["c"])

        E00 = self.parameters["InitInf"]
        S0 = self.parameters["N"] - E00
        self.parameters["y0"] = [S0, E00, 0, 0, 0, 0, 0, 0, 0]

        self.pModel = get_model_params(self.parameters)

        # intervention parameters
        self.pModelInt = deepcopy(self.pModel)
        self.pModelInt["be"] = self.pModelInt["be"] * (1 - self.parameters["s0"]/100)
        self.pModelInt["b0"] = self.pModelInt["b0"] * (1 - self.parameters["s0"]/100)
        self.pModelInt["b1"] = self.pModelInt["b1"] * (1 - self.parameters["s1"]/100)
        self.pModelInt["b2"] = self.pModelInt["b2"] * (1 - self.parameters["s2"]/100)
        self.pModelInt["b3"] = self.pModelInt["b3"] * (1 - self.parameters["s3"]/100)



    def compute(self, c, time_intervals, rtol: float, atol: float):
        """ compute the seair model given parameters
        :return: n days x [S, E0, E1, I0, I1, I2, I3, R, M]
        """
        # simulation with intervention

        myc = c if c is not None else self.parameters["c"]
        myt = time_intervals if time_intervals is not None else self.parameters["time_intervals"]
        results = []
        for i in range(len(myt) - 1):
            # ode solver extra parameters dictionary
            if myc[i] == 0:
                params = deepcopy(self.pModel)
            else:
                params = deepcopy(self.pModelInt)

            params["c"] = myc[i]
        
            beg = myt[i]
            end = myt[i + 1] + 1

            # Resolution ODEs sans contrôle
            if len(results) == 0:
                y0 = self.parameters["y0"]  # [self.S0, 0, 0, 0.8 * self.I0, 0, 0, 0, 0.2 * self.I0, 0, 0]
            else:
                beg += 1
                y0 = results_interv[-1]

            results_interv = odeint(set_odes_seir, y0=y0, t=range(beg, end), args=(params,), 
                                    rtol=rtol, atol=atol, mxstep=5000)

            results.append(results_interv)

        results = np.vstack(results)

        # n days x [S, E0, E1, I0, I1, I2, I3, R, M]
        return results

if __name__ == "__main__":
    model = SEIIIRModel(default_parameters())
    output = model.compute()