import string
from copy import deepcopy

import numpy as np
from gym import Env, spaces

from src.model.economie import Economie
from src.model.model_seair_alizon import SEAIRModelAlizon as SEAIRModel, \
    SEAIR_alizon_ratios2y0_throws as SEAIR_ratios2y0_throws, measures2c_alizon
from src.model.policy_utils import all_measures

NORMALIZE_ICU = True


class Zone:
    def __init__(self, optimize_req, index):
        self.name: string = optimize_req['zones'][index]
        self.label: int = optimize_req['zone_labels'][index]
        self.population: int = int(optimize_req['population_size'][index])
        self.icu_beds: int = optimize_req['ICU_beds'][index]
        # retrieve parameters
        model_name = optimize_req['compartmental_model']
        model_type = 'macro_' + model_name
        self.alizon_params: dict = deepcopy(optimize_req['compartmental_model_parameters']['common'])
        self.alizon_params.update(optimize_req['compartmental_model_parameters'][model_type])
        self.alizon_params['i_sat'] = optimize_req['ICU_beds'][index]
        self.icu_ratio = self.alizon_params['ICURatio']
        self.accumulated_ICU_saturation_ratio: float = 0.

        # get SEAIR initial ratios [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
        ratios: dict = optimize_req['initial_ratios_' + model_name][self.name]

        # y0=[S, E1, A1, I1, R1, E2, A2, I2, R2, M] (idem y and yprev)
        #     0  1   2   3   4   5   6   7   8   9
        # careful, y0, y, yprev not normalized : 1 = 1 person
        self.y0: list = SEAIR_ratios2y0_throws(self.population, *ratios)
        self.y: list = deepcopy(self.y0)
        self.yprev: list = deepcopy(self.y0)
        self.model = SEAIRModel(alizon20_parameters=self.alizon_params, y0=self.y0)
        self.nsum: int = 0

    def model_compute(self, c_list, time_intervals):
        # same rtol and atol values as macromodel.py
        results = self.model.compute(c_list=c_list, time_intervals=time_intervals, rtol=1e-3, atol=1e-3, dt=1)
        # results = n days x [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
        self.y = results[-1].tolist()  # y0 must be a list not a numpy array
        self.model.update_y0(self.y)
        # compute ICU_saturation_ratio

        for idx, seair in enumerate(results):
            # only store saturation once a week,
            # and do not store for last day, as it will be stored on next call of model_compute()
            if idx % 7 != 0 or idx == len(results) - 1:
                continue
            self.nsum += 1
            icu_patients = self.icu_ratio * seair[7]  # I2
            # print('z:', self.name, icu_patients, self.icu_beds)
            if icu_patients > self.icu_beds:
                self.accumulated_ICU_saturation_ratio += (icu_patients - self.icu_beds) / self.icu_beds

    def dead_nb(self):
        return self.y[9]  # M

    def initial_dead_nb(self):
        return self.y0[9]  # M


class MacroRLEnv(Env):
    per_label_nobs = 14  # number of observations when per label
    per_zone_nobs = 10  # number of observations when per zone

    def __init__(self, optimize_req: dict,
                 per_zone_observation: bool = False):
        self._optimize_req = optimize_req
        self._per_zone_observation: bool = per_zone_observation
        self._lambda_death_ratio: float = 1
        self._lambda_ICU_saturation_ratio: float = 1
        self._lambda_activated_measures_ratio: float = 1
        self._lambda_gdp_accumulated_loss_ratio: float = 1
        self._lambda_color_consistency: float = 1
        self._step_duration_days = self._optimize_req['measure_step_duration'] * 7
        self._max_measure_steps = self._optimize_req['max_measure_steps']
        model_params: dict = self._optimize_req['compartmental_model_parameters']['common']
        self._num_ratios = len(self._optimize_req['compartments_Alizon20'])
        self._max_days = model_params['max_days']
        self._hrvcr = model_params['high_risk_voluntary_containment_rate']

        # ensure the model ends after the measures are over
        assert self._max_days >= self._max_measure_steps * self._step_duration_days

        # determine number of possible measures for each of the 5 measures
        self._num_measures = []
        for m in all_measures().keys():
            if m in optimize_req['max_measure_values'] and optimize_req['max_measure_values'][m] is not None:
                self._num_measures.append(1 + optimize_req['max_measure_values'][m])
            else:
                self._num_measures.append(1)
        self._labels: list = self._optimize_req['zone_label_names']
        self._num_labels: int = len(self._labels)
        self._action_list: list = self._num_measures * self._num_labels

        # Create Eco model
        self._eco: Economie = Economie()

        # set observation and action space
        # self._optimize_req['uniform_measures'] : not used anymore
        self.action_space = spaces.MultiDiscrete(self._action_list)
        # check_env(): UserWarning: Your observation has an unconventional shape
        # (neither an image, nor a 1D vector).
        # We recommend you to flatten the observation to have only a 1D vector
        if self._per_zone_observation:
            self._observation_shape = (4 * len(optimize_req['zones']) * self._num_ratios,)
        else:
            self._observation_shape = (self._num_labels * MacroRLEnv.per_label_nobs,)
        self.observation_space = spaces.Box(low=-1, high=1,
                                            shape=self._observation_shape, dtype=np.float64)

        # below comes stuff that will set in reset()
        self._elapsed_days: int = 0
        self.current_step: int = 0
        self._total_population: int = 0
        self._total_label_population: list = []
        self._nzones: int = len(optimize_req['zones'])
        self._zones: list = []
        self._c_max_containment: float = 0.8
        self.info = {}
        self._prev_death_ratio: float = 0
        self._prev_activated_measures_ratio: float = 0
        self._prev_gdp_accumulated_loss_ratio: float = 0
        self._prev_ICU_saturation_ratio: float = 0
        self.reset()
        if NORMALIZE_ICU:
            self._max_values, self._min_values = self.get_min_max_values()

    def get_min_max_values(self):
        c_deconf = measures2c_alizon(contact_tracing=0,
                                     forced_telecommuting=0,
                                     school_closures=0,
                                     social_containment_all=0,
                                     total_containment_high_risk=0,
                                     c_max_containment=self._c_max_containment)

        c_no_deconf = measures2c_alizon(contact_tracing=self._optimize_req['max_measure_values']["contact_tracing"],
                                        forced_telecommuting=self._optimize_req['max_measure_values'][
                                            "forced_telecommuting"],
                                        school_closures=self._optimize_req['max_measure_values']["school_closures"],
                                        social_containment_all=self._optimize_req['max_measure_values'][
                                            "social_containment_all"],
                                        total_containment_high_risk=self._optimize_req['max_measure_values'][
                                            "total_containment_high_risk"],
                                        c_max_containment=self._c_max_containment)

        c_list_deconf = [c_deconf]
        c_list_no_deconf = [c_no_deconf]

        result_list_deconf = [[] for _ in range(len(self._zones))]
        result_list_no_deconf = [[] for _ in range(len(self._zones))]

        time_intervals = [0, self._step_duration_days]
        time_intervals_end = [0, self._max_days - self._max_measure_steps * self._step_duration_days]
        for i in range(self._max_measure_steps + 1):

            for z, zone in enumerate(self._zones):
                if i < self._max_measure_steps:
                    zone.model_compute(c_list=c_list_deconf, time_intervals=time_intervals)
                else:
                    zone.model_compute(c_list=c_list_deconf, time_intervals=time_intervals_end)
                result_list_deconf[z].append(zone.accumulated_ICU_saturation_ratio)
        self.reset()

        for i in range(self._max_measure_steps + 1):

            for z, zone in enumerate(self._zones):
                if i < self._max_measure_steps:
                    zone.model_compute(c_list=c_list_no_deconf, time_intervals=time_intervals)
                else:
                    zone.model_compute(c_list=c_list_deconf, time_intervals=time_intervals_end)
                result_list_no_deconf[z].append(zone.accumulated_ICU_saturation_ratio)
        self.reset()
        for i in range(len(result_list_deconf)):
            for j in range(len(result_list_deconf[i])):
                assert (result_list_deconf[i][j] >= result_list_no_deconf[i][j])

        return result_list_deconf, result_list_no_deconf

    def set_weights(self,
                    lambda_activated_measures_ratio: float = 1,
                    lambda_death_ratio: float = 1,
                    lambda_gdp_accumulated_loss_ratio: float = 1,
                    lambda_ICU_saturation_ratio: float = 1,
                    lambda_color_consistency: float = 1):
        self._lambda_death_ratio = lambda_death_ratio
        self._lambda_ICU_saturation_ratio = lambda_ICU_saturation_ratio
        self._lambda_activated_measures_ratio = lambda_activated_measures_ratio
        self._lambda_gdp_accumulated_loss_ratio = lambda_gdp_accumulated_loss_ratio
        self._lambda_color_consistency = lambda_color_consistency

    def reset(self):
        self._elapsed_days = 0
        self.current_step = 0
        # reset rewards
        self._prev_death_ratio = 0
        self._prev_activated_measures_ratio = 0
        self._prev_gdp_accumulated_loss_ratio = 0
        self._prev_ICU_saturation_ratio = 0

        # Create a SEAIR model per zone
        self._zones = [Zone(self._optimize_req, i) for i in range(self._nzones)]
        self._c_max_containment = self._zones[0].alizon_params['c_max_containment']
        # Compute a bound to normalize the population of regions
        self._total_population = sum([z.population for z in self._zones])
        self._total_label_population = [0] * self._num_labels
        for z in self._zones:
            self._total_label_population[z.label] += z.population
        self._build_observation()
        return self._obs

    def _build_observation(self):
        if self._per_zone_observation:
            return self._build_per_zone_observation()

        # https://gitlab.com/covid_dia/deconf/-/blob/8009a093e507be610d47007ea61e96a41e2cfc58/src/model/macro_rl_env.py
        self._obs = np.zeros(shape=self._observation_shape, dtype=np.float64)
        for zone in self._zones:
            label = zone.label
            obs_offset = MacroRLEnv.per_label_nobs * label
            s, e1, a1, i1, r1, e2, a2, i2, r2, m = zone.y
            sp, e1p, a1p, i1p, r1p, e2p, a2p, i2p, r2p, mp = zone.yprev

            # Diff SEAIR observations first
            self._obs[obs_offset + 0] += s - sp
            self._obs[obs_offset + 1] += e1 + e2 - e1p - e2p
            self._obs[obs_offset + 2] += a1 + a2 - a1p - a2p
            self._obs[obs_offset + 3] += i1 - i1p
            self._obs[obs_offset + 4] += i2 - i2p
            self._obs[obs_offset + 5] += r1 + r2 - r1p - r2p
            self._obs[obs_offset + 6] += m - mp

            zone.yprev = zone.y

            # Then SEAIR values
            self._obs[obs_offset + 7] += s
            self._obs[obs_offset + 8] += e1 + e2
            self._obs[obs_offset + 9] += a1 + a2
            self._obs[obs_offset + 10] += i1
            self._obs[obs_offset + 11] += i2
            self._obs[obs_offset + 12] += r1 + r2
            self._obs[obs_offset + 13] += m

        # normalize by the total population for each label
        for label_idx in range(self._num_labels):
            self._obs[label_idx * MacroRLEnv.per_label_nobs:(label_idx + 1) * MacroRLEnv.per_label_nobs] /= \
                self._total_label_population[label_idx]

    def _build_per_zone_observation(self):
        diff_values = np.zeros((self._nzones, self._num_ratios))
        values = np.zeros((self._nzones, self._num_ratios))
        for z, zone in enumerate(self._zones):
            s, e1, a1, i1, r1, e2, a2, i2, r2, m = zone.y
            sp, e1p, a1p, i1p, r1p, e2p, a2p, i2p, r2p, mp = zone.yprev

            # SEAIR observations

            values[z, 0] = s
            values[z, 1] = e1
            values[z, 2] = e2
            values[z, 3] = a1
            values[z, 4] = a2
            values[z, 5] = i1
            values[z, 6] = i2
            values[z, 7] = r1
            values[z, 8] = r2
            values[z, 9] = m

            values[z] /= zone.population

            # Diff SEAIR observations

            diff_values[z, 0] = s - sp
            diff_values[z, 1] = e1 - e1p
            diff_values[z, 2] = e2 - e2p
            diff_values[z, 3] = a1 - a1p
            diff_values[z, 4] = a2 - a2p
            diff_values[z, 5] = i1 - i1p
            diff_values[z, 6] = i2 - i2p
            diff_values[z, 7] = r1 - r1p
            diff_values[z, 8] = r2 - r2p
            diff_values[z, 9] = m - mp

            diff_values[z] /= zone.population

            zone.yprev = zone.y

        brut_mean_axis_0 = np.mean(values, axis=0)
        brut_mean_axis_1 = np.mean(values, axis=1)
        brut_std_axis_0 = np.std(values, axis=0)
        brut_std_axis_1 = np.std(values, axis=1)
        diff_mean_axis_0 = np.mean(diff_values, axis=0)
        diff_mean_axis_1 = np.mean(diff_values, axis=1)
        diff_std_axis_0 = np.std(diff_values, axis=0)
        diff_std_axis_1 = np.std(diff_values, axis=1)

        obs = np.zeros((4, self._nzones, MacroRLEnv.per_zone_nobs))
        for i in range(self._nzones):
            obs[0][i] = (values[i] - brut_mean_axis_0) / (1e-9 + brut_std_axis_0)
            obs[1][i] = (values[i] - brut_mean_axis_1[i]) / (1e-9 + brut_std_axis_1[i])
            obs[2][i] = (diff_values[i] - diff_mean_axis_0) / (1e-9 + diff_std_axis_0)
            obs[3][i] = (diff_values[i] - diff_mean_axis_1[i]) / (1e-9 + diff_std_axis_1[i])

        obs = np.tanh(np.sign(obs) * np.log(1 + np.abs(obs)))
        self._obs = obs.flatten()
        assert self._obs.shape == self._observation_shape

    def step(self, action: list):
        """
        action: should be of size 10: 5 measures x 2 labels (red, green)
        """
        assert self._num_labels == 2  # red, green
        assert len(action) == 5 * self._num_labels
        c_red, c_green = 0, 0  # no control by default
        time_intervals = [0, self._max_days - self._elapsed_days]  # go till the end of 2 years
        # take actions into account if we have made less steps than max_measure_steps
        if self.current_step < self._max_measure_steps:
            c_red = measures2c_alizon(contact_tracing=action[0],
                                      forced_telecommuting=action[1],
                                      school_closures=action[2],
                                      social_containment_all=action[3],
                                      total_containment_high_risk=action[4],
                                      c_max_containment=self._c_max_containment)
            c_green = measures2c_alizon(contact_tracing=action[5],
                                        forced_telecommuting=action[6],
                                        school_closures=action[7],
                                        social_containment_all=action[8],
                                        total_containment_high_risk=action[9],
                                        c_max_containment=self._c_max_containment)
            time_intervals = [0, self._step_duration_days]

        for zone in self._zones:
            c_list = [c_red] if zone.label == 0 else [c_green]
            zone.model_compute(c_list=c_list, time_intervals=time_intervals)
        self._build_observation()
        self.current_step += 1
        self._elapsed_days += time_intervals[-1]

        # compute 4 objectives:
        # death rate, icu saturation, activation ratio, gdp_accumulated_loss_ratio - cf objectives.py
        # balance factors to have a relevant reward (may depend on model and objectives)
        color_consistency = self.color_consistency(action)
        activated_measures_ratio = self.activated_measures_ratio(action)
        death_ratio = self.death_ratio()
        gdp_accumulated_loss_ratio = self.gdp_accumulated_loss_ratio(action)
        ICU_saturation_ratio = self.ICU_saturation_ratio()
        # compute first order difference
        activated_measures_ratio_diff = activated_measures_ratio - self._prev_activated_measures_ratio
        death_ratio_diff = death_ratio - self._prev_death_ratio
        gdp_accumulated_loss_ratio_diff = gdp_accumulated_loss_ratio - self._prev_gdp_accumulated_loss_ratio
        ICU_saturation_ratio_diff = ICU_saturation_ratio - self._prev_ICU_saturation_ratio
        # now store previous values
        self._prev_activated_measures_ratio = activated_measures_ratio
        self._prev_death_ratio = death_ratio
        self._prev_gdp_accumulated_loss_ratio = gdp_accumulated_loss_ratio
        self._prev_ICU_saturation_ratio = ICU_saturation_ratio

        reward = 0
        reward += self._lambda_activated_measures_ratio * activated_measures_ratio_diff
        reward += self._lambda_death_ratio * death_ratio_diff
        reward += 50 * self._lambda_gdp_accumulated_loss_ratio * gdp_accumulated_loss_ratio_diff
        reward += self._lambda_ICU_saturation_ratio * ICU_saturation_ratio_diff
        reward += self._lambda_color_consistency * color_consistency
        reward *= -1  # all these ratios should be minimized

        done = self._elapsed_days >= self._max_days
        self.info = {'done': done,
                     'action': action,
                     'reward': reward,
                     'activated_measures_ratio': activated_measures_ratio,
                     'death_ratio': death_ratio,
                     'gdp_accumulated_loss_ratio': gdp_accumulated_loss_ratio,
                     'ICU_saturation_ratio': ICU_saturation_ratio,
                     'consistency_check': color_consistency,
                     'inactivity': 0  # not handled by macromodel
                     }
        return self._obs, reward, done, self.info

    def render(self, mode='human'):
        pass

    def elapsed_days(self):
        return self._elapsed_days

    def death_ratio(self):
        """ :return: percents, > 0 """
        deaths = [z.dead_nb() - z.initial_dead_nb() for z in self._zones]
        return sum(deaths) / self._total_population

    def ICU_saturation_ratio(self):
        """ :return: percents, > 0 """
        excess = sum([z.accumulated_ICU_saturation_ratio for z in self._zones])
        if NORMALIZE_ICU:
            if self._elapsed_days == self._max_days:
                nsteps = self._max_measure_steps
            else:
                nsteps = (self._elapsed_days // self._step_duration_days) - 1
            excess_max = sum([x[nsteps] for x in self._max_values])
            excess_min = sum([x[nsteps] for x in self._min_values])

            if excess_max == excess_min:
                return 0

            return (excess - excess_min) / (excess_max - excess_min)
        else:
            nweeks = self._elapsed_days // 7
            return excess / (nweeks * self._nzones)

    def activated_measures_ratio(self, action):
        """ :return: sum of measures normalized by their max allowed values, > 0 """
        rate: float = 0
        nactivable_measures: int = 0
        for idx, num_actions in enumerate(self._action_list):
            if num_actions > 1:
                rate += action[idx] / (num_actions - 1)
                nactivable_measures += 1
            else:
                assert action[idx] == 0
        return rate / nactivable_measures

    def gdp_accumulated_loss_ratio(self, action):
        """ :return: rate, in [0..1] """
        policy = {'zones': self._optimize_req['zones'],
                  'zone_labels': self._optimize_req['zone_labels'],
                  'contact_tracing': [action[0], action[5]],
                  'forced_telecommuting': [action[1], action[6]],
                  'school_closures': [action[2], action[7]],
                  'social_containment_all': [action[3], action[8]],
                  'total_containment_high_risk': [action[4], action[9]]}
        # objectives::gdp_accumulated_loss_ratio() makes a "/ 100"
        return self._step_duration_days // 7 * self._eco.evaluate_one(policy, self._hrvcr) / 100

    def get_info(self):
        return self.info

    def color_consistency(self, action):
        """
        Filter checking whether the input policy respects the color strictness.
        :param action: input action.
        :return int : consistency_penality . the number of inconsistent actions
        """
        consistency_penality = 0
        for k in range(len(self._num_measures)):
            if action[k] > action[k + len(self._num_measures)]:
                consistency_penality += 1
        return consistency_penality
