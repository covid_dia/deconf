import json
import pathlib
import subprocess
from copy import deepcopy

decov_path = str(pathlib.Path(__file__).parent.parent.parent.absolute())

def all_measures() -> dict:
    """ :return: all implemented measures names in alphabetical order,
                and the associated max values """
    return {'contact_tracing': 2,
            'forced_telecommuting': 2,
            'school_closures': 3,
            'social_containment_all': 3,
            'total_containment_high_risk': 1}


def set_all_measures_max_values(data, values):
    for m, v in zip(all_measures().keys(), values):
        data['max_measure_values'][m] = v


def all_measures_default_values():
    return [1, 1, 2, 2, 1]


def set_all_measures_activable(data):
    set_all_measures_max_values(data, all_measures_default_values())


def set_all_measures_not_activable(data):
    set_all_measures_max_values(data, [0] * len(all_measures().keys()))


def all_objectives() -> list:
    """ :return: all implemented objectives """
    return ['activated_measures_ratio', 'death_ratio', 'gdp_accumulated_loss_ratio',
            'ICU_saturation_ratio', 'inactivity']


def set_all_objectives_on(optimize_req: dict):
    optimize_req['objectives'] = all_objectives()


def default_eval_policy_req():
    with open(decov_path + '/doc/eval_policy_req.json', 'rb') as json_data:
        return json.load(json_data)


def default_optimize_req():
    with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
        return json.load(json_data)


def lookup(req, key, specific_table):
    """ look a parameter in a eval_policy or optimize request """
    if key in req['compartmental_model_parameters'][specific_table]:
        return req['compartmental_model_parameters'][specific_table][key]
    return req['compartmental_model_parameters']['common'][key]


def set_n_zones(data, nzones):
    model_name = data["compartmental_model"]
    data['zones'] = data['zones'][:nzones]
    data['zone_labels'] = data['zone_labels'][:nzones]
    data['population_size'] = data['population_size'][:nzones]
    data['ICU_beds'] = data['ICU_beds'][:nzones]
    cropped_initial_ratios = {}
    for i in range(nzones):
        cropped_initial_ratios[data['zones'][i]] = data['initial_ratios_' + model_name][data['zones'][i]]
    data['initial_ratios_' + model_name] = cropped_initial_ratios


def set_one_zone(data):
    set_n_zones(data, 1)


def set_n_colors(data, ncolors):
    data['zone_label_names'] = data['zone_label_names'][:ncolors]
    data['zone_labels'] = [v if v < ncolors else 0 for v in data['zone_labels']]


def set_n_measures(data, n_measures):
    default_values = all_measures_default_values()
    counter = 0
    for m, v in zip(all_measures().keys(), default_values):
        if counter < n_measures:
            data['max_measure_values'][m] = v
            counter += 1
        else:
            data['max_measure_values'][m] = 0


def policy2measure_vec(eval_policy_req, measure):
    if eval_policy_req[measure] is None:
        return None
    return [eval_policy_req[measure][week_idx]['activated'] for week_idx in range(len(eval_policy_req[measure]))]


def measure_vec2policy(vec):
    return [{'activated': v} for v in vec]


def mklist(nweeks):
    ans = [{"initial": []}]
    for _ in range(nweeks):
        ans.append({"week": []})
    return ans


def pretty_write(jsondict, outfile):
    with open(outfile, 'w') as f:
        json.dump(jsondict, f, indent=2)
    # remove line break between '[' or ',' and a number
    args = 'sed -i -Ez "s/([\\[,])\\n[ ]*([0-9])/\\1 \\2/g" {}'.format(outfile)
    subprocess.call(args, shell=True)
    # remove line break between a number and ']'
    args = 'sed -i -Ez "s/([0-9])\\n[ ]*\\]/\\1 \\]/g" {}'.format(outfile)
    subprocess.call(args, shell=True)

    # remove line break between '[' and a string delimited by '"'
    args = 'sed -i -Ez "s/\\[\\n[ ]*\\"/\\[ \\"/g" {}'.format(outfile)
    subprocess.call(args, shell=True)
    # remove line break between two strings delimited by '"'
    args = 'sed -i -Ez "s/\\",\\n[ ]*\\"/\\", \\"/g" {}'.format(outfile)
    subprocess.call(args, shell=True)
    # remove line break between a string delimited by '"' and ']'
    args = 'sed -i -Ez "s/\\"\\n[ ]*\\]/\\" \\]/g" {}'.format(outfile)
    subprocess.call(args, shell=True)


def pretty_string(json):
    outfile = '/tmp/pretty_string.json'
    pretty_write(json, outfile)
    with open(outfile, 'r') as fin:
        return fin.read()


def simplify_policy_eval_req_except_last_week(policy):
    if policy is None or not isinstance(policy, dict) or 'measure_weeks' not in policy.keys():
        return policy
    list_measures = []
    measure_weeks = policy['measure_weeks']
    if not measure_weeks:
        return policy
    new_measure_weeks = []
    for k in policy.keys():
        if isinstance(policy[k], list) and len(policy[k]) == len(measure_weeks) and isinstance(policy[k][0],
                                                                                               dict) and "activated" in \
                policy[k][0].keys():
            list_measures.append(k)
    previous_element = None
    for i in range(len(measure_weeks)):
        new_element = [policy[k][i]["activated"] for k in list_measures]
        if previous_element == new_element and i < len(measure_weeks) - 1:
            continue
        previous_element = new_element
        new_measure_weeks.append(measure_weeks[i])
    new_policy = deepcopy(policy)
    for k in list_measures:
        l = []
        for i in range(len(measure_weeks)):
            if measure_weeks[i] in new_measure_weeks:
                l.append(policy[k][i])
        new_policy[k] = l
    new_policy["measure_weeks"] = new_measure_weeks
    return new_policy
