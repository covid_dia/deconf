# -*- coding: utf-8 -*-
"""
Epidemiologic model adapted from Samuel Alizon report: http://alizon.ouvaton.org/Rapport3_Modele.html

The model is an SEAIR with differential severity:

S -> E1 -> A1 -> I1 -> R1
  -> E2 -> A2 -> I2 -> R2
                 I2 -> D

with E1 (E2): exposed with minor infection (major infection)
     A1 (A2): asymptomatic with minor infection (major infection)
     I1 (I2): people with minor infectious (major infection, ie hospitalized)
     R1 (R2): recovered people from minor infection (major infection)
     D      : deceased people


Age compartiments are not implemented in this model.


"""
import string
from copy import deepcopy
import numpy as np
import alizonpp
from scipy.integrate import odeint, solve_ivp


def mu_func(x1, x2, x3, diff=False):
    """ fonction de mortalité liée à la saturation des hôpitaux
    """
    if diff:  # même fonction, mais (quasi) différentiable
        if x2 > x3 + 30:
            return x1
        elif x2 < x3 - 30:
            return 0
        else:
            return x1 / (1 + np.exp(-5 * (x2 - x3)))
    return x1 * (x2 > x3)  # 0 morts tant que l'hosto n'es pas saturé.


def covid_ode_alizon(t, y, params):
    # fonction pour le système d'ODE lui-même
    # S=y[1] ; E1=y[2] ; A1=y[3] ; I1=y[4] ; R1=y[5] ;
    # E2=y[6] ; A2=y[7] ; I2=y[8] ; R2=y[9] ; M=y[10]

    # Lambda <- (1 - c) * Beta * (y[3] + y[7] + y[4] + bb * y[8])
    Lambda = (1 - params["c"]) * params["beta"] * (y[2] + y[6] + y[3] + params["bb"] * y[7])
    # Mu <- Mu_func(mort_sup, y[8],i.sat)
    if 'diff_mu' in params:
        diff_mu = params['diff_mu']
    else:
        diff_mu = False
    Mu = mu_func(params["mort_sup"], y[7], params["i_sat"], diff_mu)
    # Alpha <- Virulence + Mu_func(vir_sup, y[8],i.sat)
    Alpha = params["virulence"] + mu_func(params["vir_sup"], y[7], params["i_sat"], diff_mu)
    # Unlike in some other programming languages, when you use negative numbers for indexing in R,
    # it doesn’t mean to index backward from the end
    # Instead, it means to drop the element at that index, counting the usual way, from the beginning.
    # N_tot <- sum(y[-10]) = sum of all elements minus M
    N_tot = sum(y) - y[9]
    # dS <- - Lambda * y[1] - Mu * y[1]
    dS = - Lambda * y[0] - Mu * y[0]
    # dE1 <- Mild * Lambda * y[1] - Epsilon * y[2] + Mild * Nu - Mu * y[2]
    dE1 = params["mild"] * Lambda * y[0] - params["epsilon"] * y[1] + params["mild"] * params["nu"] - Mu * y[1]
    # dA1 <- Epsilon * y[2] - Sigma * y[3] - Mu * y[3]
    dA1 = params["epsilon"] * y[1] - params["sigma"] * y[2] - Mu * y[2]
    # dI1 <- Sigma * y[3] - (Gamm1 + Mu) * y[4]
    dI1 = params["sigma"] * y[2] - (params["gamm1"] + Mu) * y[3]
    # dR1 <- Gamm1 * y[4] - Mu * y[5]
    dR1 = params["gamm1"] * y[3] - Mu * y[4]
    # dE2 <- (1 - Mild) * Lambda * y[1] - Epsilon * y[6] + (1 - Mild) * Nu - Mu * y[6]
    dE2 = (1 - params["mild"]) * Lambda * y[0] - params["epsilon"] * y[5] \
          + (1 - params["mild"]) * params["nu"] - Mu * y[5]
    # dA2 <- Epsilon * y[6] - Sigma * y[7] - Mu * y[7]
    dA2 = params["epsilon"] * y[5] - params["sigma"] * y[6] - Mu * y[6]
    # dI2 <- Sigma * y[7] - (Gamm2 + Mu + Alpha) * y[8]
    dI2 = params["sigma"] * y[6] - (params["gamm2"] + Mu + Alpha) * y[7]
    # dR2 <- Gamm2 * y[8] - Mu * y[9]
    dR2 = params["gamm2"] * y[7] - Mu * y[8]
    # dM <- Alpha * y[8] + Mu * (N_tot-y[8])
    dM = Alpha * y[7] + Mu * (N_tot - y[7])

    return [dS, dE1, dA1, dI1, dR1, dE2, dA2, dI2, dR2, dM]


class ParamsAlizon:
    def __init__(self):
        self.epsilon, self.sigma, self.R0, self.gamm1, self.gamm2, self.CFR, \
        self.nu, self.mild, self.bb, self.mort_sup, self.vir_sup, self.i_sat, \
        self.beta, self.virulence, self.c = [None] * 15


def mu_func_optim(x1, x2, x3):
    # x1*(x2>x3)
    if x2 > x3:
        return x1
    return 0


def covid_ode_alizon_optim(t, y, params: ParamsAlizon):
    # fonction pour le système d'ODE lui-même
    # S=y[1] ; E1=y[2] ; A1=y[3] ; I1=y[4] ; R1=y[5] ;
    # E2=y[6] ; A2=y[7] ; I2=y[8] ; R2=y[9] ; M=y[10]
    Lambda = (1 - params.c) * params.beta * (y[2] + y[6] + y[3] + params.bb * y[7])
    # Mu <- Mu_func(mort_sup, y[8],i.sat)
    # Mu = mu_func(params.mort_sup, y[7], params.i_sat, params.diff_mu)
    Mu = mu_func_optim(params.mort_sup, y[7], params.i_sat)
    # Alpha <- Virulence + Mu_func(vir_sup, y[8],i.sat)
    # Alpha = params.virulence + mu_func(params.vir_sup, y[7], params.i_sat, params.diff_mu)
    Alpha = params.virulence + mu_func_optim(params.vir_sup, y[7], params.i_sat)
    # Unlike in some other programming languages, when you use negative numbers for indexing in R,
    # it doesn’t mean to index backward from the end
    # Instead, it means to drop the element at that index, counting the usual way, from the beginning.
    # N_tot <- sum(y[-10]) = sum of all elements minus M
    N_tot = sum(y) - y[9]
    # dS <- - Lambda * y[1] - Mu * y[1]
    dS = - Lambda * y[0] - Mu * y[0]
    # dE1 <- Mild * Lambda * y[1] - Epsilon * y[2] + Mild * Nu - Mu * y[2]
    dE1 = params.mild * Lambda * y[0] - params.epsilon * y[1] + params.mild * params.nu - Mu * y[1]
    # dA1 <- Epsilon * y[2] - Sigma * y[3] - Mu * y[3]
    dA1 = params.epsilon * y[1] - params.sigma * y[2] - Mu * y[2]
    # dI1 <- Sigma * y[3] - (Gamm1 + Mu) * y[4]
    dI1 = params.sigma * y[2] - (params.gamm1 + Mu) * y[3]
    # dR1 <- Gamm1 * y[4] - Mu * y[5]
    dR1 = params.gamm1 * y[3] - Mu * y[4]
    # dE2 <- (1 - Mild) * Lambda * y[1] - Epsilon * y[6] + (1 - Mild) * Nu - Mu * y[6]
    dE2 = (1 - params.mild) * Lambda * y[0] - params.epsilon * y[5] \
          + (1 - params.mild) * params.nu - Mu * y[5]
    # dA2 <- Epsilon * y[6] - Sigma * y[7] - Mu * y[7]
    dA2 = params.epsilon * y[5] - params.sigma * y[6] - Mu * y[6]
    # dI2 <- Sigma * y[7] - (Gamm2 + Mu + Alpha) * y[8]
    dI2 = params.sigma * y[6] - (params.gamm2 + Mu + Alpha) * y[7]
    # dR2 <- Gamm2 * y[8] - Mu * y[9]
    dR2 = params.gamm2 * y[7] - Mu * y[8]
    # dM <- Alpha * y[8] + Mu * (N_tot-y[8])
    dM = Alpha * y[7] + Mu * (N_tot - y[7])

    return [dS, dE1, dA1, dI1, dR1, dE2, dA2, dI2, dR2, dM]


def default_parameters_alizon():
    """
        default parameters for Alizon's model
        check "seair_model_param_explain.json" for doc
    """
    parameters = {
        "epsilon": 1.0,
        "sigma": 0.4,
        "R0": 2.5,
        "gamm1": 0.06,
        "gamm2": 0.05,
        "CFR": 0.15,
        "nu": 0,
        "mild": 0.9,
        "bb": 0.2,
        "mort_sup": 0.00001,
        "vir_sup": 0.01,
        "i_sat": 0.2,
        # y0: [0]=S [1]=E1 [2]=A1 [3]=var1 (I1=var1*I0) [4]=R1 [5]=E2 [6]=A2 [7]=var2 (I2=var2*I0) [8]=R2 [9]=M
        # y0 is None because it will be computed later using N0 and the other SEAIR quantities
    }
    return parameters


def default_y0_alizon():
    # [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
    return [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]


# TODO Léonard: use the contact_tracing parameter
def measures2c_alizon(contact_tracing: int = 0,
                      forced_telecommuting: int = 0,
                      school_closures: int = 0,
                      social_containment_all: int = 0,
                      total_containment_high_risk: int = 0,
                      c_max_containment: float = 0.9):
    # print('measures2c_alizon', contact_tracing, forced_telecommuting, school_closures, social_containment_all,
    #       total_containment_high_risk, c_max_containment)
    prop_pop_active = 0.4
    prop_pop_parent = 0.2
    prop_pop_children = 0.2
    prop_pop_high_school = 0.1
    prop_pop_tot_high_risk = 0.2
    prop_pop_act_high_risk = 0.1
    prop_parent_active = 0.8

    prop_time_work = 0.3
    prop_time_school = 0.3
    prop_time_social = 0.2
    prop_follow_advice_hr = 0.7

    # norm_parameter equals to c_max_containment_value_target/c_max_containment_value_computed
    norm_parameter = c_max_containment / 0.39280000000000004

    c = 0
    prop_active_parent_still_going_to_work = 1
    prop_high_risk_people_still_going_to_work = 1
    if forced_telecommuting == 1:
        c += prop_pop_active * prop_time_work / 2
        prop_active_parent_still_going_to_work = 0.5
        prop_high_risk_people_still_going_to_work = 0.5
    if forced_telecommuting == 2:
        c += prop_pop_active * prop_time_work
        prop_active_parent_still_going_to_work = 0
        prop_high_risk_people_still_going_to_work = 0

    if school_closures >= 1:
        c += prop_pop_high_school * prop_time_school
    if school_closures == 2:
        c += prop_pop_children * prop_time_school / 2
        c += prop_pop_parent * prop_parent_active * prop_time_work * prop_active_parent_still_going_to_work / 4
    if school_closures == 3:
        c += prop_pop_children * prop_time_school
        c += prop_pop_parent * prop_parent_active * prop_time_work * prop_active_parent_still_going_to_work / 2

    prop_high_risk_people_still_social = 1
    if social_containment_all == 1:
        c += 0.3 * prop_time_social
        prop_high_risk_people_still_social = 0.7
    if social_containment_all == 2:
        c += 0.6 * prop_time_social
        prop_high_risk_people_still_social = 0.4
    if social_containment_all == 3:
        c += 0.9 * prop_time_social
        prop_high_risk_people_still_social = 0.1

    if total_containment_high_risk == 1:
        c += prop_follow_advice_hr * prop_pop_tot_high_risk * prop_high_risk_people_still_social * prop_time_social
        c += prop_follow_advice_hr * prop_pop_act_high_risk * prop_high_risk_people_still_going_to_work * prop_time_work

    return c * norm_parameter


# TODO Léonard: considérer tous les cas : en télétravail par exemple une partie des High Risk sont également confinés
def measures2mild(forced_telecommuting: int = 0,
                  school_closures: int = 0,
                  social_containment_all: int = 0,
                  total_containment_high_risk: int = 0):
    prop_pop_tot_high_risk = 0.2
    prop_follow_advice_hr = 0.7

    # get major infection ratio for at risk people and others
    at_risk_mir = 0.241
    other_mir = 0.046

    prop_at_risk = prop_pop_tot_high_risk

    if total_containment_high_risk == 1:
        prop_at_risk = (1 - prop_follow_advice_hr) * prop_pop_tot_high_risk

    mild = 1 - (prop_at_risk * at_risk_mir + (1 - prop_at_risk) * other_mir)

    return mild

def SEAIR2y0_alizon(S: float, E1: float, A1: float,
             I1: float, R1: float, E2: float,
             A2: float, I2: float, R2: float,
             M: float):
    return [S, E1, A1, I1, R1, E2, A2, I2, R2, M]


# Easier to debug version
def SEAIR_alizon_ratios2y0_throws(npop: int, S: float, E1: float,
                                  A1: float, I1: float, R1: float,
                                  E2: float, A2: float, I2: float,
                                  R2: float, M: float):
    total = S + E1 + A1 + I1 + R1 + E2 + A2 + I2 + R2 + M
    if abs(total - 1) > 1E-4:
        raise ValueError('incoherent ratios')
    r = npop / total
    # r = 1
    return SEAIR2y0_alizon(S=S * r, E1=E1 * r, A1=A1 * r, I1=I1 * r, R1=R1 * r,
                    E2=E2 * r, A2=A2 * r, I2=I2 * r, R2=R2 * r, M=M * r)


def SEAIR_alizon_ratios2y0(*args, **kwargs):
    # return [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
    try:
        return SEAIR_alizon_ratios2y0_throws(*args, **kwargs)
    except ValueError:
        return None


class SEAIRModelAlizon:
    """ The SEAIR model from http://alizon.ouvaton.org/Report3_Model.html
    """

    def __init__(self, alizon20_parameters: dict, y0: list, verbose=True):
        """
        :param alizon20_parameters: cf. default_parameters()
        :param y0: [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
        :param verbose: display warning message
        """
        self.y0, self.parameters = None, None
    
        if len(y0) > 0:
            # store y0
            self.y0 = deepcopy(y0)
            if type(self.y0) == np.ndarray:
                self.y0 = self.t0.tolist()
            assert(type(self.y0) == list)

            self.S0 = self.y0[0]

        if len(alizon20_parameters) > 0:
            self.parameters = deepcopy(alizon20_parameters)

            if abs(alizon20_parameters["nu"]) > 1e-7 and verbose:
                print("WARNING: nu={} > 0 so there will be newcomers (as E1, E2) in the population!".
                      format(alizon20_parameters["nu"]))

            # calculate virulence from case fatality ratio
            self.virulence = self.parameters["CFR"] * (self.parameters["gamm2"]) / (1 - self.parameters["CFR"])
            # self.compute_Beta(self.parameters["mild"])

            # calculer Beta depuis R0
            self.Beta = self.parameters["R0"] * self.parameters["gamm1"] * self.parameters["sigma"] * (
                    self.virulence + self.parameters["gamm2"])
            denom = self.virulence * self.parameters["gamm1"] + self.parameters["gamm1"] * self.parameters["gamm2"]
            denom += self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["sigma"]
            denom += self.virulence * self.parameters["mild"] * self.parameters["sigma"]
            denom -= self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["mild"] * self.parameters["sigma"]
            denom += self.parameters["gamm2"] * self.parameters["mild"] * self.parameters["sigma"]
            self.Beta /= (self.S0 * denom)

    def update_y0(self, y0: list):
        self.y0 = deepcopy(y0)
        self.S0 = self.y0[0]

    def update_params(self, params: dict):
        assert self.y0 is not None
        self.parameters = deepcopy(params)

        if abs(params["nu"]) > 1e-7 and verbose:
            print("WARNING: nu={} > 0 so there will be newcomers (as E1, E2) in the population!".
                  format(params["nu"]))

        # calculate virulence from case fatality ratio
        self.virulence = self.parameters["CFR"] * (self.parameters["gamm2"]) / (1 - self.parameters["CFR"])
        # self.compute_Beta(self.parameters["mild"])
        # calculer Beta depuis R0
        self.Beta = self.parameters["R0"] * self.parameters["gamm1"] * self.parameters["sigma"] * (
                self.virulence + self.parameters["gamm2"])
        denom = self.virulence * self.parameters["gamm1"] + self.parameters["gamm1"] * self.parameters["gamm2"]
        denom += self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["sigma"]
        denom += self.virulence * self.parameters["mild"] * self.parameters["sigma"]
        denom -= self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["mild"] * self.parameters["sigma"]
        denom += self.parameters["gamm2"] * self.parameters["mild"] * self.parameters["sigma"]
        self.Beta /= (self.S0 * denom)

    def compute(self, c_list: list, time_intervals: list, rtol: float, atol: float, dt: int = 7, version: str = "cpp"):
        """ a slightly optimized version of compute().
            - uses the ParamsAlizon() class instead of a dict.
            - calls covid_ode_alizon_optim() that uses the simpler mu_func_optim()
        :return: n days x [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
        """
        assert self.y0 is not None
        assert self.parameters is not None
        # print('compute()', c_list, time_intervals, self.y0)
        assert len(time_intervals) - 1 == len(c_list)
        params = ParamsAlizon()
        for attr in ['epsilon', 'sigma', 'R0', 'gamm1', 'gamm2', 'CFR', 'nu', 'mild', 'bb',
                     'mort_sup', 'vir_sup', 'i_sat']:
            setattr(params, attr, self.parameters[attr])
        params.beta = self.Beta
        params.virulence = self.virulence
        parameters = deepcopy(self.parameters)
        parameters["beta"] = self.Beta
        parameters["virulence"] = self.virulence
        results = []

        parameters["dt"] = dt

        for i in range(len(time_intervals) - 1):
            # ode solver extra parameters dictionary
            params.c = c_list[i]
            parameters["c"] = c_list[i]


            beg = time_intervals[i]
            end = time_intervals[i + 1] + 1

            assert time_intervals[i] != time_intervals[i + 1]

            # Resolution ODEs sans contrôle
            if len(results):
                y0 = results_interv[-1].tolist()
            else:
                y0 = self.y0  # [S, E1, A1, I1, R1, E2, A2, I2, R2, M]

            #print(y0)
            if version == "cpp":
                run_params = deepcopy(parameters)
                results_interv = alizonpp.integrate(y0, end-beg, run_params)
                results_interv = np.asarray(results_interv)
                size = len(range(beg, end, dt))
                results_interv = results_interv.reshape((size, len(self.y0)))
            else:
                # https://www.rdocumentation.org/packages/deSolve/versions/1.28/topics/lsoda
                # lsoda() default params in R:
                # rtol = 1e-6, atol = 1e-6, maxsteps = 5000
                results_interv = odeint(covid_ode_alizon_optim, y0=y0, t=range(beg, end, dt), args=(params,), # we get the result a the end of each week 
                                        rtol=rtol, atol=atol, mxstep=5000, tfirst=True)

            if len(results):
                results.append(results_interv[1:])
            else:
                results.append(results_interv)
        return np.vstack(results)

    def compute_new_SciPy_API(self, c_list: list, time_intervals: list, method: string, rtol: float, atol: float, dt: int = 1):
        """ compute the seair model given parameters
        :return: n days x [S, E1, A1, I1, R1, E2, A2, I2, R2, M]
        """
        params = ParamsAlizon()
        for attr in ['epsilon', 'sigma', 'R0', 'gamm1', 'gamm2', 'CFR', 'nu', 'mild', 'bb',
                     'mort_sup', 'vir_sup', 'i_sat']:
            setattr(params, attr, self.parameters[attr])
        params.beta = self.Beta
        params.virulence = self.virulence
        assert len(time_intervals) - 1 == len(c_list)
        ndim = len(self.y0)

        results = []
        for i in range(len(time_intervals) - 1):
            params.c = c_list[i]
            beg = time_intervals[i]
            end = time_intervals[i + 1] + 1
            assert time_intervals[i] != time_intervals[i + 1]

            if results:
                y0 = results[-1]
            else:
                y0 = self.y0  # [S, E1, A1, I1, R1, E2, A2, I2, R2, M]

            # https://www.rdocumentation.org/packages/deSolve/versions/1.28/topics/lsoda
            # lsoda() default params in R:
            # rtol = 1e-6, atol = 1e-6, maxsteps = 5000
            # https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html#scipy.integrate.solve_ivp
            results_interv = solve_ivp(covid_ode_alizon_optim, method=method,
                                       y0=y0, t_span=(beg, end), t_eval=range(beg, end, dt),
                                       rtol=rtol, atol=atol, args=(params,))
            # results_interv.y of shape ndarray, shape (n, n_points)
            for t in range(0 if beg == 0 else beg + 1, end):
                yt = [results_interv.y[dim][t - beg] for dim in range(ndim)]
                results.append(yt)
        return np.vstack(results)
