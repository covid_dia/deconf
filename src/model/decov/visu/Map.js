function Map() { 
        this.minX = this.minY = 0;
        this.width = 1;
        this.height = 1;
        this.data = [];

        this.resize();
        this.registerEvents();
    }

Map.prototype = {

    resize: function(){
        this.canvas = document.getElementById("map");
        const w = window.innerWidth;
        const h = window.innerHeight;

        this.canvas.width  = w;
        this.canvas.height = h;

        const margin = 40;
        const ratioY = (h-margin) / this.height;
        const ratioX = (w-margin) / this.width;

        this.scale = Math.min(ratioX, ratioY);
        this.offset = [ (w - this.width * this.scale)/2,
                        (h - this.height * this.scale)/2];
        this.offset[0] -= this.minX * this.scale;
        this.offset[1] -= this.minY * this.scale;

        this.changed = true;

        console.log("ratio=",ratioX,ratioY);
        console.log("scale=",this.scale);
        console.log("offset=",this.offset);
    }, 

    windowToWorldCoordinates: function(x, y) {
        return [(x - this.offset[0]) / this.scale,
                (y - this.offset[1]) / this.scale];
    },

    worldToWindowCoordinates: function(x, y) {
        return [x * this.scale + this.offset[0],
                y * this.scale + this.offset[1]];
    },

    load: function(data) {

        function rgbToHex(r, g, b) {
          return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
        }

	// TODO: add handler to change day interactivly
	// Choose a day
        for (var record of data)
        {
            if (record["day"] > 30)
            {
                data = record["agents"];
                break;
            }
        }

        if ("pop" in data[0])
            this.format = 'circles';
        else
            this.format = 'dots';

        this.data = data;
        minX = Number.MAX_VALUE;
        maxX = -Number.MAX_VALUE;
        minY = Number.MAX_VALUE;
        maxY = -Number.MAX_VALUE;

        if (this.format == 'dots') {
            for (var m of this.data) {
                switch (m["stage"])
                {
                    case 'Sensitive':
                        m['color'] = 'green';
                        break;
                    case 'ExposedMinor':
                    case 'AsymptomaticMinor':
                    case 'InfectedMinor':
                        m['color'] = 'yellow';
                        break;
                    case 'ExposedMajor':
                    case 'AsymptomaticMajor':
                    case 'InfectedMajor':
                        m['color'] = 'orange';
                        break;
                    case 'RecoveredMinor':
                    case 'RecoveredMajor':
                        m['color'] = 'blue';
                        break;
                    case 'Dead':
                        m['color'] = 'red';
                        break;
                    default:
                        throw "Unexpected stage " + m["stage"];
                }
                minX = Math.min(m["x"], minX);
                maxX = Math.max(m["x"], maxX);
                minY = Math.min(m["y"], minY);
                maxY = Math.max(m["y"], maxY);
            }
        }
        else {
            for (var m of this.data) {
                const pop = m["pop"];
                const g = 255*(m["S"]/pop);
                const r = 255-g;
                const b = 128;
                console.log(r,g,b);
                m['color'] = rgbToHex(r,g,b);
                minX = Math.min(m["x"] - m["r"], minX);
                maxX = Math.max(m["x"] + m["r"], maxX);
                minY = Math.min(m["y"] - m["r"], minY);
                maxY = Math.max(m["y"] + m["r"], maxY);
            }
        }

        console.log(data.length,this.format," loaded");

        this.width = maxX - minX;
        this.height = maxY - minY;
        this.minX = minX;
        this.minY = minY;
        this.resize();
    },

    draw: function() {
        ctx = this.canvas.getContext("2d");

        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight);

        ctx.save();
        if (this.format == 'dots') {
            for (var m of this.data) {
                const [x, y] = this.worldToWindowCoordinates(m["x"], m["y"]);
                ctx.fillStyle = m["color"];
                ctx.fillRect(x, y, 2, 2);
            }
        }
        else
        {
            ctx.translate(this.offset[0], this.offset[1]);
            ctx.scale(this.scale, this.scale);
            for (var m of this.data) {
                const x = m["x"];
                const y = m["y"];
                const r = m["r"];
                ctx.beginPath();
                ctx.arc(x, y, r, 0, 2 * Math.PI, false);
                ctx.fillStyle = m["color"];
                ctx.fill();
            }
            ctx.restore();
        }
    },

    zoom: function(x, y, ratio) {
        const [wx, wy] = this.windowToWorldCoordinates(x, y);

        this.scale *= ratio;
        const [nx, ny] = this.worldToWindowCoordinates(wx, wy);

        this.offset[0] += x - nx;
        this.offset[1] += y - ny;

        this.changed = true;
    },

    loop: function() {

        if (this.changed)
        {
            this.draw();
            this.changed = false;
        }

        window.requestAnimationFrame(this.loop.bind(this));
    },
        
    registerEvents: function(){ 

            this.canvas.addEventListener('mousedown', function(ev){this.mousedown(ev);}.bind(this));
            this.canvas.addEventListener('mouseup', function(ev){this.mouseup(ev);}.bind(this));
            this.canvas.addEventListener('contextmenu', function(){return false;});
            this.canvas.addEventListener('mousemove', function(ev){this.mousemove(ev);}.bind(this));
            this.canvas.addEventListener('wheel', function(ev){this.mousewheel(ev);}.bind(this));
            window.addEventListener('resize', function(){this.resize()}.bind(this));
        },

    mousedown: function(ev){
            this.pressX = ev.offsetX;
            this.pressY = ev.offsetY;
            this.buttonPressed = ev.button;
        },

    mouseup: function(ev){
            delete this.pressX;
            delete this.pressY;
            delete this.buttonPressed;
        },

    mousewheel: function(ev){

            if (ev.deltaY < 0)
                this.zoom(this.mouseX, this.mouseY, 1.15);
            else
                this.zoom(this.mouseX, this.mouseY, 0.85);

            console.log("Scale=",this.scale);
        },

    mousemove: function(ev){

        this.mouseX = ev.offsetX;
        this.mouseY = ev.offsetY;
        if (this.buttonPressed === 1)
        {
            const deltaX = ev.offsetX - this.pressX;
            const deltaY = ev.offsetY - this.pressY;
            
            this.offset[0] += deltaX;
            this.offset[1] += deltaY;

            this.pressX = ev.offsetX;
            this.pressY = ev.offsetY;

            this.changed = true;
        }
    },
};

