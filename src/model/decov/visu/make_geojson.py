import json
import numpy as np
import sys

from sklearn.cluster import MiniBatchKMeans as KMeans


def decimate(agents,spots, npoints):
    features_agents = np.array([[a['x'],a['y']] for a in agents])
    kmagents = KMeans(npoints)
    agent_labels = kmagents.fit_predict(features_agents)

    features_spots = np.array([[s['x'],s['y']] for s in spots])
    kmspots = KMeans(npoints)
    spot_labels = kmspots.fit_predict(features_spots)
    
    id_to_newspots = {s['id']:spot_labels[i] for i,s in enumerate(spots)}
    
    newagents_X = kmagents.cluster_centers_
    newagents = [{'x':newagents_X[i,0],
                  'y':newagents_X[i,1],
                  "nb":np.sum(agent_labels==i),
                  "assigned":set()} for i in range(newagents_X.shape[0])]

    nmax = max([a['nb'] for a in newagents])
    for a in newagents:
        a['radius'] = 2+15*np.sqrt(a['nb']/nmax)

 
    for i in range(len(agents)):
        for j in agents[i]['assigned']:
            if j in id_to_newspots:
                newagents[agent_labels[i]]['assigned'].add(id_to_newspots[j])
            
    for a in newagents:
        a['assigned'] = list(a['assigned'])

    newspots_X = kmspots.cluster_centers_
    newspots = [{'x':newspots_X[i,0], 'y':newspots_X[i,1], 'id':i,"kind":"aggreg", "nb":np.sum(spot_labels==i), "size":0} for i in range(newspots_X.shape[0])]

    nmaxs = max([s['nb'] for s in newspots])
    for s in newspots:
        s['radius'] = 2+15*np.sqrt(s['nb']/nmaxs)
        
    for j,s in enumerate(spots):
        newspots[spot_labels[j]]['size'] += s['size']
        
    return newagents, newspots
        
    
if __name__=="__main__":
    with open(sys.argv[1]) as fp:
        print("Loading json")
        data = json.load(fp)
    spots = data['spots']
    agents = data['agents']
    spot_ids = {s['id']:i for i,s in enumerate(spots)}
    areas = list(set([s["area"] for s in spots]))
    print(areas)
    areanames = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
                 "11", "12", "13", "14", "15", "16", "17", "18", "19", "21",
                 "22", "23", "24", "25", "26", "27", "28", "29", "30", "31",
                 "32", "33", "34", "35", "36", "37", "38", "39", "40", "41",
                 "42", "43", "44", "45", "46", "47", "48", "49", "50", "51",
                 "52", "53", "54", "55", "56", "57", "58", "59", "60", "61",
                 "62", "63", "64", "65", "66", "67", "68", "69", "70", "71",
                 "72", "73", "74", "75", "76", "77", "78", "79", "80", "81",
                 "82", "83", "84", "85", "86", "87", "88", "89", "90", "91",
                 "92", "93", "94", "95", "2A", "2B"]
    
    for a in agents:
        a['radius'] = 3
        pond = 100
        #a['x'] =spots[spot_ids[a['assigned'][0]]]['x']
        a['x']=  ((pond-1) * spots[spot_ids[a['assigned'][0]]]['x'] + sum([spots[spot_ids[ass]]['x'] for ass in a['assigned'] if ass!=0]))/\
                                                                        (pond-1 + len([s for s in a['assigned'] if s!=0]))
        #a['y'] =spots[spot_ids[a['assigned'][0]]]['y']
        a['y'] =  ((pond-1) * spots[spot_ids[a['assigned'][0]]]['y'] + sum([spots[spot_ids[ass]]['y'] for ass in a['assigned'] if ass!=0]))/\
                                                                        (pond-1 + len([s for s in a['assigned'] if s!=0]))
            
    for s in spots:
        s['radius'] = 3
        
    print("Found", len(spots), "spots and ",len(agents), "agents")
    #print("Decimating")
    #agents, spots = decimate(agents,spots, 3000)
    #print("Keeping", len(agents), "agents and", len(spots),"spots")
    #spot_ids = {s['id']:i for i,s in enumerate(spots)}
        
    def latlong(x,y):
        lat = -y / 111.0
        lng = x / (111.0 * np.cos(lat * 3.14159265359 / 180))
        return lng, lat
        
    print("dumping")
    geodata = {}
    for area in areas:
        drawn_spots = set()
        print(areanames[area])
        features = []
        geodata[areanames[area]] = {"type":"FeatureCollection", "features":features}
            
        for a in agents:
            if spots[spot_ids[a['assigned'][0]]]['area'] == area:
                features.append({"type":"Feature",
                                 "properties":{"type":"agent","r":a["radius"]},
                                 "geometry":{"type":"Point",
                                             "coordinates": latlong(a['x'],a['y'])}})

                for s in a['assigned']:
                    if s!= 0:
                        spot = spots[spot_ids[s]]
                        if spot['size']>1:
                            if not s in drawn_spots:
                                features.append({"type":"Feature",
                                                 "properties":{"type":"spot","kind":spot["kind"], "r":spot["radius"]},
                                                 "geometry":{"type":"Point",
                                                             "coordinates": latlong(spot['x'],spot['y'])}})
                                drawn_spots.add(s)
                            
                            features.append({"type":"Feature",
                                             "properties":{"type":"link"},
                                             "geometry":{"type":"LineString",
                                                         "coordinates":[latlong(a['x'],a['y']),
                                                                        latlong(spot['x'],
                                                                                spot['y'])]}})
            
    with open('microdata.geojson','w') as ffp:
        ffp.write('var microdata = \n')
        json.dump(geodata, ffp)
        ffp.write("\n")
                           
        

        
