decov
=====

Low-level model for covd19 spreading.

Project files for vscode and make are provided.


Build and run
=============

Command-line interface:
```
$ make                          # to build default (release) configuration
$ build/RELEASE/decov -h        # to show help
$ make OPTIM=0                  # to build in debug mode
$ build/DEBUG/decov -h
```

Test
====

```
$ make test     # to launch the test suite
$ build/RELEASE/test PartOfName  # to run a particular test
$ build/RELEASE/test -v  # to run tests in verbose mode 
$ build/RELEASE/test -d  # to allow test debugging (ie do not catch exceptions)
```

Naming conventions
==================

- `TypesLikeThis` : user-defined types, class names, etc.
- `camelCase` : class members and local variables
- `static_vars` : static variables and function
- `NotSupposedToChange` : constants, constexpr, etc.
- `MACROS` : preprocessors macros and tokens
