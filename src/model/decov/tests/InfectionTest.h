#pragma once
#include "TestBase.h"
#include "Simulation.h"
#include <omp.h>
namespace decov
{
    struct InfectionTest : public TestBase
    {
        Random re;

        InfectionTest(bool verbose)
          : TestBase(verbose)
        { }

        void run() override
        {
            testTwoAgentsOneNotInfected(Stages::Sensitive, Stages::ExposedMinor);
            testTwoAgentsOneNotInfected(Stages::ExposedMinor, Stages::InfectedMinor);
            testTwoAgentsOneNotInfected(Stages::RecoveredMinor, Stages::InfectedMinor);
            testTwoAgentsOneNotInfected(Stages::ExposedMajor, Stages::InfectedMajor);
            testTwoAgentsOneNotInfected(Stages::RecoveredMajor, Stages::InfectedMajor);

            testTwoAgentsOneInfected(Stages::InfectedMinor, Stages::RecoveredMinor);
            testTwoAgentsOneInfected(Stages::AsymptomaticMinor, Stages::RecoveredMinor);
            testTwoAgentsOneNotInfected(Stages::InfectedMajor, Stages::RecoveredMajor); // Moving in Not Infected because hospital
            testTwoAgentsOneInfected(Stages::AsymptomaticMajor, Stages::RecoveredMajor);
            testContaminationChainInfectedFourAgents();
            testContaminationChainInfected(20);
            testContaminationChainInfected(50);
            testContaminationPartialChain(4,4);
            testContaminationPartialChain(2,100);
            testContaminationPartialChain(100,2);
            testContaminationPartialChain(25,25);
        }
	  
        void testTwoAgentsOneNotInfected(Stages notInfectedStage, Stages nextStage)
        {
            testTwoAgentsOneNotInfectedSamePlace(notInfectedStage, nextStage);
            testTwoAgentsVirusNotInfectiousSamePlace(notInfectedStage, nextStage);
            testTwoAgentsNotSamePlace(notInfectedStage, nextStage);
            testTwoAgentsSamePlacePromiscuityZero(notInfectedStage, nextStage);
        }

        void testTwoAgentsOneInfected(Stages infectedStage, Stages nextStage)
        {
            testTwoAgentsOneInfectedSamePlace(infectedStage, nextStage);
            testTwoAgentsVirusNotInfectiousSamePlace(infectedStage, nextStage);
            testTwoAgentsNotSamePlace(infectedStage, nextStage);
            testTwoAgentsNotSamePlaceSameNeighborhood(infectedStage,nextStage);
            testTwoAgentsSamePlacePromiscuityZero(infectedStage, nextStage);
            testTwoAgentsOneInfectedSchool(infectedStage, nextStage);
            testTwoAgentsOneInfectedSchoolAllClosed(infectedStage, nextStage);
            //testTwoAgentsOneInfectedSchoolHigherEducationClosedAlternatingPupils(infectedStage, nextStage);
            testTwoAgentsOneInfectedSchoolOnlyHigherEducationClosedMinors(infectedStage, nextStage);
            testTwoAgentsOneInfectedSchoolOnlyHigherEducationClosedMajors(infectedStage, nextStage);
            testTwoAgentsOneInfectedSchoolNone(infectedStage, nextStage);
            testTwoAgentsOneInfectedWorkNone(infectedStage, nextStage);
            //testTwoAgentsOneInfectedWorkAlternating(infectedStage, nextStage);
            testTwoAgentsOneInfectedWorkComplete(infectedStage, nextStage);
        }

        void testTwoAgentsOneNotInfectedSamePlace(Stages notInfectedStage, Stages nextStage)
        {
            // Checking when two agents meet and Infection proba is 1 that when the first agent is Sensitive
            // and the second is not infected, the first one will NOT be exposed.

            int numAgents = 2;
            int maxContacts = 1;
            
            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test.
            p.NeighborhoodTime = 0;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.MeasuresComplianceProbability = 0;
            p.Areas.push_back({});

            p.resize(2 * 7);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area

            state.contactHistory.resize(numAgents);

            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            notInfectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                state.assignAgent(id, 0, SpotKinds::Home);
                state.assignAgent(id, 0, SpotKinds::Neighborhood1);

                state.assignAgent(id, 0, SpotKinds::Neighborhood2);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            // Initialize parameters with one municipality in one area

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();


            check(state.agents[0].stage == Stages::Sensitive);
            
        }        

        void testTwoAgentsOneInfectedSamePlace(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet and Infection proba is 1 that when the first agent is Sensitive
            // and the second is of stage in parameter, the first one will be exposed.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test.
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});

            p.resize(20);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area

            state.contactHistory.resize(numAgents);

            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                state.assignAgent(id, 0, SpotKinds::Home);
                state.assignAgent(id, 0, SpotKinds::Neighborhood1);
                state.assignAgent(id, 0, SpotKinds::Neighborhood2);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);
            
        }        

        void testTwoAgentsVirusNotInfectiousSamePlace(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet and Infection proba is 1 that when the first agent is Sensitive
            // the second is of stage in parameter and the virus is not Infectious, the first one will stay sensitive.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 0.0f; // Virus not infectious.
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 0;
            p.Areas.push_back({});

            p.resize(20);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area

            state.contactHistory.resize(numAgents);

            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                state.assignAgent(id, 0, SpotKinds::Home);
                state.assignAgent(id, 0, SpotKinds::Neighborhood1);
                state.assignAgent(id, 0, SpotKinds::Neighborhood2);

            }
            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);

            s.runDay();
            check(state.agents[0].stage == Stages::Sensitive);
            
        }     

        void testTwoAgentsNotSamePlace(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents are in different locations, the virus will not spread.


            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious.
            p.MeasuresComplianceProbability = 0;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
     
            SimulationState state(p);
            // Initialize parameters with one munipality and two areas
#if DECOV_TRACE_CONTACTS == 1
            state.contactHistory.resize(numAgents);
#endif
            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 3, 3, 1);           
            state.addSpot(SpotKinds::School, numAgents, 3, 3, 0);           
            state.addSpot(SpotKinds::School, numAgents, 3, 3, 1);           
            state.addSpot(SpotKinds::Work, numAgents, 3, 3, 0);           
            state.addSpot(SpotKinds::Work, numAgents, 3, 3, 1);           
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);           
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; a++) {
              AgentId id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);

                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);              
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();
            check(state.agents[0].stage == Stages::Sensitive);
            
        }        

        void testTwoAgentsNotSamePlaceSameNeighborhood(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents are in different locations but same Neighborhood, the virus will spread.


            int numAgents = 2;
            int maxContacts = 1;
            int numDays = 20;
            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious.
            p.WorkTime = 0;
            p.SchoolTime = 0;
            p.NeighborhoodTime = 1;
            p.MeasuresComplianceProbability = 0;
            p.Areas.push_back({});
            p.Areas.push_back({});

            p.resize(numDays);

            SimulationState state(p);
            // Initialize parameters with one munipality and two areas

            state.contactHistory.resize(numAgents);

            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 3, 3, 1);           
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);
            // Create and assign agents to it
            Random re;
            AgentId id0 = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);

            state.assignAgent(id0, 0, SpotKinds::Home);
            state.assignAgent(id0, 0, SpotKinds::Neighborhood1);
            state.assignAgent(id0, 0, SpotKinds::Neighborhood2);
                
            AgentId id1 = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);

            state.assignAgent(id1, 1, SpotKinds::Home);               
            state.assignAgent(id1, 0, SpotKinds::Neighborhood1);
            state.assignAgent(id1, 0, SpotKinds::Neighborhood2);
         
            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            for (int i = 0; i < numDays; i++) {
                s.runDay();
                //std::cout << state.agents[0].stage << " " << state.agents[1].stage << std::endl;
            }
            check(state.agents[0].stage != Stages::Sensitive);
            
        }        

        void testTwoAgentsSamePlacePromiscuityZero(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet and promiscuity is at 0, the virus does not spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 0;
            p.InfectionStrength = 200.0f; // Virus very infectious.
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});

            p.resize(20);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area
#if DECOV_TRACE_CONTACTS == 1
            state.contactHistory.resize(numAgents);
#endif
            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 2, 2, 0);
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
               
                state.assignAgent(id, 0, SpotKinds::Home);
                state.assignAgent(id, 0, SpotKinds::Neighborhood1);
                state.assignAgent(id, 0, SpotKinds::Neighborhood2);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();
            check(state.agents[0].stage == Stages::Sensitive);
            
        }        

        void testTwoAgentsOneInfectedSchool(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::School, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);

            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::School);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);
            
        } 

        void testTwoAgentsOneInfectedSchoolAllClosed(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].SchoolClosure[0] = SchoolClosureValues::AllClosed;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::School, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);

            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::School);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::Sensitive);
            
        } 

        void testTwoAgentsOneInfectedSchoolHigherEducationClosedAlternatingPupils(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 3;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].SchoolClosure[0] = SchoolClosureValues::HigherEducationClosedAlternatingPupils;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 2);
            state.addSpot(SpotKinds::School, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 2);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 2);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 2) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::School);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);
            s.runDay(); s.runDay(); s.runDay(); s.runDay();

            check(state.agents[1].stage == Stages::Sensitive);           
        } 

        void testTwoAgentsOneInfectedSchoolOnlyHigherEducationClosedMinors(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].SchoolClosure[0] = SchoolClosureValues::OnlyHigherEducationClosed;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::School, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 17), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 17), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::School);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);
       
        } 

        void testTwoAgentsOneInfectedSchoolNone(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].SchoolClosure[0] = SchoolClosureValues::None;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::School, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::School);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);
       
        } 

        void testTwoAgentsOneInfectedSchoolOnlyHigherEducationClosedMajors(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].SchoolClosure[0] = SchoolClosureValues::OnlyHigherEducationClosed;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::School, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::NotActive,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::School);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::Sensitive);
       
        } 

        void testTwoAgentsOneInfectedWorkNone(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].ForcedTelework[0] = ForcedTeleworkValues::None;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::Work, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::CanTelework,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::CanTelework,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::Work);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);
       
        } 

        void testTwoAgentsOneInfectedWorkAlternating(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 3;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].ForcedTelework[0] = ForcedTeleworkValues::Alternating;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::Home, numAgents, 2, 2, 2);
            state.addSpot(SpotKinds::Work, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 2);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 2);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 2) {
                    id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::CanTelework,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::CanTelework,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::Work);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::ExposedMinor || state.agents[0].stage == Stages::ExposedMajor);

            s.runDay(); s.runDay(); s.runDay();

            check(state.agents[1].stage == Stages::Sensitive);
       
        } 

        void testTwoAgentsOneInfectedWorkComplete(Stages infectedStage, Stages nextStage)
        {
            // Checking when two agents meet at school, the virus will spread.

            int numAgents = 2;
            int maxContacts = 1;

            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious for test
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            p.Areas.push_back({});
            p.Areas.push_back({});
            p.resize(20);
            p.Areas[0].ForcedTelework[0] = ForcedTeleworkValues::Complete;
            SimulationState state(p);

            state.contactHistory.resize(numAgents);

            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Home, numAgents, 1, 1, 1);
            state.addSpot(SpotKinds::Work, numAgents, 2, 2, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 3, 3, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4, 4, 0);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 5, 5, 1);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 6, 6, 1);
            // Create and assign agents to spots
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = 0;
                if (a == 1) {
                    id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::CanTelework,
                                            infectedStage, nextStage, -1);
                }
                else {
                     id = state.addAgent(0, re.rand<int>(18, 99), false, TeleworkStatuses::CanTelework,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                }
                // each agent assigned to separated spots but school
                state.assignAgent(id, a, SpotKinds::Home);
                state.assignAgent(id, a, SpotKinds::Neighborhood1);
                state.assignAgent(id, a, SpotKinds::Neighborhood2);
                state.assignAgent(id, 0, SpotKinds::Work);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            s.runDay();

            check(state.agents[0].stage == Stages::Sensitive);
       
        } 

        void testContaminationChainInfectedFourAgents()
        {
            // Contamination chain by work for 4 agents.
            int numAgents = 4;
            int maxContacts = numAgents;    
            int nAreas = (numAgents + 1)/2;
            int numDays = 20;
            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious.
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            for (int i = 0; i <= nAreas; i++) {
        
                p.Areas.push_back({});
            }
            p.resize(numDays);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area
#if DECOV_TRACE_CONTACTS == 1
            state.contactHistory.resize(numAgents);
#endif
            // Create one spot
            state.addSpot(SpotKinds::Home, numAgents, 0, 0, 0);
            state.addSpot(SpotKinds::Work, numAgents, 1, 1, 0);
            state.addSpot(SpotKinds::Home, numAgents, 2, 2, 1);
            state.addSpot(SpotKinds::Neighborhood1, numAgents, 4*numAgents+1, 4*numAgents+1, 0);
            state.addSpot(SpotKinds::Neighborhood2, numAgents, 4*numAgents+2, 4*numAgents+2, 0);
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; a++) {
                AgentId id = 0;
                if (a == 0) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                                Stages::InfectedMinor, Stages::RecoveredMinor, 10);
                }
                else {
                    if (a == 3) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                                Stages::Sensitive, Stages::Sensitive, -1);
                    }
                    else {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::CannotTelework,
                    
                                                Stages::Sensitive, Stages::Sensitive, -1);
                    }

                }
                
                if (a <= 1)
                {
                    state.assignAgent(id, 0, SpotKinds::Home);
                }
                else
                {   
                    state.assignAgent(id, 1, SpotKinds::Home);       
                }

                if (a == 1 || a == 2)
                {
                    state.assignAgent(id, 0, SpotKinds::Work);
                }

                state.assignAgent(id, 0, SpotKinds::Neighborhood1);
                state.assignAgent(id, 0, SpotKinds::Neighborhood2);                  
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            int i = 0;
            while ((i < 10*numAgents) && (state.agents[numAgents-1].stage == Stages::Sensitive)) 
            {
                s.runDay();
                for (int a = 0; a < numAgents; a++) 
                {
                    //std::cout << state.agents[a].stage << "   ";
                    if (a > 0)
                    {
                        check(state.agents[a].stage == Stages::Sensitive || (state.agents[a-1].stage != Stages::Sensitive && state.agents[a-1].stage != Stages::ExposedMinor && state.agents[a-1].stage != Stages::ExposedMajor)); // Checking that if a is not Sensitive then the previous one what also not Sensitive.
                    }
                }
            i++;            
            }
            check(state.agents[numAgents-1].stage != Stages::Sensitive);
        }        


        void testContaminationChainInfected(int numAgents)
        {
           // Contamination chain by work for any number of agents.
            int maxContacts = numAgents;    
            int nAreas = (numAgents + 1)/2;
            int numDays = 20*numAgents;
            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious.
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            for (int i = 0; i <= nAreas; i++) {
        
                p.Areas.push_back({});  
            }
            p.resize(numDays);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area
#if DECOV_TRACE_CONTACTS == 1
            state.contactHistory.resize(numAgents);
#endif
            for (int i = 0; i <= nAreas; i++) {
                state.addSpot(SpotKinds::Home, numAgents, 2*i, 2*i, i);
                state.addSpot(SpotKinds::Work, numAgents, 2*i+1, 2*i+1, i);
            }
            // Create one spot
            int spotn1 = state.addSpot(SpotKinds::Neighborhood1, numAgents, 4*numAgents+1, 4*numAgents+1, 0);
            int spotn2 = state.addSpot(SpotKinds::Neighborhood2, numAgents, 4*numAgents+2, 4*numAgents+2, 0);
            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; a++) {
                AgentId id = 0;
                if (a == 0) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                                Stages::InfectedMinor, Stages::RecoveredMinor, 10);
                }
                else {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::CannotTelework,
                                                Stages::Sensitive, Stages::Sensitive, -1);
                }
                
                    state.assignAgent(id, a/2, SpotKinds::Home);

                if (a >= 1)
                {
                    state.assignAgent(id, (a+1)/2, SpotKinds::Work);
                }

                state.assignAgent(id, spotn1, SpotKinds::Neighborhood1);
                state.assignAgent(id, spotn2, SpotKinds::Neighborhood2);                  
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents);
            
            // Run 
            Simulator s(p, state, 0);
            int i = 0;
            while ((i < numDays) && (state.agents[numAgents-1].stage == Stages::Sensitive)) 
            {
                s.runDay();
                for (int a = 0; a < numAgents; a++) 
                {
                    //std::cout << state.agents[a].stage << "   ";
                    if (a > 0)
                    {
                        check(state.agents[a].stage == Stages::Sensitive || (state.agents[a-1].stage != Stages::Sensitive && state.agents[a-1].stage != Stages::ExposedMinor && state.agents[a-1].stage != Stages::ExposedMajor)); // Checking that if a is not Sensitive then the previous one what also not Sensitive.
                    }
                }
            //std::cout << std::endl << "-------------------" << std::endl;
            i++;            
            }
            check(state.agents[numAgents-1].stage != Stages::Sensitive);
        }        

        void testContaminationPartialChain(int numAgents1, int numAgents2)
        {
           // Contamination chain cut between numAgents1 first agents and the others.
            int numAgents = numAgents1 + numAgents2+1;
            int maxContacts = numAgents;    
            int nAreas = (numAgents + 1)/2;
            int numDays = 20*numAgents;
            Parameters p;
            p.BarrierGesturesPromiscuityFactor = 1;
            p.InfectionStrength = 200.0f; // Virus very infectious.
            p.NeighborhoodTime = 0;
            p.MeasuresComplianceProbability = 1;
            for (int i = 0; i <= nAreas; i++) 
            {        
                p.Areas.push_back({});    
            }
            p.resize(numDays);

            SimulationState state(p);
            // Initialize parameters with one municipality in one area
#if DECOV_TRACE_CONTACTS == 1
            state.contactHistory.resize(numAgents);
#endif
            for (int i = 0; i <= nAreas; i++) {
                state.addSpot(SpotKinds::Home, numAgents, 2*i, 2*i, i);
                state.addSpot(SpotKinds::Work, numAgents, 2*i+1, 2*i+1, i);
            }
            // Create one spot
            int spotn1 = state.addSpot(SpotKinds::Neighborhood1, numAgents, 4*numAgents+1, 4*numAgents+1, 0);
            int spotn2 = state.addSpot(SpotKinds::Neighborhood2, numAgents, 4*numAgents+2, 4*numAgents+2, 0);
            // Create and assign agents to it
            Random re;
            for (int ag = 0; ag < numAgents-1; ag++) {
                int a = 0;
                if (ag < numAgents1) {
                    a = ag;
                }
                else {
                    a = ag + 1;
                }
                AgentId id = 0;
                if (a == 0) {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                                Stages::InfectedMinor, Stages::RecoveredMinor, 10);
                }
                else {
                    id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::CannotTelework,
                                                Stages::Sensitive, Stages::Sensitive, -1);
                }
                
                    state.assignAgent(id, a/2, SpotKinds::Home);

                if (a >= 1)
                {
                    state.assignAgent(id, (a+1)/2, SpotKinds::Work);
                }

                state.assignAgent(id, spotn1, SpotKinds::Neighborhood1);
                state.assignAgent(id, spotn2, SpotKinds::Neighborhood2);                  
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;
            // Initialize parameters with one municipality in one area



            check((int)state.agents.size() == numAgents-1);
            
            // Run 
            Simulator s(p, state, 0);
            int i = 0;
            while ((i < numDays) && (state.agents[numAgents1-1].stage == Stages::Sensitive)) 
            {
                s.runDay();
                    
                for (int a = 0; a < numAgents-1; a++) 
                {
                    if ((a > 0) && (a < numAgents1))
                    {
                        check(state.agents[a].stage == Stages::Sensitive || (state.agents[a-1].stage != Stages::Sensitive && state.agents[a-1].stage != Stages::ExposedMinor && state.agents[a-1].stage != Stages::ExposedMajor)); // Checking that if a is not Sensitive then the previous one what also not Sensitive.
                    }
                    else {
                        check(a == 0 || state.agents[a].stage == Stages::Sensitive);
                    }
                }
            i++;            
            }
            check(state.agents[numAgents1-1].stage != Stages::Sensitive);
        }        
    };
}
