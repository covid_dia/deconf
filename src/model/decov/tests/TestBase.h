#pragma once
#include "Error.h"
#include "Types.h"
#include <iostream>
#include <cmath>
#include <map>

namespace decov
{
struct TestBase
{
    bool verbose;

    TestBase(bool verbose)
      : verbose(verbose)
    {
        // ENABLE_TRACE_FOR(this);
    }

    virtual ~TestBase() {}

    template <typename... Args>
    void check(bool condition, Args... msg)
    {
        if (!condition)
            throw Error("check failed: ", msg...);
    }

    template <typename T, typename... Args>
    void checkAlmostEqual(T v1, T v2, T epsilon, Args... msg)
    {
        auto delta = std::abs(v1 - v2);
        if (delta > epsilon)
            throw Error("|", v1, "-", v2, "| = ", delta , " > ", epsilon, ": checkAlmostEqual failed: ", msg...);
    }

    virtual void run() = 0;

    enum class Statuses
    {
        NOT_RUN,
        PASSED,
        FAILED
    };

    template <typename Test>
    static Statuses run(std::map<std::string, int> &testNames, bool dontCatch, bool display)
    {
        auto name = decov::type_name<Test>();
        if (!testNames.empty())
        {
            bool found = false;
            for (auto &[pattern, count] : testNames)
            {
                if (name.find(pattern) != name.npos)
                {
                    std::cout << name << " matches " << pattern << "\n";
                    count++;
                    found = true;
                }
            }
            if (!found)
                return Statuses::NOT_RUN;
        }

        std::cout << name << " ";
        std::cout.flush();
        if (dontCatch)
        {
            Test(display).run();
        }
        else
        {
            try
            {
                Test(display).run();
            }
            catch (Error & e)
            {
                if (display)
                    std::cerr << e.what() << std::endl; 
                std::cout << "\33[91mFAILED\33[0m\n";
                return Statuses::FAILED;
            }
        }
        std::cout << "\33[32mPASSED\33[0m\n";
        std::cout.flush();
        return Statuses::PASSED;
    }
};
} // namespace decov
