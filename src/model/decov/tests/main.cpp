#include "TestBase.h"
#include "SpatialIndexTest.h"
#include "DistributionsTest.h"
#include "RandomTest.h"
#include "ContactTest.h"
#include "InfectionTest.h"

using namespace decov;
int main(int argc, const char **argv)
{
    std::map<std::string, int> testsToRun;
    bool dontCatch = false;
    bool display = false;

    for (int a = 1; a < argc; ++a)
    {
        std::string arg = argv[a];
        if (arg == "-d")
            dontCatch = true;
        else if (arg == "-i" || arg == "-v")
            display = true;
        else if (arg[0] != '-')
            testsToRun.emplace(arg, 0);
        else
            throw decov::Error("Unknown option:", arg);
    }

    bool error = false;
#define T(NAME) error |= TestBase::run<decov::NAME##Test>(testsToRun, dontCatch, display) == TestBase::Statuses::FAILED;
    T(SpatialIndex)
    T(Weibull)
    T(Random)
    T(Contact)
    T(Infection)
    T(ListDistribution)
    T(HistogramDistribution)
#undef T

    for (auto &[name, count] : testsToRun)
    {
        if (count == 0)
            throw decov::Error("No test matching: ", name);
    }
    return error ? 1 : 0;
}
