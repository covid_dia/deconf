#include "TestBase.h"
#include "SpatialIndex.h"

namespace decov
{
struct SpatialIndexTest : public TestBase
{
    SpatialIndexTest(bool verbose)
      : TestBase(verbose)
    { }

    void run() override
    {
        std::vector<Spot> spots;

        Spot  A(1, 0, 0, -1);
        Spot _B(2, -1, 1, -1);
        Spot _C(3, 1, 1, -1);
        Spot _D(4, 100, 100, -1);
        Spot _E(5, -10, -10, -1);

        spots.push_back(_B);
        spots.push_back(_C);
        spots.push_back(_D);
        spots.push_back(_E);

        SpotIndex B = 0;
        SpotIndex C = 1;
        SpotIndex D = 2;
        SpotIndex E = 3;

        SpatialIndex index(spots);

        auto closer1 = index.getClosestFreeSpots(A, 1);
        check(closer1.size() == 1);
        check(closer1[0] == B || closer1[0] == C);

        auto closer2 = index.getClosestFreeSpots(A, 3);
        check(std::count(closer2.begin(), closer2.end(), D) == 0);
        check(closer2[0] == E);

        spots[C].agents.resize(spots[C].size);
        auto closer3 = index.getClosestFreeSpots(A, 2);
        check(closer3.size() == 1);
        check(closer3[0] == B);

        spots[B].agents.resize(spots[B].size);
        auto closer4 = index.getClosestFreeSpots(A, 1);
        check(closer4.size() == 1);
        check(closer4[0] == E);
    }
};
}
