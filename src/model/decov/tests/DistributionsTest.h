#pragma once
#include "Distributions.h"
#include "TestBase.h"
#include <iomanip>
namespace decov
{

struct WeibullTest : public TestBase
{
    WeibullTest(bool verbose)
      : TestBase(verbose)
    { }

    static constexpr int MAX_DAYS = 50;

    // TODO: use values from json
    void run() override
    {
        if (verbose)
            std::cerr << "Exposed:\n";
        float meanE = show({2, 3.6f, 13});
        checkAlmostEqual(meanE, 4.2f, 0.05f, "exposed duration");

        if (verbose)
            std::cerr << "Asymptomatic:\n";
        float meanA = show({0.7f, 0.1f, 3});
        checkAlmostEqual(meanA, 1.0f, 0.05f, "asymptomatic duration");

        if (verbose)
            std::cerr << "I1:\n";
        float meanI1 = show({5, 17.4f, 28});
        checkAlmostEqual(meanI1, 17.0f, 0.05f, "I1 duration");

        if (verbose)
            std::cerr << "I2:\n";
        float meanI2 = show({5, 20.7f, 34});
        checkAlmostEqual(meanI2, 20.0f, 0.05f, "I2 duration");

        if (verbose)
            std::cerr << "Dead:\n";
        float meanD = show({2, 10.5f, 34});
        checkAlmostEqual(meanD, 10.5f, 0.5f, "death day");
    }

    float show(TruncatedWeibullDistribution d)
    {
        Random re;
        float mean = 0;
        int N = 100000;
        std::array<int, MAX_DAYS> counts = {};
        for (int i = 0; i < N; ++i)
        {
            int day = d.sample(re);
            mean += day;
            counts[day]++;
        }
        mean /= N;
        if (verbose)
        {
            std::cerr << "Mean=" << mean << "\n";

            int max = *std::max_element(counts.begin(), counts.end());

            int width = 60;

            bool started = false;
            for (int d = 0; d < MAX_DAYS; ++d)
            {
                int c = counts[d];
                int l = c * width / max;
                std::cout << "day " << std::setw(2) << d << ": "
                          << std::string(l, '#')
                          << std::string(width + 1 - l, ' ') << ' '
                          << counts[d] << "\n";
                if (counts[d] == 0)
                {
                    if (started)
                        break;
                }
                else
                    started = true;
            }
        }

        return mean;
    }
};

struct ListDistributionTest : public TestBase
{
    ListDistributionTest(bool verbose)
      : TestBase(verbose)
    { }

    void run() override
    {
        ListDistribution<int> l = {{{0, 1}, {5, 2}, {10, 3}}};

        check(l[-1] == 1);
        check(l[0] == 1);
        check(l[4] == 1);
        check(l[5] == 2);
        check(l[10] == 3);
        check(l[11] == 3);
    }
};

struct HistogramDistributionTest : public TestBase
{
    HistogramDistributionTest(bool verbose)
      : TestBase(verbose)
    { }

    void run() override
    {
        HistogramDistribution<int,int> h = {{0, 5, 10, 11}, {1, 2, 3}, 6};

        check(h.min() == 0);
        check(h[0] == 1);
        check(h[4] == 1);
        check(h[5] == 2);
        check(h[10] == 3);

        try
        {
            h[11];
        }
        catch(const Error & e)
        {
            return;
        }
        
        throw Error("Should raise an exception");
    }
};
} // namespace decov
