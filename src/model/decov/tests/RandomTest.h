#pragma once
#include "TestBase.h"
#include "Random.h"
namespace decov
{
    struct RandomTest : public TestBase
    {
        Random re;

        RandomTest(bool verbose)
          : TestBase(verbose)
        { }

        void testRound(float value)
        {
            static constexpr int N = 10000;
            int sum = 0;
            for (int i = 0; i < N; ++i)
            {
                sum += re.round(value);
            }

            checkAlmostEqual((float)sum/N, value, (float)1e-2);
        }

        void testRouletteWheel()
        {
            std::array<float,5> weights = { 1, 4, 0, 10, 0.1f };
            std::array<int,weights.size()> counts = {};

            float sum = std::accumulate(weights.begin(), weights.end(), 0.0f);
            int N = 500000;
            for (int i = 0; i < N; ++i)
            {
                int r = re.select(weights, sum);
                check(r >= 0 && r < (int)counts.size());
                counts[r]++;
            }

            for (int i = 0; i < (int)weights.size(); ++i)
            {
                float ratio = counts[i] / (float)N;
                float expected = weights[i] / sum;
                if (verbose)
                {
                    std::cout << weights[i] << "/" << counts[i] << ": "
                              << ratio << "-" << expected << "="
                              << std::abs(ratio-expected) << "\n";
                }
                checkAlmostEqual(ratio, expected, (float)1e-3, "Bad ratio");
            }
        }

        void testRouletteWheel2()
        {
            std::array<int,7> a = {0,0,1,0,0,0,0};
            for (int i = 0; i < 10000; ++i)
            {
                check(re.select(a, 1) == 2);
            }
        }

        void run() override
        {
            testRound(-773.6);
            testRound(-12.1);
            testRound(12.9);
            testRound(99912.5);

            testRouletteWheel();
            testRouletteWheel2();
        }


    };
}
