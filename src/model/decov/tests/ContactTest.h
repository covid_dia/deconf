#pragma once
#include "TestBase.h"
#include "Simulation.h"
#include <omp.h>
namespace decov
{
    struct ContactTest : public TestBase
    {
        Random re;

        ContactTest(bool verbose)
          : TestBase(verbose)
        { }

        void run() override
        {
            for (int agents: { 1, 2, 10, 20, 50, 100, 500, 1000 })
            {
                for (int maxContacts: { 10, 20, 50, 100 })
                {
                    testContacts(agents, maxContacts, 1);
                }
            }
        }

        void testContacts(int numAgents, int maxContacts, int numDays)
        {
            if (verbose)
                std::cout << "numAgents=" << numAgents << ", maxContacts=" << maxContacts << std::endl;
            const int spotSize = numAgents;

            [[maybe_unused]]
            constexpr double threshold = .85;

            // Initialize parameters with one municipality in one area
            Parameters p;
            p.Areas.push_back({});

            p.resize(numDays);

            SimulationState state(p);

#if DECOV_TRACE_CONTACTS == 1
            state.contactHistory.resize(numAgents);
#endif

            // Create one spot
            int spotIndex = state.addSpot(SpotKinds::Home, spotSize, 0, 0, 0);

            // Create and assign agents to it
            Random re;
            for (int a = 0; a < numAgents; ++a)
            {
                AgentId id = state.addAgent(0, re.rand<int>(0, 99), false, TeleworkStatuses::NotActive,
                                            Stages::Sensitive, Stages::Sensitive, -1);
                state.assignAgent(id, spotIndex, SpotKinds::Home);

                // allocate 100% to home
                state.allocateTime(state.agents[id], SpotKinds::Home, 1);
            }

            // Set max number of contacts
            p.MaxContacts[SocialDistancingValues::None] = maxContacts;

            // Run 
            Simulator s(p, state, 0);
            s.mustKeepContacts = true;

#if ! defined(_OPENMP)
            double numContacts = 0;
#endif
            for (int d = 0; d < numDays; ++d)
            {
                s.spreadInfection();
                s.computeContactsMatrix();

#if ! defined(_OPENMP)
                // Check that no more than maxContacts per agent during this day
                for (int i = 0; i < numAgents; ++i)
                {
                    if (numAgents > maxContacts + 1)
                    {
                        check((int)s.numContacts.size() == numAgents, s.numContacts.size(), "!=", numAgents);
                        check(s.numContacts[i] <= maxContacts);
                        numContacts += s.numContacts[i];
                    }
                }
#endif
            }

#if ! defined(_OPENMP)
            if (numAgents > maxContacts + 1)
            {
                // Check that average is as closer as possible of maxContacts
                double avg = numContacts / numDays / numAgents;
                if (verbose)
                    std::cout << "numAgents=" << numAgents << ", spotSize="
                            << spotSize << ", maxContacts=" << maxContacts << ": avgContacts="
                            << avg << std::endl;
                check(avg <= maxContacts, avg, ">", maxContacts);
                check(avg > threshold * maxContacts, avg, "<=", threshold * maxContacts);
            }
#endif

#if DECOV_DUMP_STATS == 1
            // Output contact matrix
            auto& matrix = state.indicators.contactsMatrix;
            int expectedContacts = std::min(numAgents - 1, maxContacts);

            for (int a = 0; a < ByAgeContactMatrix::N; ++a)
            {
                if (matrix.counts[a] == 0)
                    continue;
                int numContacts = 0;
                for (int b = 0; b < ByAgeContactMatrix::N; ++b)
                {
                    numContacts += matrix.contacts[a][b];
                }
                
                double avg = (double) numContacts / numDays / matrix.counts[a];
                check(avg <= expectedContacts, avg, ">", expectedContacts);
                check(avg >= threshold * expectedContacts, avg, "<", threshold * expectedContacts);
            }
#endif
        }
    };
}
