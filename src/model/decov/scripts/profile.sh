#!/bin/sh -xe
# Generate weighted callgraph
EXE=build/RELEASE_PROF/decov 
make PROFILE=1
$EXE $*
gprof $EXE gmon.out > profile.txt
gprof2dot profile.txt > profile.dot
xdot profile.dot
