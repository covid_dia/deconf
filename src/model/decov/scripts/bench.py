import subprocess
import time

if __name__=="__main__":
    cmd = "./build/RELEASE/decov -p scripts/eval_policy_req_ctr.json -s 10 -geo test.geo -r ep_res.json 2> /dev/null"
    print("Using command:", cmd)
    #cache warm start
    subprocess.call(cmd, shell=True)
    nruns = 10
    t = time.time()
    for i in range(nruns):
        print("Doing run", i)
        subprocess.call(cmd, shell=True)
    ttot = time.time()-t
    print("Did", nruns, "runs in", ttot, "s: average",ttot/nruns)
