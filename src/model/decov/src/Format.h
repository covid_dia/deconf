#pragma once
/*!
 * \file Format.h
 * \brief Utilitary functions to convert string 
 * \author Sam
 * \version 0.1
 */


#include <string>
#include <iomanip>
namespace decov
{

    /*! \fn to_string
     * 
     * Convert to a string
     *
     * \param v, template input
     * \return string
     */
    template <typename T>
    static std::string to_string(T v)
    {
        std::ostringstream os;
        if constexpr (std::is_floating_point<T>::value)
            os << std::fixed << std::setprecision(2) << v;
        else
            os << v;
        return os.str();
    }

    /*! \fn to_lower
     * 
     * Format a string into a lower case string
     *
     * \param s, the string to format
     * \return string, the string in lower case
     */
    inline std::string to_lower(std::string s)
    {
        for (char & c: s)
        {
            if (c >= 'A' && c <= 'Z')
                c = c - ('A' - 'a');
        }
        return s;
    }

}
