#pragma once
#include "DataLoader.h"
#include "DataMaker.h"
#include "File.h"
#include "Logger.h"
#include "ParametersLoader.h"
#include "ReportWriter.h"
#include "Simulator.h"
#include "StateExporter.h"
#include "Timer.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <set>

namespace decov
{
/// Top level class to run a simulation.
struct Simulation final
{
    Parameters parameters;
    SimulationState *state = nullptr;
    Timer timer;

    OutputFile &report;
    OutputFile &csvReport;
    OutputFile &geoReport;
    OutputFile &cmReport;

    std::vector<Indicators> results;
    StateExporter exporter;

    int seed;

    Simulation(int seed,
               InputFile &paramFile, InputFile &dataFile, InputFile &loadFile,
               OutputFile &report, OutputFile &csvReport,
               OutputFile &geoReport, OutputFile &cmReport,
               OutputFile &saveFile)
        : report(report),
          csvReport(csvReport),
          geoReport(geoReport),
          cmReport(cmReport),
          seed(seed)
    {
        Timer timer;
        timer.start();

        // Set parameters
        if (paramFile)
        {
            std::cerr << "Load parameters: " << paramFile.path << std::endl;
            ParametersLoader(parameters, seed).load(*paramFile);
        }

        state = new SimulationState(parameters);

        // Initialize populations and spots
        if (loadFile)
        {
            std::cerr << "Load state: " << loadFile.path << std::endl;
            nlohmann::json j = nlohmann::json::from_bson(*loadFile);
            //nlohmann::json j; (*loadFile) >> j;
            from_json(j, *state);
        }
        else
        {
            // Load datas (must be done after parameters are set)
            std::cerr << "Load data: " << dataFile.path << std::endl;
            DataLoader(parameters, seed).load(*dataFile);
            std::cerr << "  Load time: " << timer.elapsed() << std::endl;

            std::cerr << "Make data" << std::endl;
            DataMaker dataMaker(parameters, *state, seed);
            dataMaker.make();
        }

        if (saveFile)
        {
            std::cerr << "Save state: " << saveFile.path << std::endl;
            nlohmann::json j;
            to_json(j, *state);
            nlohmann::json::to_bson(j, *saveFile);
            //(*saveFile) << j.dump(2);
        }

        std::cerr << "  Initialization time: " << timer.elapsed() << std::endl;

#if DECOV_DUMP_STATS == 1
        state->printStats();
#endif
    }

    ~Simulation() { delete state; }

    void logDay() const
    {
        std::cerr << "    S=" << state->indicators.count(Stages::Sensitive) << std::endl;
        std::cerr << "    A+I=" << state->indicators.contagious() << std::endl;
        std::cerr << "    D=" << state->indicators.count(Stages::Dead) << std::endl;
        std::cerr << "    R="
                  << (state->indicators.count(Stages::RecoveredMinor) +
                      state->indicators.count(Stages::RecoveredMajor))
                  << std::endl;
#if DECOV_DUMP_STATS == 1
        std::cerr << "    R0=" << state->indicators.R0 << std::endl;
#endif
    }

    void run(bool verbose = true)
    {
        results.push_back(state->indicators);

        Simulator simulator(parameters, *state, seed);

        std::cerr << "Run simulation for " << parameters.SimulationDuration << " days" << std::endl;
        if (verbose)
        {
            std::cerr << "  Initial situation:" << std::endl;
            logDay();
        }

        if (geoReport)
        {
	    simulator.alwaysKeepContacts = true;
            exporter.initialize(*state);
        }
        timer.start();
        while (true)
        {
            simulator.runDay();

            bool finished = state->day == parameters.SimulationDuration;
            if (parameters.EpidemyFinishedStop && state->indicators.contaminated() == 0)
            {
                std::cerr << "  Stop after day " << state->day << " because the epidemy is finished"
                          << std::endl;
                finished = true;
            }

            if ((double)parameters.DeadRatioThresholdStop * state->agents.size() <
                state->indicators.count(Stages::Dead))
            {
                std::cerr << "  Stop after day " << state->day << " because too much dead" << std::endl;
                finished = true;
            }

            if (finished || state->day % 7 == 0)
            {
                if (verbose)
                {
                    std::cerr << "  After week " << state->day / 7 << ":" << std::endl;
                    logDay();
                }

                if (geoReport)
                {
                    exporter.addRecord(*state);
                }
            }

            results.push_back(state->indicators);
            if (finished)
                break;
        }

        std::cerr << "  Simulation time: " << (timer.elapsed() / state->day) << " per day" << std::endl << std::endl;

        writeReports();
    }

    void writeReports()
    {
        ReportWriter rw(results, parameters);

        if (report)
        {
            std::cerr << "Generate json report: " << report.path << std::endl;
            rw.writeJsonReport(*report);
        }

        if (csvReport)
        {
            std::cerr << "Generate csv report: " << csvReport.path << std::endl;
            rw.writeCSVReport(*csvReport);
        }

        if (geoReport)
        {
            std::cerr << "Write geo report: " << geoReport.path << std::endl;
            exporter.writeJsonExport(*geoReport);
        }

        if (cmReport)
        {
            std::cerr << "Write contacts matrices: " << cmReport.path << std::endl;
            rw.writeContactMatrix(*cmReport);
        }
    }
};
} // namespace decov
