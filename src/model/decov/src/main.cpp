/*!
 * \file main.cpp
 * \brief Start the simulation
 * \author Sam
 * \version 0.1
 */

#include "Options.h"
#include "Simulation.h"

using namespace decov;
int main(int argc, const char **argv)
{
    Options o;
    o.onError([](const std::string &msg) {
        std::cerr << msg << "\n\nPass option -h for help\n\n";
        std::exit(1);
    });
    o.add<bool>("help", "Write this help and exit", false, "h", [&o](auto &v) {
        if (!std::get<bool>(v))
            return;
        std::cerr << "decov: micro-agents-based simulator for coronavirus propagation.\n\nOptions:\n";
        o.help();
        std::exit(0);
    });
    o.add<int>("seed", "Seed to initialize random engine", 0, "s");
    o.add<std::string>("report", "Write json report in given file", "", "r");
    o.add<std::string>("csv-report", "Write raw CSV report in given file", "", "csv");
    o.add<std::string>("geo-report", "Write a geographic-based json report", "", "geo");
    o.add<std::string>("contacts-matrix", "Write contacts matrices in given json file", "", "cm");
    o.add<std::string>("parameters", "Read parameters from given json file", "", "p");
    o.add<std::string>("data", "Create initial state using statistical data read from given json file", "", "d");
    o.add<std::string>("load", "Load initial state from given file. Some parameters won't be taken into account.");
    o.add<std::string>("save", "Save initial state into given file");
    o.add<bool>("verbose", "Output verbose information each week", false, "v");

    o.parse(argc, argv);

    InputFile parameters(o["parameters"]);
    InputFile data(o["data"]);
    InputFile loadFile(o["load"]);
    OutputFile report(o["report"]);
    OutputFile csvReport(o["csv-report"]);
    OutputFile geoReport(o["geo-report"]);
    OutputFile cmReport(o["contacts-matrix"]);
    OutputFile saveFile(o["save"]);

    if (!((bool)data ^ (bool)loadFile))
    {
        std::cerr << "You must choose between options -data and -load.\n";
        return 1;
    }

    if (! parameters)
    {
        std::cerr << "You must use a parameters file.\n";
        return 1;
    }

    int seed = o["seed"];

    Simulation s(seed,
                 parameters, data, loadFile,
                 report, csvReport,
                 geoReport, cmReport,
                 saveFile);

    s.run(o["verbose"]);

    return 0;
}
