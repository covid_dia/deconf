#pragma once
#include "Parameters.h"
#include "Random.h"
#include "SimulationState.h"
#include "SpatialIndex.h"

namespace decov
{
/// Populates the initial state of simulation according to parameters
class DataMaker
{
    Parameters &parameters;
    SimulationState &state;
    Random re;

    struct AreaStageCounts
    {
        EnumArray<int, Stages> remaining;
        int sum = 0;
    };

    // Precomputed stages for every agents to prevent sampling rounding problem
    // as much as possible
    std::vector<AreaStageCounts> areaStageCounts;

public:
    DataMaker(Parameters &parameters, SimulationState &state, int seed)
        : parameters(parameters), state(state)
    {
        #pragma omp parallel private(re)
        {
            re.setSeed(seed);
        }

        // ENABLE_TRACE_FOR(this);
    }

    void make()
    {
        initializeHospitalSaturation();
        makeStageRepartition();
        makeHomes();
        makeOtherSpots();
        assignAgentsToSpots();
    }

private:
    // Initialized SAT indicator based on target population and initial ratios to use the right
    // probability to die during init
    void initializeHospitalSaturation()
    {
#pragma omp parallel for
        for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
        {
            auto &area = parameters.Areas[areaId];
            float hospitalizedRatio = area.InitialStagesRatios[Stages::InfectedMajor];
            int hospitalized = (int)(0.5f + (float)area.Agents * hospitalizedRatio);
            int beds = area.HospitalBeds;
            auto &areaIndicators = state.indicators.areas[areaId];
            if (hospitalized <= beds)
                areaIndicators.satRatio = 1.0f;
            else
                areaIndicators.satRatio = (float)beds / (float)hospitalized;
        }
    }

    void makeStageRepartition()
    {
        TRACE_FUNC("");

        // Precompute the number of agents of each stage per area
        areaStageCounts.resize(parameters.Areas.size());

        for (int s = 0; s < (int)Stages::COUNT; ++s)
        {
            auto stage = (Stages)s;

            float total_f = 0;
            for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
            {
                auto &area = parameters.Areas[areaId];
                total_f += (float)area.Agents * area.InitialStagesRatios[stage];
            }

            int remaining = (int)(0.5f + total_f);

            for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
            {
                auto &area = parameters.Areas[areaId];
                AreaStageCounts &asc = areaStageCounts[areaId];

                ASSERT(remaining >= 0);

                if (areaId == (int)parameters.Areas.size() - 1)
                {
                    asc.remaining[stage] = remaining;
                    asc.sum += remaining;
                }
                else
                {
                    int n = std::min(remaining,
                                     re.round(area.InitialStagesRatios[stage] * (float)area.Agents));
                    asc.remaining[stage] = n;
                    asc.sum += n;
                    remaining -= n;
                }
            }
        }

        // Adjust the number of sensitive agents
        for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
        {
            auto &area = parameters.Areas[areaId];
            AreaStageCounts &asc = areaStageCounts[areaId];
            int delta = asc.sum - area.Agents;
            asc.sum -= delta;
            asc.remaining[Stages::Sensitive] -= delta;
            if (asc.remaining[Stages::Sensitive] < 0)
                throw Error("Not enough initial sensitive agents for correct rounding");
        }
    }

    void makeHomes()
    {
        TRACE_FUNC("");

        // Store home spots by kind
        EnumArray<std::vector<SpotIndex>, HomeKinds> homesByKind;

        // Keep current index to assign by kind for children and adults
        EnumArray<int, HomeKinds> nextHome;

        std::vector<std::pair<AgentId,HomeKinds>> children;

        for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
        {
            auto &area = parameters.Areas[areaId];

            homesByKind.clear();
            nextHome.clear();
            children.clear();

            // Count number of spots in this area
            int numSpots = 0;
            SpotIndex firstIndexInArea = state.spots[SpotKinds::Home].size();

            // Create the right number of homes for this area, following the density
            for (auto &municipality : area.Municipalities)
            {
                float numHomes = (float)(area.NumHomes * municipality.Population) / (float)area.Agents;

                for (int k = 0; k < (int) HomeKinds::COUNT; ++k)
                {
                    auto homeKind = (HomeKinds) k;
                    int numHomesForThisKind = std::max(1, re.round(area.NumHomesByKind[homeKind] * numHomes));

                    for (int h = 0; h < numHomesForThisKind; ++h)
                    {
                        int spotIndex = createSpot(areaId, municipality, SpotKinds::Home, 0);
                        homesByKind[homeKind].push_back(spotIndex);
                        numSpots ++;
                    }
                }
            }

#if DECOV_DUMP_STATS == 1 && !defined(_OPENMP)
            std::cerr << "Agent=" << area.Agents << "\n";
            int totalHomes=0;
            for (int k = 0; k < (int) HomeKinds::COUNT; ++k)
            {
                auto kind = (HomeKinds) k;
                totalHomes += homesByKind[kind].size();
                std::cerr << kind << ": " << homesByKind[kind].size() << ": ";
                for (int index: homesByKind[kind])                
                {
                    std::cerr << index << ",";
                }
                std::cerr << "\n";
            }
            std::cerr << "Total homes=" << totalHomes << "\n";
            EnumArray<int, HomeKinds> agentsByHomeKind;
#endif

            // Create agents and assign them to homes
            for (int a = 0; a < area.Agents; ++a)
            {
                int agentId = createAgent(areaId);

                int age = state.agents[agentId].age;
                auto homeKind = (HomeKinds) re.select(area.HomesDistribution[age]._array, 1.0f);

#if DECOV_DUMP_STATS == 1 && !defined(_OPENMP)
                agentsByHomeKind[homeKind]++;
#endif
                int spotIndex;

                if (age <= Parameters::SchoolAgeLimit &&
                    (int) homeKind >= (int) HomeKinds::SingleParentUnder12Child // else considered as an adult
                    )
                {
                    children.emplace_back(agentId, homeKind);
                    continue;
                }
                else if (homeKind == HomeKinds::Other || nextHome[homeKind] == (int)homesByKind[homeKind].size())
                {
                    // Fallback to Other kind
                    homeKind = HomeKinds::Other;
                    // No composition constraint: just circle through all
                    spotIndex = homesByKind[homeKind][nextHome[homeKind]];
                    nextHome[homeKind] = (nextHome[homeKind] + 1) % (int)homesByKind[homeKind].size();
                }
                else
                {
                    // Assign to the next free home regarding the number of adults
                    spotIndex = homesByKind[homeKind][nextHome[homeKind]];
                    if (numAdults(spotIndex, homeKind) + 1 == numAdults(homeKind))
                    {
                        nextHome[homeKind]++;
                    }
                }

                Spot &home = state.spots[SpotKinds::Home][spotIndex];
                home.size++;
                state.assignAgent(agentId, spotIndex, SpotKinds::Home);
            }

            // Assign children to homes
            nextHome.clear();
            for (auto pair: children)
            {
                int agentId = pair.first;
                HomeKinds homeKind = pair.second;

                if (nextHome[homeKind] == (int)homesByKind[homeKind].size())
                    homeKind = HomeKinds::Other; // TODO: and what if no other spot?

                int spotIndex = homesByKind[homeKind][nextHome[homeKind]];
                Spot * home = & state.spots[SpotKinds::Home][spotIndex];
                if (home->size == 0)
                {
                    // No adult: ignore this spot and the remaining
                    if (nextHome[homeKind] == 0)
                        homeKind = HomeKinds::Other;
                    else
                        nextHome[homeKind] = 0;
                    spotIndex = homesByKind[homeKind][nextHome[homeKind]];
                    home = & state.spots[SpotKinds::Home][spotIndex];
                }

                nextHome[homeKind] = (nextHome[homeKind] + 1) % (int) homesByKind[homeKind].size();
                
                home->size++;
                state.assignAgent(agentId, spotIndex, SpotKinds::Home);
            }

#if DECOV_DUMP_STATS == 1 && !defined(_OPENMP)
            for (int k = 0; k < (int) HomeKinds::COUNT; ++k)
            {
                auto kind = (HomeKinds) k;
                std::cerr << kind << ": " << agentsByHomeKind[kind] << " agents\n";
                std::cerr << " adult index=" << nextHome[kind] << "\n";           
            }

            int ok = 0;
            int numWithChild = 0;
            int totalChildren = 0;
#endif

            // Compute telework, active and at_risk statuses per home
            for (int k = 0; k < (int) HomeKinds::COUNT; ++k)
            {
                auto homeKind = (HomeKinds) k;
                auto & homes = homesByKind[homeKind];
                for (int spotIndex: homes)
                {
                    Spot & home = state.spots[SpotKinds::Home][spotIndex];
                    if (home.size == 0)
                        continue;

                    int numWorking = re.round(home.size * area.WorkingRatios[homeKind]);

                    // Sort agents from the most probable to work to the less:
                    // First over 25, then over 65, then under 25, from older to younger
                    auto adjustAge = [](int age){
                        if (age > Parameters::SchoolAgeLimit && age < Parameters::SeniorAge)
                            return age + Parameters::MaxAge;
                        return age;
                    };

                    auto sortedAgents = home.agents;
                    std::sort(sortedAgents.begin(), sortedAgents.end(), [&](AgentId a1, AgentId a2) {
                        int age1 = adjustAge(state.agents[a1].age);
                        int age2 = adjustAge(state.agents[a2].age);
                        return age2 < age1;
                    });

#if DECOV_DUMP_STATS == 1 && ! defined(_OPENMP)
                    // Checks homes composition is good
                    checkHomeComposition(home, spotIndex, homeKind, sortedAgents, ok, numWithChild, totalChildren);
#endif

                    bool atRisk = false;
                    for (int a = 0; a < (int)sortedAgents.size(); a++)
                    {
                        Agent & agent = state.agents[sortedAgents[a]];
                        atRisk |= agent.atRisk;
                    }

                    int notWorkingAdult = 0;
                    bool someCanTelework = false;
                    for (int a = 0; a < (int)sortedAgents.size(); a++)
                    {
                        Agent & agent = state.agents[sortedAgents[a]];

                        agent.atRiskAtHome = atRisk;

                        if (numWorking > 0 && agent.age >= Parameters::MinWorkingAge)
                        {
                            if (re.rand() < area.CannotTeleworkRatio)
                                agent.teleworkStatus = TeleworkStatuses::CannotTelework;
                            else
                            {
                                agent.teleworkStatus = TeleworkStatuses::CanTelework;
                                someCanTelework = true;
                            }
                            numWorking--;
//#pragma omp atomic
                            area.ActiveAgents++;
                        }
                        else if (agent.age > Parameters::SchoolAgeLimit)
                        {
                            notWorkingAdult++;
                        }
                        else
                        {
//#pragma omp atomic
                            area.AtSchoolAgents++;
                        }
                    }

                    if (notWorkingAdult == 0)
                    {
                        // Adjust telework status for one working adult
                        Agent & youngest = state.agents[sortedAgents.back()];
                        if (youngest.age < 15)
                        {
                            for (int a = 0; a < (int)home.agents.size(); a++)
                            {
                                Agent & agent = state.agents[home.agents[a]];
                                if (someCanTelework && agent.teleworkStatus != TeleworkStatuses::CanTelework)
                                    continue;
                                agent.childrenToKeep = youngest.age < 12 ? ChildrenToKeep::Under12
                                                                         : ChildrenToKeep::Under15;
                                break;
                            }
                        }
                    }
                }
            }

            // Remove empty homes
            auto & homes = state.spots[SpotKinds::Home];
            for (int spotIndex = firstIndexInArea; spotIndex < (int)homes.size(); ++spotIndex)
            {
                Spot & home = homes[spotIndex];
                if (home.size > 0)
                    continue;
                if (spotIndex < (int) homes.size() - 1)
                {
                    std::swap(homes[spotIndex], homes[homes.size() - 1]);
                    // Change spotIndex for assigned agents
                    for (int agentId : homes[spotIndex].agents)
                    {
                        state.agents[agentId].spots[SpotKinds::Home].first = spotIndex;
                    }
                }
                homes.pop_back();                
                spotIndex--;
		        numSpots--;
            }

#if DECOV_DUMP_STATS == 1 && ! defined(_OPENMP)
            std::cerr << "Goog compositions: " << ok << "/" << numSpots << "\n";
            std::cerr << "Home with child: " << numWithChild << " with avg of " << (float) totalChildren / numWithChild << " children\n";
            if ((float)ok / numSpots < .85f)
                throw Error("Too much erroneous homes");
#endif
        }

        // Initialize contact matrix now we know the precise number of agents
#if DECOV_TRACE_CONTACTS == 1
        state.contactHistory.resize((int)state.agents.size());
#endif

#if DECOV_DUMP_STATS
        // Check that initial number of infected agents is right for each stage
        for (int s = 0; s < (int)Stages::COUNT; ++s)
        {
            float expected = 0;
            float actual = 0;
            auto stage = (Stages)s;
            for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
            {
                actual += state.indicators.areas[areaId].counts[stage];

                auto &area = parameters.Areas[areaId];
                expected += (float)area.Agents * area.InitialStagesRatios[stage];
            }

            std::cerr << "Initial stage " << stage << ": expected=" << expected << ", actual=" << actual
                      << std::endl;
        }
#endif
    }

    void makeOtherSpots()
    {
        TRACE_FUNC("");

        // For each area, create the right number of spots in each municipality
        for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
        {
            auto &area = parameters.Areas[areaId];

            TRACE("Area", area.Name, "agents=", area.Agents);

#pragma omp parallel for private(re)
            for (int s = (int)SpotKinds::Home + 1; s < (int)SpotKinds::COUNT; ++s)
            {
                auto spotKind = (SpotKinds)s;
                const HistogramDistribution<int, float> *sizeDistribution;
                switch (spotKind)
                {
                case SpotKinds::Work:
                    sizeDistribution = &area.WorkplaceSizes;
                    break;
                case SpotKinds::School:
                    sizeDistribution = &area.SchoolSizes;
                    break;
                case SpotKinds::Neighborhood1:
                    sizeDistribution = &parameters.Neighborhood1Sizes;
                    break;
                case SpotKinds::Neighborhood2:
                    sizeDistribution = &parameters.Neighborhood2Sizes;
                    break;
                default:
                    throw Error("Unexpected spot kind: ", spotKind);
                }

                int delta = 0;
                int pop;
                if (spotKind == SpotKinds::Work)
                    pop = area.ActiveAgents;
                else if (spotKind == SpotKinds::School)
                    pop = area.AtSchoolAgents;
                else
                    pop = area.Agents;
                int room = 0;

                for (auto &municipality : area.Municipalities)
                {
                    bool last = &municipality == &area.Municipalities.back();
                    int agents;
                    if (last)
                        agents = pop - room;
                    else
                        agents = std::min(pop - room, re.round((float)municipality.Population *
                                                               (float)pop / (float)area.Agents) +
                                                          delta);
                    while (agents > 0)
                    {
                        int spotSize = std::min(agents, sizeDistribution->sample(re));
                        if (last)
                        {
                            int nextRemaining = agents - spotSize;
                            if (nextRemaining > 0)
                            {
                                int nextLackingAgents = sizeDistribution->min() - nextRemaining;
                                if (nextLackingAgents > 0 &&
                                    spotSize - nextLackingAgents >= sizeDistribution->min())
                                    // Adjust size of last two spots
                                    spotSize -= nextLackingAgents;
                            }
                        }

                        createSpot(areaId, municipality, spotKind, spotSize);

                        agents -= spotSize;
                        room += spotSize;
                    }

                    // If too many places were created, decrease the places for next municipality
                    // in order to keep average filling of spots at a maximum.
                    delta += agents;
                }

                if (room != pop)
                    throw Error("Failed to create the needed spots for target population");
            }
        }
    }

    // Creates a inactive agent following the age pyramid and at risk factor by age for given area
    AgentId createAgent(int areaId)
    {
        auto &area = parameters.Areas[areaId];

        // Peek stage at random
        auto &asc = areaStageCounts[areaId];
        auto stage = (Stages)re.select(asc.remaining._array, asc.sum);
        asc.remaining[stage]--;
        asc.sum--;

        // Peek age at random
        int age = area.Ages.sample(re);
        ASSERT(age <= Parameters::MaxAge);
        bool atRisk = re.rand() < area.AtRiskPersons[age];

        // Prepare next stage change
        auto r = parameters.sampleStageDuration(stage, re, state.indicators.areas[areaId].satRatio);
        Stages nextStage = r.first;
        int duration = r.second;
        if (duration > 1)
            duration = re.rand<int>(1, duration);

        return state.addAgent(areaId, age, atRisk, TeleworkStatuses::NotActive, stage, nextStage, duration);
    }

    void assignAgentsToSpots()
    {
        TRACE_FUNC("");

        #pragma omp parallel for private(re)
        for (int sk = (int)SpotKinds::Home + 1; sk < (int)SpotKinds::COUNT; ++sk)
        {
            auto spotKind = (SpotKinds)sk;
            TRACE("Assign", spotKind);

            int n = parameters.SpotsScatteringFactor[spotKind];
            SpatialIndex index(state.spots[spotKind]);

            for (int agentId = 0; agentId < (int)state.agents.size(); ++agentId)
            {
                auto &agent = state.agents[agentId];
                if (spotKind == SpotKinds::Work && agent.teleworkStatus == TeleworkStatuses::NotActive)
                    // Only assign active agent to work
                    continue;
                if (spotKind == SpotKinds::School && !agent.atSchool())
                    // Only assign not active young people to school
                    continue;

                const auto &closest = index.getClosestFreeSpots(state.getHome(agent), n);

                if (closest.empty())
                    throw Error("No more free ", spotKind, " spots");

                SpotIndex chosen = re.choice(closest.begin(), closest.end());
                state.assignAgent(agentId, chosen, spotKind);
            }
        }
    }

    SpotIndex createSpot(AreaId areaId, Municipality municipality, SpotKinds spotKind, int spotSize)
    {
        float angle = re.rand() * 2 * M_PI;
        float dist = re.rand() * municipality.Radius;
        float x = municipality.X + std::cos(angle) * dist;
        float y = municipality.Y + std::sin(angle) * dist;
        return state.addSpot(spotKind, spotSize, x, y, areaId);
    }

    int numAdults(int spotIndex, HomeKinds homeKind) const
    {
        Spot & home = state.spots[SpotKinds::Home][spotIndex];
        int count = 0;
        for (int agentId: home.agents)
        {
            int age = state.agents[agentId].age;
            if (age > Parameters::SchoolAgeLimit)
                count++;
            else if (age > 14 && (int)homeKind <= (int)HomeKinds::Couple)
                // Under 25 in these kinds are not considered as children
                count++;
        }
        return count;
    }

    int numAdults(HomeKinds kind) const
    {
        switch (kind)
        {
        case HomeKinds::Alone:
        case HomeKinds::AloneSenior:
        case HomeKinds::SingleParentOver11Children:
        case HomeKinds::SingleParentUnder12Child:
            return 1;
        case HomeKinds::CoupleSenior:
        case HomeKinds::Couple:
        case HomeKinds::CoupleUnder12Child:
        case HomeKinds::CoupleOver11Children:
            return 2;
        default:
            throw;
        }
    }

    void checkHomeComposition(const Spot & home, int spotIndex, HomeKinds homeKind, const std::vector<AgentId> & sortedAgents, int & ok, int & numWithChild, int & totalChildren) const
    {
        int last_age = state.agents[sortedAgents.back()].age;
        switch (homeKind)
        {
        case HomeKinds::AloneSenior:
            ASSERT(home.size == 1);
            if (last_age >= Parameters::SeniorAge)
                ok++;
            else
                std::cerr << "Bad " << homeKind << ": last_age=" << last_age << "\n";
            break;
        case HomeKinds::Alone:
            ASSERT(home.size == 1);
            if (last_age < Parameters::SeniorAge)
                ok++;
            else
                std::cerr << "Bad " << homeKind << ": last_age=" << last_age << "\n";
            break;
        case HomeKinds::CoupleSenior:
            if (home.size == 2 && last_age >= Parameters::SeniorAge)
                ok++;
            else
                std::cerr << "Bad " << homeKind << ": size=" << home.size << ", last_age=" << last_age << "\n";
            break;
        case HomeKinds::Couple:
            if (home.size == 2 && last_age < Parameters::SeniorAge)
                ok++;
            else
                std::cerr << "Bad " << homeKind << ": size=" << home.size << ", last_age=" << last_age << "\n";
            break;
        case HomeKinds::SingleParentUnder12Child:
            if (numAdults(spotIndex, homeKind) == 1 && last_age <= 11)
            {
                ok++;
                numWithChild++;
                totalChildren += home.size - 1;
            }
            else
                std::cerr << "Bad " << homeKind << ": numAdults=" << numAdults(spotIndex, homeKind) << ", last_age=" << last_age << "\n";
            break;
        case HomeKinds::SingleParentOver11Children:
            if (numAdults(spotIndex, homeKind) == 1 && last_age > 11)
            {
                ok++;
                numWithChild++;
                totalChildren += home.size - 1;
            }
            else
                std::cerr << "Bad " << homeKind << ": numAdults=" << numAdults(spotIndex, homeKind) << ", last_age=" << last_age << "\n";
            break;
        case HomeKinds::CoupleUnder12Child:
            if (numAdults(spotIndex, homeKind) == 2 && last_age <= 11)
            {
                ok++;
                numWithChild++;
                totalChildren += home.size - 2;
            }
            else
                std::cerr << "Bad " << homeKind << ": numAdults=" << numAdults(spotIndex, homeKind) << ", last_age=" << last_age << "\n";
            break;
        case HomeKinds::CoupleOver11Children:
            if (numAdults(spotIndex, homeKind) == 2 && last_age > 11)
            {
                ok++;
                numWithChild++;
                totalChildren += home.size - 2;
            }
            else
                std::cerr << "Bad " << homeKind << ": numAdults=" << numAdults(spotIndex, homeKind) << ", last_age=" << last_age << "\n";
            break;
        case HomeKinds::Other:
            ok++;
            break;
        default:
            throw;
        }
    }
};
} // namespace decov
