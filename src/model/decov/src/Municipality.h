#pragma once
namespace decov
{
    struct Municipality
    {
        int Population; // number of inhabitants after sampling
        float Radius;   // in km
        float X;        // geographical coordinates of barycenter, in km
        float Y;
    };
}
