#pragma once
#include <functional>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <cxxabi.h>
#include "Types.h"

# define _DECOV_CONCAT_LAZY(x, y) x ## y
# define DECOV_CONCAT(x, y) _DECOV_CONCAT_LAZY(x, y)
# define _DECOV_STR_LAZY(x) #x
# define DECOV_STR(x) _DECOV_STR_LAZY(x)

#if DECOV_ENABLE_TRACE == 1
# define TRACE(...) Logger::instance()->log(this, __VA_ARGS__)
# define TRACE_FUNC(...) \
  Logger::Indenter DECOV_CONCAT(autoindent_, __COUNTER__) \
  (decov::Logger::instance()->logEnabled[this] ? 1 : 0);  \
  TRACE(__FILE__ ":" DECOV_STR(__LINE__), __func__, "(", __VA_ARGS__,")")
# define ENABLE_TRACE_FOR(o) decov::Logger::instance()->logEnabled[o] = true;
#else
# define TRACE(...)
# define INDENT
# define TRACE_FUNC(...)
# define ENABLE_TRACE_FOR(...) 
#endif

namespace decov
{
    template <typename Arg, typename ...Args>
    static void joinRec(std::ostream & os, const std::string & sep, Arg & arg, Args ...args)
    {
        os << arg;
        if constexpr (sizeof...(Args) > 0)
        {
            os << sep;
            joinRec(os, sep, args...);
        }
    }

    /// Join arguments into a string.
    template <typename ...Args>
    static std::string join(const std::string & sep, Args ...args)
    {
        std::ostringstream oss;
        joinRec(oss, sep, args...);
        return oss.str();
    }

    /// A simple logger, just for debugging purpose.
    struct Logger
    {
        struct Indenter
        {
            int _level;

            Indenter(int level=1)
              : _level(level)
            {
                Logger::instance()->indent += _level;
            }

            ~Indenter()
            {
                Logger::instance()->indent -= _level;
            }
        };

        /// Logs the given arguments separated by a whitespace
        template <typename Class, typename ...Args>
        void log(Class * obj, Args ...args)
        {
            if (! logEnabled[obj])
                return;
            if (callback != nullptr)
                callback(std::cerr);
            std::cerr << std::string(indent, ' ');
            std::cerr << type_name<Class>() << ": ";
            joinRec(std::cerr, " ", args...);
            std::cerr << '\n';
        }

        /// \return the Logger singleton
        static Logger * instance()
        {
            static Logger logger;
            return & logger;
        }

        int indent;
        std::function<void(std::ostream&)> callback = nullptr;
        std::unordered_map<const void*,bool> logEnabled;
    };
}
