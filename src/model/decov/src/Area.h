#pragma once
#include "Distributions.h"
#include "Municipality.h"
namespace decov
{
/// Properties specific to a geographic area
struct Area
{
    // TODO: separated classes for data used only by data maker from data used during simulation?
    std::string Name;

    /// Number of agents after sampling, only used by DataMaker
    int Agents = 0;
   
    // Temporay, set and used by DataMaker
    int ActiveAgents = 0;

    // Temporay, set and used by DataMaker
    int AtSchoolAgents = 0;
    
    /// Total number of beds in this region (with sampling already taken into account)
    /// This is ICUBeds / ICURatio
    int HospitalBeds = 162;

    /// Ratios of agents at the begining of each given stage at first day of simulation (whatever
    /// their age)
    EnumArray<float, Stages> InitialStagesRatios = {{{
        0.99f,  // S
        0.009f, // E1
        0.001f, // E2
        0,      // A1
        0,      // A2
        0,      // I1
        0,      // I2
        0,      // R1
        0,      // R2
        0,      // M
    }}};

    //@{
    /// Political measurs.
    /// In weeks from the start of the simulation
    std::vector<ForcedTeleworkValues> ForcedTelework;
    std::vector<SchoolClosureValues> SchoolClosure;
    std::vector<SocialDistancingValues> SocialDistancing;
    std::vector<HighRiskLockDownValues> HighRiskLockDown;
    std::vector<ContactTracingValues> ContactTracing;
    //@}


    // Everything below comes from data json and is used by DataMaker only

    /// Active agents who still go to work in spite of measures (agricultors, hospitals, authorized
    /// shops...)
    float CannotTeleworkRatio;

    /// School sizes distribution
    HistogramDistribution<int, float> SchoolSizes;

    /// Workplace sizes distribution
    HistogramDistribution<int, float> WorkplaceSizes;

    /// Distribution of at risk people by age
    ListDistribution<float> AtRiskPersons;

    /// For each day of the simulation
    std::vector<bool> SchoolHolidays;

    /// List of existing municipalities for this area
    std::vector<Municipality> Municipalities;

    /// Total number of homes in this area, after sampling
    int NumHomes;

    /// Ratio of homes of each kind, total makes 1
    EnumArray<float, HomeKinds> NumHomesByKind;

    /// Probability to work for each agent belonging to a home, by kind
    EnumArray<float, HomeKinds> WorkingRatios;

    /// Age pyramid
    HistogramDistribution<int, float> Ages;

    /// Distribution of agents by age among home kinds (sum = 1 for each kind)
    ListDistribution<EnumArray<float, HomeKinds>> HomesDistribution;
};
} // namespace decov
