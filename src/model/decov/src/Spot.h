#pragma once
#include "Definitions.h"
#include <vector>
#include <numeric>
#include <third_parties/nlohmann/json.hpp>
namespace decov
{
/// A spot represents any place in which an agent can meet another one.
struct Spot
{
    int size;         //< Max number of agent assigned
    float x, y;       //< Geographical coordinates in km (origin has no matter)
    AreaId areaId;    //< Id of the area this spot belongs to

    std::vector<AgentId> agents;       //< Ids of the agents associated to this spot
    std::vector<float> presenceRatios; //< Presence ratios for current day, for each assigned agent

#if DECOV_TRACE_CONTACTS == 1
    // Number of days remaining for lock-down (for home only)
    int lockDownRemainingDays = 0;
#endif

    Spot()
    { }

    Spot(int size, float x, float y, AreaId areaId)
        : size(size),
          x(x),
          y(y),
          areaId(areaId)
    {
    }

    /// \return true if the number of agent equal the spot size else false.
    bool full() const
    {
        return (int)agents.size() == size;
    }

    /// Assigns an agent to a spot.
    ///
    /// \param agentId, the agent to be stored
    /// \return indexInSpot, the index of the agent in the spot
    IndexInSpot assign(AgentId agentId)
    {
        ASSERT(! full());
        ASSERT(std::find(agents.begin(), agents.end(), agentId) == agents.end());
        agents.push_back(agentId);
        presenceRatios.push_back(0);
        return (IndexInSpot)agents.size() - 1;
    }

    friend std::ostream & operator << (std::ostream & os, const Spot & spot)
    {
        os << "Spot with " << spot.agents.size() << "/" << spot.size << " at " << spot.x << "," << spot.y << " in area " << spot.areaId;
        return os;
    }

    using json = nlohmann::json;

    friend void to_json(json & j, const Spot & s)
    {
        j = json{{"size", s.size},
                 {"x", s.x},
                 {"y", s.y},
                 {"areaId", s.areaId},
                 {"agents", s.agents}};
                 // presenceRatios not serialized
                 // lockDownRemainingDays not serialized
    }

    friend void from_json(const json & j, Spot & s)
    {
        j.at("size").get_to(s.size);
        j.at("x").get_to(s.x);
        j.at("y").get_to(s.y);
        j.at("areaId").get_to(s.areaId);
        j.at("agents").get_to(s.agents);
        s.presenceRatios.resize(s.agents.size());
    }
};

/// A callable to found the closest spots from another one.
struct SpotDistanceComparer
{
    const std::vector<Spot> & spots;
    const Spot *ref = nullptr;

    SpotDistanceComparer(const std::vector<Spot> & spots)
      : spots(spots)
    {}

    void setRef(const Spot *ref)
    {
        this->ref = ref;
    }

    static constexpr float distance2(const Spot *s1, const Spot *s2)
    {
        float dx = (s1->x - s2->x);
        float dy = (s1->y - s2->y);
        return dx * dx + dy * dy;
    }

    /// \return true if s1 is closer than s2
    bool operator()(SpotIndex i1, SpotIndex i2) const
    {
        return distance2(& spots[i1], ref) < distance2(& spots[i2], ref);
    }
};
}
