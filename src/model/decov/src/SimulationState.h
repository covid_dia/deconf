#pragma once
#include "Agent.h"
#include "ContactHistory.h"
#include "Indicators.h"
#include "Random.h"
#include "Spot.h"
#include <vector>
namespace decov
{
/// This class contains the whole internal state of the simulation.
struct SimulationState
{
    /// List of spots for each kind
    EnumArray<std::vector<Spot>, SpotKinds> spots;

    /// List of agents
    std::vector<Agent> agents;

    /// Next day to simulate
    int day = 0;

    /// Observable variables, output of the simulation by area
    Indicators indicators;

#if DECOV_TRACE_CONTACTS == 1
    ContactHistory contactHistory;
#endif

    SimulationState(Parameters &parameters)
        : indicators((int)parameters.Areas.size())
#if DECOV_TRACE_CONTACTS == 1
          ,
          contactHistory(parameters.TracedIncubationPeriod)
#endif
    {
    }

    SimulationState(const SimulationState &) = delete;

    //@{
    /// \return the AreaId of this agent's home
    const Spot &getHome(const Agent &agent) const
    {
        int homeIndex = agent.spots[SpotKinds::Home].first;
        return spots[SpotKinds::Home][homeIndex];
    }

    Spot &getHome(const Agent &agent)
    {
        int homeIndex = agent.spots[SpotKinds::Home].first;
        return spots[SpotKinds::Home][homeIndex];
    }
    //@}

    /// Creates and adds a spot
    /// \return a temporary reference, which remains valid as long as this
    /// function is not called another time.
    SpotIndex addSpot(SpotKinds kind, int size, float x, float y, AreaId areaId)
    {
        int index = (int)spots[kind].size();
        spots[kind].emplace_back(size, x, y, areaId);
        return index;
    }

    /// Creates and adds an agent
    template <typename... Args>
    AgentId addAgent(AreaId areaId, Args... args)
    {
        agents.emplace_back(args...);
        auto &agent = agents.back();
        indicators.areas[areaId].counts[agent.stage]++;
#if DECOV_DUMP_STATS == 1
        indicators.contactsMatrix.handleNewAgent(agent.age);
#endif
        return agents.size() - 1;
    }

    /// Assigns an Agent to a Spot
    void assignAgent(AgentId agentId, SpotIndex spotIndex, SpotKinds kind)
    {
        auto &agent = agents[agentId];
        Spot &spot = spots[kind][spotIndex];

        agent.spots[kind] = {spotIndex, spot.assign(agentId)};
    }

    /// Allocates a time to an Agent in a Spot
    void allocateTime(Agent &agent, SpotKinds spotKind, float timeRatio)
    {
        ASSERT(timeRatio >= 0 && timeRatio <= 1);
        ASSERT(agent.has(spotKind));

        auto &[spotIndex, index] = agent.spots[spotKind];
        Spot &spot = spots[spotKind][spotIndex];
        spot.presenceRatios[index] = timeRatio;
    }

    void printStats() const
    {
        // Compute and print stats
        std::cerr << "  Num agents: " << agents.size() << std::endl;
        for (int sk = 0; sk < (int)SpotKinds::COUNT; ++sk)
        {
            auto &spots = this->spots[(SpotKinds)sk];
            int assignedAgents = 0;
            int totalSize = 0;
            float distanceToHome = 0;
            int minSize = std::numeric_limits<int>::max();
            int maxSize = std::numeric_limits<int>::min();
            for (auto &spot : spots)
            {
                assignedAgents += spot.agents.size();
                totalSize += spot.size;

                minSize = std::min(spot.size, minSize);
                maxSize = std::max(spot.size, maxSize);

                for (AgentId agentId : spot.agents)
                {
                    const Agent &agent = agents[agentId];
                    distanceToHome += std::sqrt(SpotDistanceComparer::distance2(&getHome(agent), &spot));
                }
            }
            std::cerr << "  " << (SpotKinds)sk << "=" << spots.size()
                      << ", minSize=" << minSize
                      << ", maxSize=" << maxSize
                      << ", meanSize=" << (float)totalSize / (float)spots.size()
                      << ", fill ratio=" << ((float)assignedAgents / (float)totalSize)
                      << ", average distance to home=" << (distanceToHome / assignedAgents) << std::endl;
        }
    }

    using json = nlohmann::json;

    friend void to_json(json &j, const SimulationState &s)
    {
        j = json{{"spots", s.spots},
                 {"agents", s.agents},
                 // day not serialized
                 {"indicators", s.indicators}};
        // contactHistory not serialized
    }

    friend void from_json(const json &j, SimulationState &s)
    {
        j.at("spots").get_to(s.spots);
        j.at("agents").get_to(s.agents);
        j.at("indicators").get_to(s.indicators);

#if DECOV_DUMP_STATS == 1
        for (auto &agent : s.agents)
        {
            s.indicators.contactsMatrix.handleNewAgent(agent.age);
        }
#endif

#if DECOV_TRACE_CONTACTS == 1
        s.contactHistory.resize((int)s.agents.size());
#endif
    }
};
} // namespace decov
