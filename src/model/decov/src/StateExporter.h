#pragma once
#include "SimulationState.h"
#include <third_parties/nlohmann/json.hpp>
namespace decov
{
/// Exports the internal evolution of the simulation after it ended.
struct StateExporter
{
    using json = nlohmann::json;

    json root = json::object();

    void addRecord(const SimulationState &state)
    {
        json record = json::object();

        record["day"] = state.day;

        // Export agents
        json &agents = record["agents"] = json::array();

        AgentId agentIndex = 0;
        for (auto &agent : state.agents)
        {
            // const Spot & home = *agent.spots[SpotKinds::Home].first;

            json d = json::object();
            d["stage"] = get_enum_name(agent.stage);
            // d["x"] = home.x;
            // d["y"] = home.y;
            //
#if DECOV_TRACE_CONTACTS
            d["contacts"] = json::array();
            state.contactHistory.contacts.back().getContacts(
                agentIndex, [&](AgentId cid) { d["contacts"].push_back(cid); });
#endif
            agents.push_back(d);
            ++agentIndex;
        }

        root["weeks"].push_back(record);
    }

    void initialize(const SimulationState &state)
    {

        root["weeks"] = json::array();

        // Export spots
        json &spots = root["spots"] = json::array();
        auto spot_names = get_enum_names<SpotKinds>();
        for (auto i = 0; i < (int) SpotKinds::COUNT; ++i)
        {
	    for (auto j=0; j<(int)state.spots[(SpotKinds)i].size(); ++j)
            {
		auto & spot = state.spots[(SpotKinds)i][j];
                if (spot.agents.size() > 0)
                {
                    json d = json::object();
                    d["kind"] = spot_names[i];
                    d["x"] = spot.x;
                    d["y"] = spot.y;
                    d["size"] = spot.size;
                    d["id"] = i+j*((int) SpotKinds::COUNT);
                    d["area"] = (int)spot.areaId;
                    spots.push_back(d);
                }
            }
        }
        // Export agents
        json &agents = root["agents"] = json::array();
        for (auto &agent : state.agents)
        {
            json d = json::object();
            json &assigned = d["assigned"] = json::array();
            d["age"] = agent.age;
	    for(auto j = 0; j<(int) SpotKinds::COUNT; ++j){
		    if(agent.spots[(SpotKinds) j].first>=0){
			    assigned.push_back(agent.spots[(SpotKinds) j].first*(int)SpotKinds::COUNT + j);
		    }
	    }
            agents.push_back(d);
        }
    }

    void writeJsonExport(std::ostream &os) { os << root.dump(); }
};
} // namespace decov
