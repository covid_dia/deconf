#pragma once
#include "Spot.h"
#include "SpotGrid.h"
#include "SpotSorter.h"
#include <deque>
namespace decov
{
/// To find the top n closest free spots a one kind from a particular home.
struct SpatialIndex
{
    int counter = -1;
    std::deque<std::pair<int,int>> active;
    SpotGrid grid;
    SpotSorter sorter;

    SpatialIndex(std::vector<Spot> & spots)
      : grid(spots),
        sorter(spots)
    { 
        //ENABLE_TRACE_FOR(this);
    }

    /// Returns free spots, looking in n closest ones from the given one.
    /// If no free spot in the n closest ones, look further until finding one
    /// free.
    const std::vector<SpotIndex> & getClosestFreeSpots(const Spot & home, int n)
    {
        //TRACE_FUNC(n, *home);

        // Clear and increment counter
        int numCells = 0;
        int numSpots = 0;
        sorter.begin(& home, n);
        active.clear();
        counter++;

        // Push the cell of home spot
        auto coords = grid.getCellCoordsClamped(home);
        [[maybe_unused]] bool r = tryPush(coords);
        ASSERT(r);
        bool first = true;
        
        // Propagate to neighbor cells until the right number of spots found
        while(! active.empty())
        {
            auto c = active.front();
            active.pop_front();
            numCells ++;

            auto & cell = grid.getCell(c);
            for (int spotIndex: cell.spots)
            {
                sorter.add(spotIndex);
                numSpots ++;
            }

            if (first)
            {
                // Take neighbords of first cell even if enough spots in start cell,
                // to properly handle spots that are close to an edge.
                first = false;
            }
            else if (sorter.finished())
                continue;

            // Push neighbor cells
            int x = c.first;
            int y = c.second;

            if (x > 0)
                tryPush({x-1,y});
            if (y > 0)
                tryPush({x,y-1});
            if (x < grid.width - 1)
                tryPush({x+1,y});
            if (y < grid.height - 1)
                tryPush({x,y+1});
        }

        const auto & results = sorter.getClosestFreeSpots();

        //TRACE("Cells:", numCells,"spots:", numSpots, "free:", results.size());
        return results;
    }

    bool tryPush(std::pair<int, int> c)
    {
        auto & cell = grid.getCell(c);
        if (cell.counter == counter)
            return false;
        cell.counter = counter;
        active.push_back(c);
        return true;
    }
};
} // namespace decov
