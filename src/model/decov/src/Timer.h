#pragma once
#include "Error.h"
#include <chrono>
namespace decov
{
/// Chrono helper
struct Timer
{
    std::chrono::high_resolution_clock::time_point start_time;
    bool started = false;

    void start()
    {
        start_time = std::chrono::high_resolution_clock::now();
        started = true;
    }

    // Returns number of seconds elapsed since last start() call.
    double elapsed()
    {
        if (! started)
            throw Error("Timer not started");

        auto stop_time = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time).count() / 1000.0;
    }
};
}
