#pragma once
#include "Error.h"
#include <fstream>
namespace decov
{
    /// Wraps cin or regular file with path
    struct InputFile
    {
        std::string path;
        std::istream * stream = nullptr;
        bool allocated = false;

        InputFile(std::string path)
          : path(path)
        {
            if (path == "")
                return;
            if (path == "stdin")
            {
                stream = & std::cin;
                return;
            }

            stream = new std::ifstream(path);
            allocated = true;
            if (! stream->good())
                throw Error("Cannot read file ", path);
        }

        ~InputFile()
        {
            if (allocated)
                delete stream;
        }

        std::istream & operator * () { return *stream; }
        std::istream * operator -> () { return stream; }

        operator bool () const { return stream != nullptr; }
    };

    /// Wraps cout or regular file with path
    struct OutputFile
    {
        std::string path;
        std::ostream * stream = nullptr;
        bool allocated = false;

        OutputFile(std::string path)
          : path(path)
        {
            if (path == "")
                return;
            if (path == "stdout")
            {
                stream = & std::cout;
                return;
            }

            stream = new std::ofstream(path);
            allocated = true;
            if (! stream->good())
                throw Error("Cannot create file ", path);
        }

        ~OutputFile()
        {
            if (allocated)
                delete stream;
        }

        std::ostream & operator * () { return *stream; }
        std::ostream * operator -> () { return stream; }

        operator bool () const { return stream != nullptr; }
    };
}
