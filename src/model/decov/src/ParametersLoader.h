#pragma once
#include "Parameters.h"
#include <third_parties/nlohmann/json.hpp>
namespace decov
{
struct ParametersLoader
{
    using json = nlohmann::json;

    Parameters &p;
    Random re;

    ParametersLoader(Parameters &p, int seed) : p(p), re(seed) {}

    template <typename E, typename V> void importEnumArray(EnumArray<V, E> &array, const json &j)
    {
        if ((int)j.size() != (int)E::COUNT)
            throw Error("Unpexpected number of element in array");
        for (int e = 0; e < (int)E::COUNT; ++e)
        {
            array[(E)e] = j[e];
        }
    }

    //! get a parameter from JSON, either from the "common" or the "micro" dictionnary
    static inline const json &lookup(const json &params, const std::string &key)
    {
        if (params["micro"].contains(key))
            return params["micro"][key];
        return params["common"][key];
    }

    Stages parseStage(const std::string & name)
    {
        if (name == "S")
            return Stages::Sensitive;
        if (name == "E1")
            return Stages::ExposedMinor;
        if (name == "A1")
            return Stages::AsymptomaticMinor;
        if (name == "I1")
            return Stages::InfectedMinor;
        if (name == "R1")
            return Stages::RecoveredMinor;
        if (name == "E2")
            return Stages::ExposedMajor;
        if (name == "A2")
            return Stages::AsymptomaticMajor;
        if (name == "I2")
            return Stages::InfectedMajor;
        if (name == "R2")
            return Stages::RecoveredMajor;
        if (name == "M" || name == "D")
            return Stages::Dead;
        throw Error("Unknown infection stage name: ", name);
    }

    void load(std::istream &jsonFile)
    {
        json root;
        jsonFile >> root;

        std::vector<std::string> stageNames = root["compartments_Alizon20"];
        if ((int)Stages::COUNT != (int) stageNames.size())
            throw Error("Not the same number of stages");

        json &params = root["compartmental_model_parameters"];

        float samplingRatio = p.PopulationSamplingRatio = lookup(params, "PopulationSamplingRatio");
        float ICURatio = p.ICURatio = lookup(params, "ICURatio");

        std::vector<std::string> areaNames = root["zones"];
        json &initialRatios = root["initial_ratios_Alizon20"];

        p.LethalRate = lookup(params, "CFR");
        int simulationDuration = p.SimulationDuration = lookup(params, "max_days");
        int maxWeek = simulationDuration / 7;

        p.Areas.resize(areaNames.size());
        p.resize(simulationDuration);

        for (int areaId = 0; areaId < (int)areaNames.size(); ++areaId)
        {
            auto &area = p.Areas[areaId];

            area.Name = areaNames[areaId];
            int measuresLabel = root["zone_labels"][areaId];

            area.Agents = re.round((float)root["population_size"][areaId] * samplingRatio);

            int ICUBeds = root["ICU_beds"][areaId];
            area.HospitalBeds = re.round((float)ICUBeds * samplingRatio / ICURatio);

            std::vector<float> ratios = initialRatios[area.Name];
            
            float sum  = 0;
            for (int s = 0; s < (int)stageNames.size(); ++s)
            {
                auto stage = parseStage(stageNames[s]);
                sum += area.InitialStagesRatios[stage] = ratios[s];
            }

            // Fix eventual rounding problem
            float newSum = 0;
            for (int s = 0; s < (int)Stages::COUNT; ++s)
            {
                newSum += area.InitialStagesRatios[(Stages)s] /= sum;
            }
            area.InitialStagesRatios[Stages::Sensitive] += 1 - newSum;

            std::vector<int> measureWeeks = root["measure_weeks"];
            std::sort(measureWeeks.begin(), measureWeeks.end());

            auto importPolicy = [&](const std::string &key, auto &values) {
                json &j = root[key];

                values.clear();
                values.resize(maxWeek);

                int sliceIndex = 0;
                for (int week = 0; week < maxWeek; ++week)
                {
                    if (sliceIndex < (int)measureWeeks.size() && week == measureWeeks[sliceIndex])
                    {
                        if (j.type() != json::value_t::null)
                        {
                            int level = j[sliceIndex]["activated"][measuresLabel];
                            typename std::decay_t<decltype(values)>::value_type value;
                            if (!try_cast_to_enum(level, value))
                                throw Error("Invalid level ", level, " for measure ", key);

                            values[week] = value;
                        }
                        sliceIndex++;
                        continue;
                    }

                    if (week > 0)
                        values[week] = values[week - 1];
                }
            };

            importPolicy("forced_telecommuting", area.ForcedTelework);
            importPolicy("social_containment_all", area.SocialDistancing);
            importPolicy("school_closures", area.SchoolClosure);
            importPolicy("total_containment_high_risk", area.HighRiskLockDown);
            importPolicy("contact_tracing", area.ContactTracing);
        }

        auto importWeibull = [&](TruncatedWeibullDistribution &d, const json &j) {
            d.shape = j[0];
            d.scale = j[1];
            d.max = j[2];
        };

        importWeibull(p.ExposedDuration, lookup(params, "ExposedDuration"));
        importWeibull(p.AsymptomaticDuration, lookup(params, "AsymptomaticDuration"));
        importWeibull(p.MinorInfectionDuration, lookup(params, "MinorInfectionDuration"));
        importWeibull(p.MajorInfectionDuration, lookup(params, "MajorInfectionDuration"));
        importWeibull(p.MajorInfectionDurationBeforeDeath,
                      lookup(params, "MajorInfectionDurationBeforeDeath"));

        p.SimulationStart = lookup(params, "SimulationStart");
        p.AtRiskMajorInfectionRatio = lookup(params, "AtRiskMajorInfectionRatio");
        p.OtherMajorInfectionRatio = lookup(params, "OtherMajorInfectionRatio");
        p.EpidemyFinishedStop = (int)lookup(params, "StopWhenEpidemyFinished");
        p.DeadRatioThresholdStop = lookup(params, "StopWhenDeadRatioExceedsThreshold");
        p.InfectionStrength = lookup(params, "InfectionStrength");

        p.TracedIncubationPeriod = lookup(params, "TracedIncubationPeriod");
        p.ProbabilityToTest = lookup(params, "ProbabilityToTest");
        p.TestAccuracy = lookup(params, "TestAccuracy");
        p.ContactTracingProbability = lookup(params, "ContactTracingProbability");
        p.TestLockDownAcceptanceProbability = lookup(params, "TestLockDownAcceptanceProbability");

        p.WorkTime = lookup(params, "WorkTime");
        p.SchoolTime = lookup(params, "SchoolTime");
        p.NeighborhoodTime = lookup(params, "NeighborhoodTime");

        p.BarrierGesturesStartDay = lookup(params, "BarrierGesturesStartDay");
        p.BarrierGesturesPromiscuityFactor = lookup(params, "BarrierGesturesPromiscuityFactor");
        p.MeasuresComplianceProbability = lookup(params, "MeasuresComplianceProbability");

        importEnumArray(p.NeighborhoodProbabilities, lookup(params, "NeighborhoodProbabilities"));
        importEnumArray(p.MaxContacts, lookup(params, "MaxContacts"));

        p.SpotsScatteringFactor[SpotKinds::Work] = lookup(params, "DispersionWork");
        p.SpotsScatteringFactor[SpotKinds::School] = lookup(params, "DispersionSchool");
        p.SpotsScatteringFactor[SpotKinds::Neighborhood1] = lookup(params, "DispersionNeighborhood1");
        p.SpotsScatteringFactor[SpotKinds::Neighborhood2] = lookup(params, "DispersionNeighborhood2");
    }
};
} // namespace decov
