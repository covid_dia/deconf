#pragma once
#include "Definitions.h"
#include <unordered_map>
#include <vector>
#ifdef _OPENMP
#include <omp.h>
#endif
namespace decov
{
#if DECOV_MITIGATE_RAM == 1
typedef std::unordered_map<int, uint64_t> ContactMap;
#ifdef _OPENMP
//#error "Sparse container is not compatible with parallelism. Please disable openMP"
#endif
#else
typedef std::vector<uint64_t> ContactMap;
#endif

struct ByAgeContactMatrix
{
    static constexpr int Step = 10;
    static constexpr int N = 1 + Parameters::MaxAge / Step;

    std::array<std::array<int, N>, N> contacts = {};
    std::array<int, N> counts = {};

    void clear()
    {
        for (int i = 0; i < Step; ++i)
        {
            contacts[i].fill(0);
        }
    }

    void handleNewAgent(int age)
    {
        counts[age / Step]++;
    }

    void addContact(int age1, int age2)
    {
        ASSERT(age1 <= Parameters::MaxAge);
        ASSERT(age2 <= Parameters::MaxAge);

        contacts[age1 / Step][age2 / Step] ++;
        contacts[age2 / Step][age1 / Step] ++;
    }
};

// Keep trace of every agent contacts
struct ContactMatrix final
{
    ContactMatrix()
    {
        //ENABLE_TRACE_FOR(this);
    }

    std::vector<ContactMap> contacts;
#ifdef _OPENMP
    std::vector<omp_lock_t> locks;

    ~ContactMatrix()
    {
        for (omp_lock_t & lock: locks)
        {
            omp_destroy_lock(& lock);
        }
    }
#endif

    void resize(int numAgents)
    {
#ifdef _OPENMP
        int oldSize = locks.size();
        locks.resize(numAgents);
        for (int i = oldSize; i < numAgents; ++i)
        {
            omp_init_lock(& locks[i]);
        }
#endif
        contacts.resize(numAgents);
        clear();
    }

    void clear()
    {
        int numAgents = (int)contacts.size();   
        
        for (int i = 0; i < numAgents; ++i)
        {
            contacts[i].clear();
#if DECOV_MITIGATE_RAM != 1
            contacts[i].resize(1 + (numAgents - 1) / 64);
#endif
        }
    }

    void setContact(AgentId agent, AgentId contact)
    {
        ASSERT(agent < (int) contacts.size());
        ASSERT(contact < (int) contacts.size());

        uint64_t mask = 1;
        mask <<= (contact % 64);

#ifdef _OPENMP
        omp_set_lock(& locks[agent]);
#endif
        ContactMap &list = contacts[agent];
        uint64_t &bits = list[contact / 64];
        bits |= mask;
#ifdef _OPENMP
        omp_unset_lock(& locks[agent]);
#endif
    }

    bool hasContact(AgentId agent, AgentId contact) const
    {
        uint64_t mask = 1;
        mask <<= (contact % 64);
        const ContactMap &list = contacts[agent];
        auto it = list.find(contact / 64);
        if (it == list.end())
            return false;
        const uint64_t &bits = it->second;
        return (bits & mask) != 0;
    }

    void addContact(AgentId a1, AgentId a2)
    {
        ASSERT(a1 != a2);
        setContact(a1, a2);
        setContact(a2, a1);
    }

    template <typename Callback> 
    void getContacts(AgentId agent, Callback handleContact) const
    {
        const ContactMap &map = contacts[agent];

#if DECOV_MITIGATE_RAM == 1
        for (const auto &[i, bits] : map)
        {
#else
        for (int i = 0; i < (int)map.size(); ++i)
        {
            uint64_t bits = map[i];
            if (bits == 0)
                // As this matrix is sparse, this should happen very often
                continue;
#endif
            for (int j = 0; j < 64; ++j)
            {
                uint64_t mask = 1;
                mask <<= j;

                if ((mask & bits) != 0)
                {
                    handleContact(i * 64 + j);
                }
            }
        }
    }
};

struct ContactHistory
{
    int currentDayIndex = 0;
    std::vector<ContactMatrix> contacts;

    ContactHistory(int numKeptDays) : contacts(numKeptDays) {}

    void resize(int numAgents)
    {
        for (auto &matrix : contacts)
        {
            matrix.resize(numAgents);
        }
    }

    ContactMatrix & currentDay()
    {
        return contacts[currentDayIndex];
    }

    auto begin() const { return contacts.begin(); }
    auto end() const { return contacts.end(); }

    void newDay()
    {
        int keptDays = (int)contacts.size();
        currentDayIndex = (currentDayIndex + 1) % keptDays;
        contacts[currentDayIndex].clear();
    }

    void addContact(AgentId a1, AgentId a2) { currentDay().addContact(a1, a2); }
};
} // namespace decov
