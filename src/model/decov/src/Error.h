/*!
 * \file Error.h
 * \brief File to handle errors
 * \author Sam
 * \version 0.1
 */

#pragma once
#include <sstream>
#include <exception>
#include <Logger.h>

#define LOCATION decov::join("", __FILE__ ":", __LINE__, ": in ", __func__, ": ")

#if DECOV_ENABLE_ASSERT == 1
#define ASSERT(cond) do { if (! (cond)) throw Error("Assertion failed: ", #cond); } while(false)
#define ASSERT_EQUAL(a, b) do { if (a != b) throw Error("Assertion failed: ", a , "!=", b); } while(false)
#define ASSERT_ALMOST_EQUAL(a, b, eps) do { if (std::abs(a - b) > eps) throw Error("Assertion failed: |", a , "-", b, "|=", std::abs(a-b), ">", eps); } while(false)
#else
#define ASSERT(...)
#define ASSERT_EQUAL(...)
#define ASSERT_ALMOST_EQUAL(...)
#endif

namespace decov
{
    /*! \struct Error
     * 
     * catch and print error messages
     */
    struct Error : public std::exception
    {
        Error()
        { }

        template <typename ...Args>
        explicit Error(Args ...args)
        {
            _msg = join("", args...);
        }

        explicit Error(const std::string & s)
        {
           _msg = s;
        }

        const char* what() const throw()
        {
            return _msg.c_str();
        }

        Error(const Error & e)
        {
            _msg = e._msg;
        } 

    protected:
        std::string _msg;
    };
}
