#pragma once
#include "SimulationState.h"
#include <third_parties/nlohmann/json.hpp>
namespace decov
{
/// Converts daily Indicators into serval kind of file reports
struct ReportWriter
{
    using json = nlohmann::json;
    const std::vector<Indicators> &results;
    Parameters &parameters;

    ReportWriter(const std::vector<Indicators> &results, Parameters &parameters)
        : results(results), parameters(parameters)
    {
    }

    int upSample(int n) { return (int)(0.5f + (float)n / parameters.PopulationSamplingRatio); }

    void writeCSVReport(std::ostream &os)
    {
        os << "day,S,E,A,I1,I2,R,D,Dpp,tested,";
#if DECOV_DUMP_STATS == 1
        os << "tracedContacts,R0,";
#endif
        os << "activity,";
        os << "ForcedTelework,SchoolClosure,SocialDistancing,HighRiskLockDown,ContactTracing,"
              "BarrierGestures\n";

        for (int day = 0; day < (int)results.size(); ++day)
        {
            os << day << ",";
            const Indicators &i = results[day];
            os << upSample(i.count(Stages::Sensitive)) << ",";
            os << upSample(i.count(Stages::ExposedMinor) + i.count(Stages::ExposedMajor)) << ",";
            os << upSample(i.count(Stages::AsymptomaticMinor) + i.count(Stages::AsymptomaticMajor))
               << ",";
            os << upSample(i.count(Stages::InfectedMinor)) << ",";
            os << upSample(i.count(Stages::InfectedMajor)) << ",";
            os << upSample(i.count(Stages::RecoveredMinor) + i.count(Stages::RecoveredMajor)) << ",";
            os << upSample(i.count(Stages::Dead)) << ",";
            os << (float)i.count(Stages::Dead) / i.count() << ",";
            os << upSample(i.tested()) << ",";
#if DECOV_DUMP_STATS == 1
            os << upSample(i.tracedContacts()) << ",";
            os << i.R0 << ",";
#endif
            os << i.cumulatedActivityRate(parameters) << ",";

            std::string forcedTelework, schoolClosure, socialDistancing, highRiskLockDown,
                contactTracing;
            for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
            {
                auto &area = parameters.Areas[areaId];
                int week = day / 7;
                forcedTelework += to_string((int)area.ForcedTelework[week]);
                schoolClosure += to_string((int)area.SchoolClosure[week]);
                socialDistancing += to_string((int)area.SocialDistancing[week]);
                highRiskLockDown += to_string((int)area.HighRiskLockDown[week]);
                contactTracing += to_string((int)area.ContactTracing[week]);
            }

            os << "'" << forcedTelework << "',"
               << "'" << schoolClosure << "',"
               << "'" << socialDistancing << "',"
               << "'" << highRiskLockDown << "',"
               << "'" << contactTracing << "'," << (int)(parameters.BarrierGesturesStartDay <= day)
               << "\n";
        }
    }

    void writeContactMatrix(std::ostream &os)
    {
#if DECOV_DUMP_STATS == 0
        throw Error("Contact matrix not computed");
#else
        json root;
        json &days = root["days"];

        for (int d = 0; d < (int)results.size(); ++d)
        {
            json &day = days[d];

            const ByAgeContactMatrix &matrix = results[d].contactsMatrix;

            json &ages = day["ages"] = json::array();
            for (int age = 0; age < Parameters::MaxAge; age += ByAgeContactMatrix::Step)
            {
                ages.push_back(age);
            }
            json &counts = day["agents_by_age"] = json::array();
            for (int i = 0; i < ByAgeContactMatrix::N; ++i)
            {
                counts.push_back(matrix.counts[i]);
            }
            json &contacts = day["contacts"] = json::object();
            for (int i = 0; i < ByAgeContactMatrix::N; ++i)
            {
                json &row = contacts[std::to_string(i * ByAgeContactMatrix::Step)] = json::array();
                for (int j = 0; j < ByAgeContactMatrix::N; ++j)
                {
                    float ratio;
                    if (matrix.counts[i] == 0)
                        ratio = 0;
                    else
                        ratio = matrix.contacts[i][j] / (float)matrix.counts[i];
                    row.push_back(ratio);
                }
            }
        }

        os << root.dump();
#endif
    }

    void writeJsonReport(std::ostream &os)
    {
        json root;
        root["success"] = true;
        root["compartmental_model"] = "Alizon20";

        json &zones = root["zones"] = json::array();
        for (auto &area : parameters.Areas)
        {
            zones.push_back(area.Name);
        }

        root["max_days"] = results.size() - 1; // 0 = initial
        json &cases = root["cases"] = json::array();
        json &i2 = root["hospital_patients"] = json::array();
        json &icu = root["ICU_patients"] = json::array();
        json &dead = root["deaths"] = json::array();
        json &tests = root["tests"] = json::array();
#if DECOV_DUMP_STATS == 1
        json &tracedContacts = root["traced_contacts"] = json::array();
#endif
        json &activity = root["activity"] = json::array();

        for (int day = 0; day < (int)results.size(); ++day)
        {
            if (day % 7 != 0)
                // Keep only full weeks
                continue;

            std::string key;
            if (day == 0)
                key = "initial";
            else
                key = "week";
            json week_cases_dict;
            json &week_cases = week_cases_dict[key] = json::array();
            json week_i2_dict;
            json &week_i2 = week_i2_dict[key] = json::array();
            json week_icu_dict;
            json &week_icu = week_icu_dict[key] = json::array();
            json week_dead_dict;
            json &week_dead = week_dead_dict[key] = json::array();
            json week_tests_dict;
            json &week_tests = week_tests_dict[key] = json::array();
#if DECOV_DUMP_STATS == 1
            json week_traced_contacts_dict;
            json &week_traced_contacts = week_traced_contacts_dict[key] = json::array();
#endif
            json week_activity_dict;
            json &week_activity = week_activity_dict[key] = json::array();

            for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
            {
                const Indicators &i = results[day];

                auto &areaIndicators = i.areas[areaId];

                week_cases.push_back(upSample(areaIndicators.counts[Stages::InfectedMinor] +
                                              i.areas[areaId].counts[Stages::InfectedMajor]));
                week_i2.push_back(upSample(areaIndicators.counts[Stages::InfectedMinor]));
                week_icu.push_back(
                    upSample(areaIndicators.counts[Stages::InfectedMajor] * parameters.ICURatio));
                week_dead.push_back(upSample(areaIndicators.counts[Stages::Dead]));
#if DECOV_DUMP_STATS == 1
                week_traced_contacts.push_back(upSample(areaIndicators.tracedContacts));
#endif
                week_tests.push_back(upSample(areaIndicators.tested));
                week_activity.push_back(areaIndicators.activityRate / (float)parameters.Areas[areaId].ActiveAgents);
            }

            cases.push_back(week_cases_dict);
            i2.push_back(week_i2_dict);
            icu.push_back(week_icu_dict);
            dead.push_back(week_dead_dict);
            tests.push_back(week_tests_dict);
#if DECOV_DUMP_STATS == 1
            tracedContacts.push_back(week_traced_contacts_dict);
#endif
            activity.push_back(week_activity_dict);
        }

        os << root.dump();
    }
};
} // namespace decov
