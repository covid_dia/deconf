#pragma once
#include "ContactHistory.h"
namespace decov
{

/// Observable variables for an area
struct AreaIndicators
{
    EnumArray<int, Stages> counts;
    int tested = 0;
#if DECOV_DUMP_STATS
    int tracedContacts = 0;
#endif
    float satRatio = 1;

    /// Cumulated activity rate of every agents in this area
    double activityRate = 0;

    using json = nlohmann::json;

    friend void to_json(json & j, const AreaIndicators & i)
    {
        j = json{{"counts", i.counts}};
        // tested not serialized
        // tracedContacts not serialized
        // satRatio not serialized
    }

    friend void from_json(const json & j, AreaIndicators & i)
    {
        j.at("counts").get_to(i.counts);
    }
};

/// Observable variables, output of the simulation
struct Indicators
{
    std::vector<AreaIndicators> areas;

#if DECOV_DUMP_STATS
    ByAgeContactMatrix contactsMatrix;
    float R0 = 0;
#endif

    Indicators(int numAreas)
      : areas(numAreas)
    { }

    int count(Stages stage) const
    {
        int n = 0;
        for (auto & area : areas)
        {
            n += area.counts[stage];
        }
        return n;
    }

    /// Returns a global average activity rate cumulated since the beginning of the simulation.
    /// Devide it by the number of days to get a ratio between 0 and 1
    float cumulatedActivityRate(Parameters & parameters) const
    {
        float activityRate = 0;
        float active = 0;
        for (int areaId = 0; areaId < (int)areas.size(); ++areaId)
        {
            activityRate += areas[areaId].activityRate;
            active += parameters.Areas[areaId].ActiveAgents;
        }

        return activityRate / active;
    }
    
#if DECOV_DUMP_STATS == 1
    int tracedContacts() const
    {
        int c = 0;
        for (auto & area : areas)
        {
            c += area.tracedContacts;
        }
        return c;
    }
#endif

    int tested() const
    {
        int t = 0;
        for (auto & area : areas)
        {
            t += area.tested;
        }
        return t;
    }

    int count() const
    {
        int p = 0;
        for (auto & area : areas)
        {
            for (int n: area.counts)
            {
                p+=n;
            }
        }
        return p;
    }

    int contagious() const
    {
        return count(Stages::AsymptomaticMinor) + count(Stages::AsymptomaticMajor) +
               count(Stages::InfectedMinor) + count(Stages::InfectedMajor);
    }

    int contaminated() const
    {
        return contagious() + count(Stages::ExposedMinor) + count(Stages::ExposedMajor);
    }

    using json = nlohmann::json;

    friend void to_json(json & j, const Indicators & i)
    {
        j = json{{"areas", i.areas}};
        // contactsMatrix not serialized
        // R0 not serialized
    }

    friend void from_json(const json & j, Indicators & i)
    {
        j.at("areas").get_to(i.areas);
    }
};
} // namespace decov
