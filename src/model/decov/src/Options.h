#pragma once
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <functional>
#include <unordered_map>
#include <map>
#include <vector>
#include <string>
#include <variant>
#include "Error.h"

namespace decov
{
/// A simple command-line options parsing class based on C++17 variants.
class Options
{
public:
    typedef std::variant<bool, int, double, float, std::string> Variant;

    /// This class groups the definition and current value of an option.
    struct Opt
    {
        std::string longName;
        std::string shortName;
        std::string category;
        std::string help;

        Variant value;
        Variant defaultValue;

        /// Set the default value of this option
        template <typename T>
        void setDefault(const T &t) { value = defaultValue = t; }

        /// Callback which fires before this option is set with given value
        std::function<void(Variant &value)> callback;

        //@{
        /// Implicit value conversion
        operator bool() const { return std::get<bool>(value); }
        operator int() const { return std::get<int>(value); }
        operator double() const { return std::get<double>(value); }
        operator float() const { return std::get<float>(value); }
        operator std::string() const { return std::get<std::string>(value); }
        //@}

        bool operator==(const std::string &s) const
        {
            return std::get<std::string>(value) == s;
        }
    };

    Options()
        : _argumentsAllowed(false),
          _errorCallback(nullptr)
    {
    }

    /// Returns the list of non-option arguments
    const std::vector<std::string> &arguments() { return _arguments; }

    /// Allows non-option arguments on the command-line
    // TODO: help message for arguments
    void allowArguments(bool allowed) { _argumentsAllowed = allowed; }

    /// Adds an option. Callback is fired each time an option is set.
    template <typename V>
    void add(
        const std::string &longName,
        const std::string &help,
        const V &defaultValue = V(),
        const std::string &shortName = "",
        std::function<void(Variant &)> callback = [](auto &) {})
    {
        if (shortName != "")
        {
            auto p = _shortToLongNames.emplace(shortName, longName);
            if (!p.second)
                throw Error("Option ", shortName, " already bound to ", p.first->second);
        }

        if (!_options.emplace(longName, Opt{longName, shortName, _currentCategory, help, defaultValue, defaultValue, callback}).second)
            throw Error("Option ", longName, " specified twice");
    }

    /// Gets option by name
    const Opt &get(const std::string &longName) const
    {
        auto i = _options.find(longName);
        if (i == _options.end())
            throw Error("Unknown option: ", longName);
        return i->second;
    }

    /// Gets option by name
    const Opt &operator[](const std::string &longName) const
    {
        return get(longName);
    }

    /// Gets option by name (non-const version)
    Opt &operator[](const std::string &longName)
    {
        return const_cast<Opt &>(get(longName));
    }

    /// Gets options value by name
    template <typename T>
    T get(const std::string &longName) const
    {
        return static_cast<T>(get(longName));
    }

    /// Next added options will be put in this category
    void category(const std::string &name)
    {
        _currentCategory = name;
    }

    /// Writes help message in stderr
    void help(const std::string &msg = "");

    /// If parsing failed, shows help and exits
    void parse(int argc, const char **argv);

    /// Dumps the command-line related to the last invocation of parse() method.
    const std::string &commandLine() { return _commandLine; }

    /// Sets a callback to invoke when parsing fails
    void onError(std::function<void(const std::string &message)> callback)
    {
        _errorCallback = callback;
    }

    void error(const std::string &msg)
    {
        if (_errorCallback != nullptr)
            _errorCallback(msg);
        else
        {
            std::cerr << msg << '\n';
            std::exit(1);
        }
    }

private:
    bool _argumentsAllowed;
    std::vector<std::string> _arguments;
    std::map<std::string, Opt> _options;
    std::unordered_map<std::string, std::string> _shortToLongNames;
    std::string _currentCategory;
    std::string _commandLine;
    std::function<void(const std::string &)> _errorCallback;
};

namespace
{
template <class... Ts>
struct overloaded : Ts...
{
    using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;
} // namespace

namespace
{
std::string describe_operand(const Options::Opt &o)
{
    return std::visit(overloaded{
                          [](bool) { return " [<bool>]"; },
                          [](int) { return " <int>"; },
                          [](double) { return " <double>"; },
                          [](float) { return " <float>"; },
                          [](const std::string &) { return " <string>"; }},
                      o.defaultValue);
}

void dump_default(const Options::Opt &o, std::ostream &os)
{
    std::visit(overloaded{
                   [&](auto v) { os << v; },
                   [&](bool v) { os << std::boolalpha << v; },
                   [&](const std::string &v) { os << '"' << v << '"'; }},
               o.defaultValue);
}
} // namespace

void Options::help(const std::string &msg)
{
    int width = 0;
    for (auto &[s, l] : _shortToLongNames)
    {
        int w = 8 + s.size() + l.size();
        w += describe_operand(_options[l]).size();
        if (w > width)
            width = w;
    }

    std::map<std::string, std::vector<Opt *>> byCat;
    for (auto &[_, o] : _options)
    {
        (void)_;
        byCat[o.category].push_back(&o);
    }

    for (auto &[c, v] : byCat)
    {
        if (c != "")
            std::cerr << c << ":\n";
        for (auto o : v)
        {
            std::cerr.setf(std::ios::left);

            std::string d = "  -" + o->longName;
            if (o->shortName != "")
                d += ", -" + o->shortName;
            d += describe_operand(*o);
            std::cerr << std::setw(width) << d << o->help;
            std::cerr << " (default: ";
            dump_default(*o, std::cerr);
            std::cerr << ")\n";
        }
        std::cerr << "\n";
    }
}

namespace
{
template <typename T>
bool try_parse_simple(T &t, const std::string &s)
{
    std::istringstream is(s);
    is >> t;
    if (is.fail())
        return false;
    return is.peek() == EOF;
}

template <typename T>
bool try_parse(T &t, const std::string &s)
{
    return try_parse_simple<T>(t, s);
}

template <>
bool try_parse<std::string>(std::string &is, const std::string &os)
{
    if (os[0] == '-')
        return false;

    is = os;
    return true;
}

template <>
bool try_parse<bool>(bool &t, const std::string &s)
{
    if (try_parse_simple<bool>(t, s))
        return true;
    // Retry with boolalpha for bool options
    std::istringstream is(s);
    is >> std::boolalpha >> t;
    if (is.fail())
        return false;
    return is.peek() == EOF;
}

template <>
bool try_parse<double>(double &t, const std::string &s)
{
    std::istringstream is(s);
    is >> t;
    if (is.fail())
        return false;
    char c;
    is >> c;
    if (is.eof())
        return true;
    if (c == '%')
        t /= 100;
    return is.peek() == EOF;
}
} // namespace

void Options::parse(int argc, const char **argv)
{
    _commandLine.clear();
    for (int i = 0; i < argc; ++i)
        _commandLine += std::string(argv[i]) + ' ';
    _commandLine.pop_back();

    _arguments.clear();

    for (int i = 1; i < argc; ++i)
    {
        std::string opt = argv[i];

        if (opt[0] != '-')
        {
            if (_argumentsAllowed)
            {
                _arguments.push_back(opt);
                continue;
            }

            error("Unexpected argument: " + opt);
        }

        std::string key = argv[i];
        while (key[0] == '-')
            key = key.substr(1);

        std::string val;

        auto it = _options.find(key);
        if (it == _options.end())
        {
            auto it2 = _shortToLongNames.find(key);
            if (it2 == _shortToLongNames.end())
            {
                int c;
                bool valueCut = false;
                for (c = 1; c < (int)key.size(); ++c)
                {
                    if (key[c] < '0' || key[c] > '9')
                        continue;
                    valueCut = true;
                    break;
                }

                if (valueCut)
                {
                    val = key.substr(c);
                    key = key.substr(0, c);
                    i--;

                    it2 = _shortToLongNames.find(key);
                    if (it2 == _shortToLongNames.end())
                        error("Unknown short option: -" + key);
                }
                else
                    error("Unknown option: " + opt);
            }

            it = _options.find(it2->second);
        }

        auto &op = it->second;

        // Get option value
        if (val.empty())
        {
            if (i == argc - 1)
            {
                // Particular case for bool options, default to true
                if (std::get_if<bool>(&op.defaultValue) != nullptr)
                    val = "1";
                else
                    error("Option " + opt + " needs an argument");
            }
            else
                val = argv[i + 1];
        }

        // Parse value
        Variant value;
        std::visit([&](auto v) {
            if (try_parse(v, val))
            {
                value = v;
                i++;
                return;
            }

            if (std::get_if<bool>(&op.defaultValue) != nullptr)
                value = true;
            else
                error("Invalid argument \"" + val + "\" for option " + opt);
        },
                   op.defaultValue);

        // Invoke callback before setting the value
        op.callback(value);
        op.value = value;
    }
}
} // namespace decov
