#pragma once
#include <limits>
namespace decov
{
/// Xor shift 128 plus random rengine
struct XorShift128plus
{
    typedef uint64_t result_type;

    uint64_t a;
    uint64_t b;

    XorShift128plus(uint64_t seed = 0)
    { 
        this->seed(seed);
    }

    void seed(uint64_t seed)
    { 
        a = 123456789ul + (seed >> 32);
        b = 987654321ul + (seed & 0xFFFFFFFF);
    }

    static constexpr uint64_t min() { return 0; }
    static constexpr uint64_t max() { return std::numeric_limits<uint64_t>::max(); }

    uint64_t operator() ()
    {
        uint64_t t = a;
        const uint64_t s = b;
        a = s;
        t ^= t << 23;
        t ^= t >> 17;
        t ^= s ^ (s >> 26);
        b = t;
        return t + s;
    }
};
}
