#pragma once
#include <random>
#include "XorShift128plus.h"
#include "Error.h"
namespace decov
{
/// Standard random wrapper
struct Random
{
    //std::mt19937_64 _random;
    XorShift128plus _random;

    Random(int seed=0)
        : _random(seed)
    {
    }

    void setSeed(int seed)
    {
        _random.seed(seed);
    }

    /// Returns inside [0; 1)
    float rand()
    {
        return std::uniform_real_distribution<float>(0, 1)(_random);
    }

    template <typename T>
    T rand(T from, T to);

    template <typename T>
    T normal(T mean = 0, T stddev = 1)
    {
        return std::normal_distribution<T>(mean, stddev)(_random);
    }

    template <typename T>
    T weibull(T shape, T scale)
    {
        return std::weibull_distribution<T>(shape, scale)(_random);
    }

    /// Probabilistic rounding of a floating value to preserve float average.
    int round(float value)
    {
        int rounded = (int)value;
        int sign = (value >= 0) - (value < 0);
        float remaining = value - (float) rounded;
        return rounded + sign * (int)(rand() < sign * remaining);
    }

    template <typename IT>
    void shuffle(IT begin, IT end)
    {
        std::shuffle(begin, end, _random);
    }

    template <typename IT>
    auto choice(IT begin, IT end)
    {
        int size = end - begin;
        return begin[rand<int>(0, size - 1)];
    }

    template <typename C>
    int select(const C & array, typename C::value_type sum)
    {
        return select(array.data(), array.size(), sum);
    }

    /// Roulette wheel: returns index in [0;count-1] with probabilities array[i]/sum
    template <typename T>
    int select(const T *array, int count, T sum)
    {
        if constexpr (std::is_integral<T>::value)
            ASSERT_EQUAL(std::accumulate(array, array+count, T()), sum);
        else
            ASSERT_ALMOST_EQUAL(std::accumulate(array, array+count, T()), sum, 1e-6);
        ASSERT(sum > 0);
        T acc = 0;
        T p = rand<T>(0, sum);
        for (int i = 0; i < count - 1; ++i)
        {
            T weight = array[i];
            if (weight <= 0)
                continue;
            acc += weight;
            if (p <= acc)
                return i;
        }
        return count - 1;
    }
};

// Chooses inside [min; max]
template <>
inline int Random::rand<int>(int min, int max)
{
    return std::uniform_int_distribution<int>(min, max)(_random);
}

// Chooses inside [min; max)
template <>
inline float Random::rand<float>(float min, float max)
{
    return std::uniform_real_distribution<float>(min, max)(_random);
}

template <>
inline bool Random::rand<bool>(bool min, bool max)
{
    if (min == max)
        return min;
    return rand<int>(0, 1) == 0;
}

template <>
inline long Random::rand<long>(long min, long max)
{
    return std::uniform_int_distribution<long>(min, max)(_random);
}
} // namespace decov
