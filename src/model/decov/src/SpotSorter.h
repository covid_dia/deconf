#pragma once
namespace decov
{
/// Keeps a heap of spots based on their distance and free properties
struct SpotSorter
{
    std::vector<SpotIndex> heap;
    SpotDistanceComparer closer;
    int numFree;
    int n = -1;

    SpotSorter(const std::vector<Spot> & spots)
      : closer(spots)
    {
    }

    void begin(const Spot * home, int n)
    {
        heap.clear();
        this->n = n;
        closer.setRef(home);
        numFree = 0;
    }

    bool finished() const
    {
        return numFree > 0 && (int) heap.size() == n;
    }

    void add(SpotIndex spotIndex)
    {
        ASSERT(n > 0);

        bool full = closer.spots[spotIndex].full();

        if ((int) heap.size() >= n)
        {
            if (! closer(spotIndex, heap[0]) && (numFree > 0 || full))
                // Ignore a further new spot unless it is the first free found
                return;

            if (numFree == 1 && ! closer.spots[heap[0]].full() && full)
            {
                // Keep the only free spot even if further that this new one
                return;
            }

            std::pop_heap(heap.begin(), heap.end(), closer);
            if (! closer.spots[heap.back()].full())
                numFree--;
            heap.pop_back();
        }

        if (! full)
            numFree++;
        heap.push_back(spotIndex);
        std::push_heap(heap.begin(), heap.end(), closer);
    }

    const std::vector<SpotIndex> & getClosestFreeSpots()
    {
        // Remove full spots from the closest list
        for (int i = 0; i < (int)heap.size(); ++i)
        {
            if (closer.spots[heap[i]].full())
            {
                std::swap(heap[i], heap[heap.size() - 1]);
                heap.pop_back();
                i--;
            }
        }

        ASSERT(numFree == (int)heap.size());

        return heap;
    }
};
}
