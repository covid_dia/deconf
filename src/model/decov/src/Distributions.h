#pragma once
#include <algorithm>
#include "Random.h"
namespace decov
{

/// Clamped normal distribution
template <typename T>
struct NormalDistribution
{
    T _min = -std::numeric_limits<T>::max;
    T _max = std::numeric_limits<T>::max;
    float _mean = 0;
    float _stddev = 1;

    T min() const { return min; }

    T sample(Random &re) const 
    {
        T v = (T)re.normal(_mean, _stddev);
        if (v <= _min)
            return _min;
        if (v >= _max)
            return _max;
        return v;
    }
};

/// Histogram-based distribution
template <typename T, typename TNum>
struct HistogramDistribution
{
    std::vector<T> bounds;

    /// values[n] concerns individual in category [bounds[n]; bounds[n+1])
    std::vector<TNum> numbers;

    TNum sum;

    T min() const { return bounds[0]; }

    TNum operator[](T key) const
    {
        ASSERT(numbers.size() == bounds.size() - 1);

        if (key < bounds[0])
            throw Error("Key too low ", key);

        for (int i = 1; i < (int)bounds.size(); ++i)
        {
            if (key < bounds[i])
                return numbers[i - 1];
        }

        throw Error("Key too high ", key);
    }

    /// Samples an individual: selects a category based on population ratios
    /// and samples uniformly inside each category.
    T sample(Random &re) const 
    {
        ASSERT(numbers.size() == bounds.size() - 1);

        int slice = re.select(numbers, sum);
        T low = bounds[slice];
        T high = bounds[slice + 1];
        return low + re.rand() * (high - low);
    }
};

/// Fixed categories based on discontinuous intervalles.
/// A bit like histogram, but no interpolation is performed inside a category.
template <typename T>
struct ListDistribution
{
    /// second[n] concerns individuals in category [first[n]; first[n+1])
    std::vector<std::pair<int,T>> map;

    /// Returns the category value for the given individual
    T operator[](int key)
    {
        T value = map[0].second;
        for (auto & [b, v]: map)
        {
            if (key < b)
                break;
            value = v;
        }

        return value;
    }
};

/// Weibull distribution, to choose a duration in days within [1;max]
struct TruncatedWeibullDistribution
{
    float shape;
    float scale;
    int max;

    int min() const { return 1; }

    int sample(Random & re) const 
    {
        return std::min(max, 1 + int(0.5f + re.weibull(shape, scale)));
    }  
};

} // namespace decov
