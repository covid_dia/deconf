#pragma once
#include "Enum.h"
namespace decov
{
#ifndef DECOV_MITIGATE_RAM
#error "Define DECOV_MITIGATE_RAM to 1 if saving memory is more important that speed for you"
#endif

#ifndef DECOV_DUMP_STATS
#error "Define DECOV_DUMP_STATS to 1 to compute R0 and contacts statistics for CSV report"
#endif

#ifndef DECOV_TRACE_CONTACTS
#error "Define DECOV_TRACE_CONTACTS to 1 to enable contact tracing"
#endif

/// Infection stages
ENUM(Stages, Sensitive, ExposedMinor, ExposedMajor, AsymptomaticMinor, AsymptomaticMajor, InfectedMinor,
     InfectedMajor, RecoveredMinor, RecoveredMajor, Dead)

/// Kind of spots, ie. places where people can meet and spread the virus.
/// Neighborhood represent all kind of social conctacts: shopping, people talking in the street, in
/// the subway, etc. We affect two neighborhood spots to each people in order to increase scattering.
ENUM(SpotKinds, Home, Work, School, Neighborhood1, Neighborhood2)

/// Possible measures levels for ForcedTelwork
ENUM(ForcedTeleworkValues, None, Alternating, Complete)

/// Possible measures levels for SchoolClosure
ENUM(SchoolClosureValues, None, OnlyHigherEducationClosed, HigherEducationClosedAlternatingPupils, AllClosed)

/// Possible measures levels for SocialDistancing
ENUM(SocialDistancingValues, None, NoMoreThan100, NoMoreThan10, Maximum)

/// Possible measures levels for HighRiskLockDown
ENUM(HighRiskLockDownValues, None, Recommanded)

/// Possible measures levels for ContactTracing
ENUM(ContactTracingValues, None, TestAndLockDownContacts, TestAndLockDownContactsFull)

/// Telework flavor 
ENUM(TeleworkStatuses, NotActive, CannotTelework, CanTelework)

/// Children keeping flavor to evaluate telework productivity
ENUM(ChildrenToKeep, None, Under15, Under12)

/// Kinds of home
// (order matters: non children homes first)
ENUM(HomeKinds,
     AloneSenior,
     Alone,
     CoupleSenior,
     Couple,
     SingleParentUnder12Child,
     SingleParentOver11Children,
     CoupleUnder12Child,
     CoupleOver11Children,
     Other)

// Create somes aliases for better readbility of containers declarations:

/// Index in the state agent list
typedef int AgentId;

/// Index in the spot daily presence list
typedef int IndexInSpot;

/// Index in the parameters area list
typedef int AreaId;

/// Index in the spot list
typedef int SpotIndex;

} // namespace decov
