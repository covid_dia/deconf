/*!
 * \file Enum.h
 * \brief File containing template for the different enum
 * \author Sam
 * \version 0.1
 */


#pragma once
#include <regex>
#include <vector>
#include <string>
#include "Format.h"
#include "Error.h"
#include <third_parties/nlohmann/json.hpp>

#define ENUM(NAME, ...)                                           \
enum class NAME                                                   \
{                                                                 \
    __VA_ARGS__,                                                  \
    COUNT                                                         \
};                                                                \
template<>                                                        \
inline const std::vector<std::string> & get_enum_names<NAME>()    \
{                                                                 \
    static auto names = parse_enum_list(","#__VA_ARGS__);         \
    return names;                                                 \
}                                                                 \
template<>                                                        \
inline const char * get_enum_name<NAME>()                         \
{                                                                 \
    return #NAME;                                                 \
}                                                                 \
inline std::ostream & operator << (std::ostream & os, NAME value) \
{                                                                 \
    os << get_enum_name<NAME>(value);                             \
    return os;                                                    \
}                                                                 \
inline void to_json(nlohmann::json & j, const NAME & e)           \
{                                                                 \
    j = (int)e;                                                   \
}                                                                 \
inline void from_json(const nlohmann::json & j, NAME & e)         \
{                                                                 \
    int v = j;                                                    \
    if (! try_cast_to_enum(v, e))                                 \
        throw Error("Invalid value ", v, " for enum " #NAME);     \
}

namespace decov
{
    /// std::array indexed by an enum
    template <typename T, typename Enum>
    struct EnumArray
    {
        void clear(const T & t = T())
        {
            _array.fill(t);
        }

        auto begin() { return _array.begin(); }
        auto end() { return _array.end(); }
        auto begin() const { return _array.begin(); }
        auto end() const { return _array.end(); }

        T & operator [] (Enum e)
        {
            return _array[(int)e];
        }

        const T & operator [] (Enum e) const
        {
            return _array[(int)e];
        }

        std::array<T, (int)Enum::COUNT> _array = {};

        int size() const { return Enum::COUNT; }

        using json = nlohmann::json;

        friend void from_json(const json & j, EnumArray & e)
        {
            e._array = j;
        }

        friend void to_json(json & j, const EnumArray & e)
        {
            j = e._array;
        }
    };

    /// Specialized by ENUM macro
    template <typename Enum>
    const std::vector<std::string> & get_enum_names();

    /// Specialized by ENUM macro
    template <typename Enum>
    const char * get_enum_name();

    /*! \fn get_enum_names
     *
     *  Returns the possible values of the given enum as a std::array
     * 
     * \return array, an array of casted enum to string
     */
    template <typename Enum>
    constexpr std::array<Enum, (int)Enum::COUNT> get_enum_values()
    {
        std::array<Enum, (int)Enum::COUNT> values;
        for (int i = 0; i < (int) Enum::COUNT; ++i)
        {
            values[i] = static_cast<Enum>(i);
        }
        return values;
    }

    /*! \fn get_enum_name
     *
     *  Returns the name of the given enum
     * 
     * \param value, an enum value
     * \return string, the string name corresponding to an enum
     */
    template <typename Enum>
    const std::string & get_enum_name(Enum value)
    {
        int v = (int)value;
        if (v < 0 || v >= (int)Enum::COUNT)
            throw Error(LOCATION, "Invalid value ", v, " for enum ", get_enum_name<Enum>());
        return get_enum_names<Enum>()[v];
    }

    /*! \fn try_parse_enum
     *
     *  Tries parsing a value of the given enum
     * 
     * \param name, a string 
     * \param value, an enum value
     * \return bool, true if the name was found, else false
     */
    template <typename Enum>
    bool try_parse_enum(const std::string & name, Enum & value)
    {
        auto & names = get_enum_names<Enum>();
        for (int i = 0; i < (int)names.size(); ++i)
        {
            if (names[i] == name)
            {
                value = (Enum)i;
                return true;
            }
        }
        return false;
    }

    /// Tries to import enum value from int
    template <typename Enum>
    bool try_cast_to_enum(int intValue, Enum & value)
    {
        if (intValue < 0 || intValue >= (int)Enum::COUNT)
            return false;
        value = (Enum)intValue;
        return true;
    }

    // For use from macro above
    inline std::vector<std::string> parse_enum_list(const std::string & s)
    {
        std::vector<std::string> list;
        std::regex re(",\\s*([A-Z][A-Za-z0-9]*)");
        for (auto it = std::sregex_iterator(s.begin(), s.end(), re);
             it != std::sregex_iterator();
             ++it)
        {
            auto match = *it;
            list.push_back(match[1].str());
        }
        return list;
    }
}
