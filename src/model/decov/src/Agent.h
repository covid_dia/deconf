#pragma once
#include "Definitions.h"
#include "Spot.h"
namespace decov
{
/// A simulated agent (ie. people)
struct Agent
{
    int age;      //< Age of the agent
    bool atRisk;  //< Is this agent an at risk person

    TeleworkStatuses teleworkStatus; //< Kind of work, ie possibility to telework or not

    Stages stage;     //< Infection stage
    Stages nextStage; //< Next infection stage
    int stageRemainingDays;   //< Number of days before stage change

#if DECOV_DUMP_STATS == 1
    // To compute R0
    int numInfections = 0;                //< Number of other agents infected by this one
    int numContagiousDays = 0;            //< Number of days spent in contagious stages
#endif

    /// Assigned spots
    EnumArray<std::pair<SpotIndex, IndexInSpot>, SpotKinds> spots;

    /// Tracks that agent's stage will change from Sensitive to Exposed after this day
    bool infected = false;

    /// Tells if the agent must take care of a child, which impacts its productivity
    /// during forced telework because school closures. Only for active agents.
    ChildrenToKeep childrenToKeep = ChildrenToKeep::None;

    /// Is there an at risk agent in the same home of this one?
    bool atRiskAtHome = false;

    Agent()
    { }

    Agent(int age, bool atRisk, TeleworkStatuses ts, Stages stage, Stages nextStage, int remainingDays)
      : age(age),
        atRisk(atRisk),
        teleworkStatus(ts),
        stage(stage),
        nextStage(nextStage),
        stageRemainingDays(remainingDays)
    { 
        spots.clear({-1,-1});
    }

    bool active() const
    {
        return teleworkStatus != TeleworkStatuses::NotActive;
    }

    bool atSchool() const
    {
        return !active() && age <= Parameters::SchoolAgeLimit;
    }

    /// \return true if the agent has a specific spot kind.
    bool has(SpotKinds spotKind) const
    {
        return spots[spotKind].first >= 0;
    }

    /// \return true if the agent is contagious.
    bool contagious() const
    {
        return (stage == Stages::AsymptomaticMinor) |
               (stage == Stages::AsymptomaticMajor) |
               (stage == Stages::InfectedMinor) |
               (stage == Stages::InfectedMajor);
    }

    friend std::ostream &operator<<(std::ostream &os, const Agent &agent)
    {
        os << "Agent(age=" << agent.age << ", stage=" << agent.stage << ")";
        return os;
    }

    using json = nlohmann::json;

    friend void to_json(json & j, const Agent & a)
    {
        j = json{{"age", a.age},
                 {"atRisk", a.atRisk},
                 {"teleworkStatus", a.teleworkStatus},
                 {"stage", a.stage},
                 {"nextStage", a.nextStage},
                 {"stageRemainingDays", a.stageRemainingDays},
                 {"spots", a.spots},
                 {"childrenToKeep", a.childrenToKeep},
                 {"atRiskAtHome", a.atRiskAtHome},
                 };
        // infected not serialized
    }

    friend void from_json(const json & j, Agent & a)
    {
        j.at("age").get_to(a.age);
        j.at("atRisk").get_to(a.atRisk);
        j.at("teleworkStatus").get_to(a.teleworkStatus);
        j.at("stage").get_to(a.stage);
        j.at("nextStage").get_to(a.nextStage);
        j.at("stageRemainingDays").get_to(a.stageRemainingDays);
        j.at("spots").get_to(a.spots);
        j.at("childrenToKeep").get_to(a.childrenToKeep);
        j.at("atRiskAtHome").get_to(a.atRiskAtHome);
    }
};
} // namespace decov
