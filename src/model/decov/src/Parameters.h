/*!
 * \file Parameters.h
 * \brief File storing agent and spot states during the simulation
 * \author Sam
 * \version 0.1
 */

#pragma once
#include "Definitions.h"
#include "Distributions.h"
#include "Area.h"
#include "Enum.h"
#include "Error.h"
#include "Random.h"
#include <cmath>
#include <limits>
#include <vector>
namespace decov
{
struct Parameters
{
    // Maximum age of an agent
    static constexpr int MaxAge = 99;

    // age < 25 => considered as children in data.json
    static constexpr int SchoolAgeLimit = 24;

    // under 16 cannot work
    static constexpr int MinWorkingAge = 16;

    static constexpr int SeniorAge = 65;

    // For contact tracing
    //@{
    int TracedIncubationPeriod = 7;
    float ProbabilityToTest = 0.7f;
    float TestAccuracy = 0.65f;
    float ContactTracingProbability = 0.7f;
    float TestLockDownAcceptanceProbability=0.6f;
    //@}

    /// Strength of the infection, to be calibrated to reach the good R0.
    float InfectionStrength = 0.25f;

    // TODO: use real contagious duration adding a mean() method to WeibullDistribution
    float AverageContagiousDuration = 1.0f + 17.0f * .9f + 20.0f * .1f;

    TruncatedWeibullDistribution ExposedDuration = {2, 3.6f, 13};        //< time spend in E state
    TruncatedWeibullDistribution AsymptomaticDuration = {0.7f, 0.1f, 3}; //< time spend asymptomatic
    TruncatedWeibullDistribution MinorInfectionDuration = {5, 17.4, 28}; //< time spend in I1
    TruncatedWeibullDistribution MajorInfectionDuration = {5, 20.7, 34}; //< time spend in I2
    TruncatedWeibullDistribution MajorInfectionDurationBeforeDeath = {2, 10.5f, 34}; //< time spent in I2 before dying

    float AtRiskMajorInfectionRatio = 0.241f; //< E2 / (E1+E2) for at risk persons
    float OtherMajorInfectionRatio = 0.046f;  //< E2 / (E1+E2) for not at risk persons
    float LethalRate = 0.15f;                 //< Probability to die in I2 stage
    float ICURatio = 0.24f;                   //< Probability to need ICU for I2

    /// Returns a probable stage duration and next stage for an agent
    /// satRatio = min(Number of hospital beds / I2, 1)
    std::pair<Stages, int> sampleStageDuration(Stages stage, Random &re, float satRatio) const
    {
        switch (stage)
        {
        case Stages::ExposedMinor:
            return {Stages::AsymptomaticMinor, ExposedDuration.sample(re)};
        case Stages::ExposedMajor:
            return {Stages::AsymptomaticMajor, ExposedDuration.sample(re)};
        case Stages::AsymptomaticMinor:
            return {Stages::InfectedMinor, AsymptomaticDuration.sample(re)};
        case Stages::AsymptomaticMajor:
            return {Stages::InfectedMajor, AsymptomaticDuration.sample(re)};
        case Stages::InfectedMinor:
            return {Stages::RecoveredMinor, MinorInfectionDuration.sample(re)};
        case Stages::InfectedMajor:
            ASSERT(satRatio >= 0 && satRatio <= 1);
            if (re.rand() < ICURatio * (1 - satRatio) + LethalRate * satRatio)
                return {Stages::Dead, MajorInfectionDurationBeforeDeath.sample(re)};
            else
                return {Stages::RecoveredMajor, MajorInfectionDuration.sample(re)};
        case Stages::Sensitive:
        case Stages::RecoveredMinor:
        case Stages::RecoveredMajor:
        case Stages::Dead:
            // Stable states
            // Use from initialization only
            return { stage, -1 };
        case Stages::COUNT:
            break;
        }
        throw;
    }

    //@{
    /// Part of day spent in each spot, average on a week.
    float WorkTime = 0.3f;
    float SchoolTime = 0.3f;
    float NeighborhoodTime = 0.2f;
    //@}

    /// Forbidding of some spots reduce the frequency of visit
    EnumArray<float, SocialDistancingValues> NeighborhoodProbabilities = {
        0.8,    // None
        0.5f, // NoMoreThan100
        0.33f, // NoMoreThan10
        0.2f, // Maximum
    };

    HistogramDistribution<int, float> Neighborhood1Sizes = {{1000, 2000, 10000}, {.95f, .05f}, 1.0f};
    HistogramDistribution<int, float> Neighborhood2Sizes = {{100, 200, 1000}, {.95f, .05f}, 1.0f};

    /// Maximum number of unique persons met on a unique spot each day.
    EnumArray<int, SocialDistancingValues> MaxContacts = {
        20, // None
        20,  // NoMoreThan100
        15,  // NoMoreThan10
        10,  // Maximum
    };

    //@{
    /// Factor to weight the risks of transmission based on political measures
    float BarrierGesturesPromiscuityFactor = 0.5f;
    //@}

    /// From the start of the simulation, day at which barrier gestures start to apply
    int BarrierGesturesStartDay = 14;

    /// Probability for an agent to comply with political measures
    float MeasuresComplianceProbability = 0.9f;

    /// Number of closer spots of this kind to take into account when assigning an agent to one of
    /// them
    EnumArray<int, SpotKinds> SpotsScatteringFactor = {{{
        1,    // Home
        1000, // Work
        6,    // School
        50,   // Neighborhood1
        50    // Neighborhood2
    }}};

    /// Nunber of simulated days
    /// Make sure this matches the number of weeks of measures in Areas.
    int SimulationDuration = 0;

    /// Simulation first day in 2020 year
    int SimulationStart = 10 * 7;

    /// Stop if the situation is stable, even if SimulationDuration is not reached
    /// (ie. if no more E,A and I)
    bool EpidemyFinishedStop = true;

    /// Stop when the number of dead is too high (ratio of the whole population)
    float DeadRatioThresholdStop = 0.02f;

    /// Use a value < 1 to diminuish the accuracy of the simulation (for speed purpose)
    float PopulationSamplingRatio = 0.001f;


    /// Area-specific parameters
    std::vector<Area> Areas = {};

    bool isHolidays(int day, AreaId areaId) const
    {
        return Areas[areaId].SchoolHolidays[SimulationStart + day];
    }

    Area &getAreaByName(const std::string &name)
    {
        for (Area &area : Areas)
        {
            if (area.Name == name)
                return area;
        }
        throw Error("Area not in parameters: ", name);
    }

    void resize(int days)
    {
        for (Area &area : Areas)
        {
            area.ForcedTelework.resize(days / 7 + 1);
            area.SchoolClosure.resize(days / 7 + 1);
            area.SocialDistancing.resize(days / 7 + 1);
            area.HighRiskLockDown.resize(days / 7 + 1);
            area.ContactTracing.resize(1 + (days + TracedIncubationPeriod) / 7);
            area.SchoolHolidays.resize(days + SimulationStart);
        }
    }
};
} // namespace decov
