#pragma once
#include "Parameters.h"
#include <third_parties/nlohmann/json.hpp>
namespace decov
{
/// Loader for statistical data from json
struct DataLoader
{
    Parameters &p;
    Random re;

    DataLoader(Parameters &p, int seed)
      : p(p),
        re(seed)
    {
        // ENABLE_TRACE_FOR(this);
    }

    using json = nlohmann::json;

    template <typename E, typename V>
    void importEnumArray(EnumArray<V, E> &array, const json &j)
    {
        if ((int)j.size() != (int)E::COUNT)
            throw Error("Unpexpected number of element in array");

        for (auto it = j.begin(); it != j.end(); ++it)
        {
            const std::string key = it.key();

            V value = it.value();

            E e;
            if (!try_parse_enum(key, e))
                throw Error("Unexpected key: ", key);
            array[e] = value;
        }
    }

    template <typename T, typename CB>
    void importByAge(ListDistribution<T> &t, const json &j, CB valueConverter)
    {
        t.map.clear();

        std::map<int, T> map;
        for (auto it = j.begin(); it != j.end(); ++it)
        {
            std::string age_s = it.key();
            if (age_s == "total" || age_s == "working_ratio")
                continue;
            int age = std::stoi(age_s);
            map[age] = valueConverter(it.value());
        }

        for (auto &[age, value] : map)
        {
            t.map.emplace_back(age, value);
        }

        if (t.map.back().first > Parameters::MaxAge)
            throw Error("Age should not exceed", Parameters::MaxAge);
    }

    void importQuantiles(HistogramDistribution<int, float> &t, const json &j)
    {
        int q1 = (int)(0.5f + (float)j["q1"]);
        int q2 = (int)(0.5f + (float)j["q2"]);
        int q3 = (int)(0.5f + (float)j["q3"]);
        int min = j["min"];
        int max = j["max"];

        if (j.size() != 5)
            throw Error("Invalid quantiles format");

        t.bounds = {min, q1, q2, q3, max};
        t.numbers = {.25f, .25f, .25f, .25f};
        t.sum = 1.0f;
    }

    template <typename T>
    void importHistogram(HistogramDistribution<int, T> &t, const json &j)
    {
        t.bounds.clear();
        t.numbers.clear();

        std::map<int, T> map;

        for (auto it = j.begin(); it != j.end(); ++it)
        {
            std::string age_s = it.key();
            T value = it.value();
            int age = std::stoi(age_s);
            map[age] = value;
        }

        T sum = 0;
        for (auto &[age, value] : map)
        {
            t.bounds.push_back(age);
            t.numbers.push_back(value);
            sum += value;
        }

        t.sum = sum;

        if (t.numbers.back() != 0)
            throw Error("Last bound value in histogram must be zero");

        t.numbers.pop_back();
    }

    void importNormal(NormalDistribution<int> & dist, const json & j)
    {
        dist = { (int) j["min"], (int)j["max"], (float)j["mean"], (float)j["std"] };
    }

    void load(std::istream &jsonFile)
    {
        json areas;
        jsonFile >> areas;

        for (auto & area: p.Areas)
        {
            if (areas.find(area.Name) == areas.end())
                throw Error("Area not in data: ", area.Name);
        }

        [[maybe_unused]] int grandTotalPop = 0;
        [[maybe_unused]] int targetPopSum = 0;
        for (auto it = areas.begin(); it != areas.end(); ++it)
        {
            std::string name = it.key();
            const json &areaInfo = it.value();

            Area &area = p.getAreaByName(name);
            const json &municipalitiesInfo = areaInfo["municipalities"];

            TRACE("Load area", name, "with", municipalitiesInfo.size(), "municipalities");

            // Ratio of people who cannot telework because of their job
            area.CannotTeleworkRatio = areaInfo["cannot_telework"];

            // Import schools distributions
            importQuantiles(area.SchoolSizes, areaInfo["schools"]);

            // Import workplaces distribution
            importHistogram(area.WorkplaceSizes, areaInfo["workplaces"]);

            // Import At risk ratios
            importByAge(area.AtRiskPersons, areaInfo["at_risk"], [](const json &j) { return (float)j; });

            // Import holidays
            const json &holidays = areaInfo["holidays"];
            area.SchoolHolidays.clear();
            for (auto it2 = holidays.begin(); it2 != holidays.end(); ++it2)
            {
                int from = (*it2)["from_day"];
                int to = (*it2)["to_day"];
                if (from > to)
                    throw Error("Invalid bound");
                while ((int)area.SchoolHolidays.size() < from)
                {
                    area.SchoolHolidays.push_back(false);
                }
                while ((int)area.SchoolHolidays.size() <= to)
                {
                    area.SchoolHolidays.push_back(true);
                }
            }

            // Import home distributions
            const json &homesInfo = areaInfo["homes"];

            // Number of homes
            importEnumArray(area.NumHomesByKind, homesInfo["total"]);
            float total = std::accumulate(area.NumHomesByKind.begin(), area.NumHomesByKind.end(), 0.0f);
            for (float & ratio : area.NumHomesByKind)
            {
                ratio /= total;
            }
            area.NumHomes = re.round(total * p.PopulationSamplingRatio);

            // Working ratio
            importEnumArray(area.WorkingRatios, homesInfo["working_ratio"]);

            // Age repartition
            importHistogram(area.Ages, areaInfo["ages_pyramid"]);

            importByAge(area.HomesDistribution, homesInfo, [&](const json &j) {
                EnumArray<float, HomeKinds> a;
                importEnumArray(a, j);
                float total = std::accumulate(a.begin(), a.end(), 0.0f);
                for (float & ratio: a)
                {
                    ratio /= total;
                }
                return a;
            });

            // Import municipalities
            area.Municipalities.clear();

            // A first pass to compute an accurate sampling population ratio
            int seed = re.rand<int>(0, 10000);
            Random re2(seed);
            int totalPop = 0;
            for (auto it2 = municipalitiesInfo.begin(); it2 != municipalitiesInfo.end(); ++it2)
            {
                totalPop += re2.round((float)(*it2)["Population"] * 1000.0f * p.PopulationSamplingRatio);
            }
            float adjustRatio = (float)area.Agents / totalPop;
            targetPopSum += area.Agents;

            // Second pass with same seed to reach the wanted target
            re2.setSeed(seed);
            for (auto it2 = municipalitiesInfo.begin(); it2 != municipalitiesInfo.end(); ++it2)
            {
                const json &c = *it2;

                std::string lat_s = c["X"];
                float lat = std::stof(lat_s);
                float y_km = lat * 111;

                std::string lng_s = c["Y"];
                float lng = std::stof(lng_s);
                float x_km = lng * 111 * std::cos(lat * M_PI / 180);

                float area_ha = c["Area"];
                float area_km = area_ha / 100;
                float radius = std::sqrt(area_km / M_PI);

                int sampledPop =
                    re2.round((float)c["Population"] * 1000 * p.PopulationSamplingRatio * adjustRatio);
                ASSERT(sampledPop >= 0);
                if (sampledPop == 0)
                    // Probabilistic skipping of too small municipalities
                    continue;

                grandTotalPop += sampledPop;
                area.Municipalities.push_back({sampledPop, radius, x_km, -y_km});
            }
        }

        TRACE("Target pop:", targetPopSum, "Sum of municipalities pop:", grandTotalPop);
    };
};
} // namespace decov
