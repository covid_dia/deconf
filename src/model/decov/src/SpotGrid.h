#pragma once
#include <algorithm>
#include <cmath>
#include <limits>
#include <vector>
namespace decov
{
/// Quick access to spots based on their geographical position, for assigning agents
struct SpotGrid
{
    struct Cell
    {
        std::vector<SpotIndex> spots = {};
        int counter = -1;
    };

    static constexpr float N = 5;
    float minY, minX;
    int width, height;
    float step;
    std::vector<Cell> grid;

    explicit SpotGrid(const std::vector<Spot> &spots)
    {
        //ENABLE_TRACE_FOR(this);

        float maxX, maxY;

        minY = minX = std::numeric_limits<float>::max();
        maxY = maxX = -std::numeric_limits<float>::max();
        for (auto &p : spots)
        {
            minX = std::min(p.x, minX);
            maxX = std::max(p.x + .1f, maxX);
            minY = std::min(p.y, minY);
            maxY = std::max(p.y + .1f, maxY);
        }

        // try to reach size*size*N ~= spots.size()
        step = std::sqrt((float)spots.size() / N) * .5f * (1 / (maxX - minX) + 1 / (maxY - minY));

        width = 1 + (int)((maxX - minX) * step);
        height = 1 + (int)((maxY - minY) * step);
        grid.resize(width * height);

        TRACE("Create", width, "x", height, "grid for", spots.size(), "spots");

        for (int index = 0; index < (int)spots.size(); ++index)
        {
            add(index, spots[index]);
        }
    }

    // Suitable to compute coordinates for a spot which has not been added
    // to the grid (like reference home spot)
    std::pair<int, int> getCellCoordsClamped(const Spot & spot) const
    {
        int x = (int)((spot.x - minX) * step);
        int y = (int)((spot.y - minY) * step);
        x = std::min(width - 1, std::max(0, x));
        y = std::min(height - 1, std::max(0, y));
        return {x, y};
    }

    int getCellIndex(std::pair<int, int> coords) const
    {
        int x = coords.first;
        int y = coords.second;
        ASSERT(x < width && y < height);
        return x + y * width;
    }

    Cell & getCell(std::pair<int, int> coords)
    {
        int index = getCellIndex(coords);
        ASSERT(index >= 0 && index < (int)grid.size());
        return grid[index];
    }

private:
    // Compute the coordinates in the grid of the given spot, provinding it is in the list passed to
    // the constructor.
    std::pair<int, int> getCellCoords(const Spot &spot)
    {
        float x = (spot.x - minX) * step;
        float y = (spot.y - minY) * step;
        ASSERT(x >= 0 && x < width && y >= 0 && y < height);
        return {(int)x, (int)y};
    }

    void add(SpotIndex index, const Spot &spot)
    {
        std::pair<int, int> coords = getCellCoords(spot);
        Cell &cell = getCell(coords);
        cell.spots.push_back(index);
    }
};
} // namespace decov
