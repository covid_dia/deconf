#pragma once
#include "ContactHistory.h"
#include "Parameters.h"
#include "SimulationState.h"
namespace decov
{

/// Simulator that unrolls the simulation day after day, making SimulationState evolve.
struct Simulator
{
    const Parameters &parameters;
    SimulationState &state;
    Random re;
    std::vector<IndexInSpot> choiceBuffer;
    std::vector<int> numContacts;
    bool mustKeepContacts = false;
    bool alwaysKeepContacts = false;

    Simulator(const Parameters &parameters, SimulationState &state, int seed)
        : parameters(parameters), state(state)
    {
        // ENABLE_TRACE_FOR(this);

#pragma omp parallel private(re)
        {
            re.setSeed(seed);
        }
    }

    /// Runs a day of simulation, updating simulation state
    void runDay()
    {
        checkContactTracingMeasure();
        allocateTime();
        spreadInfection();
        updateHospitalSaturation();
        updateStages();
        computeR0();
        computeContactsMatrix();
        advanceToNextDay();
    }

    // Check contact tracing future measures to know if contacts must be kept
    void checkContactTracingMeasure()
    {
#if DECOV_DUMP_STATS == 1
        mustKeepContacts = true;
#else
#if DECOV_TRACE_CONTACTS == 1
        if (alwaysKeepContacts)
        {
            mustKeepContacts = true;
            return;
        }

        mustKeepContacts = false;
        for (const Area &area : parameters.Areas)
        {
            int week = state.day / 7;
            for (int i = 0; i < 1 + parameters.TracedIncubationPeriod / 7; ++i)
            {
                if (area.ContactTracing[week + i] != ContactTracingValues::None)
                {
                    mustKeepContacts = true;
                    break;
                }
            }
            if (mustKeepContacts)
                break;
        }
#endif
#endif
    }

    void advanceToNextDay()
    {
        state.day++;
#if DECOV_TRACE_CONTACTS == 1
        state.contactHistory.newDay();

#pragma omp parallel for
        for (int s = 0; s < (int)state.spots[SpotKinds::Home].size(); ++s)
        {
            Spot &home = state.spots[SpotKinds::Home][s];

            // Decrease lock-down count-down
            home.lockDownRemainingDays -= (home.lockDownRemainingDays > 0);
        }
#endif
    }

    // Agents choose their time spent in each assigned spot according to the policy
    void allocateTime()
    {
#pragma omp parallel for private(re)
        for (int agentId = 0; agentId < (int)state.agents.size(); ++agentId)
        {
            auto &agent = state.agents[agentId];

            float homeTime = 1;
            float workTime = 0;
            float schoolTime = 0;
            float neighborhood1Time = 0;
            float neighborhood2Time = 0;

            if ((agent.stage == Stages::InfectedMajor) | (agent.stage == Stages::Dead))
            {
                // Hospital or dead => out of the simulation
                homeTime = 0;
            }
            else
            {
                AreaId areaId = state.getHome(agent).areaId;
                auto &area = parameters.Areas[areaId];
                int week = state.day / 7;
                bool infringe = re.rand() > parameters.MeasuresComplianceProbability;

                bool authorizedWeek = (agentId % 2 == week % 2);

                // School
                // Note: we consider that even an infringing child won't go to school if school is
                // closed ;)
                bool goToSchool = false;
                auto schoolMeasure = area.SchoolClosure[week];
                bool schoolOpened =
                    (int)schoolMeasure <= (int)SchoolClosureValues::OnlyHigherEducationClosed ||
                    (schoolMeasure == SchoolClosureValues::HigherEducationClosedAlternatingPupils &&
                     authorizedWeek);
                if (agent.has(SpotKinds::School) && !parameters.isHolidays(state.day, areaId))
                {
                    ASSERT(agent.age <= Parameters::SchoolAgeLimit);

                    if (agent.age >= 18)
                        goToSchool = schoolMeasure == SchoolClosureValues::None;
                    else
                        goToSchool = schoolOpened;
                }

                // Work
                bool goToWork = false;
                bool active = agent.teleworkStatus != TeleworkStatuses::NotActive;
                bool mustTakeCareOfChildren = !schoolOpened && agent.childrenToKeep != ChildrenToKeep::None;

                if (active && !mustTakeCareOfChildren)
                {
                    ASSERT(agent.has(SpotKinds::Work));

                    auto workMeasure = area.ForcedTelework[week];
                    goToWork = infringe ||
                               agent.teleworkStatus == TeleworkStatuses::CannotTelework ||
                               workMeasure == ForcedTeleworkValues::None ||
                               (workMeasure == ForcedTeleworkValues::Alternating && authorizedWeek);
                }

                // Other kind of contacts
                auto socialMeasure = area.SocialDistancing[week];
                bool socialize =
                    infringe || re.rand() < parameters.NeighborhoodProbabilities[socialMeasure];

                // Additional lock down
                if (!infringe)
                {
                    if (
                        // Lock down symptomatic if at least one SocialDistancing measure
                        (socialMeasure != SocialDistancingValues::None &&
                         agent.stage == Stages::InfectedMinor) ||
#if DECOV_TRACE_CONTACTS == 1
                        // Lock down if belongs to the contacts of a confirmed infected agent
                        state.getHome(agent).lockDownRemainingDays > 0 ||
#endif
                        // Lock down agents who live with an at risk agent if recommanded
                        (agent.atRiskAtHome &&
                         area.HighRiskLockDown[week] == HighRiskLockDownValues::Recommanded))
                    {
                        goToWork = goToSchool = socialize = false;
                    }
                }

                // Compute time spent in each spot
                if (goToWork)
                {
                    // Agent is allowed to work, reduce it's hometime by worktime
                    workTime = parameters.WorkTime;
                    homeTime -= parameters.WorkTime;

                    #pragma omp atomic
                    state.indicators.areas[areaId].activityRate += 1;
                }
                else if (goToSchool)
                {
                    // Agent is allowed to go to school, reduce it's hometime by schooltime
                    schoolTime = parameters.SchoolTime;
                    homeTime -= parameters.SchoolTime;
                }
                else if (agent.teleworkStatus == TeleworkStatuses::CanTelework)
                {
                    float rate;
                    switch (agent.childrenToKeep)
                    {
                    case ChildrenToKeep::Under15: rate = .75f; break;
                    case ChildrenToKeep::Under12: rate = .25f; break;
                    default:
                        rate = 1;
                    }
                    #pragma omp atomic
                    state.indicators.areas[areaId].activityRate += rate;
                }

                if (socialize)
                {
                    // Agent is allowed to socialize or it gets a good roll to shop:
                    // choose between one of the two neighborhood spots
                    // and reduce it's hometime.
                    if (re.rand() < 0.5f)
                        neighborhood1Time = parameters.NeighborhoodTime;
                    else
                        neighborhood2Time = parameters.NeighborhoodTime;
                    homeTime -= parameters.NeighborhoodTime;
                }
            }

            // Allocate spent time into spots
            state.allocateTime(agent, SpotKinds::Home, homeTime);
            if (agent.has(SpotKinds::Work))
                state.allocateTime(agent, SpotKinds::Work, workTime);
            else if (agent.has(SpotKinds::School))
                state.allocateTime(agent, SpotKinds::School, schoolTime);
            state.allocateTime(agent, SpotKinds::Neighborhood1, neighborhood1Time);
            state.allocateTime(agent, SpotKinds::Neighborhood2, neighborhood2Time);

            TRACE("Allocate:");
            TRACE(" home=", homeTime);
            TRACE(" work=", workTime);
            TRACE(" school=", schoolTime);
            TRACE(" neighborhood1=", neighborhood1Time);
            TRACE(" neighborhood2=", neighborhood2Time);
        }
    }

    // Computes who is newly infected after this day: for each sensitive
    // agent in each spot, compute its infected flag according to met agents.
    void spreadInfection()
    {
#pragma omp parallel for private(numContacts, choiceBuffer, re)
        for (int sk = 0; sk < (int)SpotKinds::COUNT; ++sk)
        {
            auto spotKind = (SpotKinds)sk;
            auto &spots = state.spots[spotKind];

#pragma omp parallel for private(numContacts, choiceBuffer, re)
            for (int s = 0; s < (int)spots.size(); ++s)
            {
                auto &spot = spots[s];

                // Update list of agents which are present today
                choiceBuffer.clear();
                bool hasContagious = false;
                bool hasSensitive = false;
                for (IndexInSpot a = 0; a < (IndexInSpot)spot.agents.size(); ++a)
                {
                    if (spot.presenceRatios[a] == 0)
                        continue;

                    choiceBuffer.push_back(a);
                    auto &agent = state.agents[spot.agents[a]];

                    hasContagious |= agent.contagious();
                    hasSensitive |= (agent.stage == Stages::Sensitive);
                }

                int numPresent = (int)choiceBuffer.size();
                TRACE(numPresent, "agents present in spot", spotKind, "of size", spot.size);

#if DECOV_DUMP_STATS == 0 && DECOV_TRACE_CONTACTS == 0
                if ((!hasContagious) | (!hasSensitive))
                    continue;
#endif

                auto &area = parameters.Areas[spot.areaId];
                int maxContacts = parameters.MaxContacts[area.SocialDistancing[state.day / 7]];

                if (numPresent <= maxContacts + 1)
                {
                    // Every agent meets every other agent
                    for (int i = 0; i < numPresent; ++i)
                    {
                        IndexInSpot a1 = choiceBuffer[i];
                        for (int j = i + 1; j < numPresent; ++j)
                        {
                            IndexInSpot a2 = choiceBuffer[j];

                            simulateContact(spot, a1, a2);
                        }
                    }
                }
                else // maxContacts < numPresent - 1
                {
                    // Random choice of contacts to insure no more than maxContacts per agent
                    numContacts.clear();
                    numContacts.resize(spot.agents.size());
                    re.shuffle(choiceBuffer.begin(), choiceBuffer.end());

                    while (numPresent > 1)
                    {
                        IndexInSpot a1 = choiceBuffer.back();
                        numPresent--;
                        choiceBuffer.pop_back();

                        int start = re.rand<int>(0, numPresent - 1);
                        int numRemainingContacts = std::min(numPresent, maxContacts - numContacts[a1]);

                        for (int j = 0; j < numRemainingContacts; ++j)
                        {
                            int k = (start + j) % numPresent;
                            IndexInSpot a2 = choiceBuffer[k];

                            if (numContacts[a2] >= maxContacts)
                                continue;

                            numContacts[a1]++;
                            numContacts[a2]++;
                            simulateContact(spot, a1, a2);
                        }
                    }
                }
            }
        }
    }

    // Computes if the two given agent met enough to spread infection during this day.
    void simulateContact(const Spot &spot, IndexInSpot a1, IndexInSpot a2)
    {
        TRACE("Simulate contact at", spot, "between agents", a1, "and", a2);

        AgentId id1 = spot.agents[a1];
        AgentId id2 = spot.agents[a2];

        ASSERT(id1 != id2);

        Agent &agent1 = state.agents[id1];
        Agent &agent2 = state.agents[id2];

#if DECOV_TRACE_CONTACTS == 1
        if (mustKeepContacts)
            state.contactHistory.addContact(id1, id2);
#endif
        // Find who is sensitive and who is contagious
        Agent *sensitive;
        [[maybe_unused]] Agent *contagious;

        if (agent1.stage == Stages::Sensitive)
        {
            if (!agent2.contagious())
                return;

            sensitive = &agent1;
            contagious = &agent2;
        }
        else if (agent2.stage == Stages::Sensitive)
        {
            if (!agent1.contagious())
                return;

            sensitive = &agent2;
            contagious = &agent1;
        }
        else
            return;

        // Compute promiscuity factor based on barrier gestures
        float promiscuityFactor;
        if (parameters.BarrierGesturesStartDay >= state.day)
            promiscuityFactor = parameters.BarrierGesturesPromiscuityFactor;
        else
            promiscuityFactor = 1;

        // Compute probability to spread
        float e = re.rand();
        float p = parameters.InfectionStrength * spot.presenceRatios[a1] * spot.presenceRatios[a2] *
                  promiscuityFactor;
        bool infected = e < p;

        if (!infected)
            return;

            // Apply infection
#if DECOV_DUMP_STATS == 1
        bool oldStatus;
#pragma omp atomic capture
        {
            oldStatus = sensitive->infected;
            sensitive->infected = true;
        }
        if (oldStatus == false)
        {
#pragma omp atomic
            contagious->numInfections++;
        }
#else
        sensitive->infected = true;
#endif
    }

    void updateHospitalSaturation()
    {
// Update hospital saturation indicators for each area before modification of counts
#pragma omp parallel for private(re)
        for (int areaId = 0; areaId < (int)parameters.Areas.size(); ++areaId)
        {
            auto &areaIndicators = state.indicators.areas[areaId];
            int hospitalized = areaIndicators.counts[Stages::InfectedMajor];
            int beds = parameters.Areas[areaId].HospitalBeds;
            if (hospitalized <= beds)
                areaIndicators.satRatio = 1.0f;
            else
                areaIndicators.satRatio = (float)beds / (float)hospitalized;
        }
    }

    // Updates infection stages of all agents.
    void updateStages()
    {
#pragma omp parallel for private(re)
        for (AgentId agentId = 0; agentId < (int)state.agents.size(); ++agentId)
        {
            auto &agent = state.agents[agentId];
            AreaId areaId = state.getHome(agent).areaId;
            auto &areaIndicators = state.indicators.areas[areaId];

            if (agent.infected)
            {
                float majorProb = re.rand();
                if ((agent.atRisk && majorProb < parameters.AtRiskMajorInfectionRatio) ||
                    majorProb < parameters.OtherMajorInfectionRatio)
                    agent.nextStage = Stages::ExposedMajor;
                else
                    agent.nextStage = Stages::ExposedMinor;
                agent.infected = false;
            }
            else
            {
                agent.stageRemainingDays--;
                if (agent.stageRemainingDays != 0)
                    continue;
            }

#pragma omp atomic
            --areaIndicators.counts[agent.stage];

#pragma omp atomic
            ++areaIndicators.counts[agent.nextStage];

            agent.stage = agent.nextStage;
            auto p = parameters.sampleStageDuration(agent.stage, re, areaIndicators.satRatio);
            agent.nextStage = p.first;
            agent.stageRemainingDays = p.second;

            if ((agent.stage != Stages::InfectedMinor) & (agent.stage != Stages::InfectedMajor))
                continue;

            lockDown(agentId, areaId);
        }
    }

    void lockDown(AgentId agentId, int areaId)
    {
#if DECOV_TRACE_CONTACTS == 1
        const auto &contact_tracing = parameters.Areas[areaId].ContactTracing[state.day / 7];
        if (contact_tracing == ContactTracingValues::None)
            return;

        // If full lockdown : skip contact testing probability check
        if (contact_tracing == ContactTracingValues::TestAndLockDownContacts &&
            re.rand() >= parameters.ProbabilityToTest)
            return;

        auto &areaIndicators = state.indicators.areas[areaId];

#pragma omp atomic
        areaIndicators.tested++;

        // Lock down agent and its family even if test is negative
        if (contact_tracing == ContactTracingValues::TestAndLockDownContactsFull ||
            re.rand() <= parameters.TestLockDownAcceptanceProbability)
        {
            Agent &agent = state.agents[agentId];
            int lockDownDuration = parameters.TracedIncubationPeriod;
            if (agent.stage == Stages::InfectedMinor)
                // If not hospitalized, lock down until recovered
                lockDownDuration = std::max(lockDownDuration, agent.stageRemainingDays);
            state.getHome(agent).lockDownRemainingDays = lockDownDuration;
        }

        if (re.rand() >= parameters.TestAccuracy)
            // Negative result
            return;

        [[maybe_unused]] int tracedContacts = 0;

        // Lock down all contacts met in the last Parameters::MaxContactKeptDays days
        for (auto &day : state.contactHistory)
        {
            day.getContacts(agentId, [&](AgentId cid) {
                if (re.rand() < parameters.ContactTracingProbability)
                {
                    // Lock down contact home
                    Spot &contactHome = state.getHome(state.agents[cid]);
#if DECOV_DUMP_STATS == 1
#pragma omp atomic
                    areaIndicators.tracedContacts++;
#endif
                    contactHome.lockDownRemainingDays = parameters.TracedIncubationPeriod;
                }
            });
        }
#endif
    }

    void computeR0()
    {
#if DECOV_DUMP_STATS == 1
        int sumInfections = 0;
        int sumContagiousDays = 0;

        for (auto &agent : state.agents)
        {
            if (agent.contagious())
                agent.numContagiousDays++;

            if (agent.numContagiousDays > 0)
            {
                sumContagiousDays += agent.numContagiousDays;
                sumInfections += agent.numInfections;
            }
        }

        state.indicators.R0 = parameters.AverageContagiousDuration * sumInfections / sumContagiousDays;
#endif
    }

    void computeContactsMatrix()
    {
#if DECOV_DUMP_STATS == 1
#if DECOV_TRACE_CONTACTS == 0
#error "Contact tracing must be enabled"
#endif
        // Uncomment to keep contact matrix by day instead of cumulated
        // state->indicators.contactsMatrix.clear();

        // Fill contacts matrix using current day of contact history
        auto & fullMatrix = state.contactHistory.currentDay();

        #pragma omp parallel for
        for (int agentId = 0; agentId < (int)state.agents.size(); ++agentId)
        {
            Agent & agent1 = state.agents[agentId];
            int age1 = agent1.age;
            ASSERT(age1 <= Parameters::MaxAge);
            fullMatrix.getContacts(agentId, [&](int contactId){

                Agent & agent2 = state.agents[contactId];
                int age2 = agent2.age;

                ASSERT(age2 <= Parameters::MaxAge);

                #pragma omp atomic
                state.indicators.contactsMatrix.contacts[age1 / ByAgeContactMatrix::Step][age2 / ByAgeContactMatrix::Step] ++;
            });
        }
#endif
    }
}; // namespace decov
} // namespace decov
