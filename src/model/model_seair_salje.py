# -*- coding: utf-8 -*-
""" 
Epidemiologic model adapted from Pasteur institute paper:
https://hal-pasteur.archives-ouvertes.fr/pasteur-02548181


The model is an SEAIR with age classes and contact probabilities:

S -> E -> A -> I1 -> R
               I1 -> I2 -> M
                     I2 -> R

with E      : exposed with minor infection
     A      : asymptomatic with minor infection
     I1 (I2): people with minor infectious (major infection, ie hospitalized)
     R      : recovered people
     M      : deceased people

Probabilities are extracted from Béraud et al, PloSONE 2015.

"""

from copy import deepcopy

import numpy as np
import pandas as pd
import scipy
from scipy.integrate import odeint
import saljepp

def get_contact_matrix(path="contact_matrix_weekday_all.csv"):
    df_contact = pd.read_csv(path)
    label = df_contact.columns[1:]
    contacts = df_contact.values[:, 1:].astype(float)
    return contacts


def get_age_proba(path="doc/france_contact_page.csv"):
    df_demo = pd.read_csv(path)
    demography = df_demo["demography"].values
    p_age = df_demo["p_age"].values
    p_age = p_age / sum(p_age)
    return demography, p_age


def make_contact_symmetric(contacts, proportion):
    sym_contact = np.zeros_like(contacts)
    for i in range(len(contacts)):
        for j in range(len(contacts)):
            sym_contact[i, j] = (1 / (proportion[j] + proportion[i])) * (
                    (contacts[i, j] * proportion[i]) + contacts[j, i] * proportion[j])
    return sym_contact


##############################################################
# Model

def SEAIR2y0_salje(S: float, E: float, A: float,
             I1: float, I2: float, R: float,  # C: float,
             M: float):
    return [S, E, A, I1, I2, R, M]


# TODO Léonard: load ratio values from JSON
def measures2c_salje(contact_tracing: int = 0,
                     forced_telecommuting: int = 0,
                     school_closures: int = 0,
                     social_containment_all: int = 0,
                     total_containment_high_risk: int = 0,
                     c_max_containment: float = 0.9):
    # HARD CODED
    # <20, 20-29, 30-39, 40-49, 50-59, 60-69, 70-79, 80+
    # active: [1:5]
    # high risks: [6:]
    n_ages = 8  # number of age classes

    prop_pop_active = 0.4
    prop_pop_parent = 0.2
    prop_pop_children = 0.2
    prop_pop_high_school = 0.1
    prop_pop_tot_high_risk = 0.2
    prop_pop_act_high_risk = 0.1
    prop_parent_active = 0.8

    prop_time_work = 0.3
    prop_time_school = 0.3
    prop_time_social = 0.2
    prop_follow_advice_hr = 0.7

    # norm_parameter equals to c_max_containment_value_target/c_max_containment_value_computed
    norm_parameter = c_max_containment / 0.39280000000000004

    c = np.zeros(n_ages)
    # c = 0
    prop_active_parent_still_going_to_work = 1
    prop_high_risk_people_still_going_to_work = 1
    if forced_telecommuting == 1:
        c[1:5] += prop_pop_active * prop_time_work / 2
        prop_active_parent_still_going_to_work = 0.5
        prop_high_risk_people_still_going_to_work = 0.5
    if forced_telecommuting == 2:
        c[1:5] += prop_pop_active * prop_time_work
        prop_active_parent_still_going_to_work = 0
        prop_high_risk_people_still_going_to_work = 0

    if school_closures >= 1:
        c[2] += prop_pop_high_school * prop_time_school
    if school_closures == 2:
        c[1:2] += prop_pop_children * prop_time_school / 2
        c[1:2] += prop_pop_parent * prop_parent_active * prop_time_work * prop_active_parent_still_going_to_work / 4
    if school_closures == 3:
        c[1:2] += prop_pop_children * prop_time_school
        c[1:2] += prop_pop_parent * prop_parent_active * prop_time_work * prop_active_parent_still_going_to_work / 2

    prop_high_risk_people_still_social = 1
    if social_containment_all == 1:
        c += 0.3 * prop_time_social
        prop_high_risk_people_still_social = 0.7
    if social_containment_all == 2:
        c += 0.6 * prop_time_social
        prop_high_risk_people_still_social = 0.4
    if social_containment_all == 3:
        c += 0.9 * prop_time_social
        prop_high_risk_people_still_social = 0.1

    if total_containment_high_risk == 1:
        c[6:] += prop_follow_advice_hr * prop_pop_tot_high_risk * prop_high_risk_people_still_social * prop_time_social
        c[6:] += prop_follow_advice_hr * prop_pop_act_high_risk * prop_high_risk_people_still_going_to_work * prop_time_work

    return c * norm_parameter


# Easier to debug version
def SEAIR_salje_ratios2y0_throws(npop: int, S: float, E: float,
                                 A: float, I1: float, I2: float,
                                 R: float, M: float):
    total = S + E + A + I1 + I2 + R + M
    if abs(total - 1) > 1E-4:
        raise ValueError('incoherent ratios')
    r = npop / total
    # r = 1
    return SEAIR2y0_salje(S=S * r, E=E * r, A=A * r, I1=I1 * r,
                    I2=I2 * r, R=R * r, M=M * r)


def SEAIR_salje_ratios2y0(*args, **kwargs):
    # return [S, E, A, I1, I2, R, M]
    try:
        return SEAIR_salje_ratios2y0_throws(*args, **kwargs)
    except ValueError:
        return None


# Renvoie un jeu de conditions initiales en répartissant les compartiments en fonction d'une pyramide des âges
def SEAIR_salje_ratios_age2y0(p_age, *args, **kwargs):
    # return classes_d'age x [S, E, A, I1, I2, R, M]

    try:
        return np.dot(np.array(SEAIR_salje_ratios2y0_throws(*args, **kwargs)).reshape(-1, 1), p_age.reshape(1, -1))
    except ValueError:
        return None


class ParamsSalje:
    def __init__(self):
        self.beta, self.contact, self.R0, self.p_age, self.epsilon, self.sigma, self.rho1, self.rho2, \
        self.gamm1, self.kappa, self.mort_sup, self.P_I1_I2, self.P_I2_D, \
        self.i_sat, self.measures = [None] * 15

def mu_func(x1, x2, x3):
    """ fonction de mortalité liée à la saturation des hôpitaux
    """
    #  (1 - x1) * 0.001 / (1 + exp(-0.05 * (x2 - i.sat))) # 0 morts tant que l'hosto n'es pas saturé.
    return x1 * (x2 > x3)


def covid_ode_salje(y, t, params):
    beta = params["beta"]
    contact = params["contact"]
    epsilon = params["epsilon"]
    sigma = params["sigma"]
    rho1 = params["rho1"]
    rho2 = params["rho2"]
    # rho3 = params["rho3"]
    gamm1 = params["gamm1"]
    # gamm2 = params["gamm2"]
    kappa = params["kappa"]
    mort_sup = params["mort_sup"]
    i_sat = params["i_sat"]
    P_I1_I2 = params["P_I1_I2"]
    # P_I2_C = params["P_I2_C"]
    P_I2_D = params["P_I2_D"]

    n = P_I2_D.shape[0]  # number of age group
    y = np.asarray(y).reshape((7, n))
    d_states = np.zeros((7, n))

    Mu = mu_func(params["mort_sup"], sum(y[4] * .24), params["i_sat"])  # ICU = I2*.24
    N_tot = y.sum(axis=0)
    N_tot -= y[6]
    Lambda = beta * np.inner(params["measures"], y[[2,3,4]]).sum(axis=1)


    # Lambda = np.asarray([beta * sum([C[i, j] * (y[2, j] + y[3, j] + y[4, j] + y[6, j]) for j in range(n)]) for i in range(n)])
    # dS
    d_states[0] = - Lambda * y[0] - Mu * y[0]
    # dE
    d_states[1] = Lambda * y[0] - epsilon * y[1] - Mu * y[1]
    # dA
    d_states[2] = epsilon * y[1] - sigma * y[2] - Mu * y[2]
    # dI1
    d_states[3] = sigma * y[2] - (rho1 * y[3] * (1 - P_I1_I2)) - (gamm1 * y[3] * P_I1_I2) - Mu * y[3]
    # dI2
    d_states[4] = gamm1 * y[3] * P_I1_I2 - (rho2 * y[4] * (1 - P_I2_D)) - (kappa * y[4] * P_I2_D) - Mu * y[4]
    # dR
    d_states[5] = (rho1 * y[3] * (1 - P_I1_I2)) + (rho2 * y[4] * (1 - P_I2_D)) - Mu * y[5]
    # states[i] = (rho1 * y[3, i])  + (rho2 * y[4, i]) + (rho3 * y[6], i) - Mu * y[5, i]
    # dM
    d_states[6] = kappa * y[4] * P_I2_D + Mu * N_tot

    # print()
    # print(d_states)
    return d_states.flatten().tolist()



def mu_func_optim(x1, x2, x3):
    # x1*(x2>x3)
    if x2 > x3:
        return x1
    return 0


def covid_ode_salje_optim(y, t, params: ParamsSalje):
    n = params.P_I2_D.shape[0]  # number of age group
    y = np.asarray(y).reshape((7, n))
    d_states = np.zeros((7, n))
    #beta = Beta(params)

    Mu = mu_func_optim(params.mort_sup, sum(y[4] * .24), params.i_sat)  # ICU = I2*.24
    N_tot = y.sum(axis=0)
    N_tot -= y[6]
    Lambda = params.beta * np.inner(params.measures, y[[2,3,4]]).sum(axis=1)

    # pprint(Lambda)
    # Lambda = np.asarray([beta * sum([C[i, j] * (y[2, j] + y[3, j] + y[4, j] + y[6, j]) for j in range(n)]) for i in range(n)])
    # dS
    d_states[0] = - Lambda * y[0] - Mu * y[0]
    # dE
    d_states[1] = Lambda * y[0] - params.epsilon * y[1] - Mu * y[1]
    # dA
    d_states[2] = params.epsilon * y[1] - params.sigma * y[2] - Mu * y[2]
    # dI1
    d_states[3] = params.sigma * y[2] - (params.rho1 * y[3] * (1 - params.P_I1_I2)) - (params.gamm1 * y[3] * params.P_I1_I2) - Mu * y[3]
    # dI2
    d_states[4] = params.gamm1 * y[3] * params.P_I1_I2 - (params.rho2 * y[4] * (1 - params.P_I2_D)) - (params.kappa * y[4] * params.P_I2_D) - Mu * y[4]
    # dR
    d_states[5] = (params.rho1 * y[3] * (1 - params.P_I1_I2)) + (params.rho2 * y[4] * (1 - params.P_I2_D)) - Mu * y[5]
    # states[i] = (rho1 * y[3, i])  + (rho2 * y[4, i]) + (rho3 * y[6], i) - Mu * y[5, i]
    # dM
    d_states[6] = params.kappa * y[4] * params.P_I2_D + Mu * N_tot
    # print()
    # print(d_states)
    return d_states.flatten().tolist()

class SEAIRModelSaljeWithAge:
    def __init__(self, parameters: np.ndarray, y0: list):
        self.y0, self.parameters = None, None
        if len(y0) > 0:
            self.y0 = np.copy(y0)
            self.n_states, self.n_ages = self.y0.shape        
        if len(parameters) > 0:
            self.parameters = deepcopy(parameters)
        #self.params["S0"] = self.y0[0]

    def update_y0(self, y0: list):
        if len(y0) > 0:
            self.y0 = np.copy(y0)
            self.n_states, self.n_ages = self.y0.shape

    def update_params(self, params: dict):
        self.parameters = deepcopy(params)       

    def compute(self, c_list: list, time_intervals: list, rtol: float, atol: float, dt:int = 7, version:str="cpp"):
        """ compute ode system states
        """
        assert self.y0 is not None
        assert self.parameters is not None

        assert len(time_intervals) - 1 == len(c_list)
        params = ParamsSalje()
        for attr in ["contact", "p_age", "R0", "epsilon", "sigma", "rho1", "rho2",
                     "gamm1", "kappa", "mort_sup", "P_I1_I2", "P_I2_D",
                     "i_sat"]:
            setattr(params, attr, self.parameters[attr])
        results = []

        parameters = deepcopy(self.parameters)
        parameters["dt"] = dt
        for i in range(len(time_intervals) - 1):
            # update C
            measures = np.diag(1 - np.asarray(c_list[i]))
            measures = np.dot(measures, params.contact)
            beta = self.compute_beta_optim(params)
            setattr(params, "beta", beta)
            setattr(params, "measures", measures)

            beg = time_intervals[i]
            end = time_intervals[i + 1] + 1

            assert time_intervals[i] != time_intervals[i + 1]

            # Resolution ODEs sans contrôle
            if len(results) == 0:
                y0 = self.y0  # [S, E, A, I1, I2, R, M]
            else:
                y0 = results_interv[-1]
            y0 = y0.flatten().tolist()


            if version == "cpp":
                parameters["beta"] = beta
                parameters["measures"] = measures.flatten().tolist()
                run_params = deepcopy(parameters) # TOFIX dictionary is destroy when calling integrate(), temp fix: make a copy
                results_interv = saljepp.integrate(y0, end-beg, run_params)
                size = len(range(beg, end, dt))
                results_interv = np.asarray(results_interv)
                results_interv = results_interv.reshape((size, self.n_states, self.n_ages))
            else:
                results_interv = odeint(covid_ode_salje_optim, y0=y0, t=range(beg, end, 7), args=(params,),
                                        rtol=rtol, atol=atol, mxstep=5000)
            
            results_interv = results_interv.reshape((int((beg - end)/7), self.n_states, self.n_ages))

            if len(results) == 0:
                results.append(results_interv)
            else:
                results.append(results_interv[1:])

        results = np.vstack(results)
        return results

    @staticmethod
    def compute_beta_optim(params: ParamsSalje):
        tmp = (params.contact + params.contact.T * (np.outer(params.p_age, (1 / params.p_age).T))) / 2
        tmp = np.dot(tmp, tmp)
        contacts = np.zeros_like(tmp)
        for i in range(len(contacts)):
            for j in range(len(contacts)):
                contacts[i, j] = tmp[i, j] * params.p_age[i] / params.p_age[j]

        Beta = params.R0 / (
                (params.sigma * (params.rho1 + params.gamm1) * (params.rho2 + params.kappa)) * max(
            np.real(scipy.linalg.eigvals(contacts))))
        return Beta

    @staticmethod
    def compute_beta(params: dict):
        tmp = (params["contact"] + params["contact"].T * (np.outer(params["p_age"], (1 / params["p_age"]).T))) / 2
        tmp = np.dot(tmp, tmp)
        contacts = np.zeros_like(tmp)
        for i in range(len(contacts)):
            for j in range(len(contacts)):
                contacts[i, j] = tmp[i, j] * params["p_age"][i] / params["p_age"][j]

        Beta = params["R0"] / (
                (params["sigma"] * (params["rho1"] + params["gamm1"]) * (params["rho2"] + params["kappa"])) * max(
            np.real(scipy.linalg.eigvals(contacts))))
        return Beta

def default_parameters_salje(C, p_age, i_sat, P_I1_I2, P_I2_D):
    Epsilon = 1 / 4
    Sigma = 1.0
    Gamm1 = 1 / 1.5
    # Gamm2   = 1/1.5 #1/3.
    Rho1 = 1 / 14  # 0.4
    Rho2 = 1 / 20.5  # 1/20.5
    # Rho3    = 0.2 #1/20.5
    Kappa = 1 / 14
    R0 = 3.2
    mort_sup = 1e-5
    c_value = 0.99

    params = {
        "R0": R0,
        "contact": C,
        # "beta": beta,
        "epsilon": Epsilon,
        "sigma": Sigma,
        "gamm1": Gamm1,
        "rho1": Rho1,
        "rho2": Rho2,
        "kappa": Kappa,
        "mort_sup": mort_sup,
        "i_sat": i_sat,
        "P_I1_I2": P_I1_I2,
        # "P_I2_C": P_I2_C,
        "P_I2_D": P_I2_D,
        "p_age": p_age,
    }
    return params


def default_states_salje(S0, I0, p_age, n_age, n_states):
    y0_states = np.zeros((n_states, n_age))
    y0_states[0] = np.asarray([S0] * n_age) * sum(p_age) / n_age
    y0_states[1] = np.asarray([I0] * n_age) * sum(p_age) / n_age
    y0_states = y0_states
    return y0_states

if __name__ == "__main__":

    df = pd.read_json("data/pasteur_data.json")
    P_I1_I2 = df["P(Host|Infected)"].values / 100
    P_I2_D = df["P(Death|Hosp)"].values / 100

    demo, p_age = get_age_proba(path="data/france_contact_page.csv")
    contacts = get_contact_matrix("data/contact_matrix_weekday_all.csv")

    N = 100000
    S0 = (N - 1) / N
    E0 = 1 - S0

    i_sat = 100 / N

    y0 = np.zeros((7, 8))
    y0[0] = np.asarray([S0]*8) * sum(p_age) / 8
    y0[1] = np.asarray([E0]*8) * sum(p_age) / 8
    #y0 = y0.flatten().tolist()

    parameters = default_parameters_salje(contacts, p_age, i_sat, P_I1_I2, P_I2_D)

    model = SEAIRModelSaljeWithAge(parameters, y0)
    c_list = [[0.9]*8]
    model.compute(time_intervals=[0, 1], c_list=c_list, rtol=1e-3, atol=1e-3)

    measures = np.diag(1 - np.asarray(c_list[0]))
    parameters["measures"] = np.dot(measures, parameters["contact"])
    parameters["beta"] = model.compute_beta(parameters)

    res2 = covid_ode_salje(y0.flatten().tolist(), 1, parameters)

    #parameters["measures"] = parameters["measures"].flatten().tolist()
    #res3 = saljepp.covid_ode_salje_pp(y0.flatten().tolist(), 1, parameters)

    #print(res2)
    #print(res3)
