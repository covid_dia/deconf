import json
import threading
import requests
import os
import shutil

from src.model.micromodel import MicroModelRequestHandler, MicroModelServer
from src.model.policy_utils import set_all_measures_activable, all_measures, default_eval_policy_req, \
    default_optimize_req, set_all_objectives_on
from src.optim.base_optim import BaseOptimServer
from src.optim.nsgax_optim import NSGAXOptimRequestHandler
from src.optim.objectives import Objectives

from SALib.sample import saltelli
from SALib.analyze import sobol
import numpy as np
import pickle
import matplotlib.pyplot as plt
import pandas as pd

def tweak_eval_policy(optimized_policy_filename, reoptimize=False):
    # start model server in thread
    model_server = MicroModelServer(('localhost', port), MicroModelRequestHandler)
    threading.Thread(target=model_server.serve_forever).start()

    # load and customize the JSON "optimize_req" request
    optimize_req = default_optimize_req()
    set_all_measures_activable(optimize_req)
    set_all_objectives_on(optimize_req)
    optimize_req['max_measure_steps'] = 4  # weeks

    if reoptimize:
        # start optim server in thread
        optim_server = BaseOptimServer(('localhost', port+1), NSGAXOptimRequestHandler)
        threading.Thread(target=optim_server.serve_forever).start()
        # send "optimize" request
        response = requests.post('http://localhost:'+str(port+1)+'/optimize', data=json.dumps(optimize_req))
        # check results, cf "doc/optimize_res.json"
        optimize_res = response.json()
        assert (optimize_res['success'])
        # take the first policy in the Front and save to file (will be used in tweak_eval_policy)
        optimized_policy = optimize_res['policies'][0]
        #for m in all_measures():
        #    print(m, optimized_policy[m])
        with open(optimized_policy_filename, 'w') as f:
            json.dump(optimized_policy, f)
        # clean
        optim_server.shutdown()
        optim_server.socket.close()
    else:
        # load our optimized_policy
        with open(optimized_policy_filename, 'r') as f:
            optimized_policy = json.load(f)
    
    for _ in [0]:
        break
    
    #for m in all_measures():
    #    print(m, optimized_policy[m])
            
    # load and customize the JSON "eval_policy_req" request
    eval_policy_req = default_eval_policy_req()
    eval_policy_req['measure_weeks'] = optimized_policy['measure_weeks']
    for measure in all_measures():
        eval_policy_req[measure] = optimized_policy[measure]
    
    #------------------------------
    # Sensitivity Analysis:
    
    #dirName = './sensitivity/tmp/'
    if not os.path.exists(dirName):
        os.makedirs(dirName)
    shutil.copy(__file__, dirName+'sensitivity_saved.py')
    
    #pickle.dump(eval_policy_req, open( dirName+'eval_policy_req.pkl', 'wb' ) )

    """
    problem = { # from case0
    'num_vars': 28, # 
    'names': ['AsymptomaticDuration',
              'AtRiskMajorInfectionRatio',
              'BarrierGesturesPromiscuityFactor',
              'BarrierGesturesStartDay',
              'ContactTracingProbability',
              'DispersionNeighborhood1',
              'DispersionNeighborhood2',
              'DispersionSchool',
              'DispersionWork',
              'ExposedDuration',
              'InfectionStrength',
              'MajorInfectionDuration',
              'MajorInfectionDurationBeforeDeath',
              'MaxContacts',
              'MeasuresComplianceProbability',
              'MinorInfectionDuration',
              'NeighborhoodProbabilities',
              'NeighborhoodTime',
              'OtherMajorInfectionRatio',
              'PopulationSamplingRatio',
              'ProbabilityToTest',
              'SchoolTime',
              'SimulationStart',
              'StopWhenDeadRatioExceedsThreshold',
              'TestAccuracy',
              'TestLockDownAcceptanceProbability',
              'TracedIncubationPeriod',
              'WorkTime'], 
    'bounds': [[0.7, 0.1, 3],
               [0.241],
               [0.5],
               [0],
               [0.7],
               [50],
               [50],
               [6],
               [1000],
               [2, 3.6, 13],
               [0.5],
               [5, 20.7, 34],
               [2, 10.5, 34],
               [100, 50, 20, 10],
               [0.9],
               [5, 17.4, 28],
               [1, 0.8, 0.5, 0.2],
               [0.2],
               [0.046],
               [0.001],
               [0.7],
               [0.3],
               [132],
               [1.0],
               [0.65],
               [0.6],
               [7],
               [0.3]]
    }
    """
    problem = { # from case0
    'num_vars': 2, # +16 when expanding lists for AsymptomaticDuration, ExposedDuration, etc.
    'names': ['AtRiskMajorInfectionRatio',
              'BarrierGesturesPromiscuityFactor'], 
    'bounds': [[0.2169, 0.2651],
               [0.45  , 0.55  ]]
    }


    
    # Design of exp.:     
    param_values = saltelli.sample(problem, nsample)

    # model requests
    print('Nb runs: {}'.format(param_values.shape[0]))
   
    Y = dict()
    Y['end'] = []
    Y['week'] = []
    Si = dict()
    for idx_doe, X in enumerate(param_values):   
        print(idx_doe)
        #for idx,key in enumerate(eval_policy_req['compartmental_model_parameters']['micro'].keys()):
        #    print('eval_policy_req[\'compartmental_model_parameters\'][\'micro\'][\''+key+'\']=X['+str(idx)+']')
        """
        eval_policy_req['compartmental_model_parameters']['micro']['AsymptomaticDuration']=X[0]
        eval_policy_req['compartmental_model_parameters']['micro']['AtRiskMajorInfectionRatio']=X[1]
        eval_policy_req['compartmental_model_parameters']['micro']['BarrierGesturesPromiscuityFactor']=X[2]
        eval_policy_req['compartmental_model_parameters']['micro']['BarrierGesturesStartDay']=X[3]
        eval_policy_req['compartmental_model_parameters']['micro']['ContactTracingProbability']=X[4]
        eval_policy_req['compartmental_model_parameters']['micro']['DispersionNeighborhood1']=X[5]
        eval_policy_req['compartmental_model_parameters']['micro']['DispersionNeighborhood2']=X[6]
        eval_policy_req['compartmental_model_parameters']['micro']['DispersionSchool']=X[7]
        eval_policy_req['compartmental_model_parameters']['micro']['DispersionWork']=X[8]
        eval_policy_req['compartmental_model_parameters']['micro']['ExposedDuration']=X[9]
        eval_policy_req['compartmental_model_parameters']['micro']['InfectionStrength']=X[10]
        eval_policy_req['compartmental_model_parameters']['micro']['MajorInfectionDuration']=X[11]
        eval_policy_req['compartmental_model_parameters']['micro']['MajorInfectionDurationBeforeDeath']=X[12]
        eval_policy_req['compartmental_model_parameters']['micro']['MaxContacts']=X[13]
        eval_policy_req['compartmental_model_parameters']['micro']['MeasuresComplianceProbability']=X[14]
        eval_policy_req['compartmental_model_parameters']['micro']['MinorInfectionDuration']=X[15]
        eval_policy_req['compartmental_model_parameters']['micro']['NeighborhoodProbabilities']=X[16]
        eval_policy_req['compartmental_model_parameters']['micro']['NeighborhoodTime']=X[17]
        eval_policy_req['compartmental_model_parameters']['micro']['OtherMajorInfectionRatio']=X[18]
        eval_policy_req['compartmental_model_parameters']['micro']['PopulationSamplingRatio']=X[19]
        eval_policy_req['compartmental_model_parameters']['micro']['ProbabilityToTest']=X[20]
        eval_policy_req['compartmental_model_parameters']['micro']['SchoolTime']=X[21]
        eval_policy_req['compartmental_model_parameters']['micro']['SimulationStart']=X[22]
        eval_policy_req['compartmental_model_parameters']['micro']['StopWhenDeadRatioExceedsThreshold']=X[23]
        eval_policy_req['compartmental_model_parameters']['micro']['StopWhenEpidemyFinished']=X[24]
        eval_policy_req['compartmental_model_parameters']['micro']['TestAccuracy']=X[25]
        eval_policy_req['compartmental_model_parameters']['micro']['TestLockDownAcceptanceProbability']=X[26]
        eval_policy_req['compartmental_model_parameters']['micro']['TracedIncubationPeriod']=X[27]
        eval_policy_req['compartmental_model_parameters']['micro']['WorkTime']=X[28]
        """
        eval_policy_req['compartmental_model_parameters']['micro']['AtRiskMajorInfectionRatio']=X[0]
        eval_policy_req['compartmental_model_parameters']['micro']['BarrierGesturesPromiscuityFactor']=X[1]
        
        
        # send "eval_policy" request
        response = requests.post('http://localhost:'+str(port)+'/eval_policy', data=json.dumps(eval_policy_req))
        # check results, cf "doc/eval_policy_res.json"
        eval_policy_res = response.json()
        assert (eval_policy_res['success'])
 
        total_population_size = sum(optimize_req['population_size'])
        death_init = eval_policy_res['deaths'][0]['initial']
        Yw=[]
        for idx_w,__ in enumerate(eval_policy_res['deaths']):
            if idx_w is not 0:
                death_w = eval_policy_res['deaths'][idx_w].get('week')
                Yw.append((sum(death_w) - sum(death_init)) / total_population_size)
        Y['week'].append(Yw)

        obj = Objectives(optimize_req)
        policy_objectives = obj.compute(True, eval_policy_req, eval_policy_res)
        Y['end'].append(policy_objectives[1])
        
    # duplicate last value to ensure same dim.    
    length = max(map(len, Y['week']))
    Y['week']=[xi+[xi[-1]]*(length-len(xi)) for xi in Y['week']]
    
    Si['end'] = sobol.analyze(problem, np.array(Y['end']))

    # Fig & export:
    
    #pickle.dump(eval_policy_res, open( "./sensitivity/eval_policy_res.pkl", "wb" ) )
    #pickle.dump(eval_policy_req, open( "./sensitivity/eval_policy_req.pkl", "wb" ) )
    pickle.dump(Y, open( dirName+'Y_death_ratio.pkl', 'wb' ) )
    pickle.dump(Si, open( dirName+'Si_death_ratio.pkl', 'wb' ))
   
    marker = ['', 'o', '*'] # to avoid when redundancy in style
 
    fig, ax = plt.subplots()
    ax2 = ax.bar(problem['names'], Si['end']["ST"], label="ST")
    ax1 = ax.bar(problem['names'], Si['end']["S1"], label="S1")
    plt.xticks(np.arange(len(problem['names'])), problem['names'], rotation='vertical')
    ax.set_ylabel('Sobol Indices')
    plt.legend(bbox_to_anchor=(1.01, 1.01))
    plt.savefig(dirName+'Sobol_1_T_death_ratio.png', bbox_inches='tight')

    fig, ax = plt.subplots()
    plt.imshow(Si['end']['S2'])
    plt.xticks(np.arange(len(problem['names'])), problem['names'], rotation='vertical')
    plt.yticks(np.arange(len(problem['names'])), problem['names'])
    plt.colorbar()
    plt.title('Sobol Indices (order 2)')
    plt.savefig(dirName+'S2_death_ratio.png', bbox_inches='tight')
    
    Si['week']=[]
    for idx_w,__ in enumerate(Y['week'][0]):
        Si['week'].append(sobol.analyze(problem, np.array(Y['week'])[:,idx_w]))
    
    S1=[]
    plt.figure()
    for idx, name in enumerate(problem['names']):
        S1.append([i['S1'][idx] for i in Si['week']])
        plt.plot(S1[idx],'--',label=name, marker=marker[int(np.floor(idx/10))], markevery=10)
    plt.legend(bbox_to_anchor=(1.01, 1.01))
    plt.xlabel('week')
    plt.ylabel('Sobol Indices (order 1)')
    plt.savefig(dirName+'S1_death_ratio_week.png', bbox_inches='tight')
    
    ST=[]
    plt.figure()
    for idx, name in enumerate(problem['names']):
        ST.append([i['ST'][idx] for i in Si['week']])
        plt.plot(ST[idx],'--',label=name, marker=marker[int(np.floor(idx/10))], markevery=10)
    plt.legend(bbox_to_anchor=(1.01, 1.01))
    plt.xlabel('week')
    plt.ylabel('Sobol Indices (total)')
    plt.savefig(dirName+'ST_death_ratio_week.png', bbox_inches='tight')

    plt.figure()
    Yq = (np.quantile(np.array(Y['week']),[0.0, 0.025, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 0.975, 1.0], axis=0)).T
    fig, (ax1) = plt.subplots(sharex=True)
    ax1.plot(np.arange(Yq.shape[0]), Yq[:,5], 'r')
    for ii in range(5):
        alph=0.1+(ii/10.)
        ax1.fill_between(np.arange(Yq.shape[0]), Yq[:,ii], Yq[:,10-ii], color=0.5*np.array([0,0,1]), alpha=alph)
    plt.legend(['median', '100%', '95%', '75%', '50%', '25%'], bbox_to_anchor=(1, 1), loc='upper left')
    plt.xlabel('week')
    plt.ylabel('death_ratio')
    plt.savefig(dirName+'death_ratio_week.png', bbox_inches='tight')


    """    
    # check results, cf "doc/eval_policy_res.json"
    eval_policy_res = response.json()
    assert (eval_policy_res['success'])
    # compute objectives (metrics)
    obj = Objectives(optimize_req)
    policy_objectives = obj.compute(True, eval_policy_req, eval_policy_res)
    print('Objectives:', obj.objectives, policy_objectives)
    """
    
    # clean
    model_server.shutdown()
    model_server.socket.close()


if __name__ == '__main__':
    #optimized_policy_filename = '/tmp/optimized_policy.json'  # after tweak_eval_policy(reoptimize=True)
    # optimized_policy_filename = './sensitivity/OptimBB/files/eval_policy_req0.json' # from BBoptim
    optimized_policy_filename = './sensitivity/optimized_policy.json' # from NSGA 
    nsample = 5  # total nb samples = nsample∗(num_vars +2)
    port = 8000  # allow change for // runs.
    dirName = './sensitivity/micro/sample{}/'.format(nsample)
    tweak_eval_policy(optimized_policy_filename, reoptimize=False)
