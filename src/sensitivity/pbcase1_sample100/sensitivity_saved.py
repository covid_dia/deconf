import json
import threading
import requests
import os
import shutil

from src.model.macromodel import MacroModelRequestHandler, MacroModelServer
from src.model.policy_utils import set_all_measures_activable, all_measures, default_eval_policy_req, \
    default_optimize_req, set_all_objectives_on
from src.optim.base_optim import BaseOptimServer
from src.optim.nsgax_optim import NSGAXOptimRequestHandler
from src.optim.objectives import Objectives

from SALib.sample import saltelli
from SALib.analyze import sobol
import numpy as np
import pickle
import matplotlib.pyplot as plt


def tweak_eval_policy(optimized_policy_filename, reoptimize=False):
    # start model server in thread
    model_server = MacroModelServer(('localhost', port), MacroModelRequestHandler, verbose=False)
    threading.Thread(target=model_server.serve_forever).start()

    # load and customize the JSON "optimize_req" request
    optimize_req = default_optimize_req()
    set_all_measures_activable(optimize_req)
    set_all_objectives_on(optimize_req)
    optimize_req['max_measure_steps'] = 4  # weeks

    if reoptimize:
        # start optim server in thread
        optim_server = BaseOptimServer(('localhost', port+1), NSGAXOptimRequestHandler)
        threading.Thread(target=optim_server.serve_forever).start()
        # send "optimize" request
        response = requests.post('http://localhost:'+str(port+1)+'/optimize', data=json.dumps(optimize_req))
        # check results, cf "doc/optimize_res.json"
        optimize_res = response.json()
        assert (optimize_res['success'])
        # take the first policy in the Front and save to file (will be used in tweak_eval_policy)
        optimized_policy = optimize_res['policies'][0]
        #for m in all_measures():
        #    print(m, optimized_policy[m])
        with open(optimized_policy_filename, 'w') as f:
            json.dump(optimized_policy, f)
        # clean
        optim_server.shutdown()
        optim_server.socket.close()
    else:
        # load our optimized_policy
        with open(optimized_policy_filename, 'r') as f:
            optimized_policy = json.load(f)
    
    #for m in all_measures():
    #    print(m, optimized_policy[m])
            
    # load and customize the JSON "eval_policy_req" request
    eval_policy_req = default_eval_policy_req()
    eval_policy_req['measure_weeks'] = optimized_policy['measure_weeks']
    for measure in all_measures():
        eval_policy_req[measure] = optimized_policy[measure]
    
    #------------------------------
    # Sensitivity Analysis:
    
    #dirName = './sensitivity/tmp/'
    if not os.path.exists(dirName):
        os.makedirs(dirName)
    shutil.copy(__file__, dirName+'sensitivity_saved.py')
    
    # problem inputs:
    if pb_case==0: # 0: regression bounds
        problem = {
        'num_vars': 12,
        'names': ['R0', 'epsilon', 'gamm1', 'gamm2', 'bb', 'c', 'mild', 'mort_sup', 'vir_sup', 'nu', 'sigma', 'CFR'], 
        'bounds': [[3.05, 3.19],
                   [0.28, 0.315],
                   [0.145, 0.16],
                   [0.01, 0.013],
                   [0.01, 0.04],
                   [0.86, 0.88],
                   [0.785, 0.80],
                   [4e-6, 5.26e-6],
                   [0.002, 0.0032],
                   [2e-6, 3.1e-6],
                   [0.74, 0.85],
                   [0.151, 0.165]]
        }
    elif pb_case==1: # 1: arbitrary bounds
        problem = {
        'num_vars': 12,
        'names': ['R0', 'epsilon', 'gamm1', 'gamm2', 'bb', 'c', 'mild', 'mort_sup', 'vir_sup', 'nu', 'sigma','CFR'], 
        'bounds': [[1.0, 5.0],
                   [0.2, 0.4],
                   [0.1, 0.2],
                   [0.005, 0.015],
                   [0.01, 0.04],
                   [0.7, 0.9],
                   [0.7, 0.9],
                   [3e-6, 6e-6],
                   [0.001, 0.004],
                   [1e-6, 4e-6],
                   [0.7, 0.9],
                   [0.150, 0.170]]
        }

    
    # Design of exp.:     
    param_values = saltelli.sample(problem, nsample) 
    
    # model requests
    print(param_values.shape[0])
    Y = dict()
    Y['end'] = []
    Y['week'] = []
    Si = dict()
    for idx_doe, X in enumerate(param_values):
        
        print(idx_doe)
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['R0']= X[0]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['epsilon']= X[1]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['gamm1']= X[2]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['gamm2']= X[3]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['bb']= X[4]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['c']= X[5]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['mild']= X[6]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['mort_sup']= X[7]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['vir_sup']= X[8]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['nu']= X[9]
        eval_policy_req['compartmental_model_parameters']['macro_Alizon20']['sigma']= X[10]
        eval_policy_req['compartmental_model_parameters']['common']['CFR']= X[11]

        # send "eval_policy" request
        response = requests.post('http://localhost:'+str(port)+'/eval_policy', data=json.dumps(eval_policy_req))
        # check results, cf "doc/eval_policy_res.json"
        eval_policy_res = response.json()
        assert (eval_policy_res['success'])
 
        total_population_size = sum(optimize_req['population_size'])
        death_init = eval_policy_res['deaths'][0]['initial']
        Yw=[]
        for idx_w,__ in enumerate(eval_policy_res['deaths']):
            if idx_w is not 0:
                death_w = eval_policy_res['deaths'][idx_w].get('week')
                Yw.append((sum(death_w) - sum(death_init)) / total_population_size)
        Y['week'].append(Yw)

        obj = Objectives(optimize_req)
        policy_objectives = obj.compute(True, eval_policy_req, eval_policy_res)
        Y['end'].append(policy_objectives[1])
        
    
    # Fig & export:
    
    Si['end'] = sobol.analyze(problem, np.array(Y['end']))
    fig, ax = plt.subplots()
    ax2 = ax.bar(problem['names'], Si['end']["ST"], label="ST")
    ax1 = ax.bar(problem['names'], Si['end']["S1"], label="S1")
    plt.xticks(np.arange(len(problem['names'])), problem['names'], rotation='vertical')
    ax.set_ylabel('Sobol Indices')
    plt.legend(bbox_to_anchor=(1.01, 1.01))
    plt.savefig(dirName+'Sobol_1_T_death_ratio.png', bbox_inches='tight')

    fig, ax = plt.subplots()
    plt.imshow(Si['end']['S2'])
    plt.xticks(np.arange(len(problem['names'])), problem['names'], rotation='vertical')
    plt.yticks(np.arange(len(problem['names'])), problem['names'])
    plt.colorbar()
    plt.title('Sobol Indices (order 2)')
    plt.savefig(dirName+'S2_death_ratio.png', bbox_inches='tight')

    Si['week']=[]
    for idx_w,__ in enumerate(Y['week'][0]):
        Si['week'].append(sobol.analyze(problem, np.array(Y['week'])[:,idx_w]))

    S1=[]
    plt.figure()
    for idx, name in enumerate(problem['names']):
        S1.append([i['S1'][idx] for i in Si['week']])
        plt.plot(S1[idx],'--',label=name)
    plt.legend(bbox_to_anchor=(1.01, 1.01))
    plt.xlabel('week')
    plt.ylabel('Sobol Indices (order 1)')
    plt.savefig(dirName+'S1_death_ratio_week.png', bbox_inches='tight')
    
    ST=[]
    plt.figure()
    for idx, name in enumerate(problem['names']):
        ST.append([i['ST'][idx] for i in Si['week']])
        plt.plot(ST[idx],'--',label=name)
    plt.legend(bbox_to_anchor=(1.01, 1.01))
    plt.xlabel('week')
    plt.ylabel('Sobol Indices (total)')
    plt.savefig(dirName+'ST_death_ratio_week.png', bbox_inches='tight')

    plt.figure()
    Yq = (np.quantile(np.array(Y['week']),[0.0, 0.025, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 0.975, 1.0], axis=0)).T
    fig, (ax1) = plt.subplots(sharex=True)
    ax1.plot(np.arange(Yq.shape[0]), Yq[:,5], 'r')
    for ii in range(5):
        alph=0.1+(ii/10.)
        ax1.fill_between(np.arange(Yq.shape[0]), Yq[:,ii], Yq[:,10-ii], color=0.5*np.array([0,0,1]), alpha=alph)
    plt.legend(['median', '100%', '95%', '75%', '50%', '25%'], bbox_to_anchor=(1, 1), loc='upper left')
    plt.xlabel('week')
    plt.ylabel('death_ratio')
    plt.savefig(dirName+'death_ratio_week.png', bbox_inches='tight')

        
        

    # Export:
    #pickle.dump(eval_policy_res, open( "./sensitivity/eval_policy_res.pkl", "wb" ) )
    #pickle.dump(eval_policy_req, open( "./sensitivity/eval_policy_req.pkl", "wb" ) )
    pickle.dump(Y, open( dirName+'Y_death_ratio.pkl', 'wb' ) )
    pickle.dump(Si, open( dirName+'Si_death_ratio.pkl', 'wb' ))

    """    
    # check results, cf "doc/eval_policy_res.json"
    eval_policy_res = response.json()
    assert (eval_policy_res['success'])
    # compute objectives (metrics)
    obj = Objectives(optimize_req)
    policy_objectives = obj.compute(True, eval_policy_req, eval_policy_res)
    print('Objectives:', obj.objectives, policy_objectives)
    """
    
    # clean
    model_server.shutdown()
    model_server.socket.close()


if __name__ == '__main__':
    optimized_policy_filename = '/tmp/optimized_policy.json'
    pb_case = 1 # 0:regression bounds, 1: arbitrary bounds
    nsample=100 # total nb samples = nsample∗(num_vars +2)
    port = 8100 # allow change for // runs. 
    dirName = './sensitivity/pbcase{}_sample{}/'.format(pb_case,nsample)
    tweak_eval_policy(optimized_policy_filename, reoptimize=False)
