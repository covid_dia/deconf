# !/usr/bin/env python3
import threading
import time
from copy import deepcopy
from random import randint
from threading import Lock

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront


class RandomOptimRequestHandler(BaseOptimRequestHandler):

    def __init__(self, request=None, client_address=None, server=None):
        self.server = None
        if (request, client_address, server) != (None, None, None):
            BaseOptimRequestHandler.__init__(self, request, client_address, server)

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):
        """
        Inherits BaseOptimRequestHandler.optimize(), see there for doc.
        """
        ntries = 10  # number of random tries - use the server value if defined
        if hasattr(self.server, "ntries"):
            ntries = self.server.ntries
        nlabels = len(optimize_req['zone_label_names'])
        objectives = Objectives(optimize_req)
        pareto_front = ParetoFront()
        step_duration = optimize_req['measure_step_duration']
        for ntry in range(ntries):
            eval_policy_req = deepcopy(policy_common_values)
            measure_weeks = []
            for i in range(optimize_req['max_measure_steps']):
                if i > 0 and randint(0, 100) > 30:
                    continue
                measure_weeks.append(i * step_duration)
                for m, m_max in activable_measures_dict.items():
                    arr = []
                    current_measure_value = m_max
                    for j in range(nlabels):
                        current_measure_value = randint(0, current_measure_value)
                        arr.append(current_measure_value)
                    eval_policy_req[m].append({'activated': arr})
            # lift all measures at the end
            measure_weeks.append(optimize_req['max_measure_steps'] * step_duration)
            for m in activable_measures_dict:
                eval_policy_req[m].append({'activated': [0] * nlabels})
            eval_policy_req['measure_weeks'] = measure_weeks
            # send POST
            # assert(color_strictness_filter(eval_policy_req)) Uncomment if you want to be sure that eval_policy_req respects the color constraints.
            ok, eval_policy_res = self.call_eval_policy(eval_policy_req, activable_measures_dict)
            # now check if the policy has good objectives
            objs = objectives.compute(ok, eval_policy_req, eval_policy_res)
            # keep if on Pareto front
            pareto_front.update(eval_policy_req, eval_policy_res, new_metrics=objs)
        return True, pareto_front

    def optimize_create_jobs(self, optimize_req, policy_common_values, activable_measures_dict,
                             job_list, create_join_lock=None, interface=None, ntries=10):

        if interface is None:
            interface = self

        nlabels = len(optimize_req['zone_label_names'])
        step_duration = optimize_req['measure_step_duration']

        for ntry in range(ntries):
            eval_policy_req = deepcopy(policy_common_values)
            measure_weeks = []
            for i in range(optimize_req['max_measure_steps']):
                if i > 0 and randint(0, 100) > 30:
                    continue
                measure_weeks.append(i * step_duration)
                for m, m_max in activable_measures_dict.items():
                    arr = [randint(0, m_max) for _ in range(nlabels)]
                    eval_policy_req[m].append({'activated': arr})
            # lift all measures at the end
            measure_weeks.append(optimize_req['max_measure_steps'] * step_duration)
            for m in activable_measures_dict:
                eval_policy_req[m].append({'activated': [0] * nlabels})
            eval_policy_req['measure_weeks'] = measure_weeks
            # send POST
            job = (interface.call_eval_policy_async(eval_policy_req, activable_measures_dict),
                   eval_policy_req)
            if create_join_lock is not None:
                create_join_lock.acquire()
            job_list.append(job)
            if create_join_lock is not None:
                create_join_lock.release()

        # indicates that no new jobs will be created
        if create_join_lock is not None:
            create_join_lock.acquire()
        self.job_create_step = False
        if create_join_lock is not None:
            create_join_lock.release()

    def optimize_join_jobs(self, optimize_req, activable_measures_dict,
                           job_list, pareto_front, create_join_lock=None,
                           interface=None):

        if interface is None:
            interface = self

        objectives = Objectives(optimize_req)

        if create_join_lock is not None:
            create_join_lock.acquire()
        job_list_len = len(job_list)
        expect_new_jobs = self.job_create_step
        if create_join_lock is not None:
            create_join_lock.release()

        job_index = 0
        while job_list_len > 0 or expect_new_jobs:
            if job_list_len == 0:
                # sleep a small amount of time to save CPU
                time.sleep(1)
            else:
                if job_index >= job_list_len:
                    job_index = 0

                if create_join_lock is not None:
                    create_join_lock.acquire()
                job = job_list[job_index]
                if create_join_lock is not None:
                    create_join_lock.release()

                job_uid, eval_policy_req = job
                # check if the job has ended
                ok, eval_policy_res, job_done = interface.call_eval_policy_poll_job(eval_policy_req,
                                                                                    activable_measures_dict,
                                                                                    job_uid)
                if job_done:
                    # print("job " + job_uid + " ended with status " + str(ok))
                    if create_join_lock is not None:
                        create_join_lock.acquire()
                    job_list.remove(job)
                    if create_join_lock is not None:
                        create_join_lock.release()
                    # now check if the policy has good objectives
                    objs = objectives.compute(ok, eval_policy_req, eval_policy_res)
                    # keep if on Pareto front
                    pareto_front.update(eval_policy_req, eval_policy_res, new_metrics=objs)
                else:
                    job_index = job_index + 1
                    # sleep a small amount of time to save CPU
                    time.sleep(1 / job_list_len)

            if create_join_lock is not None:
                create_join_lock.acquire()
            job_list_len = len(job_list)
            expect_new_jobs = self.job_create_step
            if create_join_lock is not None:
                create_join_lock.release()

    # use this version to run all jobs before starting to collect results
    def optimize_async(self, optimize_req, policy_common_values, activable_measures_dict,
                       interface=None, ntries=10):

        # structure to store the list of jobs (uid, eval_policy_req)
        job_list = []
        # indicates that new jobs are being created
        self.job_create_step = True

        self.optimize_create_jobs(optimize_req, policy_common_values,
                                  activable_measures_dict, job_list,
                                  interface, ntries)

        pareto_front = ParetoFront()

        self.optimize_join_jobs(optimize_req, activable_measures_dict, job_list, pareto_front,
                                interface)

        return True, pareto_front

    # use this version to concurrently run jobs and collect results
    def optimize_async_para(self, optimize_req, policy_common_values, activable_measures_dict,
                            interface=None, ntries=10):

        # protect shared job_list and job_create_step variables
        create_join_lock = Lock()

        # structure to store the list of jobs (uid, eval_policy_req)
        job_list = []
        # indicates that new jobs are being created
        self.job_create_step = True

        tid = threading.Thread(target=self.optimize_create_jobs,
                               args=(optimize_req, policy_common_values,
                                     activable_measures_dict, job_list, create_join_lock,
                                     interface, ntries))
        tid.daemon = False
        tid.start()

        pareto_front = ParetoFront()

        self.optimize_join_jobs(optimize_req, activable_measures_dict,
                                job_list, pareto_front, create_join_lock,
                                interface)

        # wait for thread termination
        tid.join()

        return True, pareto_front


if __name__ == '__main__':
    launch_server(BaseOptimServer, RandomOptimRequestHandler)
