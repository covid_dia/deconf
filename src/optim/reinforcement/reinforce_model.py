import json
import os
from copy import deepcopy
from datetime import datetime

import numpy as np
import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter

from src.model.macro_rl_env import MacroRLEnv
from src.model.policy_utils import decov_path
from src.model.policy_utils import set_n_zones, set_n_measures, set_n_colors
from src.optim.reinforcement.models import Actor, init_xavier_uniform, prepare_input
from src.optim.utils import Pareto, log_results, compute_discounted_reward


class ReinforceModel(nn.Module):

    def __init__(self, archi, optimize_req, policy_common_values, activable_measures_dict):
        super(ReinforceModel, self).__init__()
        self.archi = archi
        self.layers_size = archi['layers_size_actor']
        self.GAMMA = archi['GAMMA']
        self.max_episode_num = archi['max_episode_num']
        self.learning_rate = archi['lr_actor']

        self.optimize_req = optimize_req
        self.policy_common_values = policy_common_values
        self.activable_measures_dict = activable_measures_dict
        self.num_zones = 1 if self.optimize_req['uniform_measures'] else len(self.optimize_req['zones'])
        self.max_measure_steps = self.optimize_req['max_measure_steps']
        self.max_weeks = self.optimize_req['compartmental_model_parameters']['common']['max_days'] // 7
        self.num_measures = len(self.optimize_req['max_measure_values'])
        self.input_size = self.num_zones*4*7+1
        self.num_zones = 1 if self.optimize_req['uniform_measures'] else len(self.optimize_req['zones'])
        self.best_reward = None
        eval_policy_req = deepcopy(self.policy_common_values)
        self.env = MacroRLEnv(self.optimize_req, eval_policy_req)
        self.pareto = Pareto(2)

        self.actor = Actor(archi)
        self.apply(init_xavier_uniform)

        print("\nModel's state_dict:")
        for param_tensor in self.state_dict():
            print(param_tensor, "\t", self.state_dict()[param_tensor].size())
        print()

    def forward(self, state):
        dists = self.actor(state)
        return dists

    def get_action(self, state):

        dists = self.forward(prepare_input(state, self.archi))
        samples = [dist.sample() for dist in dists]

        dep_labels = torch.Tensor(self.archi['zone_labels']).long()
        log_probs_array = [dist.log_prob(sample)[dep_labels].unsqueeze(0) for dist, sample in zip(dists, samples)]

        actions = [sample[dep_labels] for sample in samples]
        log_probs = torch.mean(torch.cat(log_probs_array, dim=1), dim=1)
        entropy = torch.mean(torch.cat([dist.entropy() for dist in dists])).unsqueeze(0)

        return actions, log_probs, entropy

    def compute_reward(self, infos):
        all_zone_rewards = np.empty((self.max_weeks, self.num_zones))
        for i in range(self.max_weeks):
            for j in range(self.num_zones):
                zone_pop = infos[i][j]['zone_pop']
                ratios = infos[i][j]['ratios']
                new_deads = infos[i][j]['new_deads']
                cumul_deads = infos[i][j]['cumul_deads']
                new_gdb_loss = infos[i][j]['new_gdb_loss']
                cumul_gdb_loss = infos[i][j]['cumul_gdb_loss']
                ICU_saturation = infos[i][j]['ICU_saturation']

                # all_zone_rewards[i][j] = - (cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
                # all_zone_rewards[i][j] = - (10*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
                # all_zone_rewards[i][j] = - (100*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
                all_zone_rewards[i][j] = - (1e-3*ICU_saturation + 1e2*new_gdb_loss)
                # all_zone_rewards[i][j] = - (1000*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
                # all_zone_rewards[i][j] = - (10000*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
        mean_reward = np.mean(all_zone_rewards)
        return all_zone_rewards, mean_reward, self.check_if_best(mean_reward)

    def update_policy(self, R, log_probs, entropies):
        policy_gradient = []
        entropy_loss = -torch.mean(torch.cat(entropies))
        for log_prob, Gt in zip(log_probs, R):
            policy_gradient.append(-log_prob * torch.Tensor(Gt))
        self.actor.optimizer.zero_grad()
        policy_gradient = torch.stack(policy_gradient).mean() + 1e-4*entropy_loss
        policy_gradient.backward()
        self.actor.optimizer.step()

        return entropy_loss

    def check_if_best(self, reward):

        if self.best_reward is None:
            self.best_reward = reward
            torch.save(self.state_dict(), self.dir_model)
            return True
        if reward > self.best_reward:
            self.best_reward = reward
            torch.save(self.state_dict(), self.dir_model)
            return True
        return False

    def train_model(self):

        self.datestring = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
        self.dir_run = os.path.join('.', 'runs_reinforce', self.datestring)
        os.mkdir(self.dir_run)
        self.dir_model = os.path.join(self.dir_run, 'model.pt')
        self.dir_archi = os.path.join(self.dir_run, 'archi.json')
        with open(self.dir_archi, 'w') as f:
            json.dump(self.archi, f)

        self.writer = SummaryWriter(log_dir=self.dir_run)

        self.episode = 0
        while self.episode < self.max_episode_num:
            self.train()
            self.episode +=1


    def train(self, mode=True):

        eval_policy_req = deepcopy(self.policy_common_values)
        state = self.env.reset()
        all_log_probs = []
        all_entropies = []
        infos = []
        for i in range(self.max_weeks):
            if i < self.max_measure_steps:
                actions, log_probs, entropy = self.get_action(state)
                all_log_probs.append(log_probs)
                all_entropies.append(entropy)
            else: # All measures are lift after max measure week
                actions = [np.zeros(self.num_zones)]*self.num_measures
            for j, measure in enumerate(self.activable_measures_dict.keys()):
                eval_policy_req[measure].append({'activated': actions[j].tolist()})
            state, done, info = self.env.step(actions)
            infos.append(info)
        eval_policy_req['measure_weeks'] = np.arange(self.max_weeks).tolist()
        nb_dead = self.env.get_total_counts()['M']
        total_gdb_loss = self.env.get_total_gdb_loss()
        if mode:
            reward, mean_reward, better = self.compute_reward(infos)
            discounted_reward, mean_reward_d = compute_discounted_reward(archi, reward)
            log_results(self.writer, self.pareto, self.episode, self.datestring, self.env, infos, mean_reward_d, better)
            entropy_loss = self.update_policy(discounted_reward, all_log_probs, all_entropies)
            self.writer.add_scalar('metrics/entropy_loss', entropy_loss, self.episode)
        return eval_policy_req

class RewardNormalizer:
    def __init__(self, num_steps, num_zones):
        self.window_size_limit = 30
        self.window = np.zeros((self.window_size_limit, num_steps, num_zones))
        self.window_size_current = 0

    def normalize(self, rewards):
        self.window_size_current = wsc = min(self.window_size_current+1, self.window_size_limit)
        self.window[1:] = self.window[:-1]
        self.window[0] = rewards
        normalized_rwd = (rewards - np.mean(self.window[:wsc], axis=0))/(1e-9 + np.std(self.window[:wsc], axis=0))
        # return normalized_rwd
        return rewards

if __name__ == '__main__':

    with open(decov_path + "/doc/optimize_req.json") as f:
        optimize_req = json.load(f)

    set_n_zones(optimize_req, 1)
    set_n_measures(optimize_req, 2)
    set_n_colors(optimize_req, 1)

    model_name = optimize_req['compartmental_model']
    # TODO: policy_common_values and activable_measures_dict are computed in BaseOptim, this is copy and past
    policy_common_values = {k: optimize_req[k] for k in
                            ['zones', 'zone_label_names', 'zone_labels',
                             'population_size', 'ICU_beds',
                             'compartmental_model', 'compartments_'+model_name,
                             'initial_ratios_'+model_name, 
                             'compartmental_model_parameters']}
    activable_measures_dict = {}
    for m, m_max in optimize_req['max_measure_values'].items():
        policy_common_values[m] = None
        if m_max > 0:
            policy_common_values[m] = []
            activable_measures_dict[m] = m_max

    archi = {
        'layers_size_actor': [128, 64, 32],
        'GAMMA': 0.9,
        'max_episode_num': 6000,
        'lr_actor': 1e-3
    }

    archi['num_zones'] = 1 if optimize_req['uniform_measures'] else len(optimize_req['zones'])
    archi['num_zone_labels'] = len(optimize_req['zone_label_names'])
    archi['ICU_beds'] = optimize_req['ICU_beds']
    archi['zone_labels'] = optimize_req['zone_labels']
    archi['max_measure_steps'] = optimize_req['max_measure_steps']
    archi['input_size'] = archi['num_zones']*4*7+archi['max_measure_steps']
    archi['ICURatio'] = optimize_req['compartmental_model_parameters']['common']['ICURatio']
    archi['max_weeks'] = optimize_req['compartmental_model_parameters']['common']['max_days'] // 7
    archi['num_measures'] = len(optimize_req['max_measure_values'])
    archi['num_actions_measures'] = {k:v+1 for k, v in optimize_req['max_measure_values'].items()}

    model = ReinforceModel(archi, optimize_req, policy_common_values, activable_measures_dict)
    model.train_model()
