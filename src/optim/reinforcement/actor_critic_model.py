import json
import os
from copy import deepcopy
from datetime import datetime

import numpy as np
import torch
import torch.nn as nn
from torch.optim.lr_scheduler import StepLR
from torch.utils.tensorboard import SummaryWriter

from src.model.macro_rl_env import MacroRLEnv
from src.model.policy_utils import decov_path
from src.optim.reinforcement.models import Actor, Critic, prepare_input
from src.optim.reinforcement.utils import Pareto, log_results, compute_discounted_reward


class ActorCritic(nn.Module):

    def __init__(self, archi, optimize_req, policy_common_values, activable_measures_dict):
        super(ActorCritic, self).__init__()
        self.archi = archi
        self.optimize_req = policy_common_values
        self.policy_common_values = policy_common_values
        self.activable_measures_dict = activable_measures_dict
        self.critic = Critic(archi)
        self.actor = Actor(archi)
        self.best_reward = None
        self.pareto = Pareto(2)
        eval_policy_req = deepcopy(self.policy_common_values)
        self.env = MacroRLEnv(optimize_req, eval_policy_req)
        self.actor_scheduler = StepLR(self.actor.optimizer, 50, gamma=10, last_epoch=-1)
        self.critic_scheduler = StepLR(self.critic.optimizer, 50, gamma=0.1, last_epoch=-1)

    def forward(self, state):
        value = self.critic(state)
        dists = self.actor(state)
        return value, dists

    def get_action(self, state):

        values, dists = self.forward(prepare_input(state, self.archi))
        samples = [dist.sample() for dist in dists]

        dep_labels = torch.Tensor(self.archi['zone_labels']).long()
        log_probs_array = [dist.log_prob(sample)[dep_labels].unsqueeze(0) for dist, sample in zip(dists, samples)]

        actions = [sample[dep_labels] for sample in samples]
        log_probs = torch.mean(torch.cat(log_probs_array, dim=1), dim=1)
        entropy = torch.mean(torch.cat([dist.entropy() for dist in dists])).unsqueeze(0)

        return actions, log_probs, values, entropy

    def compute_reward(self, infos):
        all_zone_rewards = np.empty((self.archi['max_weeks'], self.archi['num_zones']))
        for i in range(self.archi['max_weeks']):
            for j in range(self.archi['num_zones']):
                zone_pop = infos[i][j]['zone_pop']
                ratios = infos[i][j]['ratios']
                new_deads = infos[i][j]['new_deads']
                cumul_deads = infos[i][j]['cumul_deads']
                new_gdb_loss = infos[i][j]['new_gdb_loss']
                cumul_gdb_loss = infos[i][j]['cumul_gdb_loss']
                ICU_saturation = infos[i][j]['ICU_saturation']

                # all_zone_rewards[i][j] = - (cumul_deads/zone_pop + 0*cumul_gdb_loss)/(i+1)
                # all_zone_rewards[i][j] = - (10*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
                # all_zone_rewards[i][j] = - (100*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
                all_zone_rewards[i][j] = - (1e-3 * ICU_saturation + 1e3 * new_gdb_loss)
                # print('ICU:', 1e-2*ICU_saturation, 'gdb:', 1e3*new_gdb_loss)
                # all_zone_rewards[i][j] = - (1000*new_deads/zone_pop + new_gdb_loss)
                # all_zone_rewards[i][j] = - (10000*cumul_deads/zone_pop + cumul_gdb_loss)/(i+1)
        # all_zone_rewards = np.zeros((self.archi['max_weeks'], self.archi['num_zones']))
        # for j in range(self.archi['num_zones']):
        #     cumul_icu_saturation = infos[-1][j]['ICU_saturation_cumul']
        #     cumul_gdb_loss = infos[-1][j]['cumul_gdb_loss']
        #     all_zone_rewards[-1][j] = - (1e-3*cumul_icu_saturation + 1e2*cumul_gdb_loss)
        # print(1e-3*cumul_deads, 1e2*cumul_gdb_loss)
        mean_reward = np.mean(all_zone_rewards)
        return all_zone_rewards, mean_reward, self.check_if_best(mean_reward)

    def update_policy(self, R, log_probs, values, entropies):

        policy_gradient = []
        value_gradient = []
        entropy_loss = -torch.mean(torch.cat(entropies))
        for log_prob, value, Gt in zip(log_probs, values, R):
            Gt = torch.Tensor(Gt)
            advantage = Gt - value
            policy_gradient.append(-log_prob * advantage.detach())
            value_gradient.append(torch.pow(advantage, 2))
            # entropy_loss += - entropy

        self.actor.optimizer.zero_grad()
        self.critic.optimizer.zero_grad()
        policy_loss = torch.stack(policy_gradient).mean() + 1e-4 * entropy_loss
        value_loss = torch.stack(value_gradient).mean()
        # loss = policy_loss + value_loss + 0*entropy_loss
        policy_loss.backward()
        value_loss.backward()
        self.actor.optimizer.step()
        self.critic.optimizer.step()
        if self.episode <= 100:
            self.actor_scheduler.step()
            self.critic_scheduler.step()
        for param_group in self.actor.optimizer.param_groups:
            print('actor_lr:', param_group['lr'])

        return value_loss, entropy_loss

    def check_if_best(self, reward):

        if self.best_reward is None:
            self.best_reward = reward
            torch.save(self.state_dict(), self.dir_model)
            return True
        if reward > self.best_reward:
            self.best_reward = reward
            torch.save(self.state_dict(), self.dir_model)
            return True
        return False

    def train_model(self):

        self.datestring = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
        self.dir_run = os.path.join('.', 'runs_reinforce', self.datestring)
        os.mkdir(self.dir_run)
        self.dir_model = os.path.join(self.dir_run, 'model.pt')
        self.dir_archi = os.path.join(self.dir_run, 'archi.json')
        with open(self.dir_archi, 'w') as f:
            json.dump(self.archi, f)
        self.writer = SummaryWriter(log_dir=self.dir_run)

        self.episode = 0
        while self.episode < self.archi['max_episode_num']:
            self.train()
            self.episode += 1

    def train(self, mode=True):

        eval_policy_req = deepcopy(self.policy_common_values)
        state = self.env.reset()
        all_log_probs = []
        all_values = []
        all_entropies = []
        infos = []
        for i in range(self.archi['max_weeks']):
            if i < self.archi['max_measure_steps']:
                actions, log_probs, values, entropy = self.get_action(state)
                # print(i, actions)
                all_log_probs.append(log_probs)
                all_values.append(values)
                all_entropies.append(entropy)
            else:  # All measures are lift after max measure week
                actions = [np.zeros(self.archi['num_zones'])] * self.archi['num_measures']
            for j, measure in enumerate(self.activable_measures_dict.keys()):
                eval_policy_req[measure].append({'activated': actions[j].tolist()})
            state, done, info = self.env.step(actions)
            infos.append(info)
        eval_policy_req['measure_weeks'] = np.arange(self.archi['max_weeks']).tolist()
        if mode:
            reward, mean_reward, better = self.compute_reward(infos)
            discounted_reward, mean_reward_d = compute_discounted_reward(self.archi, reward)
            log_results(self.writer, self.pareto, self.episode, self.datestring, self.env, infos, mean_reward, better)
            value_loss, entropy_loss = self.update_policy(discounted_reward, all_log_probs, all_values, all_entropies)
            self.writer.add_scalar('metrics/value_loss', value_loss, self.episode)
            self.writer.add_scalar('metrics/entropy_loss', entropy_loss, self.episode)

            # for dep in [0, 8, 34, 77]: # Departements' covid evolution curves to display in Tensorboard
            #     f, (ax1, ax2, ax3) = plt.subplots(3, sharex='col')
            #
            #     ax1.plot([info[dep]['new_deads'] for info in infos])
            #     ax1.set_title('new_deads')
            #
            #     ax2.plot([info[dep]['cumul_deads'] for info in infos])
            #     ax2.set_title('cumul_deads')
            #
            #     ax3.plot([info[dep]['I2']*self.archi['ICURatio'] for info in infos])
            #     ax3.plot([self.archi['ICU_beds'][dep]]*len(infos), color='red')
            #     ax3.set_title('I2')
            #
            #     self.writer.add_figure('metrics/{}'.format(dep+1), f, self.episode)
            #
            #     # figure = plt.figure()
            #     # plt.plot([info[dep]['new_deads'] for info in infos])
            #     # plt.clf()
            #     # figure = plt.figure()
            #     # plt.plot([info[dep]['cumul_deads'] for info in infos])
            #     # self.writer.add_figure('cumul_deads/{}'.format(dep+1), figure, self.episode)
            #     plt.clf()

        return eval_policy_req


if __name__ == '__main__':

    with open(decov_path + "/doc/optimize_req.json") as f:
        optimize_req = json.load(f)
    # set_n_zones(optimize_req, 1)
    # set_n_measures(optimize_req, 2)
    # set_n_colors(optimize_req, 1)

    # set_all_measures_activable(data)
    optimize_req['max_measure_steps'] = 6
    optimize_req['measure_step_duration'] = 4
    model_name = optimize_req['compartmental_model']
    # TODO: policy_common_values and activable_measures_dict are computed in BaseOptim, this is copy and past
    policy_common_values = {k: optimize_req[k] for k in
                            ['zones', 'zone_label_names', 'zone_labels',
                             'population_size', 'ICU_beds',
                             'compartmental_model', 'compartments_' + model_name,
                             'initial_ratios_' + model_name, 'compartmental_model_parameters']}
    activable_measures_dict = {}
    for m, m_max in optimize_req['max_measure_values'].items():
        policy_common_values[m] = None
        if m_max > 0:
            policy_common_values[m] = []
            activable_measures_dict[m] = m_max

    archi = {
        'layers_size_actor': [512, 512, 512],
        'layers_size_critic': [512, 512, 256, 256],
        'GAMMA': 0.9,
        'max_episode_num': 6000,
        'lr_actor': 1e-5,
        'lr_critic': 1e-3
    }

    archi['num_zones'] = 1 if optimize_req['uniform_measures'] else len(optimize_req['zones'])
    archi['num_zone_labels'] = len(optimize_req['zone_label_names'])
    archi['ICU_beds'] = optimize_req['ICU_beds']
    archi['zone_labels'] = optimize_req['zone_labels']
    archi['max_measure_steps'] = optimize_req['max_measure_steps']
    archi['input_size'] = archi['num_zones'] * 4 * 7 + archi['max_measure_steps']
    archi['ICURatio'] = optimize_req['compartmental_model_parameters']['common']['ICURatio']
    archi['max_weeks'] = optimize_req['compartmental_model_parameters']['common']['max_days'] // 7
    archi['num_measures'] = len(optimize_req['max_measure_values'])
    archi['num_actions_measures'] = {k: v + 1 for k, v in optimize_req['max_measure_values'].items()}

    model = ActorCritic(archi, optimize_req, policy_common_values, activable_measures_dict)
    model.train_model()
