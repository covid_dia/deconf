from matplotlib import pyplot as plt
import numpy as np

class Pareto:
    def __init__(self, dim, keep_all=True):
        self.dim = dim
        self.keep_all = keep_all
        self.dots = []

    def update(self, dot):
        assert len(dot) == self.dim
        self.dots.append(dot)

    def get_figure(self):
        assert self.dim ==2
        plt.clf()
        f = plt.figure()
        x = [dot[0] for dot in self.dots]
        y = [dot[1] for dot in self.dots]
        plt.scatter(x, y)
        return f

def compute_discounted_reward(archi, rewards):
        discounted_rewards = np.empty((archi['max_weeks'], archi['num_zones']))
        for t in range(archi['max_weeks']):
            Gt = np.zeros(archi['num_zones'])
            pw = 0
            for r in rewards[t:]:
                Gt = Gt + archi['GAMMA']**pw * r
                pw = pw + 1
            discounted_rewards[t] = Gt

        R = discounted_rewards[:archi['max_measure_steps']]
        mean = np.mean(R)
        R -= np.mean(R, axis=0)
        R /= np.std(R, axis=0)
        # R = np.sign(R)*np.log(1+np.abs(R))
        return R, mean

def log_results(writer, pareto, episode, datestring, env, infos, mean_reward, better):
    nb_dead = env.get_total_counts()['M']
    total_gdb_loss = env.get_total_gdb_loss().detach().numpy()
    total_icu_sat_cumulated = env.get_total_icu_sat_cumulated()
    mean_cumulated_deads = np.mean(np.array([np.sum(np.array([info_zone['cumul_deads'] for info_zone in info])) for info in infos]))
    writer.add_scalar('metrics/nb_dead', nb_dead, episode)
    writer.add_scalar('metrics/mean_cumulated_deads', mean_cumulated_deads, episode)
    writer.add_scalar('metrics/gdb_loss', total_gdb_loss, episode)
    writer.add_scalar('metrics/icu_sat_cumulated', total_icu_sat_cumulated, episode)
    writer.add_scalar('metrics/rewards', mean_reward, episode)
    print('[{}] [Ep.{}] - nb_deads: {}, gdb_loss: {}, rwd: {:.5f} {}'.format(datestring, episode, nb_dead, total_gdb_loss, mean_reward, '+' if better else ''))

    pareto.update([total_gdb_loss, total_icu_sat_cumulated])
    writer.add_figure('metrics/pareto', pareto.get_figure(), episode)



