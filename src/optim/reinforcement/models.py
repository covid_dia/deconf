import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.distributions import Categorical

def init_xavier_uniform(m):
    if type(m) == nn.Linear:
        nn.init.xavier_uniform_(m.weight)

class Block(nn.Module):
    def __init__(self, input_size, layer_sizes):
        super(Block, self).__init__()
        self.linears = nn.ModuleList([])
        last_input_size = input_size
        for layer_size in layer_sizes:
            self.linears.append(nn.Linear(last_input_size, layer_size))
            last_input_size = layer_size
        self.output_size = last_input_size
        self.apply(init_xavier_uniform)

    def forward(self, x):
        for layer in self.linears:
            x = F.leaky_relu(layer(x))
        return x

class Actor(nn.Module):
    def __init__(self, archi):
        super(Actor, self).__init__()
        self.archi = archi
        self.actor_bloc = Block(archi['input_size'], archi['layers_size_actor'])
        self.linear_actions = nn.ModuleList([])
        for num_action in archi['num_actions_measures'].values():
            self.linear_actions.append(nn.Linear(self.actor_bloc.output_size, archi['num_zone_labels']*num_action))
        self.optimizer = optim.Adam(self.parameters(), lr=archi['lr_actor'])
        self.apply(init_xavier_uniform)
        print("Actor's state_dict:")
        for param_tensor in self.state_dict():
            print(param_tensor, "\t", self.state_dict()[param_tensor].size())
        print()

    def forward(self, state):
        x = self.actor_bloc(state)
        dists = []
        for linear_action, num_action in zip(self.linear_actions, self.archi['num_actions_measures'].values()):
            dists.append(Categorical(torch.softmax(torch.reshape(linear_action(x), (self.archi['num_zone_labels'], num_action)), dim=1)))
        return dists

class Critic(nn.Module):
    def __init__(self, archi):
        super(Critic, self).__init__()
        self.critic_bloc = Block(archi['input_size'], archi['layers_size_critic'])
        self.linear_values = nn.Linear(self.critic_bloc.output_size, archi['num_zones'])
        self.optimizer = optim.Adam(self.parameters(), lr=archi['lr_critic'])
        self.apply(init_xavier_uniform)
        print("Critic's state_dict:")
        for param_tensor in self.state_dict():
            print(param_tensor, "\t", self.state_dict()[param_tensor].size())
        print()

    def forward(self, state):
        return self.linear_values(self.critic_bloc(state))

def prepare_input(state, archi):
    all_brut_ratios = state[:,1:8]
    all_diff_ratio = state[:,8:15]
    step_idx = np.array([state[0][-1]])
    step_idx = torch.zeros(archi['max_measure_steps'])
    step_idx[int(state[0][-1])] = 1.0

    input = np.empty((4*all_brut_ratios.shape[0], all_brut_ratios.shape[1]))

    brut_mean_axis_0 = np.mean(all_brut_ratios, axis=0)
    brut_mean_axis_1 = np.mean(all_brut_ratios, axis=1)
    brut_std_axis_0 = np.std(all_brut_ratios, axis=0)
    brut_std_axis_1 = np.std(all_brut_ratios, axis=1)
    diff_mean_axis_0 = np.mean(all_diff_ratio, axis=0)
    diff_mean_axis_1 = np.mean(all_diff_ratio, axis=1)
    diff_std_axis_0 = np.std(all_diff_ratio, axis=0)
    diff_std_axis_1 = np.std(all_diff_ratio, axis=1)

    for i in range(all_brut_ratios.shape[0]):
        input[4*i] = (all_brut_ratios[i]-brut_mean_axis_0)/(1e-9+brut_std_axis_0)
        input[4*i+1] = (all_brut_ratios[i]-brut_mean_axis_1[i])/(1e-9+brut_std_axis_1[i])
        input[4*i+2] = (all_diff_ratio[i]-diff_mean_axis_0)/(1e-9+diff_std_axis_0)
        input[4*i+3] = (all_diff_ratio[i]-diff_mean_axis_1[i])/(1e-9+diff_std_axis_1[i])
    input = np.sign(input)*np.log(1+np.abs(input))
    input = torch.from_numpy(input).float().flatten()
    input_extended = torch.cat((input, step_idx))
    return Variable(input_extended)
