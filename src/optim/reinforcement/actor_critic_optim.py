# !/usr/bin/env python3
import json
import os

import torch

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront
from src.optim.reinforcement.actor_critic_model import ActorCritic


class ActorCriticOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):

        objectives = Objectives(optimize_req)
        pareto_front = ParetoFront()

        # Here should be the list of models you want to infer
        models_to_infer = [
            '14-05-2020_14:44:16'
        ]
        assert len(models_to_infer) != 0

        # Here if you want each model to be infer more than once
        num_infer_per_model = 5

        for model_name in models_to_infer:
            archi_path = os.path.join('.', 'reinforcement', 'runs_reinforce', model_name, 'archi.json')
            with open(archi_path) as f:
                archi = json.load(f)
            model = ActorCritic(archi, optimize_req, policy_common_values, activable_measures_dict)
            model_path = os.path.join('.', 'reinforcement', 'runs_reinforce', model_name, 'model.pt')
            model.load_state_dict(torch.load(model_path))
            for _ in range(num_infer_per_model):
                eval_policy_req = model.eval()
                ok, eval_policy_res = self.call_eval_policy(eval_policy_req, activable_measures_dict)
                objs = objectives.compute(ok, eval_policy_req, eval_policy_res)
                pareto_front.update(eval_policy_req, eval_policy_res, new_metrics=objs)

        return True, pareto_front


if __name__ == '__main__':
    launch_server(BaseOptimServer, ActorCriticOptimRequestHandler)
