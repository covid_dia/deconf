import time
from copy import deepcopy
from heapq import heappush, heappop

from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront


def extract_data_one_zone(policy, zone_index):
    # A bit quick and dirty, may need to change afterwards.
    fields_with_list_value = ['zones', 'population_size', 'ICU_beds']
    special_fields = ['initial_ratios']
    data_zone = {}
    for k in policy.keys():
        if k in fields_with_list_value or k in special_fields:
            continue
        data_zone[k] = policy[k]
    for k in fields_with_list_value:
        data_zone[k] = [policy[k][zone_index]]
    data_zone['initial_ratios'] = {policy['zones'][zone_index]: policy['initial_ratios'][policy['zones'][zone_index]]}
    return data_zone


def extend_policy(policy, measure_weeks_total, activable_measures):
    measure_weeks = policy['measure_weeks']
    for k in activable_measures:
        current_activation = None
        current_index = -1
        list_activation = []
        for i in measure_weeks_total:
            if i in measure_weeks:
                current_index += 1
                current_activation = policy[k][current_index]
            list_activation.append(current_activation)
        policy[k] = list_activation
    policy['measure_weeks'] = measure_weeks_total
    return policy


def insert_in_policy(policy, measure, activable_measures, week, element):
    weeks = policy['measure_weeks'].copy()
    if week not in weeks:
        weeks.append(week)
        weeks.sort()
        extend_policy(policy, weeks, activable_measures)
    for k in range(len(policy[measure])):
        if weeks[k] == week:
            policy[measure][k] = element
            break


def gather_policies(policy_list, list_measures, activable_measures):
    measure_weeks_total = []
    for i in range(len(policy_list)):
        for week in policy_list[i]['measure_weeks']:
            if week not in measure_weeks_total:
                measure_weeks_total.append(week)
    measure_weeks_total.sort()
    # print(policy_list)
    new_policy_list = [extend_policy(p, measure_weeks_total, activable_measures) for p in policy_list]
    # print(new_policy_list)
    fields_with_list_value = ['zones', 'population_size', 'ICU_beds']
    special_fields = ['initial_ratios']
    policy = {}
    for k in new_policy_list[0].keys():
        if k not in fields_with_list_value and k not in special_fields and k not in list_measures:
            policy[k] = new_policy_list[0][k]
    for k in fields_with_list_value:
        policy[k] = []
    policy['initial_ratios'] = {}
    for i in range(len(new_policy_list)):
        zone = new_policy_list[i]['zones'][0]
        for k in fields_with_list_value:
            policy[k].append(new_policy_list[i][k][0])
        policy['initial_ratios'][zone] = new_policy_list[i]['initial_ratios'][zone]
    for k in list_measures:
        if k in activable_measures:
            measure_weeks = policy['measure_weeks']
            policy[k] = []
            for week_index in range(len(measure_weeks)):
                list_activation = []
                for i in range(len(new_policy_list)):
                    list_activation.append(new_policy_list[i][k][week_index]['activated'][0])
                policy[k].append({'activated': list_activation})
        else:
            policy[k] = None
    return policy


def update_best_policies(current_policies, death_threasholds, new_gdp, new_death_rate, ok, p_results, new_policy):
    total_population_size = new_policy['population_size']
    for i in range(len(current_policies)):
        if new_death_rate <= death_threasholds[i] and (current_policies[i] is None or new_gdp < current_policies[i][0]):
            current_policies[i] = (new_gdp, new_policy, ok, p_results)


def generate_possible_policies_except_max(available_measures: dict, ncolors):
    l = []
    size_loop = 1
    for m_max in available_measures.values():
        size_loop *= (m_max + 1) ** ncolors

    for i in range(size_loop - 1):
        k = i
        e = []
        for m, m_max in available_measures.items():
            l2 = []
            for _ in range(ncolors):
                l2.append(k % (m_max + 1))
                k = int(k / (m_max + 1))
            e.append({'activated': l2})
        l.append(e)
    return l


def optimize_same_policy_with_filter(optimizer_class, initial_policy: dict, available_measures,
                                     max_measure_steps, step_duration,
                                     valid_policy_filter = lambda x : True, eval_filter=lambda x: True,
                                     priority_function = lambda x : x['gdp_accumulated_loss'][-1],
                                     initial_policy_results = None, timeout = None):

    count_time = 0
    start_time = time.time()
    """
    :param optimizer_class: Reference to the optimizer class to be able to call the eval.
    :param initial_policy: first policy to start from (everything is confined until max_measure_steps typically)
    :param available_measures: list of measures whose max is not 0
    :param valid_policy_filter: function returning True if selected policy is a valid one (for instance all weeks in measure_weeks are even)
    :param eval_filter: function returning True if the evaluation is satisfactory and it is meaningful to explore more this policy.
    :param priority_function: function returning from a metrics the priority we want to use for the queue.
    :param initial_policy_results: results of call_eval_policy on the initial policy if it exists.
    :param timeout: a time in seconds to stop the solving if exceeded.
    :return: Pareto Front
    """
    ncolors = len(initial_policy['zone_label_names'])

    pareto_front = ParetoFront()
    total_population_size = sum(initial_policy['population_size'])
    potential_solutions = []
    # print('initial_policy:', initial_policy)
    # print('available_measures:', available_measures)
    if initial_policy_results is None:
        ok, p_results = optimizer_class.call_eval_policy \
            (initial_policy, available_measures)
    else:
        ok, p_results = initial_policy_results

    first_priority = priority_function(p_results)
    # metrics = policy2gdp_deathrate(ok, p_results, total_population_size)
    metrics = optimizer_class.objectives.compute(ok, initial_policy, p_results)
    pareto_front.update(initial_policy, p_results, new_metrics=metrics)
    # optimizer_class.print_policy(initial_policy)
    heappush(potential_solutions, (first_priority, 0, initial_policy, 0, p_results))
    order_solutions = 1
    count_solutions = 0
    max_element = {}
    list_available_measures = []
    for k in available_measures.keys():
        list_available_measures.append(k)
        max_element[k] = [initial_policy['max_measure_values'][k] for _ in range(ncolors)]
    possible_policies = generate_possible_policies_except_max(available_measures, ncolors)
    # print(max_element)
    while potential_solutions != [] and (timeout is None or time.time() - start_time < timeout):
        current_priority, x, current_policy, current_index, y = heappop(potential_solutions)

        if current_index < max_measure_steps * step_duration:
            if current_index + step_duration < max_measure_steps * step_duration:
                x2 = order_solutions
                heappush(potential_solutions, (current_priority, x2, current_policy, current_index + step_duration, y))
                order_solutions += 1
            week = current_index
            week_end = week + step_duration
            for measure_list in possible_policies:

                new_policy = deepcopy(current_policy)
                for i in range(len(list_available_measures)):
                    insert_in_policy(new_policy, list_available_measures[i], available_measures, week, measure_list[i])
                    if week_end < max_measure_steps * step_duration:
                        insert_in_policy(new_policy, list_available_measures[i], available_measures, week_end,
                                         {'activated': max_element[list_available_measures[i]]})
                if not valid_policy_filter(new_policy):
                    continue
                """print("--------------------")
                for x in available_measures:
                    print(x," : ",new_policy[x])
                print(new_policy['measure_weeks'])
                print("--------------------")"""
                start_time_count = time.time()
                ok, p_results = optimizer_class.call_eval_policy(new_policy, available_measures)
                count_time += time.time() - start_time_count

                # metrics = policy2gdp_deathrate(ok, p_results, total_population_size)
                metrics = optimizer_class.objectives.compute(ok, new_policy, p_results)

                # optimizer_class.print_policy(new_policy)
                if eval_filter(metrics):
                    pareto_front.update(new_policy, p_results, new_metrics=metrics)
                    count_solutions += 1
                    if week_end < max_measure_steps * step_duration:
                        x2 = order_solutions
                        heappush(potential_solutions, (priority_function(p_results), x2, new_policy, week_end, p_results))
                        order_solutions += 1
    # print(count_solutions)
    # print(count_time)
    #pareto_front.write_eval_policy_reqs("/home/tristan/files")
    return pareto_front

def create_initial_policy(optimizer_class, optimize_req, policy_common_values, activable_measures_dict):
    ncolors = len(optimize_req['zone_label_names'])
    total_population_size = sum(optimize_req['population_size'])
    list_measures = optimize_req['max_measure_values'].keys()
    max_measure_steps = optimize_req['max_measure_steps']
    #  pareto_front = ParetoFront()
    optimizer_class.objectives = Objectives(optimize_req)  # dirty: store as a field within the RequestHandler
    eval_policy_req = deepcopy(policy_common_values)
    eval_policy_req['measure_weeks'] = [0]
    eval_policy_req['max_measure_steps'] = max_measure_steps
    min_element = [0 for x in range(ncolors)]
    for measure, m_max in optimize_req['max_measure_values'].items():
        if measure in activable_measures_dict:
            eval_policy_req[measure] = [{'activated': [m_max for x in range(ncolors)]}]
        else:
            eval_policy_req[measure] = None

    eval_policy_req['measure_weeks'].append(optimize_req['max_measure_steps'] * optimize_req["measure_step_duration"])
    for m in activable_measures_dict:
        eval_policy_req[m].append({'activated': min_element})
    eval_policy_req['max_measure_values'] = optimize_req['max_measure_values']
    return eval_policy_req

def create_all_deconf_policy(optimizer_class, optimize_req, policy_common_values, activable_measures_dict):
    nzones = len(optimize_req['zones'])
    total_population_size = sum(optimize_req['population_size'])
    list_measures = optimize_req['max_measure_values'].keys()
    max_measure_steps = optimize_req['max_measure_steps']
    #  pareto_front = ParetoFront()
    optimizer_class.objectives = Objectives(optimize_req)  # dirty: store as a field within the RequestHandler
    eval_policy_req = deepcopy(policy_common_values)
    eval_policy_req['measure_weeks'] = [0]
    eval_policy_req['max_measure_steps'] = max_measure_steps
    min_element = [0 for x in range(nzones)]
    for measure, m_max in optimize_req['max_measure_values'].items():
        if measure in activable_measures_dict:
            eval_policy_req[measure] = [{'activated': min_element}]
        else:
            eval_policy_req[measure] = None

    eval_policy_req['measure_weeks'].append(optimize_req['max_measure_steps'] * optimize_req['measure_step_duration'])
    for m in activable_measures_dict:
        eval_policy_req[m].append({'activated': min_element})
    eval_policy_req['max_measure_values'] = optimize_req['max_measure_values']
    return eval_policy_req

def color_strictness_filter(policy):
    """
    Filter checking whether the input policy respects the color strictness.
    :param policy: input policy.
    :return:
    """
    colors = policy["zone_label_names"]
    if len(colors) == 1:
        return True
    measure_weeks = policy["measure_weeks"]
    for k in policy.keys():
        if policy[k] != None and isinstance(policy[k], list) and len(policy[k]) == len(measure_weeks) and isinstance(policy[k][0], dict) and ('activated' in policy[k][0].keys()):
            for d in policy[k]:
                l = d['activated']
                for i in range(len(l)-1):
                    if l[i] < l[i-1]:
                        return False
    return True

