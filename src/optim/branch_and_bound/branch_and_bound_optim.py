# !/usr/bin/env python3

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.branch_and_bound.branch_and_bound_utils import create_initial_policy, optimize_same_policy_with_filter, \
    create_all_deconf_policy, color_strictness_filter

total_time = 0

class BBOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):

        eval_policy_req = create_initial_policy(self, optimize_req, policy_common_values, activable_measures_dict)
        ok, p_results = self.call_eval_policy(eval_policy_req, activable_measures_dict)
        metrics = self.objectives.compute(ok, eval_policy_req, p_results)
        icu_beds_ratio = metrics[1]
        eval_policy_req2 = create_all_deconf_policy(self, optimize_req, policy_common_values, activable_measures_dict)
        ok2, p_results2 = self.call_eval_policy(eval_policy_req2, activable_measures_dict)
        metrics2 = self.objectives.compute(ok2, eval_policy_req2, p_results2)
        icu_beds_ratio2 = metrics2[1]
        def filter_function(metrics):
            return metrics[1] <= icu_beds_ratio + (icu_beds_ratio2 - icu_beds_ratio)*0.5
        max_measure_steps = optimize_req['max_measure_steps']
        step_duration = optimize_req['measure_step_duration']
        pareto_front = optimize_same_policy_with_filter(self, eval_policy_req, activable_measures_dict, max_measure_steps, step_duration, valid_policy_filter= color_strictness_filter,
                                                        eval_filter = filter_function, initial_policy_results= (ok, p_results))

        return True, pareto_front
if __name__ == '__main__':
    launch_server(BaseOptimServer, BBOptimRequestHandler)
