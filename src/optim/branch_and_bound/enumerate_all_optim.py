# !/usr/bin/env python3

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.branch_and_bound.branch_and_bound_utils import optimize_same_policy_with_filter, create_initial_policy, \
    color_strictness_filter


class EnumerateAllOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):
        # with open('/home/tristan/charly/optimize_req.json', 'w') as json_file:
        #     json.dump(optimize_req, json_file)
        max_measure_steps = optimize_req['max_measure_steps']
        step_duration = optimize_req['measure_step_duration']
        eval_policy_req = create_initial_policy(self, optimize_req, policy_common_values, activable_measures_dict)
        pareto_front = optimize_same_policy_with_filter(self, eval_policy_req, activable_measures_dict, max_measure_steps, step_duration, valid_policy_filter= color_strictness_filter, timeout=1800)
        # pareto_front.write_eval_policy_reqs('/home/tristan/charly')
        return True, pareto_front


if __name__ == '__main__':
    launch_server(BaseOptimServer, EnumerateAllOptimRequestHandler)
