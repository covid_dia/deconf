# !/usr/bin/env python3
import argparse
import json
import os
import random
# scheduler
import threading
import time
import uuid
from copy import deepcopy
from http.server import BaseHTTPRequestHandler, HTTPServer
from os.path import expanduser
from threading import Lock

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import requests

from src.model.macromodel import error_dumps
from src.model.policy_utils import all_measures, simplify_policy_eval_req_except_last_week
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront
from src.scheduler.sched import *


# print(matplotlib.get_backend())  # 'TkAgg' by default
# $ sudo apt install python3-matplotlib
# matplotlib.use('Qt5Agg')


class BaseOptimRequestHandler(BaseHTTPRequestHandler):
    def optimize(self, optimize_req: dict,
                 policy_common_values: dict,
                 activable_measures_dict: dict):
        """
        This function should be inherited by children.
        :param optimize_req: the whole "optimized_req" request as a dict
        :param policy_common_values: compulsoory fields, copied from "optimized_req"
                ['zones', 'population_size', 'compartmental_model', 
                'compartments_'+model_name, 'initial_ratios_'+model_name, 
                'compartmental_model_parameters']
                You can deepcopy it as a skeleton for your custom policies.
                Use call_eval_policy() to test a policy.
        :param activable_measures_dict:
            ex. { 'school_closures' : 2, 'social_containment_all' : 3 }
        :return: ok, pareto_front
        """
        ok, pareto_front = False, ParetoFront()
        return ok, pareto_front

    @staticmethod
    def make_policy_common_values(optimize_req: dict):
        # copy fields from optimize_req that will be common to all policies
        # print('optimize_req:', optimize_req)
        model_name = optimize_req['compartmental_model']
        policy_common_values = {k: optimize_req[k] for k in
                                ['zones', 'zone_label_names', 'zone_labels',
                                 'population_size', 'ICU_beds',
                                 'compartmental_model', 'compartments_' + model_name,
                                 'initial_ratios_' + model_name,
                                 'compartmental_model_parameters']}
        activable_measures_dict = {}
        for m, m_max in optimize_req['max_measure_values'].items():
            policy_common_values[m] = None
            if m_max > 0:
                policy_common_values[m] = []
                activable_measures_dict[m] = m_max
        return policy_common_values, activable_measures_dict

    # disable logs "[12/Apr/2020 14:16:08] "POST /optimize HTTP/1.1" 200 -"
    def log_message(self, format_, *args):
        return

    def write_error_dumps(self, errstr):
        print('ERROR:', errstr)
        self.wfile.write(json.dumps({'error': errstr, 'success': False}).encode())

    # TBF: wouldn't it be better to check the policy in the macromodel instead of the optimizer
    @staticmethod
    def check_policy_req_lastliftall(eval_policy_req: dict, activable_measures_dict: dict):
        """ Check that the last measure corresponds to a lift of all measures
        Return True if all measures are lift else False
        """
        # max_days = eval_policy_req['compartmental_model_parameters']['common']['max_days']
        # nweeks = int(max_days / 7)
        for m in activable_measures_dict:
            # check last one lift all
            last = eval_policy_req[m][-1]
            if sum(last["activated"]) != 0:
                return False
        return True

    def call_eval_policy(self, eval_policy_req: dict, activable_measures_dict: dict):
        """
        Send a POST request to macropolicy server.
        This function can be overloaded to do somethig else.
        """
        eval_policy_req = simplify_policy_eval_req_except_last_week(eval_policy_req)
        if not self.check_policy_req_lastliftall(eval_policy_req, activable_measures_dict):
            return False, error_dumps("Check of eval_policy_req failed")
        response = requests.post(self.server.eval_policy_url, data=json.dumps(eval_policy_req))
        if response.status_code != 200:
            return False, error_dumps('call_eval_policy() failed')
        eval_policy_res = response.json()
        self.draw_policy(activable_measures_dict, eval_policy_req, eval_policy_res)
        return True, eval_policy_res

    # LC: this is the thread function to perform requests.post within call_eval_policy_async
    #     do not call otherwise
    def call_eval_policy_in_thread(self, eval_policy_req, job_uid, eval_host, eval_port):
        # build the remote model server url from the scheduler assigned resource
        eval_policy_url = 'http://%s:%d/eval_policy' % (eval_host, eval_port)
        response = None
        while response is None:
            try:
                # this is where the request is issued to the model. this is a blocking call
                response = requests.post(eval_policy_url, data=json.dumps(eval_policy_req))
            except (requests.exceptions.ConnectionError, requests.exceptions.HTTPError):
                # sleep a small amount of time to save CPU
                time.sleep(0.1)
        # store the result within the shared dict
        self.server.dict_jobuid_response_lock.acquire()
        self.server.dict_jobuid_response[job_uid] = response
        self.server.dict_jobuid_response_lock.release()

    # LC: this is the asynchronous version of call_eval_policy. returns the job uid.
    def call_eval_policy_async(self, eval_policy_req: dict, activable_measures_dict: dict,
                               job_uid=None):
        """
        Send a POST request to macropolicy server.
        This function can be overloaded to do something else.
        """
        eval_policy_req = simplify_policy_eval_req_except_last_week(eval_policy_req)
        if not self.check_policy_req_lastliftall(eval_policy_req, activable_measures_dict):
            return False, error_dumps("Check of eval_policy_req failed")

        # create a job uid if needed
        if job_uid is None:
            job_uid = str(uuid.uuid1())
        # decide a socket port in case the job is colocated with another job
        # following the IANA recommendations
        job_port = random.randint(49152, 65535)

        # create the macromodel server as a job
        self.server.scheduler.addJob(job_uid, "cd " + os.getcwd().replace(expanduser("~") + "/",
                                                                          "") + " && source venv/bin/activate && PYTHONPATH+=. python src/model/macromodel.py --interface $SCHED_RESOURCE_ADDRESS:" + str(
            job_port) + " --singlerequest && deactivate")
        # schedule and run job
        mapping = self.server.scheduler.runStatic()
        # at this point the job is running on a possibly remote resource

        # search for assigned resource
        job = self.server.scheduler.getRunningJobFromUID(job_uid)
        resource = job.resource

        # start thread to send data and wait for response
        tid = threading.Thread(target=self.call_eval_policy_in_thread,
                               args=(eval_policy_req, job_uid, resource.address, job_port))
        tid.daemon = False
        tid.start()

        return job_uid

    # LC: check if a job is terminated and process the response
    #     returns the same tuple as call_eval_policy with a third boolean element
    #     that indicates whether or not the job has terminated
    def call_eval_policy_poll_job(self, eval_policy_req: dict, activable_measures_dict: dict,
                                  job_uid):
        response = None
        # check if the job has ended. release the lock ASAP
        self.server.dict_jobuid_response_lock.acquire()
        if job_uid in self.server.dict_jobuid_response:
            response = self.server.dict_jobuid_response[job_uid]
            del self.server.dict_jobuid_response[job_uid]
        self.server.dict_jobuid_response_lock.release()
        # process the result if available
        if response is not None:
            if response.status_code != 200:
                return False, error_dumps('call_eval_policy_async() failed'), True
            eval_policy_res = response.json()
            self.draw_policy(activable_measures_dict, eval_policy_req, eval_policy_res)
            return True, eval_policy_res, True
        else:
            return False, None, False

    def draw_policy(self, activable_measures_dict: dict,
                    eval_policy_req: dict, eval_policy_res: dict):
        if not self.server.draw:
            return
        # now plot death rates, gdps
        max_days = eval_policy_req['compartmental_model_parameters']['common']['max_days']
        nweeks, nmeasures, nlabels = int(max_days / 7), len(activable_measures_dict), len(
            eval_policy_req['zone_label_names'])
        matrix = np.zeros((nmeasures, nlabels, nweeks))
        print(nmeasures, nlabels, nweeks)
        # draw measure heatmap
        for midx, m in enumerate(activable_measures_dict):
            if m not in eval_policy_req or eval_policy_req[m] is None:
                continue
            for w in range(nweeks):
                try:
                    mw_idx = eval_policy_req['measure_weeks'].index(w)
                except ValueError:
                    mw_idx = -1
                for label in range(nlabels):
                    if w > 0 and mw_idx == -1:  # copy previous week
                        matrix[midx][label][w] = matrix[midx][label][w - 1]
                    else:
                        matrix[midx][label][w] = eval_policy_req[m][mw_idx]['activated'][label]
            axs = self.axs[midx, 0]
            axs.imshow(matrix[midx], cmap='Greens', aspect='auto')
            axs.set_xlabel('week nb')
            axs.set_yticks(range(nlabels))
            axs.set_yticklabels(eval_policy_req['zone_label_names'])
            # axs.set_yticks(np.arange(nlabels)[1::2])  # We want to show every other tick
            # axs.set_yticklabels(eval_policy_req['zones'][1::2])
            # axs.set_title(m)
            axs.set_ylabel(m)
        objs = self.draw_objectives.compute(True, eval_policy_req, eval_policy_res)
        self.draw_X.append(objs[0])
        self.draw_Y.append(objs[1])
        # print(self.draw_X, self.draw_Y)
        self.axbig.plot(self.draw_X, self.draw_Y, 'ro')
        # self.fig.tight_layout()
        plt.pause(.1)

    def draw_optim_request(self, optimize_req, activable_measures_dict):
        if not self.server.draw:
            return
        optimize_draw_req = deepcopy(optimize_req)
        optimize_draw_req['objectives'] = self.server.draw
        self.draw_objectives = Objectives(optimize_draw_req)
        self.draw_X, self.draw_Y = [], []
        nmeasures = len(activable_measures_dict)
        # prepaire heatmap
        # https://matplotlib.org/3.1.1/gallery/images_contours_and_fields/image_annotated_heatmap.html
        self.axs = self.fig.subplots(nrows=nmeasures, ncols=2)
        if nmeasures > 1:
            gs = self.axs[0, 1].get_gridspec()
            # remove the underlying axes
            for ax in self.axs[0:, 1]:
                ax.remove()
            self.axbig = self.fig.add_subplot(gs[0:, 1])
        else:
            self.axs = self.axs.reshape((1, 2))
            self.axbig = self.axs[0, 1]
        self.axbig.set_xlabel(self.server.draw[0])
        self.axbig.set_ylabel(self.server.draw[1])

    # noinspection PyPep8Naming
    def do_POST(self):
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        optimize_req = json.loads(post_body)
        self.send_response(200)
        self.end_headers()

        dumps = None
        if 'optimize' in self.path:
            # check zones are valid and that population_size has the same length
            if 'zones' not in optimize_req or not optimize_req['zones']:
                return self.write_error_dumps('No zones provided.')
            ok_measures, req_measures = all_measures().keys(), optimize_req['max_measure_values'].keys()
            if not all(o in ok_measures for o in req_measures):
                return self.write_error_dumps('optimize_req contains invalid measures {}. '
                                              'Valid measures are {}'.format(req_measures, ok_measures))
            policy_common_values, activable_measures_dict = self.make_policy_common_values(optimize_req)
            if self.server.draw:
                self.fig = plt.figure()
                self.draw_optim_request(optimize_req, activable_measures_dict)

            if self.server.scheduler is not None:
                ok, pareto_front = self.optimize_async_para(optimize_req, policy_common_values,
                                                            activable_measures_dict)
            else:
                ok, pareto_front = self.optimize(optimize_req, policy_common_values,
                                                 activable_measures_dict)
            if not ok:
                return self.write_error_dumps('optimize() failed.')

            if self.server.draw != ['0', '0']:
                plt.close('all')
            # measure_weeks, forced_telecommuting, social_containment_all, school_closures, total_containment_high_risk
            dumps = {'success': True, 'zones': optimize_req['zones'],
                     'zone_label_names': optimize_req['zone_label_names'], 'policies': []}
            for preq, pres, objs in zip(pareto_front.eval_policy_reqs,
                                        pareto_front.eval_policy_ress,
                                        pareto_front.metrics):
                # forced_telecommuting, school_closures, social_containment_all, total_containment_high_risk
                pdump = {k: preq[k] for k in optimize_req['max_measure_values'].keys()}
                for k in ['measure_weeks']:
                    pdump[k] = preq[k]
                for k in [
                    'activity',
                    'cases',
                    'deaths',
                    'gdp_accumulated_loss',
                    'hospital_patients',
                    'ICU_patients',
                    'tests'
                ]:
                    pdump[k] = pres[k]
                pdump['objectives'] = objs
                dumps['policies'].append(pdump)
        if not dumps:
            dumps = error_dumps('command ' + self.path + ' not recognized')
        self.wfile.write(json.dumps(dumps).encode())


class BaseOptimServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass,
                 eval_host='localhost', eval_port=8000, draw=None,
                 enable_scheduler=False, computing_resources=None, **kwargs_request):
        # stuff that needs to be stored here
        self.draw = draw
        self.draw_objectives, self.draw_X, self.draw_Y, self.fig, self.axs, self.axbig = [None] * 6
        if self.draw:
            matplotlib.use('TkAgg')

        self.scheduler = None
        if enable_scheduler:
            # threading POST requests
            self.dict_jobuid_response_lock = Lock()
            self.dict_jobuid_response = {}
            # scheduler
            self.scheduler = Sched()
            if computing_resources is not None:
                self.scheduler.addResourcesFromFile(computing_resources)
            else:
                self.scheduler.addResource("127.0.0.1", "loopback_slot0")
                self.scheduler.addResource("127.0.0.1", "loopback_slot1")

        if eval_host.startswith('http://'):
            self.eval_policy_url = '%s:%d/eval_policy' % (eval_host, eval_port)
        else:
            self.eval_policy_url = 'http://%s:%d/eval_policy' % (eval_host, eval_port)
        # print(self.eval_policy_url)
        self.__dict__.update((k, v) for k, v in kwargs_request.items())
        HTTPServer.__init__(self, server_address, RequestHandlerClass)


def launch_server(OptimServer, OptimRequestHandler, **kwargs_request):
    optim_host = 'localhost'
    optim_port = 8001
    eval_host = 'localhost'
    eval_port = 8000
    draw = None

    parser = argparse.ArgumentParser(description='Python optim server.')
    parser.add_argument('--interface', type=str,
                        help='hostname(default=\'localhost\'):port(default=8001) of this optim server.')
    parser.add_argument('--modelinterface', type=str,
                        help='hostname(default=\'localhost\'):port(default=8000) of the model evaluation server. This does not apply if the scheduler is enabled.')
    parser.add_argument('--draw', action='store_true',
                        help='two objectives to draw policies and Pareto front. ex: ICU_saturation_ratio gdp_accumulated_loss_ratio')
    parser.add_argument('--enablescheduler', action='store_true',
                        help='Uses the in-house scheduler to dispatch model evaluations on different resources. No need to run a model evaluation as it will be remotely started by the scheduler.')
    parser.add_argument('--computingresources', type=str,
                        help='Path to a resource file to be used with the scheduler.')
    parser.add_argument('--singlerequest', action='store_true',
                        help='processes a single http request and terminates')
    parser.add_argument('--nbrequests', type=int,
                        help='processes a given number of http requests and terminates')
    args = parser.parse_args()

    if args.interface is not None:
        optim_host = ':'.join(args.interface.split(':')[:-1])
        optim_port = int(args.interface.split(':')[-1])
    if args.modelinterface is not None:
        eval_host = ':'.join(args.modelinterface.split(':')[:-1])
        eval_port = int(args.modelinterface.split(':')[-1])

    server = OptimServer((optim_host, optim_port),
                         OptimRequestHandler,
                         eval_host=eval_host, eval_port=eval_port,
                         draw=args.draw,
                         enable_scheduler=args.enablescheduler,
                         computing_resources=args.computingresources,
                         **kwargs_request)

    print('Starting server at http://%s:%d, draw:%s' % (optim_host, optim_port, args.draw))

    if args.singlerequest:
        server.handle_request()
    elif args.nbrequests:
        for _ in range(args.nbrequests):
            server.handle_request()
    else:
        server.serve_forever()
