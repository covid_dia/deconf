# !/usr/bin/env python3
import random
from copy import deepcopy

import numpy as np
from deap import algorithms
from deap import base
from deap import creator
from deap import tools

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.branch_and_bound.branch_and_bound_utils import color_strictness_filter
from src.optim.nsgax_utils import init_ind, mutation, mate
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront, is_pareto_efficient_simple

class NSGAXOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):
        np.random.seed(38)
        random.seed(38)
        """
        Inherits BaseOptimRequestHandler.optimize(), see there for doc.
        """
        MU = 10  # population_size
        if hasattr(self.server, "nsgax_mu"):
            MU = self.server.nsgax_mu

        NGEN = 5  # number of generations
        if hasattr(self.server, "nsgax_ngen"):
            NGEN = self.server.nsgax_ngen

        X = 2  # version of the NSGA algo to use
        if hasattr(self.server, "nsgax_x"):
            X = self.server.nsgax_x

        CXPB = 1.0  # probability of mating two individuals
        if hasattr(self.server, "nsgax_cxpb"):
            CXPB = self.server.nsgax_cxpb

        MUTPB = 0.5  # probability of mutating and indibidual
        if hasattr(self.server, "nsgax_mutpb"):
            MUTPB = self.server.nsgax_mutpb

        print('NSGAXOptimRequestHandler(): mu:{}, gen:{}, X:{}'.format(MU, NGEN, X))
        ncolors = len(optimize_req['zone_label_names'])
        nmeasures = len(activable_measures_dict)
        ncolors_nmeasures = ncolors * nmeasures
        time = optimize_req['max_measure_steps']
        step_duration = optimize_req['measure_step_duration']

        objectives = Objectives(optimize_req)

        # NSGA-{2,3} parameters

        # Register fitness / individual / population
        # if not hasattr(creator, 'FitnessMin'):
        creator.create("FitnessMin", base.Fitness, weights=(-1.0, -1.0))  # two objectives to minimize
        # the array size is (time x (region x policy))
        # TODO: to flatten for speed
        creator.create("Individual", np.ndarray, fitness=creator.FitnessMin)

        toolbox = base.Toolbox()
        toolbox.register("individual", init_ind, creator.Individual, time, ncolors, activable_measures_dict)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)

        def individual2policy_req(individual):
            """ transform individual into a json complient policy request

            Parameters
            ==========
            individual: np.ndarray
                the currrent individual, data are stored in a time x (ncolors*nmeasures) matrix

            Return
            ======
            policy: dict
                the dictionnary sent as a json for evaluation
                the dictionnary sent as a json for evaluation
            """
            policy = deepcopy(policy_common_values)
            measure_weeks = []
            # print('individidual:', individual, 'nmeasures:', nmeasures, 'ncolors_nmeasures:', ncolors_nmeasures)
            slices = [slice(i, i + ncolors) for i in range(0, ncolors_nmeasures, ncolors)]
            for i in range(len(individual)):
                # if all measures are set False it means that this week time is not activated
                if i == 0:  # measure weeks necessary starts at 0
                    measure_weeks.append(i * step_duration)
                    # slice measures by zones
                    for j, m in enumerate(activable_measures_dict):
                        begin = (j * ncolors)
                        # need to be cast for json
                        arr = [int(a) for a in individual[i, begin: begin + ncolors].copy()]
                        policy[m].append({"activated": arr})
                elif sum(individual[i]) > 0:
                    # print('w:', wi)
                    measure_weeks.append(i * step_duration)
                    # slice measures by zones
                    for j, m in enumerate(activable_measures_dict):
                        begin = (j * ncolors)
                        # need to be cast for json
                        arr = [int(a) for a in individual[i, begin: begin + ncolors].copy()]
                        # ensure bounds are respected
                        m_max = activable_measures_dict[m]
                        arr_clipped = [max(0, min(m_max, int(a))) for a in arr]
                        policy[m].append({"activated": arr_clipped})
                        # lift all measures at the end
            measure_weeks.append(optimize_req['max_measure_steps'] * step_duration)
            for m in activable_measures_dict:
                policy[m].append({'activated': [0] * ncolors})
            policy['measure_weeks'] = measure_weeks

            return policy

        # define evaluation
        def evaluate_verbose(individual):
            # get eval_policy_req
            eval_policy_req = individual2policy_req(individual)
            # do POST evaluation
            assert(color_strictness_filter(eval_policy_req)) # Guard to check that green < red.
            ok, eval_policy_res = self.call_eval_policy(eval_policy_req, activable_measures_dict)
            # now check if the policy has good objectives
            objs = objectives.compute(ok, eval_policy_req, eval_policy_res)
            return eval_policy_req, eval_policy_res, objs

        def evaluate(individual):
            _, _, objs = evaluate_verbose(individual)
            return objs

        # register genetic algorithm function
        toolbox.register("mate", mate, cx_time=0.1, ncolors=ncolors, nmeasures=nmeasures)
        toolbox.register("mutate", mutation,
                         activable_measures_dict=activable_measures_dict,
                         ncolors=ncolors, mut_prob=0.1)
        if X == 3:
            ref_points = tools.uniform_reference_points(nobj=2, p=12)
            toolbox.register("select", tools.selNSGA3, ref_points=ref_points)
        else:
            toolbox.register("select", tools.selNSGA2)
        toolbox.register("evaluate", evaluate)

        fop = tools.ParetoFront(similar=np.array_equal)
        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean, axis=0)
        stats.register("std", np.std, axis=0)
        stats.register("min", np.min, axis=0)
        stats.register("max", np.max, axis=0)

        logbook = tools.Logbook()
        logbook.header = "gen", "evals", "std", "min", "avg", "max"

        pop = toolbox.population(n=MU)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in pop if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Compile statistics about the population
        record = stats.compile(pop)
        logbook.record(gen=0, evals=len(invalid_ind), **record)
        # print(logbook.stream)

        # Begin the generational process
        for gen in range(1, NGEN):
            offspring = algorithms.varAnd(pop, toolbox, CXPB, MUTPB)

            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            # Select the next generation population from parents and offspring
            pop = toolbox.select(pop + offspring, MU)

            # Compile statistics about the new population
            record = stats.compile(pop)
            logbook.record(gen=gen, evals=len(invalid_ind), **record)
            # print(logbook.stream)

        # update the Pareto front with all invidiuals of the last generation
        pop_fit = np.array([ind.fitness.values for ind in pop])
        # print('pop_fit:', pop_fit)
        is_on_front = is_pareto_efficient_simple(pop_fit)
        # print('is_on_front:', is_on_front)
        pareto_front = ParetoFront()
        for i_indiv in range(MU):
            if not is_on_front[i_indiv]:
                continue
            # keep if on Pareto front
            pareto_front.update(*evaluate_verbose(pop[i_indiv]))

        return True, pareto_front


if __name__ == '__main__':
    nsgax_parameters = {"nsgax_mu": 10,
                        "nsgax_ngen": 5,
                        "nsgax_x": 2}
    launch_server(BaseOptimServer, NSGAXOptimRequestHandler, **nsgax_parameters)
