#include <third_parties/nlohmann/json.hpp>

#include "model/economie.h"
#include "model/model_seair.h"
#include "optim/pareto.h"

//#define USE_STACK // otherwise we use priority queue
#define NMEASURES   5
#define NOBJECTIVES 4


typedef unsigned short ZoneIdx;
typedef bool ZoneLabel;
//! number of steps of three weeks, can reach ~20
typedef unsigned char StepIndex;
//! index of this StepPolicy among all combinations, can reach 288
typedef unsigned short StepPolicyIndex;
typedef vector<StepPolicyIndex> StepPoliciesHist;
typedef ParetoFront<StepPoliciesHist> PolicyPareto;
// and for zones
typedef pair<StepPolicyIndex, StepPolicyIndex> ZonesStepPolicyIndex;
typedef vector<ZonesStepPolicyIndex> ZonesStepPoliciesHist;
typedef ParetoFront<ZonesStepPoliciesHist> ZonesPolicyPareto;

unsigned int global_counter = 0;

struct WeekNode {
public:
  StepIndex step_idx;
  StepPoliciesHist step_policy_hist;
  vector<SEAIRState> states;
  // objectives
  double activated_measures_ratio; // activated_measures_ratio
  // double deat; // death_ratio
  GDP gdp_accumulated_loss_ratio; // gdp_accumulated_loss_ratio
  double ICU_saturation_ratio; // ICU_saturation_ratio

  WeekNode() : step_idx(0), activated_measures_ratio(0), gdp_accumulated_loss_ratio(0), ICU_saturation_ratio(0) {}

  WeekNode(const WeekNode & B, const StepPolicyIndex & wmi) {
    step_idx = B.step_idx + 1;
    step_policy_hist = B.step_policy_hist;
    step_policy_hist.push_back(wmi);
    states = B.states;
    activated_measures_ratio = B.activated_measures_ratio;
    gdp_accumulated_loss_ratio = B.gdp_accumulated_loss_ratio;
    ICU_saturation_ratio = B.ICU_saturation_ratio;
  }

  inline void print() const {
    printf("Node: {week:%i, gdb:%g, state:%g, ", step_idx, gdp_accumulated_loss_ratio, states.front()[9]);
    printf("measures:"); for (auto v: step_policy_hist) printf("%i,", v);
    //for (auto v: _state) printf("%g, ", v);
    printf("}\n");
  }

  //! for given node, create a child for each of the possible step_policies
  template<class _Queue>
  inline void expand(_Queue & queue, StepPolicyIndex n_possible_step_policies) {
    for (StepPolicyIndex step_policy_idx = 0; step_policy_idx < n_possible_step_policies; ++step_policy_idx) {
      // --> add to Queue
#ifdef USE_STACK
      queue.emplace(WeekNode(*this, step_policy_idx));
#else // no USE_STACK
      //double prio = drand48(); // random
      //double prio = drand48()*step_idx; // random weighted: leafs are more likely to be prioritary
      double prio = global_counter++; // DFS
      //double prio = global_counter--; // BFS - careful can cause memory explosions!
      queue.emplace(make_pair(prio, WeekNode(*this, step_policy_idx)));
#endif
    }
  } // end expand()

}; // end WeekNode /////////////////////////////////////////////////////////////

//! [contact_tracing, forced_telecommuting, school_closures, social_containment_all, total_containment_high_risk]
struct Measures2c : map<StepPolicy, double> {
  Measures2c() {
    // read CSV
    ifstream file(DECOV_PATH "/data/measures2c.csv");
    string line = "";
    getline(file, line); // skip first line (comment)
    getline(file, line); // headers line
    assert(line == "contact_tracing,forced_telecommuting,school_closures,social_containment_all,total_containment_high_risk,c");
    // Iterate through each line and split the content using delimeter
    while (getline(file, line)) {
      vector<string> vec;
      boost::algorithm::split(vec, line, boost::is_any_of(" ,;"));
      StepPolicy pol;
      for (unsigned int i = 0; i < vec.size()-1; ++i) // last value is c, so dont read it
        pol.push_back(atoi(vec[i].c_str())); // measures = int values
      double c = atof(vec.back().c_str()); // c = float value
      //for (MeasureValue v : pol) printf("%i,", v); printf(": %g\n", c);
      (*this)[pol] = c;
    }
    file.close();
  }
};

//! create a vector with all possible combinations of [True, False] ^ nstep_policies
inline void mkstep_policies(const vector<MeasureValue> & possible_measure_values,
                            vector<StepPolicy> & possible_step_policies) {
  // https://stackoverflow.com/questions/48270565/create-all-possible-combinations-of-multiple-vectors
  // determine the max nb of policies
  int nmeasures = possible_measure_values.size();
  int max = 1;
  for (const MeasureValue & v : possible_measure_values) max *= v;
  possible_step_policies.clear();
  possible_step_policies.resize(max, StepPolicy(nmeasures));
  // now build combinations
  for (int pi=0; pi<max; pi++) {
    int temp = pi;
    //for (const MeasureValue & v : possible_measure_values) {
    for (int mi = nmeasures-1; mi >= 0; --mi) {
      possible_step_policies[pi][mi] = temp % possible_measure_values[mi];
      temp /= possible_measure_values[mi];
    }
  }
}

#ifdef USE_STACK
#include <stack>
typedef stack<WeekNode> Queue;
#else // no USE_STACK
#include <queue>
//! the higher the double, the more important, and hence the sooner it gets out
typedef pair<double, WeekNode> QueueElt;
struct Compare{
  bool operator()(const QueueElt & A, const QueueElt & B) { return A.first < B.first; }
};
typedef priority_queue<QueueElt, vector<QueueElt>,  Compare> Queue;
#endif

inline void measures2possible_step_policies(vector<StepPolicy> & possible_step_policies,
                                            MeasureValue contact_tracing_max,
                                            MeasureValue forced_telecommuting_max,
                                            MeasureValue school_closure_max,
                                            MeasureValue social_containment_all_max,
                                            MeasureValue total_containment_high_risk_max) {
  vector<MeasureValue> possible_measure_values;
  possible_measure_values.reserve(NMEASURES);
  possible_measure_values.push_back(1+contact_tracing_max);
  possible_measure_values.push_back(1+forced_telecommuting_max);
  possible_measure_values.push_back(1+school_closure_max);
  possible_measure_values.push_back(1+social_containment_all_max);
  possible_measure_values.push_back(1+total_containment_high_risk_max);
  mkstep_policies(possible_measure_values, possible_step_policies);
}

bool mix_paretos(ZonesPolicyPareto & ans,
                 const double weight1,
                 const PolicyPareto & p1,
                 const double weight2,
                 const PolicyPareto & p2,
                 const unsigned int max_measure_steps,
                 const Economie & eco,
                 const vector<StepPolicy> & possible_step_policies) {
  unsigned int nvar = p1._nvariables, size1 = p1.size(), size2 = p2.size();
  ans._nvariables = nvar;
  ans._objectives.clear();
  ans._eval_policy_reqs.clear();
  if (p1._nvariables != p2._nvariables)  {
    printf("Size mismatch %i != %i\n", p1._nvariables, p2._nvariables);
    return false;
  }
  double sum_weight_inv = 1. /  (weight1 + weight2);
#if 1
  Objectives omix(nvar);
  ZonesStepPoliciesHist pol_hist(max_measure_steps);
  for (unsigned int i1 = 0; i1 < size1; ++i1) {
    const Objectives & o1 = p1._objectives[i1];
    for (unsigned int i2 = 0; i2 < size2; ++i2) {
      const Objectives & o2 = p2._objectives[i2];
      // compute GDP
      double gdp = 0;
      for (unsigned int step = 0; step < max_measure_steps; ++step) {
        // convert StepPolicyIndex -> StepPolicy
        const StepPolicyIndex & idx1 = p1._eval_policy_reqs[i1][step];
        const StepPolicyIndex & idx2 = p2._eval_policy_reqs[i2][step];
        // printf("idx1:%i, idx2:%i\n", idx1, idx2);
        const StepPolicy & pol1 = possible_step_policies[idx1], & pol2 = possible_step_policies[idx2];
        //pol12.insert(pol12.end(), pol2.begin(), pol2.end());
        // printf("pol12:"); for (const auto & v : pol12) printf("%i, ", v); printf("\n");
        pol_hist[step].first = idx1;
        pol_hist[step].second= idx2;
        gdp += eco.evaluate_one_week(pol1, pol2);
      }
      // {acti, deat, gdpl, icus}
      omix[0] = .5 * o1[0] + .5 * o2[0]; // activable_measures_ratio: simple average
      omix[1] = (weight1 * o1[1] + weight2 * o2[1]) * sum_weight_inv;
      omix[2] = gdp;
      omix[3] = (weight1 * o1[3] + weight2 * o2[3]) * sum_weight_inv;
      ans.update(pol_hist, omix);
    } // end for size2
  } // end for size1
#else
  unsigned int size12 = size1 * size2, idx = 0;
  vector<ZonesStepPoliciesHist> pol_hists(size12, ZonesStepPoliciesHist(max_measure_steps));
  vector<Objectives> objectives(size12, Objectives(nvar));
  for (unsigned int i1 = 0; i1 < size1; ++i1) {
    const Objectives & o1 = p1._objectives[i1];
    for (unsigned int i2 = 0; i2 < size2; ++i2) {
      const Objectives & o2 = p2._objectives[i2];
      ZonesStepPoliciesHist & pol_hist = pol_hists[idx];
      // compute GDP
      double gdp = 0;
      for (unsigned int step = 0; step < max_measure_steps; ++step) {
        // convert StepPolicyIndex -> StepPolicy
        const StepPolicyIndex & idx1 = p1._eval_policy_reqs[i1][step];
        const StepPolicyIndex & idx2 = p2._eval_policy_reqs[i2][step];
        // printf("idx1:%i, idx2:%i\n", idx1, idx2);
        const StepPolicy & pol1 = possible_step_policies[idx1], & pol2 = possible_step_policies[idx2];
        //pol12.insert(pol12.end(), pol2.begin(), pol2.end());
        // printf("pol12:"); for (const auto & v : pol12) printf("%i, ", v); printf("\n");
        pol_hist[step].first = idx1;
        pol_hist[step].second= idx2;
        gdp += eco.evaluate_one_week(pol1, pol2);
      }
      // {acti, deat, gdpl, icus}
      Objectives & omix = objectives[idx];
      omix[0] = .5 * o1[0] + .5 * o2[0]; // activable_measures_ratio: simple average
      omix[1] = (weight1 * o1[1] + weight2 * o2[1]) * sum_weight_inv;
      omix[2] = gdp;
      omix[3] = (weight1 * o1[3] + weight2 * o2[3]) * sum_weight_inv;
      idx++;
      // ans.update(pol_hist, omix);
    } // end for size2
  } // end for size1
  vector<unsigned char> is_efficient;
  int npts = is_pareto_efficient_simple(objectives, is_efficient);
  ans._objectives.reserve(npts);
  ans._eval_policy_reqs.reserve(npts);
  for (idx = 0; idx < size12; ++idx) {
    if (!is_efficient[idx])
      continue;
    ans._objectives.push_back(objectives[idx]);
    ans._eval_policy_reqs.push_back(pol_hists[idx]);
  }
#endif
  return true; // ok
}

inline void total_pop_per_label(vector<double> & pops,
                                const unsigned int & nlabels,
                                const vector<ZoneLabel> & labels,
                                const vector<double> & population_size) {
  pops.resize(nlabels, 0.);
  ZoneIdx nzones = labels.size();
  for (ZoneIdx zi = 0; zi < nzones; ++zi)
    pops[labels[zi]] += population_size[zi];
}

template<class Req>
inline StepPolicy req2policy(const ParetoFront<Req> & pareto, int sol, int step,
                             const vector<StepPolicy> & possible_step_policies) {
  return StepPolicy(); // foo return value, should be specialized below
}

template<>
inline StepPolicy req2policy(const PolicyPareto & pareto, int sol, int step,
                             const vector<StepPolicy> & possible_step_policies) {
  StepPolicyIndex idx = pareto._eval_policy_reqs[sol][step];
  return possible_step_policies[idx];
}

template<>
inline StepPolicy req2policy(const ZonesPolicyPareto & pareto, int sol, int step,
                             const vector<StepPolicy> & possible_step_policies) {
  StepPolicyIndex idx1 = pareto._eval_policy_reqs[sol][step].first;
  StepPolicyIndex idx2 = pareto._eval_policy_reqs[sol][step].second;
  StepPolicy pol12 = possible_step_policies[idx1]; // pol1
  const StepPolicy & pol2 = possible_step_policies[idx2];
  pol12.insert(pol12.end(), pol2.begin(), pol2.end());
  return pol12;
}

template<class Req>
inline void pareto_print(const ParetoFront<Req> & pareto,
                         const vector<StepPolicy> & possible_step_policies,
                         const double label_npop,
                         const StepIndex max_measure_steps) {
  printf("(%% acti, %% deat, %% gdpl, %% icus)\n");
  for (unsigned int sol = 0; sol < pareto.size(); ++sol) {
    printf("  - ");
    for (int step = 0; step < max_measure_steps; ++step) {
      const StepPolicy & pol = req2policy(pareto, sol, step, possible_step_policies);
      for (const auto & v : pol) printf("%i", v);
      if (step < max_measure_steps-1) printf(", ");
    }
    printf(": %i\t %.6f\t %.1f\t %.1f\n", // acti, deat, gdpl, icus
           (int) (100. * pareto._objectives[sol][0]),
        100. * pareto._objectives[sol][1] / label_npop,
        100. * pareto._objectives[sol][2],
        100. * pareto._objectives[sol][3]);
  }
}

bool brute_force(ZonesPolicyPareto & pareto,
                 const vector<string> & zones,
                 unsigned int nlabels,
                 const vector<ZoneLabel> & labels,
                 const vector<double> & population_size,
                 const vector<double> & ICU_beds,
                 const vector<SEAIRState> & initial_ratios,
                 SEAIR & seair, //!< compartmental_model_parameters
                 StepIndex max_measure_steps,
                 int step_duration_weeks, // weeks
                 const vector<StepPolicy> & possible_step_policies,
                 int max_days = 2 * 365,
                 bool pareto4pruning = true) {
  unsigned int nzones = zones.size();
  if (nlabels != 2) {
    printf("brute_force(): work with <= 2 labels, got %i\n", nlabels);
    return false;
  }
  assert(nlabels == nzones);
  assert(population_size.size() == nzones);
  assert(ICU_beds.size() == nzones);
  assert(initial_ratios.size() == nzones);

  Economie eco;
  Measures2c measures2c;
  const StepPolicy & max_policy = possible_step_policies.back();
  unsigned int n_possible_step_policies = possible_step_policies.size();
  unsigned int step_duration_days = 7 * step_duration_weeks;
  unsigned int nactivable_measures = 0;
  for (const MeasureValue & v : max_policy)
    nactivable_measures += (v > 0 ? 1 : 0);
  vector<double> label_npops;
  total_pop_per_label(label_npops, nlabels, labels, population_size);

  // solve each label separately (they will be mixed later)
  vector<PolicyPareto> label_paretos(nlabels, PolicyPareto(NOBJECTIVES));
  for (unsigned int label = 0; label < nlabels; ++label) {
    PolicyPareto & label_pareto = label_paretos[label];
    // compute expected number of nodes
    unsigned int exp_nnodes = 1;
    for (StepIndex w = 1; w <= max_measure_steps; ++w)
      exp_nnodes += pow(n_possible_step_policies, w);

    // prepair the root node
    unsigned int nnodes = 1, nstopped_nodes = 0;
    WeekNode root;
    // prepaire y0 state for each zone that has the good label,
    // and store populations and ICU beds
    double label_s0 = 0;
    vector<double> label_population_size, label_ICU_beds;
    for (ZoneIdx zidx = 0; zidx < nzones; ++zidx) {
      if (labels[zidx] != label)
        continue;
      //root._states.push_back(SEAIR_ratios2y0(population_size[zidx], initial_ratios[zidx]));
      root.states.push_back(initial_ratios[zidx]);
      label_population_size.push_back(population_size[zidx]);
      label_ICU_beds.push_back(1. * ICU_beds[zidx] / population_size[zidx]);
      label_s0 += initial_ratios[zidx][0];
    } // end for zidx
    ZoneIdx label_nzones = label_population_size.size();
    seair.compute_virulence_beta(label_s0 / label_nzones);
    Queue queue;
    root.expand(queue, n_possible_step_policies);

    while (!queue.empty()) {
      nnodes++;
#ifdef USE_STACK
      WeekNode node = queue.top(); // copy
#else // no USE_STACK
      WeekNode node = queue.top().second; // copy
#endif
      queue.pop(); // pop front
      StepPolicyIndex last_pi = node.step_policy_hist.back();
      // step_policy = [contact_tracing, forced_telecommuting, school_closure, social_containment_all, total_containment_high_risk]
      const StepPolicy & label_policy = possible_step_policies[last_pi];
      // update acti (activated_measures_ratio)
      for (unsigned int i = 0; i < NMEASURES; ++i)
        if (max_policy[i])
          node.activated_measures_ratio += label_policy[i] / max_policy[i];
      // deat will be computed after integrate()
      // update gdpl (gdp_accumulated_loss_ratio)
      node.gdp_accumulated_loss_ratio += step_duration_weeks *
          eco.evaluate_one_week_one_label(label, label_policy);
      // compute c according to step_policies, cf. step_policies2c
      double c = measures2c[label_policy];

      // integrate SEAIR during step_duration_days
      for (ZoneIdx zidx = 0; zidx < label_nzones; ++zidx) {
        seair.integrate(node.states[zidx], c,
                        label_ICU_beds[zidx], step_duration_days); // a step with this policy
        // update icus, ICU patients = 30% * I2
        double zone_pop = population_size[zidx];
        double icu_patients = .3 * node.states[zidx][7] * zone_pop;
        double beds = label_ICU_beds[zidx] * zone_pop; // ratios -> beds
        if (icu_patients > beds)
          node.ICU_saturation_ratio += 7 * (icu_patients - beds)
              / (beds * max_days * label_nzones); // weight of one week
      }
      bool is_leaf = node.step_idx == max_measure_steps;
      // printf("node.step_idx:%i\n", node.step_idx);
      if (!is_leaf) {
        node.expand(queue, n_possible_step_policies);
        continue;
      }
      // we are on a leaf from now on
      // update activated_measures_ratio: convert sum -> ratio
      node.activated_measures_ratio /= max_measure_steps * nactivable_measures;
      // if leaf: integrate a week, then till max_days with c=0
      Objectives objectives(NOBJECTIVES); // {acti, deat, gdpl, icus}
      int remaining_days = max_days - step_duration_days * node.step_idx;
      double ndeaths = 0; // CAREFUL it is the number of deaths and not the ratio
      bool stopped = false;
      for (ZoneIdx zidx = 0; zidx < label_nzones; ++zidx) {
        double zone_pop = population_size[zidx];
        // lift all policies --> c = 0 for two years minus last week and the previous
        double beds = label_ICU_beds[zidx] * zone_pop; // ratios -> beds
        seair.integrate(node.states[zidx], 0, beds, remaining_days, true); // store_icu_values
        ndeaths += (node.states[zidx][9] - initial_ratios[zidx][9]) * zone_pop;
        // update mean_icu_patients
        double mean_icu_patients = seair.mean_icu_patients() * zone_pop;
        // printf("mean_icu_patients:%g, beds:%g\n", mean_icu_patients, beds);
        if (mean_icu_patients > beds) // weight of remaining days
          node.ICU_saturation_ratio += remaining_days * (mean_icu_patients - beds)
              / (beds * max_days * label_nzones);

        objectives = {node.activated_measures_ratio, ndeaths,
                      node.gdp_accumulated_loss_ratio, node.ICU_saturation_ratio};
        if (label_pareto.dominates(objectives)) {
          stopped = true;
          break; // get out of zidx loop
        }
      } // end for zidx
      if (stopped) {
        ++nstopped_nodes;
        continue; // go to next queue item
      }
      label_pareto.update(node.step_policy_hist, objectives); // update Front
    } // end while queue
    printf("expected nnodes:%i, visited nodes:%i (%.2f%%), stopped nodes:%i (%.2f%%)\n",
           exp_nnodes, nnodes, 100. * nnodes / exp_nnodes, nstopped_nodes, 100. * nstopped_nodes / nnodes);
    printf("Label %i Pareto front size:%i\n", label, label_pareto.size());
    // label_pareto.sort_by_objectives0();
    // pareto_print(label_pareto, possible_step_policies, label_npop, max_measure_steps);
  } // end for label
  mix_paretos(pareto, label_npops[0], label_paretos[0], label_npops[1], label_paretos[1],
      max_measure_steps, eco, possible_step_policies);
  printf("Final Pareto front size:%i\n", pareto.size());
  pareto.sort_by_objectives0();
  pareto_print(pareto, possible_step_policies, label_npops[0] + label_npops[1], max_measure_steps);
  return true; // ok
}

//! get a parameter from JSON, either from the "common" or the "macro" dictionnary
static inline const nlohmann::json & lookup(const nlohmann::json & params, const string & key) {
  if (params["macro_Alizon20"].contains(key))
    return params["macro_Alizon20"][key];
  if (params["common"].contains(key))
    return params["common"][key];
  printf("Could not find param with key '%s'\n", key.c_str());
  exit(-1);
}

template<class _T>
inline vector<_T> param2vec(const nlohmann::json & json_in) {
  vector<_T> out;
  out.reserve(json_in.size());
  for (double v : json_in)
    out.push_back((_T) v);
  return out;
}

nlohmann::json brute_force_json(const nlohmann::json & json_in) {
  const nlohmann::json & params = json_in["compartmental_model_parameters"];
  // prepare brute_force() arguments
  ZonesPolicyPareto pareto(NOBJECTIVES);
  unsigned int nlabels = json_in["zone_label_names"].size();
  vector<ZoneLabel> labels = param2vec<ZoneLabel>(json_in["zone_labels"]);
  vector<double> population_size = param2vec<double>(json_in["population_size"]);
  vector<double> ICU_beds = param2vec<double>(json_in["ICU_beds"]); // beds
  StepIndex max_measure_steps = json_in["max_measure_steps"]; // #steps
  int measure_step_duration = json_in["measure_step_duration"]; // weeks
  int max_days = lookup(params, "max_days");
  bool pareto4pruning = true;
  // zones
  vector<string> zones;
  for (const auto & z : json_in["zones"])
    zones.push_back(to_string(z));
  // initial_ratios
  vector<SEAIRState> initial_ratios;
  for (const string & zone : json_in["zones"])
    initial_ratios.push_back( json_in["initial_ratios_Alizon20"][zone] );
  // seair
  SEAIR seair;
  seair.epsilon = lookup(params, "epsilon");
  seair.sigma= lookup(params, "sigma");
  seair.CFR = lookup(params, "CFR");
  seair.R0 = lookup(params, "R0");
  seair.gamm1 = lookup(params, "gamm1");
  seair.gamm2 = lookup(params, "gamm2");
  seair.mild = lookup(params, "mild");
  seair.nu = lookup(params, "nu");
  seair.bb = lookup(params, "bb");
  seair.mort_sup = lookup(params, "mort_sup");
  seair.vir_sup = lookup(params, "vir_sup");
  // possible_step_policies: call measures2possible_measure_values()
  vector<StepPolicy> possible_step_policies;
  measures2possible_step_policies(possible_step_policies,
                                  json_in["max_measure_values"]["contact_tracing"],
      json_in["max_measure_values"]["forced_telecommuting"],
      json_in["max_measure_values"]["school_closures"],
      json_in["max_measure_values"]["social_containment_all"],
      json_in["max_measure_values"]["total_containment_high_risk"]);

  // call brute_force()
  bool ok = brute_force(pareto, zones, nlabels, labels,
                        population_size, ICU_beds, initial_ratios,
                        seair, max_measure_steps, measure_step_duration,
                        possible_step_policies, max_days, pareto4pruning);
  if (!ok)
    return -1;

  // build json_out
  nlohmann::json json_out;
  for (unsigned int i = 0; i < possible_step_policies.size(); ++i)
    json_out["possible_step_policies"][to_string(i)] = possible_step_policies[i];
  json_out["policies"] = nlohmann::json::array();
  for (unsigned int i = 0; i < pareto.size(); ++i) {
    StepPoliciesHist out_wp;
    vector<StepIndex> out_weeks;
    json_out["policies"].push_back({ {"measure_weeks", out_weeks}, {"step_policy_index", out_wp} });
  }
  return json_out;
}
