# !/usr/bin/env python3
import math
import operator
import random
from copy import deepcopy
from random import randint

import numpy as np
from deap import base
from deap import creator
from deap import tools

from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.branch_and_bound.branch_and_bound_utils import color_strictness_filter
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront, is_pareto_efficient_simple

random.seed(42)


class PSOOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):
        """
        Inherits BaseOptimRequestHandler.optimize(), see there for doc.
        """
        mu = 10
        gen = 3
        if hasattr(self.server, "pso_mu"):
            mu = self.server.pso_mu
        if hasattr(self.server, "pso_gen"):
            gen = self.server.pso_gen
        print('PSOOptimRequestHandler(): mu:{}, gen:{}'.format(mu, gen))
        ncolors = len(optimize_req['zone_label_names'])
        # nzones = len(optimize_req['zones'])
        measures = len(activable_measures_dict)
        ncolors_nmeasures = ncolors * measures
        step_duration = optimize_req['measure_step_duration']
        time = optimize_req['max_measure_steps']

        objectives = Objectives(optimize_req)

        def generate(part, time, ncolors, activable_measures_dict, smin, smax):
            """ instead of having a boolean vector
            values for each policy are turned into float in the [0; 1] range
            In evaluate(), the function will take (particle > 0.5) as the
            policy boolean vector
            """
            ind = []
            speed = []
            for i in range(time):
                if i > 0 and randint(0, 100) > 30:
                    ind.append([0] * ncolors * len(activable_measures_dict))
                    speed.append([0] * ncolors * len(activable_measures_dict))
                    continue
                arr = []
                speed_arr = []
                for m, m_max in activable_measures_dict.items():
                    loop_arr = []
                    current_value = m_max
                    for _ in range(ncolors):
                        current_value = randint(0, current_value)
                        loop_arr.append(current_value)
                    arr.extend(loop_arr)
                    speed_arr.extend([random.uniform(smin, smax) for _ in range(ncolors)])
                ind.append(arr)
                speed.append(speed_arr)
            # initialize ind container
            part = part(np.asarray(ind))
            part.speed = np.asarray(speed)
            part.smin = smin
            part.smax = smax
            return part

        def updateParticle(part, best, phi1, phi2, activable_measures_dict, mut_prob=0.1):
            """ Particule speed corresponds to speed of evolution.
            A high speed will increase the probability of a measure to mutate.

            """

            activated_weeks = np.sum(part, axis=1)
            idx_activated = np.where(activated_weeks > 0)[0]

            for i in idx_activated:
                # choose update sign
                u1 = (random.uniform(0, phi1) for _ in range(len(part[i])))
                u2 = (random.uniform(0, phi2) for _ in range(len(part[i])))
                v_u1 = map(operator.mul, u1, map(operator.sub, part.best[i], part[i]))
                v_u2 = map(operator.mul, u2, map(operator.sub, best[i], part[i]))
                part.speed[i] = list(map(operator.add, part.speed[i], map(operator.add, v_u1, v_u2)))
                for j, speed in enumerate(part.speed[i]):
                    if abs(speed) < part.smin:
                        part.speed[i][j] = math.copysign(part.smin, speed)
                    elif abs(speed) > part.smax:
                        part.speed[i][j] = math.copysign(part.smax, speed)
                # clip value of particule
                has_mutation = np.clip(list(map(operator.add, part[i], part.speed[i])), 0, 1) < mut_prob
                for k in range(len(has_mutation)):
                    if k % ncolors > 0:
                        has_mutation[k] = has_mutation[
                            k - 1]  # Matching the mutation of all colors for a given measure.
                new_values = np.empty(0)
                for j, (m, m_max) in enumerate(activable_measures_dict.items()):
                    loop_arr = []
                    current_value = m_max
                    for _ in range(ncolors):
                        current_value = randint(0, current_value)
                        loop_arr.append(current_value)
                    new_values = np.hstack([new_values, np.array(loop_arr)])
                part[i][has_mutation] = new_values[has_mutation].copy()
                # part[:] = list(map(operator.add, part, part.speed))

        def individual2policy_req(individual):
            """ transform individual into a json complient policy request

            Parameters
            ==========
            individual: np.ndarray
                the currrent individual, data are stored in a time x (ncolors*nmeasures) matrix

            Return
            ======
            policy: dict
                the dictionnary sent as a json for evaluation
            """
            policy = deepcopy(policy_common_values)
            measure_weeks = []
            # print('individidual:', individual, 'nmeasures:', nmeasures, 'ncolors_nmeasures:', ncolors_nmeasures)
            slices = [slice(i, i + ncolors) for i in range(0, ncolors_nmeasures, ncolors)]
            for i in range(len(individual)):
                # if all measures are set False it means that this week time is not activated
                if i == 0:  # measure weeks necessary starts at 0
                    measure_weeks.append(i * step_duration)
                    # slice measures by zones
                    for j, m in enumerate(activable_measures_dict):
                        begin = (j * ncolors)
                        # need to be cast for json
                        arr = [int(a) for a in individual[i][begin: begin + ncolors].copy()]
                        policy[m].append({"activated": arr})
                elif sum(individual[i]) > 0:
                    # print('w:', wi)
                    measure_weeks.append(i * step_duration)
                    # slice measures by zones
                    for j, m in enumerate(activable_measures_dict):
                        begin = (j * ncolors)
                        # need to be cast for json
                        arr = [int(a) for a in individual[i][begin: begin + ncolors].copy()]
                        # ensure bounds are respected
                        m_max = activable_measures_dict[m]
                        arr_clipped = [max(0, min(m_max, int(a))) for a in arr]
                        policy[m].append({"activated": arr_clipped})
                        # lift all measures at the end
            measure_weeks.append(optimize_req['max_measure_steps'] * step_duration)
            for m in activable_measures_dict:
                policy[m].append({'activated': [0] * ncolors})
            policy['measure_weeks'] = measure_weeks

            return policy

        # define evaluation
        def evaluate_verbose(individual):
            # get eval_policy_req
            eval_policy_req = individual2policy_req(individual)
            # do POST evaluation
            assert (color_strictness_filter(eval_policy_req))  # Guard to check that green < red.
            ok, eval_policy_res = self.call_eval_policy(eval_policy_req, activable_measures_dict)
            # now check if the policy has good objectives
            objs = objectives.compute(ok, eval_policy_req, eval_policy_res)
            return eval_policy_req, eval_policy_res, objs

        def evaluate(individual):
            _, _, objs = evaluate_verbose(individual)
            return objs

        # Register fitness / individual / population
        if not hasattr(creator, 'FitnessMinPSO'):
            creator.create("FitnessMinPSO", base.Fitness, weights=(-1.0, -1.0))  # two objectives to minimize
            # if not hasattr(creator, 'Particle'):
            creator.create("Particle", list, fitness=creator.FitnessMinPSO, speed=list,
                           smin=None, smax=None, best=None)  # the list size will be flatten(time x region x policy)

        # register genetic algorithm function
        toolbox = base.Toolbox()
        toolbox.register("particle", generate, creator.Particle,
                         time=time, ncolors=ncolors, activable_measures_dict=activable_measures_dict,
                         smin=-0.75, smax=0.75)
        toolbox.register("population", tools.initRepeat, list, toolbox.particle)
        toolbox.register("update", updateParticle, phi1=0.25, phi2=0.25,
                         activable_measures_dict=activable_measures_dict)
        toolbox.register("evaluate", evaluate)

        pop = toolbox.population(n=mu)
        best = None
        for g in range(gen):
            for part in pop:
                part.fitness.values = toolbox.evaluate(part)
                if not part.best or part.best.fitness > part.fitness:
                    part.best = creator.Particle(part)
                    part.best.fitness.values = part.fitness.values
                if not best or best.fitness > part.fitness:
                    best = creator.Particle(part)
                    best.fitness.values = part.fitness.values
            for part in pop:
                toolbox.update(part, best)

        # update the Pareto front with all invidiuals of the last generation
        pop_fit = np.array([ind.fitness.values for ind in pop])
        # print('pop_fit:', pop_fit)
        is_on_front = is_pareto_efficient_simple(pop_fit)
        #  print('is_on_front:', is_on_front)
        pareto_front = ParetoFront()
        for i_indiv in range(mu):
            if not is_on_front[i_indiv]:
                continue
            pareto_front.update(*evaluate_verbose(pop[i_indiv]))
        return True, pareto_front


if __name__ == '__main__':
    pso_parameters = {"pso_mu": 10,
                      "pso_ngen": 3}
    launch_server(BaseOptimServer, PSOOptimRequestHandler, **pso_parameters)
