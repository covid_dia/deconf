import random
from copy import deepcopy
from random import randint

import numpy as np

# TODO to remove in prod
random.seed(42)


def init_ind(icls, time, ncolors, activable_measures_dict):
    """ Randomly initialize an individual
    
    Parameters:
    ===========
    icls: toolbox.Individual
        the individual container 
    time: float
        the number of weeks
    ncolors: int
        the number of colors corresponding to different policies
    activable_measures_dict: dict
        dictionary of measures and their max values
        
    Return:
    =======
    ind: toolbox.Individual
        an individual
    """
    ind = []
    for i in range(time):
        if i > 0 and randint(0, 100) > 30:
            ind.append([0] * ncolors * len(activable_measures_dict))
            continue
        arr = []
        for m, m_max in activable_measures_dict.items():
            loop_arr = []
            current_value = m_max
            for _ in range(ncolors):
                current_value = randint(0, current_value)
                loop_arr.append(current_value)
            arr.extend(loop_arr)
        ind.append(arr)
        #ar = list((np.random.random((npolicies * ncolors)) > 0.5).astype(bool))
        #ind.append(ar)        
    # initialize ind container
    ind = icls(np.asarray(ind))
    return ind

def mutation(individual, activable_measures_dict, ncolors, mut_prob=0.1):
    """ clone the individual and apply mutation to its policies
    time of intervention are not mutated yet
    
    Parameters:
    ===========
    individual: toolbox.Individual
    activable_measures_dict: dict
        measure that can be activated and their max values
    mut_prob: float
        the probability of activated week i policies to mutate in a region

    Return:
    =======
    mutant: toolbox.Individual
        a copy of the individual with mutation
    """
    mutant = deepcopy(individual)
    # get activated week indexes 
    activated_weeks = np.sum(individual, axis=1)
    idx_activated = np.where(activated_weeks > 0)[0]
    
    for i in idx_activated:
        new_values = np.empty(0)
        has_mutation = np.random.random(len(mutant[i])) < mut_prob
        for k in range(len(has_mutation)):
            if k % ncolors > 0:
                has_mutation[k] = has_mutation[k-1] # Matching the mutation of all colors for a given measure.
        for j, (m, m_max) in enumerate(activable_measures_dict.items()):
            # WARNING randint from python random return [0, m_max] , randint from numpy return [0, m_max-1] => add +1 in m_max of numpy
            loop_arr = []
            current_value = m_max
            for _ in range(ncolors):
                current_value = randint(0, current_value)
                loop_arr.append(current_value)
            new_values = np.hstack([new_values, np.array(loop_arr)])
        
        #cur_values = mutant[i].copy()
        #cur_values[has_mutation] = new_values[has_mutation]

        mutant[i, has_mutation] = new_values[has_mutation].copy()

    # rand on the mutant shape and select indexes corresponding
    # to activated weeks and with a proba lower than mut_prob
    #mat_prob = np.random.random(mutant.shape)

    #i_act, j_act = np.where(mat_prob < mut_prob)
    #kept = np.where(np.isin(i_act, idx_activated))[0]
    #i_act, j_act = i_act[kept], j_act[kept]
    
    # randomly choose new values for 
    # else change
    #mutant[i_act, j_act] = ~mutant[i_act, j_act]    
    
    del mutant.fitness.values
    return (mutant,)

def mate(ind1, ind2, cx_time, ncolors, nmeasures):
    """ mate two individual by switching time of intervention
    
    Parameters
    ==========
    ind1: individual
    ind2: individual
    cx_time: float
        crossover probability on time vector
        The crossover is applied at a given time and exchange
        the measures values of all zones at this given time
    ncolors: int
        the number of colors corresponding to different policies
    nmeasures: int
        number of measures
    
    Return
    ======
    new_ind1: individual
    new_ind2: individual
    """
    ncolors_nmeasures = ncolors * nmeasures
    new_ind1, new_ind2 = deepcopy(ind1), deepcopy(ind2)
    slices = [slice(i, i+ncolors) for i in range(0, ncolors_nmeasures, ncolors)]
    for i in range(len(ind1)):
        # time i
        for cur_slice in slices:
            vec_ind1 = new_ind1[i, cur_slice] # view
            vec_ind2 = new_ind2[i, cur_slice] # view

            if np.random.random() < cx_time:
                values_ind1 = vec_ind1.copy() 
                values_ind2 = vec_ind2.copy() 
                vec_ind1[:] = values_ind2
                vec_ind2[:] = values_ind1
    # reset fitness values
    del new_ind1.fitness.values
    del new_ind2.fitness.values
    return new_ind1, new_ind2   
