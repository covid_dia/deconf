#include <iostream> // cin
#include "optim/brute_force_optim.h"
using namespace std;
using namespace nlohmann;

int main() {
  // read JSON from stdin
  printf("Reading optimize_req from stdin...\n");
  json json_in;
  cin >> json_in;
  json json_out = brute_force_json(json_in);
  cout << json_out << endl;
  return 0;
}
