# !/usr/bin/env python3
import glob
from copy import deepcopy

import numpy
from stable_baselines import PPO2
from stable_baselines.common.vec_env import DummyVecEnv

from src.model.macro_rl_env import MacroRLEnv
from src.model.policy_utils import all_measures
from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.macro_rl_train import req2key, models_folder
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront, is_pareto_efficient_simple


class RLOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req: dict, policy_common_values: dict, activable_measures_dict: dict):
        max_steps = optimize_req['max_measure_steps']
        step_duration = optimize_req['measure_step_duration']
        nlabels = len(optimize_req['zone_label_names'])
        nmeasures = len(all_measures().keys())
        # sanity checks
        if nlabels != 2:
            print('RLOptimRequestHandler: no trained model for {} labels'.format(nlabels))
            return False, None

        # prepare our pareto front
        print('optimize()', optimize_req['max_measure_values'])
        objectives = Objectives(optimize_req)
        pareto_front = ParetoFront()

        # prepair a skeleton for eval_policy_req
        eval_policy_req = deepcopy(policy_common_values)
        eval_policy_req['measure_weeks'] = [step_duration * w for w in
                                            range(max_steps + 1)]  # +1 for lifting at the end
        for m in activable_measures_dict:
            eval_policy_req[m] = [{'activated': None}] * max_steps
            # lift all measures at the end
            eval_policy_req[m].append({'activated': [0] * nlabels})

        # get the list of models we want to infer
        req_key = req2key(optimize_req)
        model_objectives = []
        model_eval_policy_reqs = []
        model_filenames = glob.glob(models_folder + '/' + req_key + '*')
        if len(model_filenames) == 0:
            print('RLOptimRequestHandler: no trained model for request', req_key)
            return False, None

        # load model and env
        # use the model in inference
        env = MacroRLEnv(optimize_req=optimize_req, per_zone_observation=True)
        # print('obs shape',env._observation_shape)
        vec_env = DummyVecEnv([lambda: env])
        model = PPO2.load(model_filenames[0])  # , env=vec_env)

        for idx, model_filename in enumerate(model_filenames):
            print('model_filename:', model_filename)
            model_eval_policy_req = deepcopy(eval_policy_req)
            # https://stable-baselines.readthedocs.io/en/master/guide/examples.html
            # load function re-creates model from scratch on each call, which can be slow.
            # If you need to e.g. evaluate same model with multiple different sets of parameters,
            # consider using load_parameters instead.
            if idx > 0:  # load_parameters() already done by PPO2.load() before loop
                model.load_parameters(load_path_or_dict=model_filename)

            obs = vec_env.reset()
            nsteps: int = 0
            while True:
                action_vec, _ = model.predict(obs)
                obs, rewards, done, info_vec = vec_env.step(action_vec)
                nsteps += 1
                info = info_vec[0]  # used this trick to deal with vectorized environment and lstm policies
                if done:
                    assert nsteps == max_steps + 1
                    # current_step is reset to 0 won't work because auto reset() in step() when episode is done
                    # assert env.current_step == nsteps + 1
                    break
                # store in eval_policy_req
                for m_idx, m in enumerate(all_measures().keys()):
                    if m not in activable_measures_dict:
                        continue
                    action = action_vec[0]  # used this trick to deal with vectorized environment and lstm policies
                    # cast action to int(), otherwise it is a int64 that makes JSON crash
                    m_values = [int(action[label * nmeasures + m_idx]) for label in range(nlabels)]
                    assert min(m_values) >= 0 and max(m_values) <= activable_measures_dict[m]
                    model_eval_policy_req[m][env.current_step - 1]['activated'] = m_values  # step starts at 1
            model_objectives.append([info[obj] for obj in optimize_req['objectives']])
            model_eval_policy_reqs.append(model_eval_policy_req)

        # check which inferences are on Pareto front
        all_objs = numpy.array(model_objectives)
        is_on_front = is_pareto_efficient_simple(all_objs)
        zips = zip(model_filenames, model_eval_policy_reqs, model_objectives)
        for idx, (model_filename, model_eval_policy_req, objs) in enumerate(zips):
            if not is_on_front[idx]:
                # print(model_filename, 'is no Pareto...')
                continue
            # print(model_filename, 'returned a Pareto point!')
            # now evaluate it properly with call_eval_policy()
            ok, eval_policy_res = self.call_eval_policy(model_eval_policy_req, activable_measures_dict)
            # now check if the policy has good objectives
            objs = objectives.compute(ok, model_eval_policy_req, eval_policy_res)
            pareto_front.update(model_eval_policy_req, eval_policy_res, new_metrics=objs)
        if sum(is_on_front) != len(pareto_front):
            print('Warning, expected {} sols, got {}.'.format(sum(is_on_front), len(pareto_front)))
        return True, pareto_front


if __name__ == '__main__':
    launch_server(BaseOptimServer, RLOptimRequestHandler)
