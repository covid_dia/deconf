from copy import deepcopy

import numpy as np
import pdb
import time
from scipy.integrate import odeint

from src.model.economie import Economie
from src.model.model_seair_alizon import covid_ode_alizon as covid_ode


def clip(p): return np.maximum(0,np.minimum(p,1))


#a bit ugly: to get the coefficients in economy #todo : its not linear !
def get_economy_impact():
    #order : forced_telecommuting, social_containment_all, school_closures, total_containment_high_risk
    econ = Economie()
    zones = ["FR-ARA", "FR-BFC", "FR-BRE", "FR-CVL", "FR-COR", "FR-GES", "FR-HDF", "FR-IDF", "FR-NAQ", "FR-NOR", "FR-OCC", "FR-PDL", "FR-PAC"]
    nregions = len(zones)
    policy = {"forced_telecommuting":[False]*nregions,
              "social_containment_all":[False]*nregions,
              "school_closures": [False]*nregions,
              "total_containment_high_risk":[False]*nregions}
    policy['zones'] = zones
    impact = np.zeros((len(zones),len(policy)-1))
    for j in range(len(zones)):
        for i,p in enumerate(["forced_telecommuting",
                              "social_containment_all",
                              "school_closures",
                              "total_containment_high_risk"]):
            policy[p][j]  = True
            impact[j][i] = econ.evaluate_one(policy)
            policy[p][j] = False
            
    return impact

def mu_func(y, x0, x1):
    res = np.zeros(y.shape)
    filt = np.logical_and(y < x1 + 30 , y > x1 - 30)
    res[filt] = x0/ (1 + np.exp(-5 * (y[filt] - x1)))
    res[y > x1 + 30] = x0
    return res
 
def mu_prime(y, x0, x1):
    res = np.zeros(y.shape)
    filt = np.logical_and(y < x1 + 30 , y > x1 - 30)
    res[filt] = x0 * 5 * np.exp(-5 * (y[filt] - x1))/ (1 + np.exp(-5 * (y[filt] - x1)))**2
    return res

#computes the ode function whose solution is
#A: the gradient with respect to the initial condition
#X: the gradient with respect to policies
#Note : X is only computed at the current week (its other components are zero)        
def diff_ode(AX,t,params):
    ny = params['ny']
    nsteps = params['nsteps']/7 #divide by 7 since originaly designed per week
    
    A = AX[:ny*ny].reshape(ny,ny)
    X = AX[ny*ny:].reshape(ny,-1)

    #interpolate
    nt = A.shape[0]
    index_lo = int(np.floor((t-params['beg'])*nsteps))
    index_hi = index_lo+1   
    if index_lo < 0:
        index_lo = index_hi = 0
    if index_hi >= params['J'].shape[0]:
        index_lo = index_hi = params['J'].shape[0]-1
    pond = (t-params['beg'])/(params['end']-params['beg'])
    if params['beg'] < t < params['end']:
        assert 0<=pond<=1, pdb.set_trace()
    
    assert index_lo >= 0 and index_hi < params['J'].shape[0], pdb.set_trace()
    #print(params['J'].shape[0], params['beg'], params['end'], index_lo, index_hi, t, pond)
    
    J = pond * params['J'][index_lo,:,:] + (1-pond) * params['J'][index_hi,:,:]
    #print(J, params['J'][index_lo,:,:])
    diffC = pond * params['diffC'][index_lo,:,:] + (1-pond) * params['diffC'][index_hi,:,:]
           
    diffA = np.dot(J,A)
    diffX = np.dot(J, X) + diffC

    return np.concatenate((diffA.flatten(),diffX.flatten()))

        
class SEAIRGradientOptimizer:
    def __init__(self, parameters):
        self.parameters = deepcopy(parameters)
        
        #order : forced_telecommuting, social_containment_all, school_closures, total_containment_high_risk
       
        self.economy_impact = get_economy_impact()
        
        self.parameters['nsteps'] = 7*24*10#discretization of function in a week
        self.parameters['niter'] = 100
        self.parameters['verbose']=False
        self.parameters['regularization']='L1'
        self.parameters['reg_lambda'] = 1e-5
        self.parameters['initial_lrs'] = [10**(-x) for x in range(2,8)]
        
        self.parameters['atol'] = 1e-7
        self.parameters['rtol'] = 1e-7
        self.parameters['mxstep']=50000

        self.N0 = self.parameters["N0"]
        self.I0 = self.parameters["I0"]
        self.S0 = self.N0 - self.I0
        self.parameters["y0"][0] = self.S0
        self.parameters["y0"][3] *= self.I0  # I1 = var1*I0
        self.parameters["y0"][7] *= self.I0  # I2 = var2*I0

        # calculate virulence from case fatality ratio
        self.virulence = self.parameters["CFR"] * (self.parameters["gamm2"]) / (1 - self.parameters["CFR"])

          # calculer Beta depuis R0
        self.Beta = self.parameters["R0"] * self.parameters["gamm1"] * self.parameters["sigma"] * (
                self.virulence + self.parameters["gamm2"])
        denom = self.virulence * self.parameters["gamm1"] + self.parameters["gamm1"] * self.parameters["gamm2"]
        denom += self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["sigma"]
        denom += self.virulence * self.parameters["mild"] * self.parameters["sigma"]
        denom -= self.parameters["bb"] * self.parameters["gamm1"] * self.parameters["mild"] * self.parameters["sigma"]
        denom += self.parameters["gamm2"] * self.parameters["mild"] * self.parameters["sigma"]
        self.Beta /= (self.S0 * denom)
        self.parameters["beta"] = self.Beta
        self.parameters["virulence"] = self.virulence
        
        self.parameters['diff_mu'] = True
        self.parameters['weight_econ'] = 1
        
    @staticmethod
    def sanitary_impact(p):
        if len(p.shape)==1:
            return 0.3 * p[0] + 0.2 * p[1] + 0.1 * (1 - p[1]) * p[3] + 0.3 * p[2]
        else:
            return 0.3 * p[:, 0] + 0.2 * p[:, 1] + 0.1 * (1 - p[:, 1]) * p[:, 3] + 0.3 * p[:, 2]
    
    @staticmethod
    def diff_sanitary_impact(p):
        if len(p.shape)==1:
            return 0.3, 0.2 - 0.1 * p[3], 0.3, 0.1 * (1 - p[1])
        else:
            return np.hstack( (0.3, 0.2 - 0.1*p[:,3], 0.3, 0.1*(1-p[:,1])))
                    
    def compute_J(self,model_result, c):
        params = self.parameters
        nt,ny = model_result.shape
        y = model_result
        
        mu = mu_func(y[:,7],params["mort_sup"],params["i_sat"])
        mu2 = mu_func(y[:,7],params["vir_sup"],params["i_sat"])
        alpha = params["virulence"] + mu2
        mu1p = mu_prime(y[:,7],params["mort_sup"],params["i_sat"])
        mu2p = mu_prime(y[:,7],params["vir_sup"],params["i_sat"])
        epsilon = params["epsilon"]
        sigma = params["sigma"]
        gamma1 = params["gamm1"]
        gamma2 =params["gamm2"]
        nu = params["nu"]
        m = params["mild"]
        b = params["bb"]
        alpha = params["virulence"] + mu2
        beta = params['beta']
        lambd = (1 - c) * beta * (y[:,2] + y[:,6] + y[:,3] + b * y[:,7])
        N = np.sum(y, axis=1)
        
        J = np.zeros((nt,ny,ny))

        
        #df_0
        J[:,0,0] = -lambd-mu
        J[:,0,2] = -(1-c)*beta*y[:,0]
        J[:,0,3] = -(1-c)*beta*y[:,0]
        J[:,0,6] = -(1-c)*beta*y[:,0]
        J[:,0,7] = -b*(1-c)*beta*y[:,0] - mu1p * y[:,0]
        #df_1
        J[:,1,0] = m*lambd
        J[:,1,1] = -epsilon-mu
        J[:,1,2] = m*(1-c)*beta*y[:,0]
        J[:,1,3] = m*(1-c)*beta*y[:,0]
        J[:,1,6] = m*(1-c)*beta*y[:,0]
        J[:,1,7] = b*m*(1-c)*beta*y[:,0] - mu1p * y[:,1]
        #df_2
        J[:,2,1] = epsilon
        J[:,2,2] = -sigma - mu
        J[:,2,6] = -mu1p*y[:,2]
        #df_3
        J[:,3,2] = sigma 
        J[:,3,3] = -(mu+gamma1)
        J[:,3,7] = -mu1p*y[:,3]
        #df_4
        J[:,4,3] = gamma1
        J[:,4,4] = -mu
        J[:,4,7] = -mu1p*y[:,4]
        #df_5
        J[:,5,0] = (1-m)*lambd
        J[:,5,2] = (1-c)*beta*(1-m)*y[:,0]
        J[:,5,3] = (1-c)*beta*(1-m)*y[:,0]
        J[:,5,5] = -(epsilon+mu)
        J[:,5,6] = (1-c)*beta*(1-m)*y[:,0]
        J[:,5,7] = (1-c)*b*beta*(1-m)*y[:,0]-mu1p*y[:,5]
        #df_6
        J[:,6,5] = epsilon
        J[:,6,6] = -(sigma+mu)
        J[:,6,7] = -mu1p*y[:,6]
        #df_7
        J[:,7,6] = sigma
        J[:,7,7] = -(gamma2 + alpha + mu) - y[:,7]*(mu2p + mu1p)
        J[:,8,8] = 0
        #df_8
        J[:,8,7] = gamma2 - mu1p*y[:,8]
        J[:,8,8] = mu
        #df_9
        J[:,9,:] = np.tile(mu.reshape(-1,1),(1,10))
        J[:,9,7] = alpha + mu2p * y[:,7] + mu1p * (N-y[:,7]-y[:,9])
        J[:,9,8] = mu 
        J[:,9,9] = 0
        
        return J

    def compute_diffC(self,model_result, policy):
        params = self.parameters
        y = model_result
        b = params['bb']
        m = params['mild']
        beta = params['beta']
        #result should be of size (nsteps, ny, npolicies = 4*nregions), but since c is independant of the region,
        #we optimize it to (nsteps, ny, 4)
        result = np.zeros((y.shape[0], y.shape[1], policy.shape[0]))
        for j,pi in enumerate(self.diff_sanitary_impact(policy)):
            factor = -pi * beta * (y[:,2]+y[:,6]+y[:,3]+b*y[:,7])
            result[:,0,j] = factor*(-y[:,0])
            result[:,1,j] = factor*m*y[:,0]
            result[:,5,j] = factor*(1-m)*y[:,0]
        return result
        
    #computes all derivatives of the flux
    #with respects to y0 and c, on the last day
    def compute_diff_final(self,model_results, policies, nweeks):
        params = {k:v for (k,v) in self.parameters.items()}
        nsteps = self.parameters['nsteps']
        nweeks_optim = policies.shape[0]
        npolicies = policies.shape[1]
        ny = model_results[0]['y'].shape[1]
        
        initial_conditions = np.concatenate((np.eye(ny).flatten(), np.zeros((ny, npolicies)).flatten()))
        
      
        As = []
        Xs = []
        for w in range(nweeks_optim):
            #print("T =", model_results[w]['t'])
            pol = policies[w,:]
            current_model_result = model_results[w]['y']
            params['J'] = self.compute_J(current_model_result, self.sanitary_impact(pol))
            params['diffC'] = self.compute_diffC(current_model_result, pol)
            params['beg'] = 7*w
            params['end'] = 7*(w+1)
            params['ny'] = ny
            AX = odeint(diff_ode, y0 = initial_conditions, t=[params['beg'],params['end']], args = (params,),
                        rtol=params['rtol'],atol=params['atol'],mxstep = params['mxstep'])
            As.append(AX[-1,:ny*ny].reshape(ny,ny))
            Xs.append(AX[-1,ny*ny:].reshape(ny,-1))
            
        #for the remaining unoptimized weeks
        if nweeks > nweeks_optim:
            current_model_result = model_results[-1]['y']
            params['J'] = self.compute_J(current_model_result, self.sanitary_impact(pol))
            params['diffC'] = self.compute_diffC(current_model_result, pol)
            params['beg'] = 7 * nweeks_optim
            params['end'] = 7 * nweeks
            params['ny'] = ny
            AX = odeint(diff_ode, y0 = initial_conditions, t=[params['beg'],params['end']], args = (params,),
                        rtol=params['rtol'],atol=params['atol'],mxstep = params['mxstep'])
            As.append(AX[-1,:ny*ny].reshape(ny,ny))
            Xs.append(AX[-1,ny*ny:].reshape(ny,-1))
            
            
        return As, Xs
                      
        
    #computes the objective function and its gradient
    def oracle(self, policies, nweeks,zone, order0=False,debug_model=False, debug_diff_ode=False):
        if debug_model:
            self.parameters['nsteps'] = 7
        
        nweeks_optim, npolicies = policies.shape
        
        cs = self.sanitary_impact(policies)
        
        y0 = self.parameters["y0"]
        model_results = []
        nsteps = self.parameters["nsteps"]
        time_steps = []

        for i in range(nweeks_optim): 
            t = np.arange(7*i*nsteps,7*(i+1)*nsteps+2,7) / nsteps
            self.parameters['c'] = cs[i] if not debug_model else 0.3
            res = odeint(covid_ode, y0=y0, t=t, args= (self.parameters,),
                         rtol=self.parameters['rtol'], atol=self.parameters['atol'],
                         mxstep=self.parameters['mxstep'])
            y0 = res[nsteps,:] #solve for one week
            model_results.append({'y':res,'t':t})
        if nweeks>nweeks_optim:
            t = np.arange(7*nweeks_optim*nsteps,7*nweeks*nsteps+2,7) / nsteps
            cs = self.sanitary_impact(np.zeros((1, policies.shape[1])))
            self.parameters['c'] = cs[0] if not debug_model else 0.3
            res = odeint(covid_ode, y0=y0, t=t, args= (self.parameters,),
                         rtol=self.parameters['rtol'], atol=self.parameters['atol'],
                         mxstep=self.parameters['mxstep'])
            model_results.append({'y':res,'t':t})

        if debug_model:
            return model_results[-1]['y']
        if debug_diff_ode:
            return model_results

        #number of deaths + impact
        econ_obj = -np.sum(policies * np.tile(self.economy_impact[zone,:],(nweeks_optim,1)))
        death_obj =  model_results[-1]['y'][-1,9]
        objective = death_obj + self.parameters['weight_econ']*econ_obj

        if order0: return objective
        
        #differentiate the flux on c and initial condition
        diffs = self.compute_diff_final(model_results, policies, nweeks)
        ny = y0.shape[0]
        diff_y0 = [a.reshape(ny,ny) for a in diffs[0]]
        diff_c = [x.reshape(ny,-1) for x in  diffs[1]]
        
        assert diff_c[0].shape == (ny,4),pdb.set_trace() #only 4 policies possible per week
        assert diff_y0[0].shape[0] == diff_y0[0].shape[1] == y0.shape[0] == 10, pdb.set_trace()
        
     

        #gradient with respect to policies
        Psi = np.zeros((ny, nweeks_optim, npolicies))
        Psi[:,0,:] = diff_c[0]
        for j in range(1,nweeks_optim):
            Psi = np.dot(diff_y0[j], Psi.reshape(ny,-1)).reshape(Psi.shape)
            Psi[:,j,:] += diff_c[j]
        if nweeks_optim<nweeks:
            Psi = np.dot(diff_y0[nweeks_optim], Psi.reshape(ny,-1)).reshape(Psi.shape)

        grad = Psi[9,:,:]
        grad -= np.tile(self.parameters['weight_econ']*self.economy_impact[zone,:], (nweeks_optim,1))
        
        return objective, grad , death_obj, econ_obj
              
    #run one optimization batch
    def optimize(self, nweeks, nweeks_optim, zone, policies,  niter):
        t0 = time.time()
        stats = []
        for i in range(niter):
            objective, gradient, death_obj, econ_obj = self.oracle(policies, nweeks, zone)
            gradient_norm = np.sqrt(np.sum(gradient.flatten()**2))
            sparsity = np.mean(policies<1e-5)
            active = np.mean(policies > 1-1e-5)
            if self.parameters['regularization'] == 'L1':
                objective += self.parameters['reg_lambda']*np.sum(np.absolute(policies).flatten())
                gradient += np.sign(policies) * self.parameters['reg_lambda']
            elif self.parameters['regularization'] == 'L2':
                objective += self.parameters['reg_lambda']*np.sqrt(np.sum(policies**2).flatten())
                gradient += 2*self.parameters['reg_lambda']*policies

            #perform linesearch
            filt = np.absolute(gradient.flatten()) > 1e-10
            lambdas = (policies.flatten()[filt]-1)/gradient.flatten()[filt]
            lambdas = np.concatenate((lambdas, (policies.flatten()[filt])/gradient.flatten()[filt]))
            lambdas = lambdas[lambdas>0]
            if lambdas.shape[0] == 0:
                if self.parameters['verbose']:
                    print('Early termination : all policies are either active or inactive, and the gradient goes out of the box')
                return policies, objective, stats
            lambd = 4*lambdas.min()
            itermax = 5
            it = 0
            while self.oracle(clip(policies - lambd*gradient), nweeks, zone,order0=True)>objective and it<itermax:
                it += 1
                if self.parameters['verbose']:
                    pass
                #print('dividing step %g size by 2'%lambd)
                lambd /= 1.5
            #print(policies)
            #print(gradient)
            if self.parameters['verbose']:
                print("obj: %.5f"%objective, "grad L2: %.5f"%gradient_norm, "deaths: %.5f"%death_obj,
                      "econ: %.5f"%econ_obj, "sparsity: %.2f%%"%(100*sparsity), "active: %.2f%%"%active, "step size: %e (%d steps)"%(lambd,it),
                      "t: %.5fs"%(time.time()-t0))
            stats.append((objective, gradient_norm, death_obj, econ_obj, sparsity, active, time.time()-t0))
           
            policies = clip(policies - lambd * gradient)
            #print(policies)
        return policies, objective, stats
    
    #do the actual gradient descent
    #nweeks : number of weeks to evaluate the impacts
    #nweeks_optim : the parameter space
    def find_best_policy(self, nweeks, nweeks_optim, zone):
        
        policies = clip(1+np.random.randn(nweeks_optim, self.economy_impact.shape[1]))
       
        policies, obj, stats = self.optimize(nweeks, nweeks_optim, zone,  policies, self.parameters['niter'])
        return policies, stats
