import string

from src.model.policy_utils import all_objectives


class Objectives:
    def __init__(self, optimize_req):
        self.objectives = []
        if 'objectives' not in optimize_req or not optimize_req['objectives']:
            print('ERROR: optimize_req does not contain any objective. Valid objectives are {}'
                  .format(all_objectives()))
            return
        if not all(o in all_objectives() for o in optimize_req['objectives']):
            print('ERROR: optimize_req contains invalid objectives {}. Valid objectives are {}'
                  .format(optimize_req['objectives'], all_objectives()))
            return
        self.objectives = optimize_req['objectives']
        if 'activated_measures_ratio' in self.objectives:
            self.nlabels = len(optimize_req['zone_label_names'])
            # self.nzones = len(optimize_req['zones'])
            self.max_measure_steps = optimize_req['max_measure_steps']
            self.activable_measures = {k: v for k, v in optimize_req['max_measure_values'].items() if v > 0}
        if 'death_ratio' in self.objectives:
            self.total_population_size = sum(optimize_req['population_size'])
        if 'ICU_saturation_ratio' in self.objectives:
            self.nzones = len(optimize_req['zones'])
            self.icu_beds = optimize_req['ICU_beds']
        if 'inactivity' in self.objectives:
            self.nzones = len(optimize_req['zones'])

    def activated_measures_ratio(self, eval_policy_req, _):
        """ :return: sum of measures normalized by their max allowed values, > 0 """
        rate: float = 0
        for measure, max_measure_value in self.activable_measures.items():
            for measure_step in eval_policy_req[measure]:
                rate += sum(measure_step['activated']) / (max_measure_value * self.nlabels)
        return rate / (self.max_measure_steps * len(self.activable_measures))

    def death_ratio(self, _, eval_policy_res):
        """ :return: percents, > 0 """
        death_beg = eval_policy_res['deaths'][0]['initial']
        death_end = eval_policy_res['deaths'][-1]['week']
        return (sum(death_end) - sum(death_beg)) / self.total_population_size

    @staticmethod
    def gdp_accumulated_loss_ratio(self, eval_policy_res):
        """ :return: rate, in [0..1] """
        ans = eval_policy_res['gdp_accumulated_loss'][-1]  # percents, in [0, 100]
        # assert ans >= 0
        return ans / 100

    # noinspection PyPep8Naming
    def ICU_saturation_ratio(self, eval_policy_req, eval_policy_res):
        """ :return: percents, > 0 """
        icu_patients = eval_policy_res['ICU_patients']
        nweeks = len(eval_policy_res['ICU_patients']) - 1  # do not count 'initial'
        excess = 0
        for idx, icu_patients_week_dict in enumerate(icu_patients):
            key = 'initial' if idx == 0 else 'week'
            icu_patients_week = icu_patients_week_dict[key]
            for zone_patients, zone_beds in zip(icu_patients_week, self.icu_beds):
                if zone_patients > zone_beds:
                    excess += (zone_patients - zone_beds) / zone_beds
        return excess / (nweeks * self.nzones)

    def inactivity(self, _, eval_policy_res):
        """ :return: taux réel de personnes actives ne pouvant pas travailler en raison des mesures, dans [0..1] """
        act_begin = sum([w if w else 0 for w in eval_policy_res['activity'][0]['initial']])
        act_end = sum([w if w else 0 for w in eval_policy_res['activity'][-1]['week']])
        ndays = 7 * (len(eval_policy_res['activity']) - 1)  # do not count 'initial'
        activity = (act_end - act_begin) / (ndays * self.nzones)
        return 1 - activity

    def by_name(self, objectives: list, objective_name: string):
        """ retrieve the value of the given objective
        in the list of floats returned by compute() """
        index = self.objectives.index(objective_name)
        return objectives[index]

    def compute(self, ok, eval_policy_req, eval_policy_res):
        if not ok:
            return None
        objectives = []
        for objective in self.objectives:
            method_to_call = getattr(self, objective)
            objectives.append(method_to_call(eval_policy_req, eval_policy_res))
        return objectives
