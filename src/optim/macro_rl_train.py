import itertools
import multiprocessing
import string
from copy import deepcopy
from os.path import isfile
from random import shuffle

import tensorflow as tf
from numpy import mean
from stable_baselines import PPO2
from stable_baselines.common.callbacks import BaseCallback
from stable_baselines.common.vec_env import DummyVecEnv

from src.model.macro_rl_env import MacroRLEnv
from src.model.policy_utils import decov_path
from src.model.policy_utils import default_optimize_req, all_measures, set_all_measures_max_values, \
    set_all_objectives_on

models_folder = decov_path + '/models'


class TensorboardCallback(BaseCallback):
    """
    Custom callback from plotting usefull informtions in tensorboard
    """

    def __init__(self, filepath, verbose: int = 0, save: bool = False):
        super(TensorboardCallback, self).__init__(verbose)
        self._filepath: string = filepath
        self._episode_fields = ['activated_measures_ratio', 'death_ratio', 'gdp_accumulated_loss_ratio',
                                'ICU_saturation_ratio', 'consistency_check', 'reward']
        self._episode_values = {f: [] for f in self._episode_fields}
        self._save = save
        self._max_episode_reward = None

    def env0fn(self, method_name: string):
        return self.training_env.env_method(method_name, indices=0)[0]

    def _on_step(self) -> bool:
        info = self.env0fn("get_info")
        for f in self._episode_fields:
            self._episode_values[f].append(info[f])
        if not info['done']:
            return True
        # update tensorboard with each field
        tf_values = [tf.Summary.Value(tag='objectives/' + f, simple_value=mean(self._episode_values[f]))
                     for f in self._episode_fields if f is not 'reward']
        tf_values.append(tf.Summary.Value(tag='step_reward', simple_value=info['reward']))
        summary = tf.Summary(value=tf_values)
        self.locals['writer'].add_summary(summary, self.num_timesteps)
        # check if reward is better than before -> we store this model
        # episode_reward = mean(self._episode_values['reward'])
        # if self._save and self._max_episode_reward is None or self._max_episode_reward <= episode_reward:
        #     print('Model {} saved after {} steps, episode_reward: {}'
        #           .format(self._filepath, self.num_timesteps, episode_reward))
        #     self.model.save(self._filepath)
        #     self._max_episode_reward = episode_reward
        # reset
        self._episode_values = {f: [] for f in self._episode_fields}
        return True


def train(optimize_req: dict, per_zone_observation: bool):
    env = MacroRLEnv(optimize_req,
                     per_zone_observation=per_zone_observation)
    env.set_weights(lambda_activated_measures_ratio=1,
                    lambda_death_ratio=1,
                    lambda_gdp_accumulated_loss_ratio=1,
                    lambda_ICU_saturation_ratio=1)
    vec_env = DummyVecEnv([lambda: env])
    model = PPO2('MlpPolicy', vec_env, verbose=1, tensorboard_log='/tmp/log_tb/ppo2_decov/')
    model.learn(total_timesteps=10000, callback=TensorboardCallback(filepath=''))
    # model.learn(total_timesteps=10000)


def req2key(optimize_req):
    ans = ''
    for m in all_measures().keys():
        ans += '0' if m not in optimize_req['max_measure_values'] else str(optimize_req['max_measure_values'][m])
    return ans


def weights2key(weights):
    return ';'.join([str(w) for w in weights])


def train_pareto(optimize_req: dict, per_zone_observation: bool, total_timesteps: int,
                 policy: string, save: bool = False):
    """
    :arg policy: among 'MlpPolicy', 'MlpLstmPolicy', 'MlpLnLstmPolicy', 'CnnPolicy', 'CnnLstmPolicy', ...
    """
    # check which objectives must be maximized
    req_key = req2key(optimize_req)

    # generate combinations of weights on these objectives
    possible_weights_per_objective: list = []
    # explicit list, same as set_weights() later. 'inactivity' not computed by MacroRLEnv
    for obj in ['activated_measures_ratio', 'death_ratio', 'gdp_accumulated_loss_ratio', 'ICU_saturation_ratio']:
        if obj not in optimize_req['objectives']:
            possible_weights_per_objective.append([0])
        else:
            # possible_weights_per_objective.append([1, 0.8, 0])
            possible_weights_per_objective.append([1, 0])  # do not keep 0.8 otherwise too many configuration
    weight_combinations_all = itertools.product(*possible_weights_per_objective)
    # remove useless weight_combinations: the ones that don't have at least one weight = 1
    weight_combinations = [weights for weights in weight_combinations_all if max(weights) == 1]
    shuffle(weight_combinations)  # shuffle the combinations to make it funnier

    vec_env = DummyVecEnv([lambda: MacroRLEnv(optimize_req=optimize_req, per_zone_observation=per_zone_observation)])

    # iterate on all these combinations
    for idx, weights in enumerate(weight_combinations):
        print('{}-{}/{}\t activated_measures={}\t death={}\t gdp_accumulated_loss={}\t  ICU_saturation={}'
              .format(req_key, idx + 1, len(weight_combinations), *weights))
        # generate an env with this combinations of weights on these objectives
        weights_key = weights2key(weights)
        filename = req_key + '-' + weights_key + '.zip'
        filepath = models_folder + '/' + filename
        # print('filepath', filepath)
        if save and isfile(filepath):
            print('Model  {} already exists, skipping it.'.format(filepath))
            continue
        vec_env.reset()
        vec_env.env_method('set_weights',
                           lambda_activated_measures_ratio=weights[0],
                           lambda_death_ratio=weights[1],
                           lambda_gdp_accumulated_loss_ratio=weights[2],
                           lambda_ICU_saturation_ratio=weights[3])

        if policy in ['MlpLstmPolicy', 'MlpLnLstmPolicy']:
            model = PPO2(policy, vec_env, verbose=1, nminibatches=1,
                         tensorboard_log='/tmp/log_tb/' + filename.replace('.zip', ''))
        else:
            model = PPO2(policy, vec_env, verbose=1, tensorboard_log='/tmp/log_tb/' + filename.replace('.zip', ''))
        model.learn(total_timesteps=total_timesteps, callback=TensorboardCallback(filepath, save=save))
        if save:
            print('Model {} saved after {} steps'.format(filepath, total_timesteps))
            model.save(filepath)

    return weight_combinations


def train_aux(max_measures, optimize_req: dict, per_zone_observation: bool,
              total_timesteps: int, policy: string):
    optimize_req2 = deepcopy(optimize_req)
    set_all_measures_max_values(optimize_req2, max_measures)
    train_pareto(optimize_req=optimize_req2, per_zone_observation=per_zone_observation,
                 total_timesteps=total_timesteps, save=True, policy=policy)


def train_pareto_episode_multiprocessing(max_measures_list: list, per_zone_observation: bool,
                                         total_timesteps: int, policy: string):
    optimize_req = default_optimize_req()
    set_all_objectives_on(optimize_req)  # important
    optimize_req['max_measure_steps'] = 5  # 5x3 weeks
    nb_cpu = multiprocessing.cpu_count()
    print('nb_cpu:', nb_cpu)
    pool = multiprocessing.Pool(nb_cpu)
    pool.starmap(train_aux, [(m, optimize_req, per_zone_observation, total_timesteps, policy)
                             for m in max_measures_list])
    print('Done.')


if __name__ == '__main__':
    # my_optimize_req = default_optimize_req()
    # set_all_measures_activable(my_optimize_req)
    # train(my_optimize_req)
    train_pareto_episode_multiprocessing(max_measures_list=[
        # [1, 0, 0, 0, 0],  # contact tracing cannot be optimized because no effect on macromodel
        [0, 1, 0, 0, 0],
        [0, 0, 2, 0, 0],
        [0, 0, 0, 2, 0],
        [0, 0, 0, 0, 1],
        # [2, 0, 0, 0, 0],  # contact tracing cannot be optimized because no effect on macromodel
        [0, 2, 0, 0, 0],
        [0, 0, 3, 0, 0],
        [0, 0, 0, 3, 0],
        [0, 0, 0, 0, 1],
        [1, 1, 2, 2, 1],
        [1, 2, 3, 3, 1]
    ],
        per_zone_observation=True,
        policy='MlpPolicy',  # 'MlpPolicy', 'MlpLstmPolicy', 'MlpLnLstmPolicy', 'CnnPolicy', 'CnnLstmPolicy', ...
        total_timesteps=5000)
