import json
import threading
import time

import matplotlib.pyplot as plt
import requests

from src.model.macromodel import MacroModelServer, MacroModelRequestHandler
from src.model.policy_utils import decov_path
from src.model.policy_utils import set_n_colors, set_n_measures, all_objectives
from src.optim.base_optim import BaseOptimServer
from src.optim.macro_rl_optim import RLOptimRequestHandler
from src.optim.nsgax_optim import NSGAXOptimRequestHandler
from src.optim.pareto import tuples2xy, tuples2xyz, ParetoFront, fuse_pareto_front_with_metric_list
from src.optim.pso_optim import PSOOptimRequestHandler
from src.optim.random_optim import RandomOptimRequestHandler

# from src.optim.reinforcement.reinforce_optim import ReinforceOptimRequestHandler

DISPLAY_TOTAL_PARETO = False  # Set to false if you do not want to see the total pareto
DISPLAY_TOTAL_PARETO_IN_TOP = True  # Set to false if the total pareto should not be in top of other paretos


def bench():
    servers, threads = [], []
    servers.append(MacroModelServer(('localhost', 8000), MacroModelRequestHandler))
    servers.append(BaseOptimServer(('localhost', 8001), RandomOptimRequestHandler))
    servers.append(BaseOptimServer(('localhost', 8002), PSOOptimRequestHandler))
    servers.append(BaseOptimServer(('localhost', 8003), NSGAXOptimRequestHandler))
    servers.append(BaseOptimServer(('localhost', 8004), RLOptimRequestHandler))
    # servers.append(BaseOptimServer(('localhost', 8004), BruteForceOptimRequestHandler))
    # servers.append(BaseOptimServer(('localhost', 8005), BBOptimRequestHandler))
    # servers.append(BaseOptimServer(('localhost', 8006), BBDeathsSameAllZonesOptimRequestHandler))
    # servers.append(BaseOptimServer(('localhost', 8007), EnumerateAllOptimRequestHandler))
    # servers.append(BaseOptimServer(('localhost', 8008), ReinforceOptimRequestHandler))
    # servers.append(BaseOptimServer(('localhost', 8009), ActorCriticOptimRequestHandler))
    for server in servers:  # start all in threads
        print('Spawning ' + server.RequestHandlerClass.__name__)
        threads.append(threading.Thread(target=server.serve_forever))
        threads[-1].start()

    with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
        data = json.load(json_data)
        # set_one_zone(data)
        # set_n_zones(data, 1)
        set_n_measures(data, 5)  # All measures
        set_n_colors(data, 2)  # All colors

        data['max_measure_steps'] = 5
        data['measure_step_duration'] = 3
        # objectives = ['death_ratio', 'gdp_accumulated_loss_ratio']
        # objectives = ['ICU_saturation_ratio', 'gdp_accumulated_loss_ratio']
        # objectives = ['activated_measures_ratio', 'ICU_saturation_ratio', 'gdp_accumulated_loss_ratio']
        objectives = all_objectives()
        data['objectives'] = objectives
        paretos = {}
        if DISPLAY_TOTAL_PARETO and not DISPLAY_TOTAL_PARETO_IN_TOP:
            paretos['total_pareto'] = []
        total_pareto = ParetoFront()
        for idx, server in enumerate(servers):
            start_time = time.time()
            if idx == 0:  # MacroModelRequestHandler
                continue
            url = 'http://localhost:{}/optimize'.format(server.server_port)
            print('Requesting {} at {}'.format(server.RequestHandlerClass.__name__, url))
            response = requests.post(url, data=json.dumps(data))

            # check results
            resp = response.json()
            assert resp['success']
            key = server.RequestHandlerClass.__name__

            paretos[key] = [p['objectives'] for p in resp['policies']]
            total_pareto = fuse_pareto_front_with_metric_list(total_pareto, paretos[key])

            print('Got a Pareto front of {} policies.'.format(len(resp['policies'])))
            print("Execution time : {}".format(time.time() - start_time))
        if DISPLAY_TOTAL_PARETO:
            paretos['total_pareto'] = total_pareto.metrics
    ndim = len(objectives)
    if ndim == 2:
        for name, pareto in paretos.items():
            x, y = tuples2xy(pareto, scale=100)
            plt.plot(x, y, marker='+', label=name)
        plt.xlabel(objectives[0])
        plt.ylabel(objectives[1])
        plt.grid()
        plt.legend(loc=1)  # Add legend North East
        plt.show()
    elif ndim == 3:
        ax = plt.axes(projection="3d")
        for name, pareto in paretos.items():
            x, y, z = tuples2xyz(pareto, scale=100)
            ax.plot(x, y, z, marker='+', label=name)
        ax.set_xlabel(objectives[0])
        ax.set_ylabel(objectives[1])
        ax.set_zlabel(objectives[2])
        plt.legend(loc=1)  # Add legend North East
        plt.show()
        # rotate the axes and update
        for angle in range(0, 360, 3):
            ax.view_init(10, angle)
            plt.draw()
            plt.pause(.001)

    for server in servers:
        print('Stopping ' + server.RequestHandlerClass.__name__)
        server.shutdown()
        server.socket.close()


if __name__ == '__main__':
    bench()
