import json
from copy import deepcopy

import numpy as np
from pymoo.factory import get_decision_making


# Fairly fast for many datapoints, less fast for many costs, somewhat readable
# https://stackoverflow.com/questions/32791911/fast-calculation-of-pareto-front-in-python
def is_pareto_efficient_simple(costs):
    """
    Find the pareto-efficient points
    :param costs: An (n_points, n_costs) array
    :return: A (n_points, ) boolean array, indicating whether each point is Pareto efficient
    """
    is_efficient = np.ones(costs.shape[0], dtype=bool)
    for i, c in enumerate(costs):
        if is_efficient[i]:
            is_efficient[is_efficient] = np.any(costs[is_efficient] < c, axis=1)  # Keep any point with a lower cost
            is_efficient[i] = True  # And keep self
    return is_efficient


def tuples2xy(pareto, scale=1):
    x = [scale * p[0] for p in pareto]
    y = [scale * p[1] for p in pareto]
    # sort them together - https://stackoverflow.com/questions/9764298/is-it-possible-to-sort-two-listswhich-reference-each-other-in-the-exact-same-w
    x, y = zip(*sorted(zip(x, y)))
    return x, y


def tuples2xyz(pareto, scale=1):
    x = [scale * p[0] for p in pareto]
    y = [scale * p[1] for p in pareto]
    z = [scale * p[2] for p in pareto]
    # sort them together - https://stackoverflow.com/questions/9764298/is-it-possible-to-sort-two-listswhich-reference-each-other-in-the-exact-same-w
    x, y, z = zip(*sorted(zip(x, y, z)))
    return x, y, z


class ParetoFront:
    def __init__(self):
        self.metrics: list = []
        self.eval_policy_reqs: list = []
        self.eval_policy_ress: list = []

    def update(self, eval_policy_req: object, eval_policy_res: object, new_metrics: list):
        """
        Update with a new possible point.
        We keep only the points that minimize the metrics (as small as possible)
        --> lower is better
        :param eval_policy_req: data to store
        :param eval_policy_res: data to store
        :param new_metrics: list of floats
        :return: True if updated
        """
        if new_metrics is None:
            return False
        dominated = []
        for i, metrics in enumerate(self.metrics):
            if np.all(np.less_equal(metrics, new_metrics)):  # new is dominated by metrics
                # print(new_metrics, 'is dominated by old', metrics)
                return False
            if np.all(np.less_equal(new_metrics, metrics)):  # metrics is dominated by new
                # print(metrics, 'is dominated by new', new_metrics)
                dominated.append(i)
        # we dominate at least one policy in our Front
        for idx in reversed(dominated):
            # print('Deleting', self.metrics[idx])
            del self.metrics[idx], self.eval_policy_reqs[idx], self.eval_policy_ress[idx]
        # print('Adding', new_metrics)
        self.metrics.append(new_metrics)
        self.eval_policy_reqs.append(eval_policy_req)
        self.eval_policy_ress.append(eval_policy_res)
        return True

    def compute_high_tradeoff(self):
        """ compute high tradeoff points on metrics using pymoo
        https://pymoo.org/decision_making/index.html#High-Trade-off-Points

        Return:
        status: bool
            status of the function.
            If the number of points in the evaluated metrics is not high enough (<5)
            status is False else True
        indexes: np.asarray
            the indexes of high tradeoff points.
            If the number of points in the evaluated metrics is not high enough (<5)
            the function returns None


        >>>     from pymoo.visualization.scatter import Scatter
        >>>     status, indexes = pareto_front.compute_high_treadeoff()
        >>>     if status:
        >>>         plot = Scatter()
        >>>         plot.add(pareto_front.metrics, alpha=0.2)
        >>>         plot.add([pareto_front.metrics[i] for i in indexes],
                              color="red", s=100)
        >>>         plot.show()

        """
        dm = get_decision_making("high-tradeoff")
        metrics = np.asarray(self.metrics)
        if metrics.shape[0] < 5:  # high-tradeoff OK in 2D or 3D, but no 4D
            # return total front indexes
            return False, None
        # compute tradeoff point
        try:
            indexes = dm.do(metrics)
            return True, indexes
        except IndexError:  # high-tradeoff randomly fails even in 3D
            print("WARNING: high-tradeoff failed")
            return False, None

    def __len__(self):
        return len(self.metrics)

    def write_eval_policy_reqs(self, path):
        counter = 0
        for x in self.eval_policy_reqs:
            with open(path + '/eval_policy_req' + str(counter) + '.json', 'w') as json_file:
                json.dump(x, json_file)
            counter += 1


def reconstruct_pareto_front_from_metric_list(l):
    p = ParetoFront()
    for metric in l:
        p.update(None, None, metric)
    return p


def fuse_pareto_front_with_metric_list(p1: ParetoFront, l):
    p2 = reconstruct_pareto_front_from_metric_list(l)
    p_fused = deepcopy(p1)
    for i in range(len(p2.metrics)):
        p_fused.update(p2.eval_policy_reqs[i], p2.eval_policy_ress[i], p2.metrics[i])
    return p_fused
