# !/usr/bin/env python3
import json
import subprocess
from copy import deepcopy

from src.model.policy_utils import decov_path
from src.model.policy_utils import all_measures
from src.optim.base_optim import launch_server, BaseOptimServer, BaseOptimRequestHandler
from src.optim.objectives import Objectives
from src.optim.pareto import ParetoFront


class BruteForceOptimRequestHandler(BaseOptimRequestHandler):

    def optimize(self, optimize_req, policy_common_values, activable_measures_dict):
        # print('optimize_req:', optimize_req)
        objectives = Objectives(optimize_req)
        pareto_front = ParetoFront()
        # call C++ executable "brute_force_optim"
        args = [decov_path + '/build/brute_force_optim']
        out_raw = subprocess.check_output(args, input=json.dumps(optimize_req).encode())
        out_raw = str(out_raw)  # convert byte array -> string
        # strip output until we find the first "{" and after last "}"
        out_raw = out_raw[out_raw.index('{'):out_raw.rindex('}') + 1]
        out_json = json.loads(out_raw)
        # print('out_json:', out_json)
        # print('Got a Pareto front of size', len(out_json['policies']))

        # now check the real metrics of each policy
        total_population_size = sum(optimize_req['population_size'])
        nzones = len(optimize_req['zones'])
        nmeasures = len(all_measures().keys())
        for policy in out_json['policies']:
            eval_policy_req = deepcopy(policy_common_values)
            measure_weeks = policy['measure_weeks']
            # assert len(policy['week_policy_index']) == len(measure_weeks)
            # convert policy index to proper eval_policy_req
            for pi in policy['week_policy_index']:
                week_policy = out_json['possible_week_policies'][str(pi)]
                assert len(week_policy) == nmeasures
                for m_idx, m in enumerate(all_measures().keys()):
                    if m not in activable_measures_dict:
                        continue
                    assert eval_policy_req[m] is not None
                    eval_policy_req[m].append({'activated': [week_policy[m_idx]] * nzones})
            # lift all measures at the end
            measure_weeks.append(optimize_req['max_measure_steps'])
            for m in activable_measures_dict:
                eval_policy_req[m].append({'activated': [0] * nzones})
            eval_policy_req['measure_weeks'] = policy['measure_weeks']
            # print(eval_policy_req)
            # send POST
            ok, eval_policy_res = self.call_eval_policy(eval_policy_req, activable_measures_dict)
            # now check if the policy has good objectives
            objs = objectives.compute(ok, eval_policy_req, eval_policy_res)
            # keep if on Pareto front
            pareto_front.update(eval_policy_req, eval_policy_res, new_metrics=objs)
        return True, pareto_front


if __name__ == '__main__':
    launch_server(BaseOptimServer, BruteForceOptimRequestHandler)
