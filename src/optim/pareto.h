#include <vector>
#include <stdio.h>

using namespace  std;
typedef vector<double> Objectives;

inline int is_pareto_efficient_simple(vector<Objectives> & objs,
                                       vector<unsigned char> & is_efficient) {
  // is_efficient = np.ones(costs.shape[0], dtype=bool)
  unsigned int size = objs.size(), objsize = objs.front().size();
  is_efficient.resize(size);
  std::fill(is_efficient.begin(), is_efficient.end(), true);
  // for i, c in enumerate(costs):
  for (unsigned int i = 0; i < size; ++i) {
    if (is_efficient[i]) {
      const Objectives & obji = objs[i];
      // is_efficient[is_efficient] = np.any(costs[is_efficient] < c, axis=1)  # Keep any point with a lower cost
      for (unsigned int j = 0; j < size; ++j) {
        if (!is_efficient[j])
          continue;
        bool isless = false;
        for (unsigned int k = 0; k < objsize ; ++k) {
          if (objs[j][k] >= obji[k])
            continue;
          isless = true;
          break;
        } // end for k
        is_efficient[j] = isless;
      } // end for j
      is_efficient[i] = true; // And keep self
    }
  }
  int ans = 0;
  for (unsigned int i = 0; i < size; ++i)
    ans += (is_efficient[i] ? 1 : 0);
  return ans;
}

template<class Req>
class ParetoFront {
public:
  unsigned int _nvariables;
  vector< Objectives > _objectives;
  vector< Req > _eval_policy_reqs;

  ParetoFront(unsigned int nvariables) : _nvariables(nvariables) {}

  inline unsigned int size() const { return _objectives.size(); }

  struct SortedData { Objectives m; Req req; };
  static bool compare(const SortedData & A,const SortedData & B) { return A.m[0] < B.m[0]; }

  void sort_by_objectives0() {
    unsigned int nelts = _objectives.size();
    std::vector<SortedData> data(nelts);
    for (unsigned int i = 0; i < nelts; ++i) {
      data[i].m = _objectives[i];
      data[i].req = _eval_policy_reqs[i];
    }
    sort(data.begin(), data.end(), compare);
    for (unsigned int i = 0; i < nelts; ++i) {
      _objectives[i] = data[i].m;
      _eval_policy_reqs[i] = data[i].req;
    }
  }

  //! We keep only the points that minimize the objectives (as small as possible)
  //! --> lower is better
  inline bool dominates(const Objectives & A, const Objectives & B) const {
    //printf("dominates: A:"); for (auto a : A) printf("%g, ", a); printf("B:"); for (auto b : B) printf("%g, ", b); printf("\n");
    for (unsigned int v = 0; v < _nvariables; ++v) {
      if (B[v] < A[v]) // A does NOT dominated B
        return false;
    }
    return true;
  }

  inline bool dominates(const Objectives & new_objectives) const {
    for (const Objectives & M : _objectives) {
      if (dominates(M, new_objectives))
        return true;
    }
    return false;
  }

  inline double previous_in_front(const double & dim0) const {
    double smallest_dim1 = INFINITY;
    for (const Objectives & M : _objectives) {
      if (M[0] <= dim0 && smallest_dim1 >= M[1])
        smallest_dim1 = M[1];
    }
    return smallest_dim1;
  }

  /*! Update with a new possible point.
  We keep only the points that minimize the objectives (as small as possible)
  --> lower is better
  \param eval_policy_req: data to store
  \param new_objectives:
  \return: True if updated */
  bool update(const Req & eval_policy_req,
              const Objectives & new_objectives) {
    if (dominates(new_objectives))
      return false;
    // we dominate at least one policy in our Front
    unsigned int nobjectives = _objectives.size();
    vector<unsigned int> dominated;
    dominated.reserve(size());
    for (unsigned int i = 0; i < nobjectives; ++i) {
      if (dominates(new_objectives, _objectives[i]))
        dominated.push_back(i);
    }
    while (!dominated.empty()) {
      _objectives.erase(_objectives.begin() + dominated.back());
      _eval_policy_reqs.erase(_eval_policy_reqs.begin() + dominated.back());
      dominated.pop_back();
    }
    // print('Adding', new_objectives)
    _objectives.push_back(new_objectives);
    _eval_policy_reqs.push_back(eval_policy_req);
    return true;
  }
}; // end class ParetoFront
