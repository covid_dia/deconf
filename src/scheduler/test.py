#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# argv
import sys

# ----------------------------------------------------------------------------------

from src.scheduler.sched import *

# ----------------------------------------------------------------------------------

def main (argv):

    sched = Sched()
    
    # add a single job(name, command)
    sched.addJob("single_job", "date >> test_log && sleep 3")
    # start scheduling: schedules nothing because no resources are available (warning)
    sched.runStatic()
    # remove non existing resource (warning)
    sched.removeResource("127.0.0.1")
    # add a single resource(address[, name])
    sched.addResource("127.0.0.1", "loopback")
    # start scheduling
    sched.runStatic()
    
    # initial set of resources and jobs from files
    sched.addResourcesFromFile("test_resources")
    sched.addJobsFromFile("test_jobs")
    # start scheduling
    sched.runStatic()
    # here:
    # - all initial jobs have been started
    # - some initial jobs might not have terminated
    # add more jobs
    sched.addJobsFromFile("test_jobs_2")
    # restart scheduling on batch2
    sched.runStatic()
    # add more resources: wont be used here because all jobs are already scheduled
    sched.addResourcesFromFile("test_resources_2")
    # wait for the termination of all jobs
    sched.terminateStatic()
    
    # load more jobs with all resources available
    sched.addJobsFromFile("test_jobs_3")
    sched.runStatic()
    # remove resources from file even if they are currently busy
    sched.removeResourcesFromFile("test_resources")
    # add more jobs to be scheduled on the remaining resources
    sched.addJobsFromFile("test_jobs")
    # the runStatic() function returns the partial mapping of jobs onto resources
    # this is a list of tuples (job_name, resource_address, resource_name)
    print(sched.runStatic())
    
    sched.terminateStatic()
    
# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)

# ----------------------------------------------------------------------------------
