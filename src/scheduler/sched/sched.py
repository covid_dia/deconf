#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# lock
from threading import Lock

# gethostname
import socket

# getuser
import getpass

# path
import os

# monotonic_ns
#from time import monotonic_ns
from time import monotonic

# popen, poll
from subprocess import Popen, PIPE, DEVNULL

# configparser
import configparser

# sleep
from time import sleep

# queue
import queue

# ----------------------------------------------------------------------------------

SCHEDULER_CONFIGURATION_FILE = "scheduler.config"

# ----------------------------------------------------------------------------------

class Utils:
    def __init__(self):
        self.start_time = self.getTime()
        self.config = None
        self.verbosity = 0
        if os.path.isfile(SCHEDULER_CONFIGURATION_FILE):
            self.log("using config file " + SCHEDULER_CONFIGURATION_FILE)
            self.config = configparser.ConfigParser()
            self.config.read(SCHEDULER_CONFIGURATION_FILE)
            if 'verbosity' in self.config['SCHEDULER']:
                self.verbosity = int(self.config['SCHEDULER']['verbosity'])
            if 'job_working_directory' in self.config['SCHEDULER']:
                Job.working_directory = self.config['SCHEDULER']['job_working_directory']
                self.log("changing job_working_directory to " + Job.working_directory)
    
    def log(self, message):
        if self.verbosity > 0:
            mformat = str(self.getTime() - self.start_time) + " " + message
            print(mformat)
        
    @staticmethod
    def getTime():
        """ time as milliseconds (?), int """
        # monotonic() = value (in fractional seconds) of a monotonic clock,
        # i.e. a clock that cannot go backwards
        return int(round(monotonic() * 1E6))
        # return monotonic_ns() * 1000  # needs Python 3.7

# ----------------------------------------------------------------------------------

    
class Resource:

    def __init__(self, utils, address, name=None):
        self.utils = utils
        self.removeFlag = False

        self.rawaddress = address

        splita = address.split('@')
        if len(splita) == 2:
            self.address = splita[1]
            self.login = splita[0]
        else:
            self.address = address
            self.login = getpass.getuser()

        self.name = name
        if name is None:
            self.name = address
        self.job = None

# ----------------------------------------------------------------------------------

    
class Job:

    working_directory = ""
    
    def __init__(self, utils, uid, command="", stdin=PIPE, stdout=DEVNULL, stderr=DEVNULL):
        self.utils = utils
        self.uid = uid
        self.command = command
        self.process = None
        self.resource = None
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.stdin_filename = None
        self.stdout_filename = None
        self.stderr_filename = None

        command_array = self.command.split(' ')
        i = 0
        while i < (len(command_array) - 1):
            if command_array[i] == "STDIN_FILE":
                self.stdin_filename = command_array[i+1]
                del command_array[i:i+2]
                i = 0
            elif command_array[i] == "STDOUT_FILE":
                self.stdout_filename = command_array[i+1]
                del command_array[i:i+2]
                i = 0
            elif command_array[i] == "STDERR_FILE":
                self.stderr_filename = command_array[i+1]
                del command_array[i:i+2]
                i = 0
            else:
                i = i + 1

        if self.stdin_filename is not None or self.stdout_filename is not None or self.stderr_filename is not None:
            self.command = ' '.join(command_array)



# ----------------------------------------------------------------------------------

class Sched:

    def __init__(self):

        self.lock = Lock()

        self.utils = Utils()
        self.utils.log("init on node " + socket.gethostname())

        self.all_resources = set()
        self.idle_resources = set()
        self.busy_resources = set()

        self.pending_jobs = queue.Queue()
        self.running_jobs = []
        self.running_jobs_index = 0
        

    def addResource(self, address, name=None):
        resource = Resource(self.utils, address, name)
        self.lock.acquire()
        self.all_resources.add(resource)
        self.idle_resources.add(resource)
        self.lock.release()
        self.utils.log("add resource " + resource.name)

    def addResourcesFromFile(self, resource_file_name):
        n = 0
        f = open(resource_file_name, "r")
        self.lock.acquire()
        for line in f:
            line_array = line.rstrip('\n').split(' ')
            if len(line_array) > 1:
                resource = Resource(self.utils, line_array[0], line_array[1])
            else:
                resource = Resource(self.utils, line_array[0])
            self.all_resources.add(resource)
            self.idle_resources.add(resource)
            n += 1
        self.lock.release()
        self.utils.log("add " + str(n) + " resources")

    def findResource(self, rawaddress, name=None):
        resource = None
        if name is None:
            name = rawaddress
        for resource in self.all_resources:
            if resource.rawaddress == rawaddress and resource.name == name:
                return resource

    def removeResourceCore(self, address, name=None):
        resource = self.findResource(address, name)
        if resource is not None:
            resource.removeFlag = True
        else:
            self.utils.log("WARNING: unable to tag resource " + address + " for removal")

    def removeResource(self, address, name=None):
        self.lock.acquire()
        self.removeResourceCore(address, name)
        self.lock.release()

    def removeResourcesFromFile(self, resource_file_name):
        f = open(resource_file_name, "r")
        self.lock.acquire()
        for line in f:
            line_array = line.rstrip('\n').split(' ')
            address = line_array[0]
            if len(line_array) > 1:
                name = line_array[1]
            else:
                name = address
            self.removeResourceCore(address, name)
        self.lock.release()


    def addJob(self, uid, command, stdin=PIPE, stdout=DEVNULL, stderr=DEVNULL):
        job = Job(self.utils, uid, command, stdin, stdout, stderr)
        self.lock.acquire()
        self.pending_jobs.put(job)
        self.lock.release()
        self.utils.log("add job " + job.uid)

    def addJobsFromFile(self, job_file_name):
        n = 0
        f = open(job_file_name, "r")
        self.lock.acquire()
        for line in f:
            line_array = line.rstrip('\n').split(' ', 1)
            if len(line_array) > 1:
                job = Job(self.utils, line_array[0], line_array[1])
            else:
                job = Job(self.utils, line_array[0])
            self.pending_jobs.put(job)
            n += 1
        self.lock.release()
        self.utils.log("add " + str(n) + " jobs")

        
    def mapJob(self, job, resource):
        if job.process is not None:
            self.utils.log("error: job " + job.uid + " is already running")
            exit()

        self.utils.log("map job " + job.uid + " on resource " + resource.name)

        job.resource = resource
        resource.job = job

        job_command = job.command.replace("$SCHED_RESOURCE_ADDRESS", resource.address)
        job_command = job_command.replace("$SCHED_RESOURCE_LOGIN", resource.login)
        job_command = job_command.replace("$SCHED_RESOURCE_NAME", resource.name)

        if job.stdin_filename is not None:
            job.stdin = open(job.stdin_filename, 'r')
        if job.stdout_filename is not None:
            job.stdout = open(job.stdout_filename, 'w')
        if job.stderr_filename is not None:
            job.stderr = open(job.stderr_filename, 'w')

        if Job.working_directory != "":
            command = ['ssh', '-oStrictHostKeyChecking=no', '-t', '-t', '-q',
                       resource.login + '@' + resource.address,
                       'cd ' + Job.working_directory, '&&', job_command]
        else:
            command = ['ssh', '-oStrictHostKeyChecking=no', '-t', '-t', '-q',
                       resource.login + '@' + resource.address, job_command]

        job.process = Popen(command, universal_newlines=True, shell=False,
                            stdin=job.stdin,
                            stdout=job.stdout, stderr=job.stderr)

    def getRunningJobFromUID(self, job_uid):
        job = next((job for job in self.running_jobs if job.uid == job_uid), None)
        return job


    def runStatic(self):
        maplist = []
        self.lock.acquire()
        self.utils.log("start scheduling with " + str(self.pending_jobs.qsize()) + " pending jobs, " + str(len(self.idle_resources)) + " idle resources, " + str(len(self.busy_resources)) + " busy resources")
        if (len(self.idle_resources) + len(self.busy_resources)) == 0:
            self.lock.release()
            self.utils.log("WARNING: no resource in the scheduler")
            return maplist
        while not self.pending_jobs.empty():
            job = self.pending_jobs.get()
            resource = self.claimResource()
            if resource is not None:
                self.mapJob(job, resource)
                self.running_jobs.append(job)
                self.busy_resources.add(resource)
                maplist.append((job.uid, resource.rawaddress, resource.name))
            else:
                self.lock.release()
                self.utils.log("error: unable to claim resource")
                return maplist
        self.utils.log("end scheduling with " + str(self.pending_jobs.qsize()) + " pending jobs, " + str(len(self.idle_resources)) + " idle resources, " + str(len(self.busy_resources)) + " busy resources")
        self.lock.release()
        return maplist


    def claimResource(self):
        resource = None
        if len(self.idle_resources) > 0:
            resource = self.idle_resources.pop()
        else:
            if len(self.running_jobs) == 0:
                self.utils.log("error: no running job to poll")
                return resource
            while resource is None:
                job = self.pollNextJob()
                if job is None:
                    self.utils.log("error: unable to poll next job")
                    return None
                resource = job.resource
                resource.job = None
                self.busy_resources.remove(resource)

        if resource is not None and resource.removeFlag:
            self.all_resources.remove(resource)
            self.utils.log("removing resource " + resource.name + " from pool")
            resource = self.claimResource()
        return resource


    def terminateStatic(self):
        self.lock.acquire()
        self.utils.log("terminate with " + str(self.pending_jobs.qsize()) + " pending jobs, " + str(len(self.idle_resources)) + " idle resources, " + str(len(self.busy_resources)) + " busy resources")
        if not self.pending_jobs.empty():
            self.utils.log("WARNING: terminating with " + str(self.pending_jobs.qsize()) + " jobs in pending list")
        while len(self.running_jobs) > 0:
            job = self.pollNextJob()
            if job is None:
                self.lock.release()
                self.utils.log("error: unable to poll next job")
                return None
            resource = job.resource
            resource.job = None
            self.busy_resources.remove(resource)
            self.idle_resources.add(resource)
        self.utils.log("terminate with " + str(self.pending_jobs.qsize()) + " pending jobs, " + str(len(self.idle_resources)) + " idle resources, " + str(len(self.busy_resources)) + " busy resources")
        self.lock.release()


    def pollNextJob(self):
        job = None
        if len(self.running_jobs) > 0:
            while job is None:
                job = self.running_jobs[self.running_jobs_index]
                if job.process is None:
                    self.utils.log("error: job " + job.uid + " is running without process")
                    return None
                #self.utils.log("poll job " + job.uid + " index " + str(self.running_jobs_index) + " on resource " + job.resource.name)
                if job.process.poll() is not None:
                    if job.stdin_filename is not None:
                        job.stdin.close()
                    if job.stdout_filename is not None:
                        job.stdout.close()
                    if job.stderr_filename is not None:
                        job.stderr.close()
                    self.running_jobs.remove(job)
                    self.utils.log("job " + job.uid + " complete on resource " + job.resource.name)
                else:
                    job = None
                    self.running_jobs_index += 1
                    sleep(1 / len(self.running_jobs))
                if self.running_jobs_index >= len(self.running_jobs):
                    self.running_jobs_index = 0
        return job


# ----------------------------------------------------------------------------------
        
