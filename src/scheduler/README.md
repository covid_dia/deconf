# Generic Scheduler

This is a generic scheduler to map command line jobs onto a pool of
remote resources.


## Test Program

Command-line for test:

```shell
python3 test.py

```

Program `test.py`

Python
```python
from src.scheduler.sched import * 
sched = Sched()

# add a single job(name, command)
sched.addJob("single_job", "date >> test_log && sleep 3")
# start scheduling: schedules nothing because no resources are available (warning)
sched.runStatic()
# remove non existing resource (warning)
sched.removeResource("127.0.0.1")
# add a single resource(address[, name])
sched.addResource("127.0.0.1", "loopback")
# start scheduling
sched.runStatic()

# initial set of resources and jobs from files
sched.addResourcesFromFile("test_resources")
sched.addJobsFromFile("test_jobs")
# start scheduling
sched.runStatic()
# here:
# - all initial jobs have been started
# - some initial jobs might not have terminated
# add more jobs
sched.addJobsFromFile("test_jobs_2")
# restart scheduling on batch2
sched.runStatic()
# add more resources: wont be used here because all jobs are already scheduled
sched.addResourcesFromFile("test_resources_2")
# wait for the termination of all jobs
sched.terminateStatic()

# load more jobs with all resources available
sched.addJobsFromFile("test_jobs_3")
sched.runStatic()
# remove resources from file even if they are currently busy
sched.removeResourcesFromFile("test_resources")
# add more jobs to be scheduled on the remaining resources
sched.addJobsFromFile("test_jobs")
# the runStatic() function returns the partial mapping of jobs onto resources
# this is a list of tuples (job_name, resource_address, resource_name)
print(sched.runStatic())

sched.terminateStatic()
```

## Scheduler configuration file

The configuration file is named `scheduler.config` and must be found
in the working directory when instantiating the Sched class. It
includes the following options:

``` config
[SCHEDULER]
# Set the level of verbosity. 0 is none, 1 is verbose.
verbosity = 1
# If set, the first command for ssh is to change directory to this
# working directory. ssh stays in the home directory otherwise.
job_working_directory = "/home/to/job/working/directory/"
```

## Add jobs

Jobs are defined by a name/uid and a command line. Function `addJob`
allows to add a single job while the `addJobsFromFile` function reads jobs
from file, one job per line. The file format is:

```
name_of_job regular shell command line
```

The job name/uid cannot contain whitespace. The command line starts
after the first whitespace.


Example of job file:
```
batch_12_job_56 cd /my/working/directory/ && ./foo bar
85f918f7-4a2c-4595-987f-98289f1f1fbe python3 --version > python_version
```

## Add resources

Resources are defined by an address and an optional name. The address
can be an IP address or a hostname and can also include the login
(login@address). Function `addResource` allows to add a single
resource while the `addResourcesFromFile` function reads resources from file,
one resource per line. The file format is:

```
address name
```

Example of resource file:
```
localhost
127.0.0.1 loopback
user@192.168.1.51 computing_machine
```

## Remove resources

Resources can be removed from the pool during the computation in order
to shrink the deployment. If the removed resource currently runs a
job, the resource is removed after the termination of the job and it
won't be used in the following schedulings. Function `removeResource`
allows to remove a single resource while the `removeResourcesFromFile`
function reads the resources from file, one resource per line. Note
that if a name has been specified when adding the resource, this must
be also specified when removing the resource has the system performs a
match for both address and name. Both functions are asynchronous and
do not wait for the termination of the jobs running on the removed
resources.


## Manage job data input and output

Once jobs are up and running, there are different mechanisms to write
data to stdin and read from stdout and stderr. The `addJob`
function takes additionnal parameters to redirect input and output:

```
addJob(name, command, stdin=PIPE, stdout=DEVNULL, stderr=DEVNULL)
```

The default behavior is to redirect stdin to the host process stdin
and to redirect stdout and stderr to `/dev/null`.

### Using files on a distributed file system

Using a distributed file system (FS) mounted on remote nodes is the
most straightforward method to manage data input and output as it
benefits from the decentralized management of shared data and the FS
mechanisms to properly scale on large infrastructures. The program or
command to run on remote nodes must take the file paths located on the
distributed FS as command line parameters to read and write data.

```
addJob("job_with_distributed_fs", "cd /path/to/remote/workspace && ./myprogram inputfile=/mount/path/to/inputfile outputfile=/mount/path/to/outputfile")
```

Note that this requires the proper management of paths and file names
to avoid all jobs to write on the same output file.

### Using files on the scheduler host node

Whenever no distributed FS is available, it is possible to specify
local files, located on the same machine that runs the scheduler. In
that case the file descriptors of the files are given as parameters to
the stdin, stdout and stderr of the job.

```
# open input and output files
inputfile_stdin = open('/path/to/local/test_inputfile', "r")
outputfile_stdout = open('/path/to/local/test_outputfile', "w")

sched = Sched()
sched.addResource("127.0.0.1", "loopback_slot0")

sched.addJob("job_with_local_files", "cd /path/to/remote/workspace && ./myprogram", stdin=inputfile_stdin, stdout=outputfile_stdout, stderr=outputfile_stdout)

sched.runStatic()
sched.terminateStatic()

# here: outputfile_stdout should contain the result of the computation

# close files
inputfile_stdin.close()
outputfile_stdout.close()

```

Another possibility is to specify the path to the local files directly
into the job command. This also allows to use local files when creating
jobs from a job file.

```
sched.addJob("job_with_local_files", "STDIN_FILE /path/to/local/test_inputfile STDOUT_FILE /path/to/local/test_outputfile STDERR_FILE /path/to/local/test_outputfile cd /path/to/remote/workspace && ./myprogram")
```

Same as using the following job file with `addJobsFromFile`:

```
job_with_local_files STDIN_FILE /path/to/local/test_inputfile STDOUT_FILE /path/to/local/test_outputfile STDERR_FILE /path/to/local/test_outputfile cd /path/to/remote/workspace && ./myprogram
```

`STDIN_FILE`, `STDOUT_FILE` and `STDERR_FILE` can appear anywhere and
in any order in the job command and will be removed along with their
respective path parameters from the command line when running the job.


### Using Unix pipes

Unix pipes are used to communicate between processes and can be
connected to the job stdin, stdout and stderr. These pipes can be used
for code-coupling to connect the scheduled jobs with a GUI, an
operational research algorithm or a decision engine.

```
# create a pipe to write into the remote job stdin
input_pipeout, input_pipein = os.pipe()
# create a pipe to read the remote job stdout
output_pipeout, output_pipein = os.pipe()

sched = Sched()
sched.addResource("127.0.0.1", "loopback_slot0")

# add a single job with pipe redirection for stdin, stdout and stderr
sched.addJob("job_with_pipes", "cd /path/to/remote/workspace && ./myprogram", stdin=input_pipeout, stdout=output_pipein, stderr=output_pipein)

sched.runStatic()

# write to stdin
os.write(input_pipein, b'these are the input data')

# read from stdout
fromprocess = os.fdopen(output_pipeout, 'r')
output_stdout = fromprocess.readline()
print(output_stdout)
fromprocess.close()	

sched.terminateStatic()

```

Note that the scheduler implementation uses Python `subprocess.Popen`
to ssh-connect and run jobs on remote nodes. While it is not advised
to directly write into stdin, [as stated in the
documentation](https://docs.python.org/3/library/subprocess.html?highlight=warning#subprocess.Popen.communicate),
connecting Unix pipes to `subprocess` appears to work as expected.

```
Warning: Use communicate() rather than .stdin.write, .stdout.read or .stderr.read to avoid
deadlocks due to any of the other OS pipe buffers filling up and blocking the child process.
```

Note that `subprocess.communicate()` is a blocking function and is not
used in this implementation as it would require to create a thread for
each job.


## Scheduler forward reference declaration

Computing resources are allocated after the job is submitted to the
scheduler. In some particular cases, the command line of the job must
include information about the resource it is deployed on. For example,
when scheduling a job that runs an `http` server which port is
listening to the same network interface connected to the scheduler
node. Forward reference declarations are replaced in the job command
after the resource allocation and just before running the job.

```
sched.addJob("job_with_fwd_decl", "./my_http_server --interface $SCHED_RESOURCE_ADDRESS:8080 --login $SCHED_RESOURCE_LOGIN --resourcename $SCHED_RESOURCE_NAME")
```

If job `job_with_fwd_decl` is mapped onto resource `user@192.168.1.51
http_server_node`, the resulting command is:

```shell
./my_http_server --interface 192.168.1.51:8080 --login user --resourcename http_server_node
```

If `name` is not specified in the resource declaration, it is set to
the `address` value including the `login` value if present.
