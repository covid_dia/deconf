/* malloc */
#include <stdlib.h>

/* assert */
#include <assert.h>

/* fprintf, scanf */
#include <stdio.h>

/* clock_nanosleep */
#include <time.h>

/* ---------------------------------------------------------------------------------- */


int
main(int argc, char *argv[]) {

  struct timespec busy_sleep;
  
  size_t buffersize = 8192;
  char * bufferin = NULL;

  busy_sleep.tv_sec = 2;
  busy_sleep.tv_nsec = 0;

  bufferin = malloc(sizeof(char) * buffersize);
  assert(bufferin);

  clock_nanosleep(CLOCK_MONOTONIC, 0, &busy_sleep, NULL);

  getline(&bufferin, &buffersize, stdin);

  clock_nanosleep(CLOCK_MONOTONIC, 0, &busy_sleep, NULL);
  
  fprintf(stdout, "repeat: %s\n", bufferin);
  fflush(stdout);
  
  free(bufferin);

  clock_nanosleep(CLOCK_MONOTONIC, 0, &busy_sleep, NULL);
  
  return 0;
}


/* ---------------------------------------------------------------------------------- */
