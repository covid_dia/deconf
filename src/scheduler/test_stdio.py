#!/usr/bin/env python3

# ----------------------------------------------------------------------------------

# argv
import sys

# pipe
import os

# thread
import threading

# ----------------------------------------------------------------------------------

from src.scheduler.sched import *


# ----------------------------------------------------------------------------------

def threadWrite(input_pipein):
    # write the input
    os.write(input_pipein, b'{  "zones": ["FR-ARA", "FR-BFC", "FR-BRE", "FR-CVL", "FR-COR", "FR-GES", "FR-HDF", "FR-IDF", "FR-NAQ", "FR-NOR", "FR-OCC", "FR-PDL", "FR-PAC"],  "population_size": [8026685, 2795301, 3329395, 2566759, 339178, 5518188, 5978266, 12213364, 5987014, 3319067, 5892817, 3786545, 5059473]}\n')

# ----------------------------------------------------------------------------------

def threadRead(output_pipeout):
    fromprocess = os.fdopen(output_pipeout, 'r')
    # read the input as stdin is routed to tty
    fromprocess.readline()
    # read the result
    output_stdout = fromprocess.readline()
    print(output_stdout)
    fromprocess.close()

# ----------------------------------------------------------------------------------

def threadReadWrite(sched, tid, scriptdir):
    # create a pipe to write into the remote job stdin
    input_pipeout, input_pipein = os.pipe()
    # create a pipe to read the remote job stdout
    output_pipeout, output_pipein = os.pipe()
    # add a single job with pipe redirection for stdin, stdout and stderr
    sched.addJob("repeat_pipe_thread_" + str(tid), "cd " + scriptdir + " && ./repeat",
                 stdin=input_pipeout, stdout=output_pipein, stderr=output_pipein)
    # start scheduling to ensure the job is up and running
    # TODO: investigate deadlock when calling the scheduler within these threads
    #       in case there are more threads than idle resources
    #       and some jobs do not terminate
    #sched.runStatic()
    # write the input
    message="this is thread number " + str(tid) + "\n"
    os.write(input_pipein, message.encode())
    fromprocess = os.fdopen(output_pipeout, 'r')
    # read the input as stdin is routed to tty
    fromprocess.readline()
    # read the result
    output_stdout = fromprocess.readline()
    print(output_stdout)
    fromprocess.close()

# ----------------------------------------------------------------------------------

def main (argv):


    # nb threads
    nb_threads = 4
    # list to host thread pids
    thread_list = []


    # get current script directory
    scriptdir = os.path.dirname(os.path.realpath(__file__))


    # open input and output files
    inputfile_stdin = open('test_inputfile', "r")
    outputfile_stdout = open('test_outputfile', "w")

    # create a pipe to write into the remote job stdin
    input_pipeout, input_pipein = os.pipe()
    # create a pipe to read the remote job stdout
    output_pipeout, output_pipein = os.pipe()


    

    sched = Sched()

    # add a single resource
    sched.addResource("127.0.0.1", "loopback_slot0")
    
    # add a single job to compile the remote program
    sched.addJob("compile_repeat", "cd " + scriptdir + " && gcc -o repeat repeat.c")

    # start scheduling and wait for the job to terminate
    sched.runStatic()
    sched.terminateStatic()


    # add single resource
    sched.addResource("127.0.0.1", "loopback_slot1")


    # start n threads to schedule jobs and manage input and output
    for i in range(nb_threads):
        th_tmp = threading.Thread(target=threadReadWrite, args=(sched, i, scriptdir))
        th_tmp.daemon = False
        th_tmp.start()
        thread_list.append(th_tmp)


    # add a single job with pipe redirection for stdin, stdout and stderr
    sched.addJob("repeat_pipe", "cd " + scriptdir + " && ./repeat",
                 stdin=input_pipeout, stdout=output_pipein, stderr=output_pipein)

    # add a single job with redirection of stdin, stdout and stderr into file
    sched.addJob("repeat_file", "cd " + scriptdir + " && ./repeat",
                 stdin=inputfile_stdin, stdout=outputfile_stdout, stderr=outputfile_stdout)

    # add a single job with redirection of stdin from path to file
    sched.addJob("repeat_file_from_command", "STDIN_FILE test_inputfile STDOUT_FILE test_outputfile_from_command cd " + scriptdir + " && ./repeat")
    
    # start scheduling
    sched.runStatic()

    # start a thread to read the output of the job
    th_read = threading.Thread(target=threadRead, args=(output_pipeout,))
    th_read.daemon = False
    th_read.start()

    # start a thread to write into the input of the job
    th_write = threading.Thread(target=threadWrite, args=(input_pipein,))
    th_write.daemon = False
    th_write.start()

    
    # wait for the jobs to terminate
    sched.terminateStatic()

    # wait for thread termination
    th_read.join()
    th_write.join()

    for th in thread_list:
        th.join()

    # close files
    inputfile_stdin.close()
    outputfile_stdout.close()
    
# ----------------------------------------------------------------------------------

if __name__  == "__main__":
    main(sys.argv)

# ----------------------------------------------------------------------------------
