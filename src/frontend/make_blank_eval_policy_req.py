import json

if __name__=="__main__":
    with open('../../doc/eval_policy_req.json') as fp:
        req = json.load(fp)
        req['measure_weeks'] = [0,1]
        measures = ["forced_telecommuting","school_closures","social_containment_all","total_containment_high_risk","contact_tracing"]
        for m in measures :
            req[m] = [{'activated':[0,0]}]*len(req['measure_weeks'])

        req['compartmental_model_parameters']['micro']['StopWhenDeadRatioExceedsThreshold'] = 1

    with open('static/eval_policy_req_blank.json', 'w') as fp:
        json.dump(req, fp)
