import csv
import os

import requests
from flask import Flask, render_template, request, url_for

app = Flask(__name__)

import json


def format_datetime(value):
    s = value % 60
    m = value // 60
    h = m // 60
    m = m % 60
    d = h // 60
    h = h % 60
    w = d // 7
    d = d % 7
    if w != 0:
        return "%dsem %dj %dh %dm %ds"%(w,d,h,m,s)
    elif d!=0:
        return "%dj %dh %dm %ds"%(d,h,m,s)
    elif h!=0:
        return "%dh %dm %ds"%(h,m,s)
    elif m!=0:
        return "%dm %ds"%(m,s)
    else:
        return '%ds' % s

app.jinja_env.filters['datetime'] = format_datetime

@app.route('/decov/', methods=['GET'])
@app.route('/deconf/', methods=['GET'])
@app.route('/', methods=['GET'])
def index():
    if request.method=='GET':
        #model_name = req["compartmental_model"]
        #macro_name = "macro_"+model_name
        model_types = ["micro", "macro_Alizon20", "common"]
        with open(url_for('static',filename='eval_policy_req_blank.json')[1:]) as fp:
            req = json.load(fp)
            req = {k:v for k,v in req.items() if not k.startswith('_')}
            for typ in model_types:
                req["compartmental_model_parameters"][typ] = {k:v for k,v in req["compartmental_model_parameters"][typ].items() if not k.startswith('_')}

            
        print(url_for('static', filename='/eval_policy_res_blank.json')[1:])
        with open(url_for('static', filename='/eval_policy_res_blank.json')[1:]) as fp: 
            res = json.load(fp)
      
        with open(url_for('static', filename="policy_level_explain.json")[1:]) as fp:
            policy_explain = json.load(fp)
        
        with open('static/seair_model_param_explain.json') as fp:
            model_params_meaning = json.load(fp)
            model_params_meaning = {k:v.split(',')[0] for k,v in model_params_meaning.items()}
            
        with open(url_for('static',filename='/optimize_req.json')[1:]) as fp: 
            optimize_req = json.load(fp)
            optimize_req = {k:v for k,v in optimize_req.items() if not k.startswith('_')}
            for typ in model_types:
                optimize_req["compartmental_model_parameters"][typ] = {k:v for k,v in optimize_req["compartmental_model_parameters"][typ].items() if not k.startswith('_')}
            optimize_req["compartmental_model_parameters"]["micro"]["StopWhenDeadRatioExceedsThreshold"] = 1.0

        with open('../../CHANGELOG.md') as fp:
            changelog = fp.read()
        
        possible_starts = {"radio-start-T0":{"date":"11/05/20",
                                 "filename":url_for('static',filename="eval_policy_req_blank.json"),
                                 "shift":0},
                           "radio-start-S21":{"date":"01/06/20",
                                    "filename":url_for('static',filename="initial_conditions_20200601.json"), 
                                    "shift":3}
                           }

        with open(url_for('static', filename='default_layout.json')[1:]) as fp:
            layout = json.load(fp)

        with open(url_for('static',filename='/objectives_explain.json')[1:]) as fp:
            objectives_explain = json.load(fp)
            
        with open(url_for('static', filename='optimizer_explain.json')[1:]) as fp:
            optimizer_explain = json.load(fp)
            
        return render_template('index.html', res=json.dumps(res), req=json.dumps(req),  
                               optim_params = optimize_req,
                               optim_params_json = json.dumps(optimize_req),
                               policy_explain=policy_explain,
                               model_params_meaning = model_params_meaning,
                               possible_starts_json = json.dumps(possible_starts),
                               layout = json.dumps(layout),changelog=changelog,
                               objectives_explain_json = json.dumps(objectives_explain),
                               optimizer_explain_json = json.dumps(optimizer_explain),
                               policy_explain_json=json.dumps(policy_explain),
                               autoescape=False, )

def add_csv_fields(data, index, filename, fieldname, factor=1):
    with open(filename, mode='r') as csv_file: 
        csv_reader = csv.DictReader(csv_file)
        
        for row in csv_reader:
            key = row['region']
            del row['region']
            if key in index:
                data['features'][index[key]]['properties'][fieldname] =\
                    {k:factor*float(v) for (k,v) in row.items()}

def add_json_fields(data, index, filename, fieldname): 
    with open(filename) as fp:
        d = json.load(fp)
        for reg in index:
            data['features'][index[reg]]['properties'][fieldname]=d[reg]
            
def add_sum_fields(data, index, field_dict, field_sum): 
    for reg in index:
        data['features'][index[reg]]['properties'][field_sum] =\
            sum(data['features'][index[reg]]['properties'][field_dict].values())

@app.route('/decov/geoData.geojson', methods = ['GET'])
@app.route('/deconf/geoData.geojson', methods = ['GET'])
@app.route('/geoData.geojson', methods = ['GET'])
def geoData():
    if request.method=='GET':
        with open('static/departements.geojson') as f:
            data = json.load(f)
            toremove = ["MAYOTTE",
                        "REUNION",
                        "GUYANE",
                        "MARTINIQUE",
                        "GUADELOUPE"]
            data['features'] = [a for a in data['features'] if not a['properties']['nom_dept'] in toremove]
            index = {a['properties']['code_dept']:i for (i,a) in enumerate(data['features'])}
            add_json_fields(data, index, 'static/data_emploi_departement.json', 'emploi')
            add_sum_fields(data, index, 'emploi', 'pop_active')
            add_json_fields(data, index, 'static/data_age_departement.json', 'age')
            add_sum_fields(data, index, 'age', 'pop')
            add_json_fields(data, index, 'static/data_risques_departement.json', 'poprisk')
            add_json_fields(data,index, 'static/pcr_departements.json','captest')
            with open('static/data_lits_hopitaux_departements.json') as fp:
                lits = json.load(fp)
                for dep in lits:
                    if not dep in toremove and not dep.startswith('_'):
                        data['features'][index[dep]]['properties']['lits_rea'] = lits[dep]
            return json.dumps(data)  
    else:
        return ""

@app.route('/favicon.ico',methods=['GET'])
def favicon():
    return url_for('static', filename="favicon.ico")

def get_model_path(model, cmd):
    with open(os.environ.get('DECOV_CONFIG')) as fp:
        config=json.load(fp)
    port = config['models'][model]['port']
    return 'http://%s:%d/%s'%(config['models'][model]['host'],port, cmd)

@app.route('/decov/relaunch_model',methods=['GET', 'POST'])
@app.route('/deconf/relaunch_model', methods=['GET', 'POST'])
@app.route('/relaunch_model', methods=['GET', 'POST'])
def relaunch_model():
    if request.json is not None:
        model = request.json['selected_model']
        a = requests.post(get_model_path(model,'eval_policy'),
                          data=json.dumps(request.json))
        return json.dumps(a.json())
    else:
        return "toto"

def get_optim_path(model,optimizer, cmd):
    with open(os.environ.get('DECOV_CONFIG')) as fp:
        config=json.load(fp)
    port = config['models'][model]['index'] + config['optimizers'][optimizer]['port']
    return 'http://%s:%d/%s'%(config['optimizers'][optimizer]['host'],port, cmd)

@app.route('/decov/optimize',methods=['GET', 'POST'])
@app.route('/deconf/optimize', methods=['GET', 'POST'])
@app.route('/optimize', methods=['GET', 'POST'])
def optimize(): 
    if request.json is not None:
        model = request.json['selected_model']
        optim = request.json['selected_optimizer']
        print(get_optim_path(model,optim, 'optimize'))
        a = requests.post(get_optim_path(model,optim, 'optimize'), data=json.dumps(request.json))
        return json.dumps(a.json())
    else:
        return "toto"


    
@app.route('/decov/policies.html',methods=['GET'])
@app.route('/deconf/policies.html', methods=['GET'])
@app.route('/policies.html', methods=['GET'])
def policies():
    with open(url_for('static', filename="policy_level_explain.json")[1:]) as fp:
          policy_explain = json.load(fp)
    model_types = ["micro", "macro_Alizon20", "common"]
    with open(url_for('static',filename='eval_policy_req_blank.json')[1:]) as fp:
        req = json.load(fp)
        req = {k:v for k,v in req.items() if not k.startswith('_')}
        for typ in model_types:
            req["compartmental_model_parameters"][typ] = {k:v for k,v in req["compartmental_model_parameters"][typ].items() if not k.startswith('_')}
            
    return render_template('policies.html', eval_policy_req = req, policy_explain=policy_explain)


@app.route('/decov/optimization.html',methods=['GET'])
@app.route('/deconf/optimization.html', methods=['GET'])
@app.route('/optimization.html', methods=['GET'])
def optimization():
    with open(url_for('static', filename="policy_level_explain.json")[1:]) as fp:
          policy_explain = json.load(fp)
          
    with open(url_for('static',filename='/optimize_req.json')[1:]) as fp: 
            optimize_req = json.load(fp)
            optimize_req = {k:v for k,v in optimize_req.items() if not k.startswith('_')}
            for typ in optimize_req["compartmental_model_parameters"]: 
                optimize_req["compartmental_model_parameters"][typ] = {k:v for k,v in optimize_req["compartmental_model_parameters"][typ].items() if not k.startswith('_')}
            optimize_req["compartmental_model_parameters"]["micro"]["StopWhenDeadRatioExceedsThreshold"] = 1.0
    

    with open(url_for('static', filename='optimizer_explain.json')[1:]) as fp:
        optimizer_explain = json.load(fp)
    
    return render_template('optimization.html', policy_explain=policy_explain,
                           optim_params = optimize_req,
                           optimizer_explain=optimizer_explain)


@app.route('/decov/map.html',methods=['GET'])
@app.route('/deconf/map.html', methods=['GET'])
@app.route('/map.html', methods=['GET'])
def map():
    return render_template('map.html')



@app.route('/decov/regional_stats.html',methods=['GET'])
@app.route('/deconf/regional_stats.html', methods=['GET'])
@app.route('/regional_stats.html', methods=['GET'])
def regional_stats():
    return render_template('regional_stats.html')

@app.route('/decov/timeline.html',methods=['GET'])
@app.route('/deconf/timeline.html', methods=['GET'])
@app.route('/timeline.html', methods=['GET'])
def timeline():
    return render_template('timeline.html')


@app.route('/decov/bargraph_emploi.html',methods=['GET'])
@app.route('/deconf/bargraph_emploi.html', methods=['GET'])
@app.route('/bargraph_emploi.html', methods=['GET'])
def bargraph_emploi():
    return render_template('bargraph_emploi.html')


@app.route('/decov/bargraph_age.html',methods=['GET'])
@app.route('/deconf/bargraph_age.html', methods=['GET'])
@app.route('/bargraph_age.html', methods=['GET'])
def bargraph_age():
    return render_template('bargraph_age.html')



@app.route('/decov/model.html',methods=['GET'])
@app.route('/deconf/model.html', methods=['GET'])
@app.route('/model.html', methods=['GET'])
def model():
    with open('static/seair_model_param_explain.json') as fp:
        model_params_meaning = json.load(fp)
        model_params_meaning = {k:v.split(',')[0] for k,v in model_params_meaning.items()}
        
    with open(url_for('static',filename='/optimize_req.json')[1:]) as fp: 
            optimize_req = json.load(fp)
            optimize_req = {k:v for k,v in optimize_req.items() if not k.startswith('_')}
            for typ in optimize_req["compartmental_model_parameters"]:
                optimize_req["compartmental_model_parameters"][typ] = {k:v for k,v in optimize_req["compartmental_model_parameters"][typ].items() if not k.startswith('_')}
            optimize_req["compartmental_model_parameters"]["micro"]["StopWhenDeadRatioExceedsThreshold"] = 1.0
            
    possible_starts = {
            "radio-start-T0":
        {
            "date":"11/05/20",
            "filename":url_for('static',filename="eval_policy_req_blank.json"),
            "shift":0
        },
        "radio-start-S21":
        {
            "date":"01/06/20",
            "filename":url_for('static',filename="initial_conditions_20200601.json"),
            "shift":3
        }
    }
    
                
    return render_template('model.html', optim_params=optimize_req,  model_params_meaning = model_params_meaning,
                           possible_starts=possible_starts)


@app.route('/decov/visualize_contact.html', methods=['GET'])
@app.route('/visualize_contact.html', methods=['GET'])
def visualize_contact():
    return render_template('visualize_contact.html')


@app.route('/pareto.html', methods=['GET'])
@app.route('/decov/pareto.html', methods=['GET'])
def pareto():
    return render_template('pareto.html')

@app.route('/radar.html', methods=['GET'])
@app.route('/decov/radar.html', methods=['GET'])
def radar():
    return render_template('radar.html')

@app.route('/solutions.html', methods=['GET'])
@app.route('/decov/solutions.html', methods=['GET'])
def solutions():
    return render_template('solutions.html')
