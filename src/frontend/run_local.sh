cp ../../data/regions.geojson static
cp ../../data/data_emploi_departement.json static
cp ../../data/data_age_region.json static
#pour test
cp ../../doc/eval_policy_req.json static
cp ../../doc/initial_conditions_20200416.json static/initial_conditions_req.json

cp ../../doc/optimize_req.json static

#configure initial conditions
curl -s -X POST -d @static/initial_conditions_req.json localhost:8000/initial_conditions -H "Content-Type: application/json"

echo

curl -s -o static/eval_policy_res.json -X POST -d @static/eval_policy_req.json localhost:8000/eval_policy -H "Content-Type: application/json" 

echo

export FLASK_ENV=development
export FLASK_APP='server.py'
flask run --port=8080
