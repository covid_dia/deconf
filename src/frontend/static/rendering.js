/* The object : a container for all global variables (data, pointers to DOM, current states, etc   */

state = {
    /* -------- layout objects --------- */
    mymap: Object(), /* The leaflet object containing the map */

    markers: [], /*an array of all the map markers that display measures. */
    
    /* -------- data objects ----------*/
    request_data: Object(), /* object to pass requests to the model */
    result_data: Object(), /* object that stores the current results of the model */
    initial_request_data : Object(), initial_result_data: Object(), /* backup for the aforementioned */

    index_regions : Object(), /* an index that maps the iso code of a region to its coordinate in the request_data and result_data objects*/
    
    possible_starts: Object(), /* The various different initial conditions for the model */
    optim_params: Object(), /*The parameters for the optimizer*/
    policies: Object(), /* storage for all policies returned as the Pareto Front */
    optimizer_explain: Object(), /* data with all possible optimizers */
    policy_explain : Object(),
    data_age: Object(), /* plotly data age distribution in regions */
    data_emploi: Object(), /* plotly data work branch distribution in regions */
    
    geojson: Object(), /* the leaflet container for the json data of regions */ 
    geodata_layer_points: null,
    geodata_layer_links: null,
    bar_layout: {
	font: {size:10, color:'#ffffff'},
	margin: {
            l: 30,
            r: 20,
            b: 60,
            t: 30,
            pad: 4
        },
        paper_bgcolor:'rgba(0,0,0,0)',
        plot_bgcolor:'rgba(0,0,0,0)',
        dragmode:false	
    },/* The plotly object showing graphs of region age and work distributions */
    
    xdata:[], ydata_cases:[], ydata_gdb:[], ydata_deaths:[], ydata_ICU:[],ydata_inactives:[], /* timeline plotly data */
    
    radar_data: [], /* optim screen radar data */
    
    /* -------- current state objects ---------- */
    selected_region: "none", /* Which region is currently selected */
    current_time : 0, /* which week is currently selected */
    max_displayed_weeks: 30,
    selected_graph_to_draw:"cases",
    input_mode:"rouge", /*how to modify the strategy : "regional"-y or "national"-y */
    initial_date:Object(), /* time at which the simulation start */
    current_optimizer:"nsgax", /* the currently selected optimizer*/
    current_model : "macro_Alizon20", /* the currently selected model */
    getgeo_mode: "spots", /* Selects whether to render contact graphs or spots for micromodel */
    /* Optim screen */ 
    active_policy:0, /*the index in policies to currently displayed solution (map and timeline)*/
    max_default_visible_optim_solutions: 5,/* should be coherent with field below*/
    visible_optim_solutions: new Set([0,1,2,3,4]),/* selected solutions to be visualized in optim screen*/
    pareto_x: "", pareto_y: "", /*which axes are selected to visualize the pareto front: {deaths, gdp,ICU}  */
    
}

/*a color palette to draw graphs */
color_palette = ['#383d96','#469449','#af363c','#e7c71f','#bb5695','#0885a1','#d67e2c','#505ba6',
		 '#c15a63','#5e3c6c','#9dbc40','#e0a32e','#735244','#c29682','#627a9d','#576c43',
		 '#8580b1','#67bdaa','#383d96','#469449','#af363c','#e7c71f','#bb5695','#0885a1',
		 '#d67e2c','#505ba6','#c15a63','#5e3c6c','#9dbc40','#e0a32e','#735244','#c29682',
		 '#627a9d','#576c43','#8580b1','#67bdaa']


function refresh_current_policy(){
    for(field in state.eval_policy_req){
	state.eval_policy_req[field] = state.policies[active_policies]=JSON.parse(JSON.stringify(state.policy))
    }
}

/* compute internal state fields */
function init_state(){
  
    /* all these fields are hard written by the server in the index.html file */
    state.request_data = initial_data.request_data;
    state.result_data = initial_data.result_data;
   
    state.optim_params = JSON.parse(JSON.stringify(initial_data.optim_params));
    state.possible_starts = initial_data.possible_starts;
    
    /* deep copy for backup */
    state.initial_request_data = JSON.parse(JSON.stringify(state.request_data));
    state.initial_result_data = JSON.parse(JSON.stringify(state.result_data));

    state.objectives_explain = initial_data.objectives_explain
    state.optimizer_explain = initial_data.optimizer_explain;
    state.policy_explain = initial_data.policy_explain;
    
    /* compute the region indices */
    state.index_regions = {};
    for(var i = 0; i<state.request_data.zones.length; ++i){state.index_regions[state.request_data.zones[i]] = i;}
    console.log(state.index_regions)
    
    state.initial_date = new Date("2020-05-11")


    state.request_data.getgeo_mode = "spots"
    
    refresh_current_policy()
    compute_all_maxes()
}


/* ############################   Common utilities   ########################## */

function deactivate_tabs(){
    $("#model-tab").css("pointer-events","none")
    $("#model-loader").css("display","block")
    $("#input-tab").css("pointer-events","none") 
    $("#input-loader").css('display','block')
    $("#optim-tab").css('pointer-events', 'none')
    $("#optim-loader").css('display','block')
}

function reactivate_tabs(){
    $("#model-tab").css("pointer-events","auto")
    $("#model-loader").css("display","none")
    $("#input-tab").css("pointer-events","auto") 
    $("#input-loader").css('display','none')
    $("#optim-tab").css('pointer-events', 'auto')
    $("#optim-loader").css('display','none')
}

/* ############################      Map utilities     ######################### */

/* initialise the map object */
function init_map(){
    state.mymap = L.map('tester', { zoomControl: false, scrollWheelZoom: true, zoomSnap:0.5, minZoom:4 })
    state.mymap.keyboard.disable(); /* we want the + and - keys to control the timeline, not the map */
    state.mymap.doubleClickZoom.disable(); /*since we deactivated scroll zoom, we don't want to risk getting stuck*/
    state.mymap.createPane('links')
    state.mymap.getPane('links').style.zIndex=380

    /*call server to get all region contents (including age and workforce) */
    jQuery.getJSON("/geoData.geojson", function (data){
	state.geojson = L.geoJSON(data, {style: style, onEachFeature: onEachFeature, pane:"tilePane"}).addTo(state.mymap);
	state.geojson['data'] = data
	state.mymap.fitBounds(state.geojson.getBounds());
	state.mymap.setZoom(5)
	refresh_map();
	state.geojson.bindPopup(function(layer) {
	    return getPopupContent(layer.feature);
	})
	
	$('#tester').css('display','block')
	$('#maploader').css('display','none')

    });
}

/* callback to display the border of regions when hovered upon */
function highlightFeature(e) {
    var layer = e.target;
    current_style = style(layer.feature)
    current_style.color = "#444444";
    layer.setStyle(current_style);
    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) layer.bringToFront();
}

/* callback for what to do when the hovering stops */
function resetHighlight(e) {
    state.geojson.resetStyle(e.target);
}


/* callback for what to do when a region is clicked */
function displayFeature(e) {
    var layer = e.target;
    state.selected_region = layer.feature.properties.code_dept; 

    /* change what is displayed in the region tabs */
    $("#nomregion").text(layer.feature.properties.nom_dept +
			 " ("+layer.feature.properties.code_dept+")");
    var population = state.request_data.population_size[state.request_data.zones.findIndex(a=>a==state.selected_region)]
    $("#popregion").text(Math.round(population).toLocaleString('fr-FR'));
    
    var ratio_actif = state.request_data.initial_ratios_Salje20[state.selected_region].reduce(function(a,b){
	return a + b.slice(1,5).reduce(function(c,d){return c+d});
    },0);
    $("#popactiveregion").text(Math.round(population*ratio_actif).toLocaleString('fr-FR'));
    $("#lits_rea").text(Math.round(layer.feature.properties.lits_rea).toLocaleString('fr-FR'));
    var risk = layer.feature.properties.poprisk
    $("#poprisk").text((Math.round(risk*population/100)).toLocaleString('fr-FR') + ' (' + risk+'%)');
    $("#captest").text(layer.feature.properties.captest * 7)  
    
    /* refresh everything */
    populate_age_work_graphs(layer)
    refresh_bar();
    refresh_input();
}

/* callback to set the content of the popups on regions */
function getPopupContent(feature){
    reg = state.index_regions[feature.properties.code_dept]
    var s1,s2,s3;
    
    s1 = Math.round(state.result_data.cases[state.current_time+1].week[reg]);
    s2 = Math.round(state.result_data.ICU_patients[state.current_time+1].week[reg]);
    s3 = Math.round(state.result_data.deaths[state.current_time+1].week[reg]);
    if(state.current_time > 1)
	s4 = state.result_data.tests[state.current_time+1].week[reg] - state.result_data.tests[state.current_time].week[reg];
    else
	s4 = state.result_data.tests[state.current_time+1].week[reg] - state.result_data.tests[0].initial[reg]; 

    s = '<p class="text-center" style="color:#000000;font-size:12px; margin-top:0px; margin-bottom:0px">Cas positifs: ' + s1 + '</p>'
    s += '<p class="text-center" style="color:#000000;font-size:12px; margin-top:2px; margin-bottom:2px">En soins intensifs: ' + s2 + '</p>'
    s += '<p class="text-center" style="color:#000000;font-size:12px; margin-top:2px; margin-bottom:2px">Décédés: ' + s3 + '</p>'
    if(state.request_data.selected_model == "micro"){
	s+= '<p class="text-center" style="color:#000000;font-size:12px; margin-top:2px; margin-bottom:2px">Tests utilisés: ' + s4 + '/'+feature.properties.captest*7 +'</p>'
    }

    return '<p class="text-center" style="color:#000000;font-size:14px; margin-top:2px; margin-bottom:2px" >'+feature.properties.nom_dept+"</p>" + s
}

/*callback for what happens on each feature */
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: displayFeature
    });
}

function computeMaxData(data){
    region_cases = data[0].initial.slice();
    for(var i = 1; i<data.length; ++i){
	region_cases = region_cases.concat(data[i].week);
    }
    
    return Math.max(...region_cases);
}

function compute_all_maxes(){
    state.maxCases = computeMaxData(state.result_data.cases);
    state.maxDeaths = computeMaxData(state.result_data.deaths);

    data = state.result_data.tests.map(function(d,i){
	    if(i==0) return d;
	    newweek = d.week.map(function(dd,j){
		if(i>1){
		    return dd -state.result_data.tests[i-1].week[j];
		}
		else{
		    return dd - state.result_data.tests[i-1].initial[j];
		}
	    });
	    return {week: newweek}
	});
    state.maxTests = computeMaxData(data);
}

/* color map function to draw data on the map */
function getColorMap(d,maxCases) {
    return d > 7*maxCases/8 ? '#800026' :
        d > 6*maxCases/8  ? '#BD0026' :
        d > 5*maxCases/8  ? '#E31A1C' :
        d > 4*maxCases/8  ? '#FC4E2A' :
        d > 3*maxCases/8   ? '#FD8D3C' :
        d > 2*maxCases/8   ? '#FEB24C' :
        d > 1*maxCases/8   ? '#FED976' :
        '#FFEDA0';
}


function style(feature){
    dep_index = state.request_data.zones.findIndex(a=>a==feature.properties.code_dept)
    if(state.request_data.zone_labels[dep_index]==1) dep_color="#009933"
    else dep_color ="#df0c0c"
    if($("#radio-green-red").prop("checked")){
	return {
            fillColor: dep_color,
            weight: 1,
            opacity: 1,
            color: "#ffffff",
            dashArray: '3',
            fillOpacity: 1
	};
    }
    if($("#radio-cases").prop("checked")){
	data = state.result_data.cases
	var color;
	if(state.current_time == 0){
 	    color = getColorMap(data[state.current_time].initial[dep_index],  state.maxCases);
	}
	else{
 	    color = getColorMap(data[state.current_time].week[dep_index],  state.maxCases);
	}
	
	return {
            fillColor: color,
            weight: 3,
            opacity: 1,
            color: dep_color,
            fillOpacity: 0.7
	};
    }
    if($("#radio-deaths").prop("checked")){
	data = state.result_data.deaths
	var color;
	if(state.current_time == 0){
 	    color = getColorMap(data[state.current_time].initial[dep_index],state.maxDeaths); 
	}
	else{
 	    color = getColorMap(data[state.current_time].week[dep_index], state.maxDeaths);
	}
	return {
            fillColor: color,
            weight: 3,
            opacity: 1,
            color: dep_color,
            fillOpacity: 0.7
	};
    }
    if($("#radio-tests").prop("checked")){
	data = state.result_data.tests.map(function(d,i){
	    if(i==0) return d;
	    newweek = d.week.map(function(dd,j){
		if(i>1){
		    return dd -state.result_data.tests[i-1].week[j];
		}
		else{
		    return dd - state.result_data.tests[i-1].initial[j];
		}
	    });
	    return {week: newweek}
	});
			
	var color;
	if(state.current_time == 0){
 	    color = getColorMap(data[state.current_time].initial[dep_index], state.maxTests); 
	}
	else{
 	    color = getColorMap(data[state.current_time].week[dep_index], state.maxTests);
	}
	return {
            fillColor: color,
            weight: 3,
            opacity: 1,
            color: dep_color,
            fillOpacity: 0.7
	};
    }
}



/* function to draw the leaflet map */
function refresh_map(){
    state.geojson.setStyle(style);
    last_week = get_keyframe_index(state.current_time);
    html = ''
    for(var i=0; i<state.request_data.zone_label_names.length; ++i){
	label = state.request_data.zone_label_names[i]
	html += '<div style="width:45%; height:100%; overflow-x:scroll;overflow-y:hidden;'+(i!=0?"border-left:solid;":"")+'border-color:#ffffff; display:inline-flex;flex-flow:row nowrap; grid-template-columns:repeat('+(state.optim_params.max_measure_values.length+1)+',1fr)">'
	html += '<img src="/static/france-'+label+'.png" style="width:auto; height:90%; display:inline-block; margin-left:10px; margin-top:1%;"'
	html += ' data-toggle="tooltip" data-placement="top" title="Mesures en vigueur pour les départements '+label+'s">'
	for(policy in state.optim_params.max_measure_values){
	    level = state.request_data[policy][last_week].activated[i]

	    if(level>0){
		html += '<img src="/static/icon_'+policy+'_'+level+'.png" style="width:auto; height:70%;display:inline-block; margin-left:10px;margin-top:4%; background-color:#888888; border:solid; border-color:blue; border-width:1px" data-toggle="tooltip" data-html="true"  data-placement="top" title="'+state.policy_explain[policy].explanation[level.toString()]+'">'

	    }
	}
	html+="</div>"
    }
    $('#grille-mesures').html(html)

    // Legend
    if(state.map_legend !== undefined) state.mymap.removeControl(state.map_legend)
    if(!$("#radio-green-red").prop("checked")){
	state.map_legend = L.control({position:"topright"});
	state.map_legend.onAdd = function(map){
	    var div = L.DomUtil.create('div', 'maplegend');
	    if($("#radio-cases").prop("checked")){
		div.innerHTML+= '<h4> Contaminés </h4>'
		for(var i=0;i<7; ++i){
		    div.innerHTML+= '<i style="background:'+getColorMap(state.maxCases*(i+0.1)/8.0, state.maxCases)+'"></i> <span> &lt; '+ Math.round(state.maxCases*(i+1)/8) +'</span> <br>'
		}
		div.innerHTML+= '<i style="background:'+getColorMap(state.maxCases, state.maxCases)+'"></i> <span> &gt; '+ Math.round(state.maxCases*7/8.0) +'</span> <br>'
	    }
	    else if($("#radio-deaths").prop("checked")){
		div.innerHTML+= '<h4> Décédés </h4>'
		for(var i=0;i<7; ++i){
		    div.innerHTML+= '<i style="background:'+getColorMap(state.maxDeaths*i/8.0, state.maxDeaths)+'"></i> <span> &lt; '+ Math.round(state.maxDeaths*(i+1)/8) +'</span> <br>'
		}
		div.innerHTML+= '<i style="background:'+getColorMap(state.maxDeaths, state.maxDeaths)+'"></i> <span> &gt; '+ Math.round(state.maxDeaths*7/8.0) +'</span> <br>'
	    }
	    else if($("#radio-tests").prop("checked")){
		div.innerHTML+= '<h4> Tests </h4>'
		if(state.maxTests == 0){
		    div.innerHTML+= '<p> Modèle micro uniquement </p>'
		    div.innerHTML+= '<p> et politique de test &gt; 0 </p>'
		}
		else{
		    for(var i=0;i<7; ++i){
			div.innerHTML+= '<i style="background:'+getColorMap(state.maxTests*i/8.0, state.maxTests)+'"></i> <span> &lt; '+ Math.round(state.maxTests*(i+1)/8) +'</span> <br>'
		    }
		    div.innerHTML+= '<i style="background:'+getColorMap(state.maxTests, state.maxTests)+'"></i> <span> &gt; '+ Math.round(state.maxTests*7/8.0) +'</span> <br>'
		}
	    }
	    return div
	}
	state.map_legend.addTo(state.mymap)
    }
    




    
}  

function refresh_map_with_micro(){
    refresh_map();
    console.log(state.request_data.selected_model)
    if(state.request_data.selected_model == "micro"){
	draw_micromodel_output();
    }
    else{
	clear_micromodel_output();
    }
}

function clear_micromodel_output(){
    if(state.geodata_layer_points!==null) state.mymap.removeLayer(state.geodata_layer_points);
    if(state.geodata_layer_links!==null) state.mymap.removeLayer(state.geodata_layer_links);
    if(state.micro_legend !==undefined) state.mymap.removeControl(state.micro_legend);
}

/* Draw agents or spots */
function draw_micromodel_output(){
    clear_micromodel_output()
    state.micro_legend = L.control({position:"topleft"});
    
    if(state.result_data.hasOwnProperty('geodata')){

	if(state.getgeo_mode == "spots"){	
	    state.geodata_layer_points = L.geoJSON(state.result_data.geodata,
						   {
						       filter: function(feature){
							   return feature.properties.type=="agent" || feature.properties.type == "spot";
						       },
						       pointToLayer: function(feature,latlong){
							   var pointstyle = {
							       radius:feature.properties.r,
							       fillColor:feature.properties.type=="agent"?"#ff7f00":"#ff00ff",
							       color:"#000000",
							       weight:1,
							       fillOpacity:0.5,
							   }
							   return L.circleMarker(latlong,pointstyle)
						       }
						   });
	    state.geodata_layer_links = L.geoJSON(state.result_data.geodata,
						  {
						      filter: function(feature){
							  return feature.properties.type=="link";
						      },
						      style: function(feature){
							  return {
							      color:"#000000",
							      weight:1,
							      opacity:0.2
							  }
						      },
						      pane:"links"
						  });

	    
	    state.micro_legend.onAdd = function(map){
		var div = L.DomUtil.create('div', 'legend');
		div.innerHTML+= '<h4> Légende </h4>'
		div.innerHTML+= '<i style="background:#ff7f00"></i> <span> Agent </span> <br>'
		div.innerHTML+= '<i style="background:#ff00ff"></i> <span> Lieu </span> '
		return div
	    }
	    
	}
	else{//state.geodata = "agents"
	    colors = {"Sensitive":"#0000ff",
		      "ExposedMinor":"#00ffff","ExposedMajor":"#00ffff",
		      "AsymptomaticMinor":"#ff7f00","AsymptomaticMajor":"#ff7f00",
		      "InfectedMinor":"#ff00ff","InfectedMajor":"#ff00ff",
		      "RecoveredMinor":'#ffffff',"RecoveredMajor":'#ffffff',
		      "Dead":'#333333'}
	    colorLegend = {
		"Sain" : colors["Sensitive"],
		"Exposé" : colors["ExposedMajor"],
		"Asymptomatique": colors["AsymptomaticMinor"],
		"Infecté" : colors["InfectedMinor"],
		"Guéri (R)": colors["Recovered"],
		"Mort" : colors["Dead"]
	    }
	    
	    state.geodata_layer_points = L.geoJSON(state.result_data.geodata[state.current_time],
						   {
						       filter:function(feature){
							   return feature.properties.type=="agent";
						       },
						       pointToLayer:function(feature,latlong){
							   var pointstyle = {
							       radius:feature.properties.r,
							       fillColor:colors[feature.properties.stage],
							       color:"#000000",
							       weight:1,
							       fillOpacity:0.5,
							   }
							   return L.circleMarker(latlong,pointstyle).bindPopup(feature.properties.stage[0]);
						       }
						   });
	    console.log(state.result_data.geodata[state.current_time])
	    state.geodata_layer_links = L.geoJSON(state.result_data.geodata[state.current_time],
						  {
						      filter: function(feature){
							  return feature.properties.type=="link";
						      },
						      style: function(feature){
							  return {
							      color:"#000000",
							      weight:1,
							      opacity:0.2
							  }
						      },
						      pane:"links"
						  });
	    
	    state.micro_legend.onAdd = function(map){
		var div = L.DomUtil.create('div', 'legend');
		div.innerHTML+= '<h4> Légende </h4>'
		for(c in colorLegend){
		    div.innerHTML+= '<i style="background:'+colorLegend[c]+'"></i> <span>'+ c+'</span> <br>'
		}
		return div
	    }
		
	}
	state.mymap.addLayer(state.geodata_layer_points)
	state.mymap.addLayer(state.geodata_layer_links)
	state.micro_legend.addTo(state.mymap)
    }
			    

    
    state.geojson.bindPopup(function(layer) {
	return getPopupContent(layer.feature);
    })
}


/* ################      Regional statistics (bar graph) utilities     ################ */

/* fill data for the plotly graphs to be later rendered */
function populate_age_work_graphs(layer) {
    /* age data for the selected region */
    state.data_age = {
	type: 'bar',
	x:[],
	y:[],
	marker: {
            color: '#32a1ce',
            line: {
		width: 2
            }
	}
    };
    for (var age in layer.feature.properties.age){
	state.data_age.x.push(age.toString() + "-" + (parseInt(age)+4).toString() + " ans");
	state.data_age.y.push(layer.feature.properties.age[age])
    }

    /* workforce data for the selected region */

    state.data_emploi = { 
	type: 'pie',
	labels:[],
	values:[],
    };
    for (var emploi in layer.feature.properties.emploi){
	state.data_emploi.labels.push(emploi.toString());
	state.data_emploi.values.push(layer.feature.properties.emploi[emploi])
    }
}


/* Initialisation of the bar graphs */
function init_bar(){
    refresh_bar()
}


/* Function to call to refresh the bar graphs for region statistics */
function refresh_bar(){
    if(state.selected_region != "none"){ 
        Plotly.newPlot('bargraph-age', [state.data_age], state.bar_layout, {responsive:true, displayModeBar: false})
    }
    if(state.selected_region != "none"){
        Plotly.newPlot('bargraph-emploi', [state.data_emploi], state.bar_layout, {responsive:true, displayModeBar: false})
    }
}

/* #############   Strategy modification utilities (a.k.a. "input") ##################   */

/* Button utilities */
function init_input(){
    $("#input-sauver").on('click', function(e){
	save_new_measure();
    });

    $("#input-annuler").on('click', function(e){
	refresh_input();
    });

    $("#input-relancer").on("click", function(e){
	deactivate_tabs();
	run_simulation()

    });
    

    $("#input-reset").on('click', function(e){
	state.request_data = JSON.parse(JSON.stringify(state.initial_request_data));
	state.result_data = JSON.parse(JSON.stringify(state.initial_result_data));
	state.current_time=0;
	refresh_map_with_micro()
	refresh_input();
	refresh_timeline_with_data();
	$("#week-set-measure").val(1)
    });

    refresh_input();
}


/* Find the last point with a measure change with regards to the current time */
function get_keyframe_index(week){/*todo dichotomy*/
    if(week < state.request_data.measure_weeks[0]) return -1; /*probably useless*/
    indices = state.request_data.measure_weeks.filter(function(i) {return i<=week});
    return indices.length-1;
}

/* utility to directly set all input selects to disabled or not */
function set_input_dropdowns_disabled(disabled){
    if(disabled){
	for(policy in state.optim_params.max_measure_values){
	    $("#form-"+policy).prop("disabled", true).tooltip('enable')
	   }
    }
    else{
	for(policy in  state.optim_params.max_measure_values){
	    $("#form-"+policy).removeAttr("disabled").tooltip('disable').tooltip('hide')
	}
    }
}

/* Refresh everything after the user changed something in the input (or cancel) */
function refresh_input(){
    last_week_index = get_keyframe_index(state.current_time);

    state.request_data.zone_label_names.forEach(function(label, index){
	if(state.current_time<state.request_data.measure_weeks[0]){
	    for(policy in state.optim_params.max_measure_values){
		active = $("#form-"+policy+"-"+label).val()
		$("#form-"+policy+"-"+label+"-"+active).prop('selected',false)
		$("#form-"+policy+"-"+label+"-3").prop('selected',false)
	    }
	}
	else{
	    for(policy in  state.optim_params.max_measure_values){
		active = $("#form-"+policy+"-"+label).val()
		$("#form-"+policy+"-"+label+"-"+active).prop('selected',false)
		if(state.request_data[policy] === null){
		    $("#form-"+policy+"-"+label+"-0").prop("selected",true)
		}
		else{
		    $("#form-"+policy+"-"+label+"-"+state.request_data[policy][last_week_index].activated[index]).prop("selected",true)
		}
	    }
	}
    })
}

/* Insert new keyframe and returns if it found smth and its position in the measure_weeks array  */
function insert_new_keyframe_if_needed(week, old_data){
    if(!state.request_data.measure_weeks.includes(week)){
    /* if the keyframe does not exist, we need to add it */
	keyframe_index = get_keyframe_index(week);
	
	/*insert new keyframe at the right spot*/
	state.request_data.measure_weeks.splice(keyframe_index+1,0,week)
	if(keyframe_index==-1 || old_data === null){
	    dummy = []
	    for(var i=0; i<state.request_data.zone_labels.length; ++i) dummy.push(false);
	    for(measure in state.optim_params.max_measure_values){
		state.request_data[measure].splice(keyframe_index+1,0,{"activated": dummy.slice()});
	    }
	    return {"previous":false,key:keyframe_index+1};
	}
	else{
	    for(measure in state.optim_params.max_measure_values ){
		state.request_data[measure].splice(keyframe_index+1,0,{"activated": old_data[measure].slice()});
	    }
	    return {"previous":false,key:keyframe_index+1};
	}
    }
    else{
	return {"previous":true, "key":find_in_measure_weeks(week)};
    }
}

/* change data of an existing keyframe */
function put_data_at_keyframe(index, input_data){
    console.log(input_data)
    state.request_data.zone_label_names.forEach(function(label, i){
	for(measure in state.optim_params.max_measure_values){
	    state.request_data[measure][index].activated[i] = input_data[label][measure];
	}
    })
}

function find_in_measure_weeks(week){//todo: implement with dichotomy to avoid computational overhead
    return state.request_data.measure_weeks.indexOf(week)
}

/* What happens when you try to add a new measure at the selected week */
function save_new_measure(){
    nweeks = parseInt($("#week-set-measure").val())

    /* save old data */
    old_last_week_index = get_keyframe_index(state.current_time+nweeks)
    old_data = Object();
    if(old_last_week_index>=0){
	for(measure in state.optim_params.max_measure_values){
	    old_data[measure] = state.request_data[measure][old_last_week_index].activated.slice();
	}
    }
    else{ /* take initial full containment as old data */
	dummy_true = [true];
	for(var j=0; j<state.request_data.zone_labels.length; ++j) dummy_true.push(true);
	for(measure in state.optim_params.max_measure_values){
	    old_data[measure] = dummy_true.slice();
	}
    }
    
    this_week = insert_new_keyframe_if_needed(state.current_time, null).key
   
    
    /* store the content of input fields into new keyframe */
    input_data = Object();
    state.request_data.zone_label_names.forEach(function(label){
	input_data[label]=Object()
	for(measure in state.optim_params['max_measure_values']){
	    input_data[label][measure] = parseInt($("#form-"+measure+"-"+label).val());
	}
    });
    put_data_at_keyframe(this_week, input_data);

    /* also insert new keyframe at current_time + nweeks */
    
    result = insert_new_keyframe_if_needed(state.current_time + nweeks, old_data); 
    next_key = result.key
    
    /* cleanup everything in between*/ 
    var k = this_week+1;
    while(state.request_data.measure_weeks[k]<state.current_time + nweeks){
	state.request_data.measure_weeks.splice(k,1)
	for(measure in state.optim_params.max_measure_values){
	    state.request_data[measure].splice(k,1);
	}
	++k
    }
    
    refresh_input();
    refresh_map();
    
}

function run_simulation(){
    state.request_data.selected_model = state.current_model
    if(state.current_model.length>5 && state.current_model.slice(0,5)=="macro"){
	state.request_data.compartmental_model = state.current_model.slice(6)
    }
    
    $.ajax({
	type: "POST",
	url: "/relaunch_model",
	data: JSON.stringify(state.request_data),
	contentType: "application/json; charset=utf-8",
	dataType: "json",
	success: function(data){
	    state.result_data=data;
	    compute_all_maxes();
	    state.getgeo_mode = state.request_data.getgeo_mode
	    
	    refresh_timeline_with_data();
	    refresh_map_with_micro();
	    reactivate_tabs();

	},
	failure: function(errMsg) {
            alert(errMsg);
	}
    });
    
}

/* ################## Timeline utilities ################## */
function incr_week(){
    ++state.current_time;
    redraw_cursor();
    refresh_map_with_micro();
    refresh_input()
}

function decr_week(){
    --state.current_time;
    if(state.current_time<0) state.current_time=0;
    redraw_cursor();
    refresh_map_with_micro();
    refresh_input();
}

function incr_max_weeks(){
    if(state.max_displayed_weeks<state.xdata.length) ++state.max_displayed_weeks;
    $("#semaine-max").html('Afficher sur '+state.max_displayed_weeks+' semaines')
    refresh_timeline();
}

function decr_max_weeks(){
    if(state.max_displayed_weeks>10) --state.max_displayed_weeks;
    $("#semaine-max").html('Afficher sur '+state.max_displayed_weeks+' semaines')
    refresh_timeline();
}


/* Initialise timeline */
function init_timeline(){
    /* keyboard shortcuts */
    $(document).on('keypress',function(e) {
	if(e.which == 43) {
	    incr_week();
	}
	if(e.which == 45) {
	    decr_week();
	}
    });

    refresh_timeline_with_data();
}

Date.prototype.getWeekNumber = function(){
  var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
  var dayNum = d.getUTCDay() || 7;
  d.setUTCDate(d.getUTCDate() + 4 - dayNum);
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
  return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
};


/* draw the current time on the timeline */
function redraw_cursor(){
    update = {
	shapes: [
	    {
		type:'rect',
		xref:'x',
		yref:'y',
		x0:state.current_time-0.5,
		y0:1.1*Math.min(-Math.max(...state.ydata_cases),Math.min(...state.ydata_gdb)),
		x1:state.current_time+0.5,
		y1:1.1*Math.max(Math.max(...state.ydata_cases),Math.max(...state.ydata_inactives)),
		opacity: 0.8,
		fillcolor:'#32a1c2',
		line:{
		    color:'#32a1c2'
		}
	    }
	]
    };
    Plotly.relayout('frise', update);
    date = new Date(state.initial_date)
    date.setDate(date.getDate() + 7*state.current_time)
    $("#semaine-affiche").text(date.toLocaleDateString("fr-FR")+" (S"+(state.initial_date.getWeekNumber()+state.current_time).toString()+")");
}

/* function to call when the timeline data has been changed */
function refresh_timeline_with_data(){
    state.xdata = [...Array(state.result_data.cases.length).keys()];
    state.ydata_cases = []//[state.result_data.cases[0].initial.reduce(function(a,b){return a+b;})]; 
    for(var l=0; l<state.result_data.cases.length-1; ++l){
	state.ydata_cases.push(state.result_data.cases[l+1].week.reduce(function(a,b){return a+b;}));
    }
    state.ydata_deaths = []//[state.result_data.deaths[0].initial.reduce(function(a,b){return a+b;})];
    for(var l=0; l<state.result_data.deaths.length-1; ++l){
	state.ydata_deaths.push(state.result_data.deaths[l+1].week.reduce(function(a,b){return a+b;}));
    }
    state.ydata_gdb = [];
    for(var l=1; l<state.result_data.cases.length; ++l){
	state.ydata_gdb.push(-state.result_data.gdp_accumulated_loss[l]);
    }
    state.ydata_ICU = []//[state.result_data.ICU_patients[0].initial.reduce(function(a,b){return a+b;})];
    for(var l=0; l<state.result_data.ICU_patients.length-1; ++l){
	state.ydata_ICU.push(state.result_data.ICU_patients[l+1].week.reduce(function(a,b){return a+b;}));
    }
    console.log(state.result_data.activity)
    state.ydata_inactives = [state.result_data.activity[0].initial.reduce(function(a,b, index){
	reg = state.request_data.zones[index];
	var population = state.request_data.population_size[index]
	var ratio_actif = state.request_data.initial_ratios_Salje20[reg].reduce(function(a,b){
	    return a + b.slice(1,5).reduce(function(c,d){return c+d});
	},0);
	return a+(7-b)*Math.round(population*ratio_actif)/7.0;}, 0)];
    for(var l=0; l<state.result_data.activity.length-1; ++l){
	state.ydata_inactives.push(state.result_data.activity[l+1].week.reduce(function(a,b, index){
	    reg = state.request_data.zones[index];
	    var population = state.request_data.population_size[index]
	    var ratio_actif = state.request_data.initial_ratios_Salje20[reg].reduce(function(a,b){
		return a + b.slice(1,5).reduce(function(c,d){return c+d});
	    },0);
	    return a+(7*(l+2)-b)*Math.round(population*ratio_actif)/7.0;}, 0));
    }
    state.ydata_inactives = state.ydata_inactives.map(function(a,index,arr){
	if(index > 0){
	    return a - arr[index-1];
	}
	else return a;
    });
    
    state.ydata_inactives = state.ydata_inactives.slice(1)
    refresh_timeline();
}

/* Redraw the timeline, for instance when the current time is updated */
function refresh_timeline(){
    var data_frise = [
	{
	    type: 'scatter',
	    name: 'Contaminés',
	    x: state.xdata.slice(0,state.max_displayed_weeks),
	    y: state.ydata_cases.slice(0,state.max_displayed_weeks),
	    line: {
		color: 'red',
		simplify: false,
	    }
	},
	{
	    type: 'scatter',
	    name: 'Economie',
	    x: state.xdata.slice(0,state.max_displayed_weeks),
	    y: state.ydata_gdb.slice(0,state.max_displayed_weeks),
	    yaxis: 'y2',
	    line: {
		color: 'orange',
		simplify: false,
	    }
	},
	{
	    type: 'scatter',
	    name: 'Lits de soins intensifs',
	    x: state.xdata.slice(0,state.max_displayed_weeks),
	    y: state.ydata_ICU.slice(0,state.max_displayed_weeks),
	    line: {
		color: 'blue',
		simplify: false,
	    }
	},
	{
	    type: 'scatter',
	    name: 'Morts',
	    x: state.xdata.slice(0,state.max_displayed_weeks),
	    y: state.ydata_deaths.slice(0,state.max_displayed_weeks),
	    line: {
		color: 'yellow',
		simplify: false,
	    }
	}
    ]
    
    if(state.request_data.selected_model == "micro"){
	data_frise.push({
	    type: 'scatter',
	    name: 'Inactifs',
	    x: state.xdata.slice(0,state.max_displayed_weeks),
	    y: state.ydata_inactives.slice(0,state.max_displayed_weeks),
	    line: {
		color: 'green',
		simplify: false,
	    }
	})
    }
    
    timeline_plot = Plotly.newPlot('frise', {
	data:data_frise,
	layout: {
	    paper_bgcolor:'rgba(0,0,0,0)',
	    plot_bgcolor:'rgba(0,0,0,0)',
	    legend: {
		font:{color:"#ffffff"}
	    },
	    margin: {
		l: 60,
		r: 20,
		b: 20,
		t: 10
	    },
	    xaxis: {tickmode: 'linear', title:'semaine',tick0:0,dtick:Math.round(10*(state.max_displayed_weeks/200)),ntick:state.xdata.length, automargin:true, fixedrange:true},
	    yaxis: {title:'Nb de personnes'},
	    yaxis2: {title: 'Points de PIB &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', side:'right', overlaying:'y'},
	    selectdirection:"v",
	    hovermode:'closest',
	    hoverdistance:-1
	},
	config: {responsive:true, displayModeBar: false}
    });
    redraw_cursor();
    
    $("#frise").on('plotly_click', function(_,data){
	state.current_time = data.points[0].x
	redraw_cursor();
	refresh_map_with_micro();
	refresh_input()
    });

    /* todo : upon double-clicking on a legend element, rescale vertically to fit the curve */
    /*$("#frise").on('plotly_legenddoubleclick', function(_,data){
	data.curveNumber
	Plotly.restyle('myDiv', update,[data.curveNumber]);
	return false;
    })*/
}

/* ##################     Simulation utilities    ################# */

function init_simulation(){
    $("#initial-valider").on("click", function(e){
	for(t in state.optim_params.compartmental_model_parameters){ 
	    for(field in state.request_data.compartmental_model_parameters[t]){
		state.request_data.compartmental_model_parameters[t][field] = JSON.parse($("#"+field).val());
	    }
	}
	var radio_start_id = $("input[name=radio-start]:checked").attr("id")

	$.getJSON(state.possible_starts[radio_start_id].filename, function(data){
	    for(field in data){
		if(field in state.optim_params){
		    state.optim_params[field] = JSON.parse(JSON.stringify(data[field]))
		}
	    }
	    state.initial_date.setDate(state.initial_date.getDate() + 7*state.possible_starts[radio_start_id].shift)
	    state.request_data.compartmental_model_params.micro.SimulationStart = 132 + 7*state.possible_starts[radio_start_id].shift
	    state.optim_params.compartmental_model_params.micro.SimulationStart = 132 + 7*state.possible_starts[radio_start_id].shift
	});
	deactivate_tabs()
	run_simulation()
    });	

    $("#initial-annuler").on("click", function(e){
	//well, nothing to do, the modal simply closes 
    });

    /* display the div to chose regions to draw when micro is selected */
    ["micro", "macro_Alizon20", "macro_Salje20"].forEach(function(d){
	$("#radio-"+d).on('click',{d:d}, function(e){
	    state.current_model = e.data.d
	    if($("#radio-micro").prop("checked")){
		$("#agent-visu-div").css("display","block");
	    }
	    else{
	
		$("#agent-visu-div").css("display","none"); 
	    }
	    
	    if($("#radio-macro_Salje20").prop("checked")){
		$("#salje-warning").css("display","block");
	    }
	    else{
		$("#salje-warning").css("display","none");
	    }
		
        if($("#radio-macro_Alizon20").prop("checked")){
		$("#alizon-warning").css("display","block");
	    }
	    else{
		$("#alizon-warning").css("display","none");
	    }
	    
	});
    })

    $('[name="radio-micro-geo-select"]').on("click",function(d){
	state.request_data['getgeo_mode'] = $('[name="radio-micro-geo-select"]:checked').attr("id").split('-')[3]
    });
    
    $("#model-relancer").on("click", function(){
	deactivate_tabs();

	state.optim_params["compartmental_model_parameters"]['common']['ICURatio'] = JSON.parse($("#alt-ICURatio").val());
	state.optim_params["compartmental_model_parameters"]['macro_Alizon20']['R0'] = JSON.parse($("#alt-R0").val());
	state.optim_params["compartmental_model_parameters"]['macro_Salje20']['R0'] = JSON.parse($("#alt-R0").val());

	state.request_data["compartmental_model_parameters"]['common']['ICURatio'] = JSON.parse($("#alt-ICURatio").val());
	state.request_data["compartmental_model_parameters"]['macro_Alizon20']['R0'] = JSON.parse($("#alt-R0").val());
	state.request_data["compartmental_model_parameters"]['macro_Salje20']['R0'] = JSON.parse($("#alt-R0").val());
	

	["micro", "macro_Alizon20", "macro_Salje20"].forEach(function(d){
	    if($("#radio-"+d).prop("checked")){
		state.current_model = d;
	    }  
	});
	state.request_data.get_geo_department = []
	state.request_data.zones.forEach(function(z){
	    if($("#check-micro-"+z).prop("checked")){
		state.request_data.get_geo_department.push(z)
	    }
	});
	
	run_simulation();
    });
}

function jump(h){
    var url = location.href;               //Save down the URL without hash.
    location.href = "#"+h;                 //Go to the target element.
    history.replaceState(null,null,url);   //Don't like hashes. Changing it back.
}


/* ##################     Optimisation utilities    ################# */
function init_optim(){
    $("#optim-valider").on("click", function(e){
	if(state.current_optimizer=="rl" && state.current_model != "macro_Alizon20"){
	    alert("L'apprentissage par renforcement n'est pour l'instant implémenté qu'avec le modèle Macro Alizon20.")
	    return;
	}

	
	deactivate_tabs()
	
	for(field in state.optim_params){
	    if(state.request_data.hasOwnProperty(field)){/* copy initial conditions and zones */
		state.optim_params[field] = state.request_data[field];
	    }
	    else if(field == "max_measure_steps"){
		state.optim_params[field] = parseInt($("#"+field).val())
	    }
	}
	for(field in state.optim_params.max_measure_values){
	    state.optim_params.max_measure_values[field] = parseInt($("#"+field).val())
	}
	
	state.request_data.selected_model = state.current_model
	state.optim_params.selected_model = state.current_model
	
	if(state.current_model.length>5 && state.current_model.slice(0,5)=="macro"){
	    state.optim_params.compartmental_model = state.current_model.slice(6)
	}
	state.optim_params.selected_optimizer = state.current_optimizer


	
	$.ajax({
	    type: "POST",
	    url: "/optimize",
	    data: JSON.stringify(state.optim_params),
	    contentType: "application/json; charset=utf-8",
	    dataType: "json",
	    success: function(data){
		if(!data.success){
		    alert("Erreur d'optimisation : arrive en général avec l'optimiseur RL lorsque la combinaison de mesures sort de celles qui ont été pré-générées. Utiliser celles-ci: [1,2,3,3,1], [0,0,0,3,0], [0,0,0,0,1] ou [1, 1, 2, 2, 1]")
		    reactivate_tabs();
		}
		else{
		    state.policies = JSON.parse(JSON.stringify(data.policies))
		    
		    select_optim_solution(0)
		    $("#optimization-screen").css('display','block')
		    jump("optimization-screen")
		    init_optim_screen()
		    reactivate_tabs();
		}
	    },
	    failure: function(errMsg) {
		alert(errMsg);
	    }
	}); 
    });
    
    $("#optim-annuler").on("click", function(e){
	$("#max_measure_steps").prop("checked", state.optim_params.max_measure_week)
	for(field in state.optim_params){
	    if(field != "zones" && field != "max_measure_week")
		$("#"+field).prop("checked", state.optim_params[field])
	} 
    });

    for(opt in state.optimizer_explain){
	$("#radio-"+opt).on('click', {"opt":opt}, function(d){
	    state.current_optimizer = d.data.opt;
	    for(opt2 in state.optimizer_explain){
		$("#explain-"+opt2).css("display","none")
	    }
	    $("#explain-"+d.data.opt).css("display","block")
	})
    }
    
}


/* ############################    Optimization screen utilities ############################## */

/* returns the true objectives (eg total nb of deaths) to be displayed */
function get_user_objective(i){
    obj = state.policies[i].objectives.slice()
    obj[0] = Math.round(1000*11*obj[0])/1000 /*times the number of measures*/
    
    obj[1] = 0
    for(var j=0; j<state.policies[i].deaths[state.policies[i].deaths.length-1].week.length; ++j){
	obj[1] += state.policies[i].deaths[state.policies[i].deaths.length-1].week[j] -  state.policies[i].deaths[0].initial[j]
    }
    obj[1] = Math.round(obj[1])
    
    obj[2] = Math.round(state.policies[i].gdp_accumulated_loss[state.policies[i].gdp_accumulated_loss.length-1]*1000)/1000
    obj[3] = 0
    for(var j=1; j<state.policies[i].ICU_patients.length; j++){
	ICUs = state.policies[i].ICU_patients[j].week
	for(var k=0; k<ICUs.length; ++k){
	    obj[3] += Math.max(0,ICUs[k]-state.optim_params.ICU_beds[k])
	}
    }
    obj[3] = Math.round(obj[3])
    
    obj[4] = Math.round(10000*obj[4])/10000
    return obj
}


function init_optim_rendering(callback){
    if(callback=="pareto"){
	state.pareto_x = state.optim_params.objectives[0]
	state.pareto_y = state.optim_params.objectives[1]
    }
    else if(callback=="radar"){
	
    }
    else if(callback=="solutions"){

	coord_select_x = '<input type="radio" class="x-select" name="optim-x-select" '
	coord_select_y = '<input type="radio" class="y-select" name="optim-y-select" '

	img_x_active = '<img class="select-label" src="/static/icon-x-active.png" style="width:30px; height:30px" '
	img_x_inactive = '<img class="select-label" src="/static/icon-x-inactive.png" style="width:30px; height:30px" '
	img_y_active = '<img class="select-label" src="/static/icon-y-active.png" style="width:30px; height:30px" '
	img_y_inactive = '<img class="select-label" src="/static/icon-y-inactive.png" style="width:30px; height:30px" '

	table_html = ["<tr><th> Légende </th><th> Sélectionner </th><th> Visible </th><th> N° de la solution </th>"];
	var index = 0
	for( var i=0; i<state.optim_params.objectives.length; ++i ){
	    obj = state.optim_params.objectives[i];
	    table_html.push('<th data-toggle="tooltip" data-placement="top" title="')
	    table_html.push(state.objectives_explain[obj].explain)
	    table_html.push('"> ')
	    table_html.push(state.objectives_explain[obj].name)
	    table_html.push('<div class="float-right">')
	    table_html.push(coord_select_x)
	    table_html.push(' onclick="set_optim_coordinate(&quot;x&quot;,&quot;')
	    table_html.push(obj)
	    table_html.push('&quot;)"')
	    table_html.push(' id="select-x-')
	    table_html.push(obj)
	    table_html.push('"><label for="select-x-')
	    table_html.push(obj)
	    table_html.push('">')
	    if(index==0) table_html.push(img_x_active)
	    else table_html.push(img_x_inactive)
	    table_html.push('id="select-x-')
	    table_html.push(obj)
	    table_html.push('-label"></label> ')
	    
	    table_html.push(coord_select_y)
	    table_html.push('id="select-y-')
	    table_html.push(obj)
	    table_html.push('" onclick="set_optim_coordinate(&quot;y&quot;,&quot;')
	    table_html.push(obj)
	    table_html.push('&quot;)">')
	    table_html.push('<label for="select-y-')
	    table_html.push(obj)
	    table_html.push('">')
	    if(index == 1) table_html.push(img_y_active)
	    else table_html.push(img_y_inactive)
	    table_html.push('id="select-y-')
	    table_html.push(obj)
	    table_html.push('-label"></label></div></th>')    
	    ++index
	}
	table_html.push('<th> Composition de la solution </th></tr>')
	
	for(var i=0; i<state.policies.length; ++i){ 
	    last_week = state.policies[i].deaths.length-1
	    r = get_user_objective(i)
	  
	    
	   
	    /* TODO: do not hardcode the static path (make template?)*/
	    table_html.push('<tr><td class="text-center">')
	    table_html.push('<svg style="width:30px;height:30px;"> <rect x="5" y="5" width="20" height="20" style="fill:'+color_palette[i]+';fill-opacity:0.5;stroke:'+color_palette[i]+'"/></svg></td>')
	    table_html.push('<td> <button class="btn btn-info" style="width:100%" onclick="select_optim_solution_jump('+i+')" id="select-optim-btn-'+i+'"> Sélectionner </button></td>')
	    if(i<state.max_default_visible_optim_solutions){
		table_html.push('<td><button type="button" class="btn btn-default" style="width:100%" onclick="toggle_optim_visible('+i+')">')
		table_html.push('<img id="button-visible-'+i+'" src="/static/eye-icon.png" style="width:20px;height:20px;"> </button></td>')
		state.visible_optim_solutions.add(i)
	    }
	    else{
		table_html.push('<td><button type="button" class="btn btn-default" style="width:100%" onclick="toggle_optim_visible('+i+')">')
		table_html.push('<img id="button-visible-'+i+'" src="/static/eye-closed-icon.png" style="width:20px;height:20px;"> </button></td>')
	    }
	    table_html.push("<td>"+i+"</td>")
	    for(var j=0; j<state.optim_params.objectives.length; ++j){
		table_html.push('<td>')
		table_html.push(get_user_objective(i)[j])
		table_html.push('</td>')
	    }
	    /* visual representation of all policies */
	    table_html.push('<td style="display:flex; flex-flow: row nowrap">')
	    for(policy in state.policy_explain){
		for(level in state.policy_explain[policy].explanation){
		    if(level == 0) continue;
		    pl = Array.from({length:state.request_data.zone_label_names.length }, v => 0);
		    state.request_data.zone_label_names.forEach(function(color, j){
			state.policies[i][policy].forEach(function(week,w){
			    if(w!=state.policies[i].measure_weeks.length-1)
				pl[j] += week.activated[j]==parseInt(level)?(state.policies[i].measure_weeks[w+1]-state.policies[i].measure_weeks[w]):0
			})
		    })
		    
		    if(pl.some(function(v){return v>0})){	
			table_html.push('<img src="/static/icon_'+policy+'_'+level+'.png" style="width:auto; height:30px;display:inline-block; background-color:#888888;margin-left:10px; border:solid; border-color:blue; border-width:1px" data-toggle="tooltip" data-html="true"  data-placement="top" title="'+state.policy_explain[policy].explanation[level.toString()]+'">')
			colors = ['red','green']/* todo : no hardcode */
			pl.forEach(function(v,i){
			    if(v>0) table_html.push('<div style="font-size:20px; height:50%;margin-top:25%display:inline-block;color:'+colors[i]+'">X'+pl[i]+'</div>')
			})
		    }
		}
	    }
	    table_html.push('</td>')
	    table_html.push("</tr>")
	}
	
	$("#table-optim-sols").html(table_html.join(''))
    }
  
    refresh_optim_screen(callback) 
}

function init_optim_screen(){
    /* activate golden layout */

    
    $.getJSON('/static/default_layout_optim.json', function(layout2){
	
	theta_radar = []
	for(var i=0; i<state.optim_params.objectives.length; ++i){
 	    theta_radar.push(state.objectives_explain[state.optim_params.objectives[i]].name_short)
	}
	
	state.radar_data = []
	for(var i=0; i<state.policies.length; ++i){
	    r = get_user_objective(i)
	    if(state.policies.length==1){
		for(var rr = 0; rr<r.length; ++rr){
		    r[rr] = 2.5
		}
	    }
	    hovertext = []
	    for(var j=0; j<state.optim_params.objectives.length; ++j){
		hovertext.push(state.objectives_explain[state.optim_params.objectives[j]].name+ ": "+ (get_user_objective(i)[j]).toString())
	    }
	    state.radar_data.push({
		type: 'scatterpolar',
		r: r,
		hovertext:hovertext,
		hoverinfo:'text',
		hoveron:'points',
		theta: theta_radar, 
		fill: 'toself',
		line:{
		    shape:"line",
		    color: color_palette[i % color_palette.length],
		},
		marker:{
		    color: color_palette[i % color_palette.length],	
		},	
	    })

	    
	}
	$("#optimGoldenComponent").html("")
	var optimLayout = new GoldenLayout( layout2 , $("#optimGoldenComponent"))
	optimLayout.registerComponent("optimComponent", function(container, componentState){
	    optimLayout.on('stateChanged', function(){
		refresh_optim_screen("pareto")
		refresh_optim_screen("radar")
	    });
	    
	    
	    $.get(componentState.href, function(data){
		container.getElement().html(data);
		if(componentState  !== null){
		    init_optim_rendering(componentState.callback);
		    $('[data-toggle="tooltip"]').tooltip({html:true})
		}
            });
	});

	$(window).resize(function () {
	    optimLayout.updateSize($(window).width(), $(window).height());
	});
	
	optimLayout.init()
    })
}

/*set the i-th solution in state policies as the active one*/
function select_optim_solution(i){
    state.active_policy = i;
    for(field in state.policies[state.active_policy]){
	if(state.request_data.hasOwnProperty(field)) state.request_data[field]= JSON.parse(JSON.stringify(state.policies[state.active_policy][field]))
	if(state.result_data.hasOwnProperty(field)) state.result_data[field]= JSON.parse(JSON.stringify(state.policies[state.active_policy][field]))
    }
   
    dummy_false = [];
    for(var j=0; j<state.request_data.zones.length; ++j) dummy_false.push(0);
    
    for(field in state.request_data){
	if(state.request_data[field] == null){ /* when the optimizer returns a null array, fill it with coherent data. */
	    state.request_data[field] = []
	    for(var j = 0; j<state.request_data.measure_weeks.length; ++j){
		state.request_data[field].push({"activated" : dummy_false.slice()})
	    }
	}
    }

    compute_all_maxes();
    
    refresh_timeline_with_data();
    refresh_input();
    refresh_map_with_micro();
    refresh_current_policy();
}

function select_optim_solution_jump(i){
    select_optim_solution(i);

    for(var i=0; i<state.policies.length; ++i){
	$("#select-optim-btn-"+i).prop('disabled', false)
    }
    $("#select-optim-btn-"+state.active_policy).prop('disabled', true)
    
    jump("main-screen");
}

/*what to do when you click on a visibility button*/
function toggle_optim_visible(i){
    button = $("#button-visible-"+i)
    if(state.visible_optim_solutions.has(i)){
	button.attr("src","/static/eye-closed-icon.png")
	state.visible_optim_solutions.delete(i)
    }
    else{
	button.attr("src","/static/eye-icon.png")
	state.visible_optim_solutions.add(i)
    }
    refresh_optim_screen("radar")
}

function set_optim_coordinate(axis, name){
    if(axis=="x") state.pareto_x = name;
    else state.pareto_y = name;

    state.optim_params.objectives.forEach(function(n){
	$("#select-"+axis+"-"+n+"-label").attr('src',"/static/icon-"+axis+"-inactive.png")
    });
    $("#select-"+axis+"-"+name+"-label").attr('src',"/static/icon-"+axis+"-active.png")
    
    refresh_optim_screen("pareto");
}


/* refreshing utility for the optimization screen */
function refresh_optim_screen(component){
    /* left graph : radar graph */
    var radar_data = [];
    if(state.radar_data.length>1){
	mins = state.optim_params.objectives.map(function(v, i){
	    return state.radar_data.reduce(function(a,b){if(b.r[i]>a.r[i]) return a; return b;}).r[i]
	})
	maxs = state.optim_params.objectives.map(function(v,i){
	    return state.radar_data.reduce(function(a,b){if(b.r[i]>a.r[i]) return b; return a;}).r[i]
	})
	
	radar_data = state.radar_data.map(function(item, index){
	    if(state.visible_optim_solutions.has(index)){
		b = JSON.parse(JSON.stringify(item))
		b.r = item.r.map(function(val, i){
		    if(maxs[i]==mins[i]) return 2.5;
		    return 1+4*(val - mins[i])/(maxs[i]-mins[i])
		})
		return b;
	    }
	    else return null;
	})
	radar_data = radar_data.filter(i => i !==null)
    }
    else{
	a = state.radar_data[0];
	b = JSON.parse(JSON.stringify(a))
	for(var i = 0; i<b.r.length; ++i){
	    b[i] = 2.5;
	}
	radar_data.push(b)
    }
    
    radar_layout = {
	paper_bgcolor:'rgba(0,0,0,0)',

	margin: {
	    l: 30,
	    r: 30,
	    b: 30,
	    t: 30
	},
	polar: {
	    radialaxis: {
		visible: false,
		range: [0, 5]
	    },
	    bgcolor:'rgba(0,0,0,0)',
	    bordercolor:'rgba(255,255,255,255)',
	    textfont:{
		color:'rgba(255,255,255,255)'
	    },
	    angularaxis: {
		tickwidth: 2,
		linewidth: 3,
		color: "rgb(0,153,204)",
		layer: "below traces"
            },
            radialaxis: {
		side: "counterclockwise",
		showline: false,
		tickwidth: 0,
		showgrid: true,
		color: "rgb(0,153,204)",
		ticks: [1,2,3,4,5],
		showticklabels: false,
            },
	},
	showlegend: false
    }
    if(component=="radar"){
	if(radar_data.length>0){
	    $("#radar-graph").show()
	    Plotly.newPlot("radar-graph", radar_data, radar_layout,{responsive:true, displayModeBar: false})
	}
	else{
	    $("#radar-graph").hide()
	}
    }

	
    /* right graph : Pareto front */
    if(component=="pareto"){
	var index_x, index_y;
	index_x = state.optim_params.objectives.findIndex(i => i==state.pareto_x)
	index_y = state.optim_params.objectives.findIndex(i => i==state.pareto_y)
	names = state.optim_params.objectives.map(i => state.objectives_explain[i].name)
	
	var xdata=[], ydata=[];
	for(var i=0; i<state.radar_data.length; ++i){
	    xdata.push(state.radar_data[i].r[index_x])
	    ydata.push(state.radar_data[i].r[index_y])
	}

	var trace = [{
	    type:"scatter",
	    mode:"markers",
	    x:xdata,
	    y:ydata,
	    marker:{
		size:20,
		symbol:"square",
		color:color_palette.slice(0,xdata.length).map(function(a){
		    return "rgba("+parseInt('0x'+a[1]+a[2])+","+parseInt('0x'+a[3]+a[4])+","+parseInt('0x'+a[5]+a[6])+",0.5)"
		}),
		line:{
		    width:2,
		    color:color_palette.slice(0,xdata.length).map(function(a){
			return "rgba("+parseInt('0x'+a[1]+a[2])+","+parseInt('0x'+a[3]+a[4])+","+parseInt('0x'+a[5]+a[6])+",0.5)"
		    }),
		},
	    }
	}]
	
	var layout = {
	    paper_bgcolor:'rgba(0,0,0,0)',
	    plot_bgcolor:'rgba(0,0,0,0)',
	    titlefont: {
		family: 'Arial, sans-serif',
		size: 18,
		color: 'rgb(0,153,204)'
	    },
	    margin: {
		l: 80,
		r: 10,
		b: 60,
		t: 20
	    },
	    xaxis: { color: "rgb(255,255,255)", title:names[index_x]},
	    yaxis: { color: "rgb(255,255,255)", title:names[index_y]}
	};
	
	Plotly.newPlot("pareto-front", trace, layout,{responsive:true, displayModeBar: false})
    }
}




/* callback to call once all the canvas are initialized */
async function init_rendering(name){
    if(name=="state"){
	init_state();
    }
    if(name=="map"){
	init_map();
    }
    if(name=="reg_stats"){
	init_bar();
    }
    if(name=="policies"){
	init_input();
    }
    if(name=="timeline"){
	init_timeline();
    }
    if(name=="optim"){
	init_optim();
    }
    if(name=="simulation"){
	init_simulation();
    }
}
