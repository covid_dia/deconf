function perc2color(perc,min,max) {
    var base = (max - min);
    
    if (base == 0) { perc = 100; }
    else {
        perc = (perc - min) / base * 100; 
    }
    var r, g, b = 0;
    if (perc < 50) {
        r = 255;
        g = Math.round(5.1 * perc);
    }
    else {
        g = 255;
        r = Math.round(510 - 5.10 * perc);
    }
    var h = r * 0x10000 + g * 0x100 + b * 0x1;
    return '#' + ('000000' + h.toString(16)).slice(-6);
}


function render_contact(){
    d3.select("#tab-contact").html("")
    d3.select("#chord").html("")
    
    d3.csv("/static/"+$("#input-chemin").val()).then(function(rows){
	tab = []
	for(j=0; j<rows.length; ++j){
	    t = [];
	    for(i in rows[0]){
		if(i!=""){
		    t.push(parseFloat(rows[j][i]))
		}
	    }
            tab.push(t)
	}

	min = tab.map(function(data){
	    return data.reduce(function(a,b){return Math.min(a,b)})
	}).reduce(function(a,b){return Math.min(a,b)});
	
	max = tab.map(function(data){
	    return data.reduce(function(a,b){return Math.max(a,b)})
	}).reduce(function(a,b){return Math.max(a,b)});

	col = d3.scaleOrdinal(d3.schemeCategory10)
	colors = []
	for(i=0;i<tab.length; ++i){
	   colors.push(col(i))
	}
	
	trth = d3.select("#tab-contact").append("tr")
	trth.append('td')
	keys = []
	index = 0
	for(i in rows[0]){
	    if(i!="") {
		keys.push(i)
		trth.append('th')//.style("background-color",colors[index-1])
		    .text(i)
	    }
	    index++;
	}
	
	for(i=0; i<tab.length; ++i){
	    tr = d3.select("#tab-contact").append("tr")
	    tr.append('th')//.style("background-color",colors[i])
		.text(keys[i])
	    tab[i].forEach(function(d){
		tr.append("td").style("background-color",perc2color(d,min,max))
		    .style("color","black").text(Math.round(100*d)/100)
            })
	}
	draw_chord(tab, keys)
    })

    
}

function draw_chord(data, keys){
    var width=400
    var height=400
    
    var outerRadius = Math.min(width, height) * 0.5
    var innerRadius = outerRadius-60
    
    chord = d3.chord()
	.padAngle(.04)
	.sortSubgroups(d3.descending)
	.sortChords(d3.descending)

    arc = d3.arc()
	.innerRadius(innerRadius)
	.outerRadius(innerRadius + 20)

    ribbon = d3.ribbon()
	.radius(innerRadius)

    color = d3.scaleOrdinal(d3.schemeCategory10)

    
    const svg = d3.select('#chord').append("svg")
	  .attr("viewBox", [-width / 2, -height / 2, width, height])
	  .attr("font-size", 10)
	  .attr("font-family", "sans-serif")
	  .style("width", "100%")
	  .style("height", "auto");

    console.log(data)
    const chords = chord(data);

    const group = svg.append("g")
	  .selectAll("g")
	  .data(chords.groups)
	  .join("g");

    console.log(group)
    
    group.append("path")
	.attr("fill", d => color(d.index))
	.attr("stroke", d => color(d.index))
	.attr("d", arc);

    group.append("text")
	.each(d => { d.angle = (d.startAngle + d.endAngle) / 2; })
	.attr("dy", ".35em")
	.attr("transform", d => `
        rotate(${(d.angle * 180 / Math.PI - 90)})
        translate(${innerRadius + 26})
        ${d.angle > Math.PI ? "rotate(180)" : ""}
      `)
	.attr("text-anchor", d => d.angle > Math.PI ? "end" : null)
    	.attr('fill', 'white')
	.text(d => keys[d.index]);


    svg.append("g")
	.attr("fill-opacity", 0.67)
	.selectAll("path")
	.data(chords)
	.join("path")
	.attr("stroke", d => d3.rgb(color(d.source.index)).darker())
	.attr("fill", d => color(d.source.index))
	.attr("d", ribbon);
    
}

