TODOLIST

Fonctions

- Ajouter les DOM/TOM sur le bord de la carte
- Gérer un état intermédiaire dans le mode d'édition "national"
- Sélectionner/Déselectionner tout d'un coup dans les mesures 
- Annulation d'une action
- Plus de raccourcis clavier
- Ajouter un bouton play pour éviter d'avoir à maintenir appuyé '+'
- gestion des utilisateurs : login/save/gestion configuration


Code
- améliorer la structure des dossiers pour ne pas avoir les assets dans le dossier src
- rendre l'affichage des paramètres en entrée du simulateur moins hard-coded par rapport aux champs de l'API -> quasi bon
- sécurité : passer à un serveur WSGI + robustifier les inputs utilisateurs + gérer des listes d'exclusion d'IP
- remplacer les appels linéaires pour scanner les tableaux rangés "measure_weeks" (par ex: dichotomie)