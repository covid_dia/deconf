#include "optim/brute_force_optim.h"
#include "gtest/gtest.h"

template<class T>
string vec2str(const vector<T> & s) {
  ostringstream out;
  for (auto & v : s) out << v << ", ";
  return out.str();
}

template<class T>
inline void assert_vecs_near(const vector<T>& a, const vector<T>& b, double epsilon = 1e-5) {
  ASSERT_EQ( a.size(), b.size() );
  for (unsigned int pi = 0; pi < a.size(); ++pi)
    ASSERT_NEAR(a[pi], b[pi], epsilon) << "a: " << vec2str(a) << endl << "b: " << vec2str(b);
}

template<class T>
inline void assert_vecs_equal(const vector<T>& a, const vector<T>& b) {
  ASSERT_EQ( a.size(), b.size() );
  for (unsigned int pi = 0; pi < a.size(); ++pi)
    ASSERT_EQ(a[pi], b[pi]) << "a: " << vec2str(a) << endl << "b: " << vec2str(b);
}

inline void assert_vecs2_equal(const vector<StepPolicy> & A, const vector<StepPolicy> & B) {
  ASSERT_TRUE(A.size() == B.size());
  for (unsigned int pi = 0; pi < A.size(); ++pi)
    assert_vecs_equal(A[pi], B[pi]);
}

//! read CSV, skip first row, skip first, second and last columns
typedef vector<vector<double> > Mat2d;
Mat2d csv2vectors_skip_row1_col12n(const string & filename) {
  Mat2d ans;
  ifstream file(filename);
  string line = "";
  getline(file, line); // skip first row
  // Iterate through each line and split the content using delimeter
  while (getline(file, line)) {
    ans.push_back(vector<double>());
    vector<string> vec;
    boost::algorithm::split(vec, line, boost::is_any_of(",;"));
    for (unsigned int i = 2; i < vec.size()-1; ++i) // skip first and last columns
      ans.back().push_back( atof(vec[i].c_str()) );
  }
  file.close();
  return ans;
}

// Pareto //////////////////////////////////////////////////////////////////////
TEST(pareto, SimpleTest) {
  ParetoFront<string> front (3);
  ASSERT_TRUE( front.dominates({2, 2, 3}, {3, 3, 3}));
  ASSERT_FALSE (front.dominates({2, 2, 3}, {3, 3, 2}));
  ASSERT_TRUE( front.dominates({3, 2, 3}, {3, 3, 3}));
  ASSERT_TRUE( front.dominates({3, 2, 1}, {3, 3, 1}));

  ASSERT_TRUE( front.update("acc", {1, 3, 3}));
  assert_vecs_equal({"acc"}, front._eval_policy_reqs);
  ASSERT_FALSE (front.update("acc", {1, 3, 3}));

  ASSERT_TRUE( front.update("cca", {3, 3, 1}));
  assert_vecs_equal({"acc", "cca"}, front._eval_policy_reqs);
  ASSERT_LT(1E10, front.previous_in_front(0)); // none
  ASSERT_EQ(3, front.previous_in_front(2)); // acc
  ASSERT_EQ(3, front.previous_in_front(3)); // cca
  ASSERT_EQ(3, front.previous_in_front(4)); // cca

  ASSERT_TRUE( front.update("cba", {3, 2, 1}));
  assert_vecs_equal({"acc", "cba"}, front._eval_policy_reqs);
  ASSERT_LT(1E10, front.previous_in_front(0)); // none
  ASSERT_EQ(3, front.previous_in_front(2)); // acc
  ASSERT_EQ(2, front.previous_in_front(3)); // cba
  ASSERT_EQ(2, front.previous_in_front(4)); // cba

  ASSERT_TRUE( front.update("bca", {2, 3, 1}));
  assert_vecs_equal({"acc", "cba", "bca"}, front._eval_policy_reqs);
  ASSERT_LT(1E10, front.previous_in_front(0)); // none
  ASSERT_EQ(3, front.previous_in_front(1)); // acc
  ASSERT_EQ(3, front.previous_in_front(1.5)); // acc
  ASSERT_EQ(3, front.previous_in_front(2)); // bca
  ASSERT_EQ(3, front.previous_in_front(2.5)); // bca
  ASSERT_EQ(2, front.previous_in_front(3)); // cba
  ASSERT_EQ(2, front.previous_in_front(3.5)); // cba

  ASSERT_TRUE( front.update("bba", {2, 2, 1}));
  assert_vecs_equal({"acc", "bba"}, front._eval_policy_reqs);

  ASSERT_TRUE( front.update("aaa", {1, 1, 1}));
  assert_vecs_equal({"aaa"}, front._eval_policy_reqs);
  front.sort_by_objectives0();
}

// Economie ////////////////////////////////////////////////////////////////////
// test_economie.py::test_evaluate_one_all_zones
TEST(economie, test_evaluate_one_all_zones) {
  Economie eco;
  // expected values obtained from economie.py
  ASSERT_NEAR(0, eco.evaluate_one_week({0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}), 1E-7);
  ASSERT_NEAR(0.4353576923076925, eco.evaluate_one_week({1, 1, 2, 2, 1}, {1, 1, 2, 2, 1}), 1E-7);
  ASSERT_NEAR(0.6039230769230788, eco.evaluate_one_week({2, 2, 3, 3, 1}, {2, 2, 3, 3, 1}), 1E-7);
}

TEST(economie, test_evaluate_one_week_one_label) {
  Economie eco;
  // expected values obtained from economie.py
  ASSERT_NEAR(0, eco.evaluate_one_week_one_label(0, {0, 0, 0, 0, 0}), 1E-7);
  ASSERT_NEAR(0, eco.evaluate_one_week_one_label(1, {0, 0, 0, 0, 0}), 1E-7);
  // 1,1,2,2,1,0,0,0,0,0,0.19348000066642496
  ASSERT_NEAR(0.19348000066642496, eco.evaluate_one_week_one_label(0, {1, 1, 2, 2, 1}), 1E-7);
  // 0,0,0,0,0,1,1,2,2,1,0.24187769164126718
  ASSERT_NEAR(0.24187769164126718, eco.evaluate_one_week_one_label(1, {1, 1, 2, 2, 1}), 1E-7);
}

// SEAIR ///////////////////////////////////////////////////////////////////////

// test_macromodel.py::test_covid_ode_alizon
inline void test_seair(int ndays, const SEAIRState & exp_y, double epsilon) {
  SEAIRState y = default_y0();
  SEAIR model(1E-6); // a bit of migration
  model.compute_virulence_beta(y[0]);
  ASSERT_NEAR(0.140134529147982, model.beta, 1E-5);
  ASSERT_NEAR(0.00882352941176471, model.virulence, 1E-5);
  model.integrate(y, 0, 0.2, ndays);
  assert_vecs_near(y, exp_y, epsilon);
}

// test_macromodel.py::test_measures2c
TEST(seair, test_measures2c) {
  Measures2c measures2c;
  // [contact_tracing, forced_telecommuting, school_closures, social_containment_all, total_containment_high_risk]
  ASSERT_NEAR(0.00000000, measures2c.at({0, 0, 0, 0, 0}), 1E-5);
  ASSERT_NEAR(0.11227087, measures2c.at({0, 0, 0, 0, 1}), 1E-5);
  ASSERT_NEAR(0.13747454, measures2c.at({0, 0, 0, 1, 0}), 1E-5);
  ASSERT_NEAR(0.27494908, measures2c.at({0, 0, 0, 2, 0}), 1E-5);
  ASSERT_NEAR(0.41242362, measures2c.at({0, 0, 0, 3, 0}), 1E-5);
  ASSERT_NEAR(0.06873727, measures2c.at({0, 0, 1, 0, 0}), 1E-5);
  ASSERT_NEAR(0.16496945, measures2c.at({0, 0, 2, 0, 0}), 1E-5);
  ASSERT_NEAR(0.26120162, measures2c.at({0, 0, 3, 0, 0}), 1E-5);
  ASSERT_NEAR(0.13747454, measures2c.at({0, 1, 0, 0, 0}), 1E-5);
  ASSERT_NEAR(0.27494908, measures2c.at({0, 2, 0, 0, 0}), 1E-5);
  ASSERT_NEAR(0.90000000, measures2c.at({0, 2, 3, 3, 1}), 1E-5);
}

// test_macromodel.py::test_no_control
TEST(seair, test1) {
  test_seair(1, {1.0000000, 5.623195e-07, 3.258251e-07, 2.446046e-08, 2.692138e-09, 6.247994e-08, 3.620279e-08,
                 2.723693e-09, 2.492727e-10, 4.398929e-11}, 1e-6);
}
TEST(seair, test2) {
  test_seair(2, {0.9999999, 6.887307e-07, 1.027335e-06, 1.628479e-07, 1.458362e-08, 7.652563e-08, 1.141483e-07,
                 1.812547e-08, 1.350773e-09, 2.383716e-10}, 1e-6);
}
TEST(seair, test100) {
  test_seair(100, {0.9555452, 2.758085e-03, 0.0058576275, 0.0175728454, 0.0139107583, 3.064539e-04, 6.508475e-04,
                   1.969708e-03, 1.299200e-03, 2.292706e-04}, 5E-3);
}
TEST(seair, test730) {
  test_seair(730, {0.1070968, 1.229087e-06, 3.072727e-06, 2.048528e-05, 0.8042451, 1.365652e-07, 3.414141e-07, 2.321666e-06,
                   0.07595644, 0.0134040}, 1E-4);
}
TEST(seair, pop100) {
  int npop = 100;
  SEAIRState y = {100, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  ASSERT_NEAR(npop, accumulate(y.begin(), y.end(), 0.), .1) << vec2str(y);
  SEAIR model(1E-6 * npop); // a bit of migration
  model.compute_virulence_beta(y[0]);
  model.integrate(y, 0, .2 * npop, 100);
  ASSERT_NEAR(npop, accumulate(y.begin(), y.end(), 0.), .1) << vec2str(y);
}
// test_macromodel.py::test_big_pop
TEST(seair, test_big_pop) {
  int npop = 200 * 1000;
  // I1 = I2 = 0.05
  SEAIRState y = {.99, 0, 0, .005, 0,  0, 0, .005, 0,  0};
  SEAIR model(0); // no migration
  model.compute_virulence_beta(y[0]);

  ASSERT_GE(2*365, model.integrate(y, 0, .2 * npop, 2*365));
  ASSERT_NEAR(1, accumulate(y.begin(), y.end(), 0.), .1 / npop) << vec2str(y);
  double error = 1;
  assert_vecs_near(y, {2.08491120e+04 / npop, 4.29528405e-10 / npop,
                       2.50357554e-09 / npop, 4.84517564e-08 / npop,
                       1.60435799e+05 / npop, 4.77255067e-11 / npop,
                       2.78175502e-10 / npop, 5.79456245e-09  / npop,
                       1.59078255e+04 / npop, 2.80726332e+03 / npop}, error);
}
// test_macromodel.py::test_SEAIRModel_nocontrol
TEST(seair, test_SEAIRModel_nocontrol) {
  SEAIRState y = default_y0();
  SEAIR model(1E-6); // a bit of migration
  model.compute_virulence_beta(y[0]);
  Mat2d exp_y = csv2vectors_skip_row1_col12n(DECOV_PATH "/test/nocontrol_alizon_params_code.csv");
  ASSERT_EQ(731, exp_y.size());
  double error = 1e-3;
  for (unsigned int wi = 0; wi < 731; ++wi) {
    assert_vecs_near(exp_y[wi], y, error);
    double c = 0;
    model.integrate(y, c, 0.2, 1);
  }
}

TEST(seair, 2A) {
  SEAIRState y = {0.9967520209705903, 2.905854560596936e-06, 3.228727289099715e-07,
                       7.798491657432273e-06, 8.66499072990144e-07, 0.00014162055075409507,
                       1.6431142042823547e-05, 0.002770856229495769, 0.0002611007807329969,
                       4.6076608363716246e-05};
  int npop = 8026685;
  SEAIR model(0); // no migration
  model.compute_virulence_beta(y[0]);
  ASSERT_NEAR(1, accumulate(y.begin(), y.end(), 0.), .1) << vec2str(y);

  model.integrate(y, 0, 559. / npop, 100); // 559 ICU beds, 100 days
  ASSERT_NEAR(1, accumulate(y.begin(), y.end(), 0.), 50. / npop) << vec2str(y); // close to 50 people!
}

TEST(seair, mean_icu_patients) {
  SEAIR seair;
  seair.icu_times.push_back(1);
  seair.icu_times.push_back(4);
  seair.icu_ratios.push_back(0);
  seair.icu_ratios.push_back(.5);
  ASSERT_NEAR(.25, seair.mean_icu_patients(), 1e-5);

  seair.icu_times.push_back(7);
  seair.icu_ratios.push_back(1);
  ASSERT_NEAR(.5, seair.mean_icu_patients(), 1e-5);
}

// mkstep_policies /////////////////////////////////////////////////////////////
TEST(mkstep_policies, test2) {
  vector<StepPolicy> possible_step_policies;
  mkstep_policies({2}, possible_step_policies);
  ASSERT_EQ(2, possible_step_policies.size());
  assert_vecs2_equal({{0}, {1}}, possible_step_policies);
}
TEST(mkstep_policies, test121) {
  vector<StepPolicy> possible_step_policies;
  mkstep_policies({1, 2, 1}, possible_step_policies);
  ASSERT_EQ(2, possible_step_policies.size());
  assert_vecs2_equal({{0,0,0}, {0,1,0}}, possible_step_policies);
}
TEST(mkstep_policies, test23) {
  vector<StepPolicy> possible_step_policies;
  mkstep_policies({2, 3}, possible_step_policies);
  ASSERT_EQ(6, possible_step_policies.size());
  assert_vecs2_equal({{0,0}, {0,1}, {0,2}, {1,0}, {1,1}, {1,2}}, possible_step_policies);
  //for (auto wp : possible_step_policies) { for (int v : wp) printf("%i", v); printf("  "); } printf("\n");
}
TEST(mkstep_policies, test332) {
  vector<StepPolicy> possible_step_policies;
  mkstep_policies({3, 3, 2}, possible_step_policies);
  ASSERT_EQ(18, possible_step_policies.size());
  assert_vecs2_equal({{0,0,0}, {0,0,1}, {0,1,0}, {0,1,1}, {0,2,0}, {0,2,1},
                      {1,0,0}, {1,0,1}, {1,1,0}, {1,1,1}, {1,2,0}, {1,2,1},
                      {2,0,0}, {2,0,1}, {2,1,0}, {2,1,1}, {2,2,0}, {2,2,1}}, possible_step_policies);
  //for (auto wp : possible_step_policies) { for (int v : wp) printf("%i", v); printf("  "); } printf("\n");
}
TEST(mkstep_policies, test33442) {
  vector<StepPolicy> possible_step_policies;
  mkstep_policies({3,3,4,4,2}, possible_step_policies);
  ASSERT_EQ(288, possible_step_policies.size());
}
TEST(mkstep_policies, measures2possible_step_policies) {
  vector<StepPolicy> possible_step_policies;
  measures2possible_step_policies(possible_step_policies, 2, 2, 3, 3, 1);
  ASSERT_EQ(288, possible_step_policies.size());
}

// total_pop_per_label /////////////////////////////////////////////////////////
TEST(total_pop_per_label, test1) {
  vector<double> pops;
  total_pop_per_label(pops, 2, {0, 1, 0, 1}, {10, 20, 30, 40});
  ASSERT_EQ(2, pops.size());
  ASSERT_EQ(10 + 30, pops.front());
  ASSERT_EQ(20 + 40, pops.back());
}

// mix_paretos /////////////////////////////////////////////////////////////////
TEST(mix_paretos, test1) {
  StepPolicy sp0(NMEASURES, 0), sp1(NMEASURES, 1);
  vector<StepPolicy> possible_step_policies = {sp0, sp1};
  PolicyPareto p1(NOBJECTIVES), p2(NOBJECTIVES);
  unsigned int max_measure_steps = 3;
  p1.update({0, 1, 0}, {1, 2, 3, 4}); //     sp0, sp1, sp0
  p2.update({1, 0, 1}, {10, 20, 30, 40}); // sp1, sp0, sp1
  Economie eco;
  ZonesPolicyPareto ans(NOBJECTIVES);
  mix_paretos(ans, 1, p1, 2, p2, max_measure_steps, eco, possible_step_policies);
  // checks
  ASSERT_EQ(1, ans.size());
  ZonesStepPoliciesHist exp = {{0,1}, {1,0}, {0,1}};
  ASSERT_EQ(exp, ans._eval_policy_reqs.front());
  Objectives omix = ans._objectives.front();
  ASSERT_EQ(.5 * 1 + .5 * 10, omix[0]);
  ASSERT_EQ((1 * 2 + 2 * 20) / 3, omix[1]);
  ASSERT_EQ(eco.evaluate_one_week_one_label(1, sp1) * 2
            + eco.evaluate_one_week_one_label(0, sp1), omix[2]);
  ASSERT_EQ((1 * 4 + 2 * 40) / 3, omix[3]);
}

// brute_force /////////////////////////////////////////////////////////////////
void brute_force_alizon(int nsteps) {
  //! [contact_tracing, forced_telecommuting, school_closure, social_containment_all, total_containment_high_risk]
  SEAIR seair(1E-6); // a bit of migration
  SEAIRState ratios = default_y0();
  // s.compute_virulence_beta(...) will be done in brute_force()
  ZonesPolicyPareto front(NOBJECTIVES);
  vector<StepPolicy> possible_step_policies; // 2 labels
  measures2possible_step_policies(possible_step_policies, 1, 1, 2, 2, 1);
  // steps of one week
  brute_force(front, {"zone1", "zone2"}, 2, {0, 1}, {1, 1}, {.2, .2}, {ratios, ratios},
              seair, nsteps, 1, possible_step_policies);
}

TEST(brute_force, alizon1) { brute_force_alizon(1); }
TEST(brute_force, alizon2) { brute_force_alizon(2); }
//TEST(brute_force, alizon3) { brute_force_alizon(3); }
//TEST(brute_force, alizon4) { brute_force_alizon(4); }
//TEST(brute_force, alizon5) { brute_force_alizon(5); }

void test_Corse(int nsteps) {
  SEAIR seair(0); // no migration
  SEAIRState ratios2a = {0.968767628631305, 0.0009812615086548175,
                         0.00020226812654821876, 0.0010845927786439365,
                         0.02793826840737677, 3.346448150209631e-05,
                         6.898057163799109e-06, 0.000347934872499635,
                         0.00031974506639406074, 0.00032340472207561274};
  SEAIRState ratios2b = {0.9854221301432934, 0.00047872212851657206,
                         9.852666395942594e-05, 0.0005242474998069962,
                         0.012999999252834476, 1.632611457097569e-05,
                         3.360107060607553e-06, 0.0001637100530804855,
                         0.00014821179111450082, 0.00015023491093505315};
  // s.compute_virulence_beta(...) will be done in brute_force()
  ZonesPolicyPareto front(NOBJECTIVES);
  vector<StepPolicy> possible_step_policies;
  //! [contact_tracing, forced_telecommuting, school_closure, social_containment_all, total_containment_high_risk]
  measures2possible_step_policies(possible_step_policies, 1, 1, 2, 2, 1);
  // steps of three weeks
  brute_force(front, {"2A", "2B"}, 2, {0, 1}, {162421, 182258}, {27, 31},
  {ratios2a, ratios2b}, seair, nsteps, 3, possible_step_policies);
}
TEST(brute_force, Corse1) { test_Corse(1); }
TEST(brute_force, Corse2) { test_Corse(2); }
//TEST(brute_force, Corse3) { test_Corse(3); }
//TEST(brute_force, Corse4) { test_Corse(4); }
//TEST(brute_force, Corse5) { test_Corse(5); }
//TEST(brute_force, Corse6) { test_Corse(6); }
//TEST(brute_force, Corse7) { test_Corse(7); }

void test_France(int nsteps) {
  nlohmann::json json_in;
  std::ifstream jsonFile(DECOV_PATH "/doc/optimize_req.json");
  jsonFile >> json_in;
  json_in["max_measure_steps"] = nsteps;
  brute_force_json(json_in);
}
TEST(brute_force, France1) { test_France(1); }
TEST(brute_force, France2) { test_France(2); }
//TEST(brute_force, France3) { test_France(3); }

int main(int argc, char **argv) {
  srand(42); // fixed seed
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
