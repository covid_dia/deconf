import threading
import unittest

from src.model.macromodel import MacroModelRequestHandler, MacroModelServer
from src.optim.base_optim import BaseOptimServer
from src.optim.branch_and_bound.enumerate_all_optim import EnumerateAllOptimRequestHandler
from test.test_base_optim import BaseOptimServerTests


class EnumerateAllOptimServerTests(BaseOptimServerTests):
    def setUp(self):
        # Start a ModelServer and an OptimServer
        self.model_server = MacroModelServer(('localhost', 8000), MacroModelRequestHandler)
        self.optim_server = BaseOptimServer(('localhost', 8001), EnumerateAllOptimRequestHandler)
        model_thread = threading.Thread(target=self.model_server.serve_forever)
        model_thread.start()
        optim_thread = threading.Thread(target=self.optim_server.serve_forever)
        optim_thread.start()

    def tearDown(self):
        self.model_server.shutdown()
        self.model_server.socket.close()
        self.optim_server.shutdown()
        self.optim_server.socket.close()

    def test_optimize_empty(self):
        self._test_optimize_empty()

    def test_optimize_bad_measure(self):
        self._test_optimize_bad_measure()

    def test_optimize_1z_1c_1m_1max_1dur(self):
        self._test_optimize_generic(nzones=1, ncolors=1, nmeasures=1, maxsteps=1, step_duration=1)

    def test_optimize_1z_1c_4m_1max_1dur(self):
        self._test_optimize_generic(nzones=1, ncolors=1, nmeasures=4, maxsteps=1, step_duration=1)

    def test_optimize_allz_1c_1m_1max_1dur(self):
        self._test_optimize_generic(nzones=None, ncolors=1, nmeasures=1, maxsteps=1, step_duration=1)

    def test_optimize_allz_1c_1m_1max_3dur(self):
        self._test_optimize_generic(nzones=None, ncolors=1, nmeasures=1, maxsteps=1, step_duration=3)

    # def test_optimize_allz_1c_2m_2max_3dur(self):
    #     self._test_optimize_generic(nzones=None, ncolors=1, nmeasures=2, maxsteps=2, step_duration=3)

    # def test_optimize_allz_2c_2m_1max_3dur(self):
    #     self._test_optimize_generic(nzones=None, ncolors=2, nmeasures=2, maxsteps=1, step_duration=3)

    # def test_optimize_allz_allc_1m_4max_3dur(self):
    #     self._test_optimize_generic(nzones = None, ncolors = None, nmeasures = 1, maxsteps = 4, step_duration = 3)

    # def test_optimize_allz_allc_allm_4max_3dur(self):
    #     self._test_optimize_generic(nzones = None, ncolors = None, nmeasures = None, maxsteps = 4, step_duration = 3)


if __name__ == '__main__':
    unittest.main()
