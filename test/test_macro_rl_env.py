import json
import threading
import unittest
from random import randint

import numpy
import requests

from src.model.macro_rl_env import MacroRLEnv, NORMALIZE_ICU
from src.model.macromodel import MacroModelRequestHandler, MacroModelServer
from src.model.policy_utils import default_optimize_req, set_all_measures_activable, default_eval_policy_req, \
    all_measures, set_all_objectives_on, measure_vec2policy, set_all_measures_max_values
from src.optim.objectives import Objectives


class MacromodelGymTests(unittest.TestCase):
    def test_ctor(self):
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        env = MacroRLEnv(optimize_req)
        self.assertEqual([1 + 1, 1 + 1, 2 + 1, 2 + 1, 1 + 1,
                          1 + 1, 1 + 1, 2 + 1, 2 + 1, 1 + 1], env._action_list)

        if env._per_zone_observation:
            self.assertEqual((4*len(optimize_req['zones']) * env._num_ratios,), env._observation_shape)
        else:
            self.assertEqual((2 * MacroRLEnv.per_label_nobs,), env._observation_shape)
        action = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        env.step(action=action)
        self.assertEqual(env._total_population, sum(env._total_label_population))

    def test_ctor_4measures(self):
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        optimize_req['max_measure_values']['contact_tracing'] = None
        env = MacroRLEnv(optimize_req)
        self.assertEqual([1, 1 + 1, 2 + 1, 2 + 1, 1 + 1,
                          1, 1 + 1, 2 + 1, 2 + 1, 1 + 1], env._action_list)
        if env._per_zone_observation:
            self.assertEqual((4*len(optimize_req['zones']) * env._num_ratios,), env._observation_shape)
        else:
            self.assertEqual((2 * MacroRLEnv.per_label_nobs,), env._observation_shape)
        # test activated_measures_ratio()
        self.assertAlmostEqual(1, env.activated_measures_ratio(action=[0, 1, 2, 2, 1] * 2))
        self.assertAlmostEqual((1 + 1 / 2 + 1 / 2 + 1) / 4, env.activated_measures_ratio(action=[0, 1, 1, 1, 1] * 2))

    def test_objectives(self):
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        # set_n_zones(optimize_req, 2)
        # self.assertListEqual(['01', '02'], optimize_req['zones'])
        env = MacroRLEnv(optimize_req)
        env._elapsed_days = 7

        # death ratio
        self.assertAlmostEqual(0, env.death_ratio())
        for zone in env._zones:  # add 10% of dead
            zone.y[-1] += .1 * zone.population
        self.assertAlmostEqual(.1, env.death_ratio())

        # ICU_saturation_ratio
        # initial_sat = env.ICU_saturation_ratio() > 0 (about 2.7%)
        # set ICU equals to 120% of beds - ICU = I2*ICU_ratio so set I2 = 120% beds / ICU_ratio
        for zone in env._zones:
            # zone.y[7] = 1.2 * zone.icu_beds / zone.icu_ratio
            zone.accumulated_ICU_saturation_ratio = .2
        if not NORMALIZE_ICU:
            self.assertAlmostEqual(.2 / 1, env.ICU_saturation_ratio())  # one week
        else:
            env._min_values = [[0 for _ in env._zones]]
            env._max_values = [[.8 for _ in env._zones]]
            self.assertAlmostEqual(len(env._zones) * 0.25, env.ICU_saturation_ratio())  # one week
        # activated_measures_ratio
        # max values = [1, 1, 2, 2, 1]
        self.assertAlmostEqual(0, env.activated_measures_ratio(action=[0, 0, 0, 0, 0] * 2))
        self.assertAlmostEqual(1, env.activated_measures_ratio(action=[1, 1, 2, 2, 1] * 2))
        self.assertAlmostEqual((1 + 1 + .5 + .5 + 1) / 5,
                               env.activated_measures_ratio(action=[1, 1, 1, 1, 1] * 2))

        # gdp_accumulated_loss_ratio
        # cf. test_economie::test_evaluate_one_all_zones()
        # self.assertAlmostEqual(0.6039230769230788, eco.evaluate_one(eval_policy_req, hrvcr=hrvcr))
        # 3 weeks, and divide by 100
        self.assertAlmostEqual(3 * .01 * 0.6039230769230788,
                               env.gdp_accumulated_loss_ratio(action=[2, 2, 3, 3, 1] * 2))


class MacromodelGymVSServerTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # start macroserver
        cls.server = MacroModelServer(('localhost', 8000), MacroModelRequestHandler)
        threading.Thread(target=cls.server.serve_forever).start()

    @classmethod
    def tearDownClass(cls):
        # cleanup macroserver
        cls.server.shutdown()
        cls.server.socket.close()

    def vs_macroserver(self, red_values, grn_values, max_values, expected_ratio):
        # activate everything for a step of three weeks in eval_policy_req
        eval_policy_req = default_eval_policy_req()
        for m, r, g in zip(all_measures().keys(), red_values, grn_values):
            eval_policy_req[m] = measure_vec2policy([[r, g], [0, 0]])
        eval_policy_req['measure_weeks'] = [0, 3]
        # print(pretty_string(eval_policy_req))
        # activate everything for a step of three weeks in optimize_req
        optimize_req = default_optimize_req()
        set_all_measures_max_values(optimize_req, max_values)
        optimize_req['max_measure_steps'] = 1
        optimize_req['measure_step_duration'] = 3

        # send request
        URL = 'http://localhost:8000/eval_policy'
        response = requests.post(URL, data=json.dumps(eval_policy_req))
        eval_policy_res = response.json()
        self.assertTrue(eval_policy_res['success'])
        # compute objectives
        set_all_objectives_on(optimize_req)
        objectives = Objectives(optimize_req)
        obj_values = objectives.compute(True, eval_policy_req, eval_policy_res)
        # print('objectives:', [(o, v) for o, v in zip(objectives.objectives, obj_values)])

        # make the same test with the Gym env
        env = MacroRLEnv(optimize_req)
        self.assertEqual(3 * 7, env._step_duration_days)
        self.assertEqual(1, env._max_measure_steps)
        done = False
        action = red_values + grn_values
        while not done:
            obs, reward, done, info = env.step(action)
        self.assertEqual(730, env.elapsed_days())
        self.assertEqual(1 + 1, env.current_step)  # 1 step, and the remainder of two 730 days

        # now compare results
        # activated_measures_ratio
        self.assertAlmostEqual(expected_ratio, env.activated_measures_ratio(action))
        self.assertAlmostEqual(expected_ratio, objectives.by_name(obj_values, 'activated_measures_ratio'), delta=1E-6)

        # death_ratio
        self.assertAlmostEqual(env.death_ratio(), objectives.by_name(obj_values, 'death_ratio'), delta=1E-3)
        #self.assertEqual(0.802, env._c_max_containment)

        # gdp_accumulated_loss_ratio
        self.assertAlmostEqual(env.gdp_accumulated_loss_ratio(action),
                               objectives.by_name(obj_values, 'gdp_accumulated_loss_ratio'), delta=1E-6)

        # ICU_saturation_ratio
        for zone in env._zones:
            self.assertEqual(0.3, zone.icu_ratio)
            self.assertEqual(105, zone.nsum)
        if not NORMALIZE_ICU:
            self.assertAlmostEqual(env.ICU_saturation_ratio(),
                               objectives.by_name(obj_values, 'ICU_saturation_ratio'), delta=5E-5)

    def test_vs_macroserver_00000_00000_22331(self):
        return self.vs_macroserver(red_values=[0, 0, 0, 0, 0], grn_values=[0, 0, 0, 0, 0], max_values=[2, 2, 3, 3, 1],
                                   expected_ratio=0)

    def test_vs_macroserver_22331_00000_22331(self):
        return self.vs_macroserver(red_values=[2, 2, 3, 3, 1], grn_values=[0, 0, 0, 0, 0], max_values=[2, 2, 3, 3, 1],
                                   expected_ratio=0.5)

    def test_vs_macroserver_10101_01010_11111(self):
        return self.vs_macroserver(red_values=[1, 1, 1, 1, 1], grn_values=[0, 0, 0, 0, 0], max_values=[1, 1, 1, 1, 1],
                                   expected_ratio=0.5)

    def test_vs_macroserver_22331_11221_22331(self):
        return self.vs_macroserver(red_values=[2, 2, 3, 3, 1], grn_values=[1, 1, 2, 2, 1], max_values=[2, 2, 3, 3, 1],
                                   expected_ratio=(5 + 1 / 2 + 1 / 2 + 2 / 3 + 2 / 3 + 1) / 10)

    # def test_reward(self):
    #     optimize_req = default_optimize_req()
    #     set_all_measures_activable(optimize_req)
    #     optimize_req['max_measure_steps'] = 20
    #     # self.assertEqual(20, optimize_req['max_measure_steps'])
    #     env = MacroRLEnv(optimize_req)
    #     env.set_weights(lambda_activated_measures_ratio=0,
    #                     lambda_death_ratio=0,
    #                     lambda_gdp_accumulated_loss_ratio=1,
    #                     lambda_ICU_saturation_ratio=0,
    #                     lambda_color_consistency=0)
    #     obs, reward, done, info = env.step(action=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    #     # print(reward)
    #     self.assertAlmostEqual(0, reward)
    #     env.reset()
    #     obs, reward, done, info = env.step(action=[1, 1, 2, 2, 1, 1, 1, 2, 2, 1])
    #     self.assertAlmostEqual(-0.013060730769230776, reward)
    #
    #     random_rewards = []
    #     for i in range(20):
    #         action = [randint(0, v - 1) for v in env.action_space.nvec]
    #         env.reset()
    #         obs, reward, done, info = env.step(action=action)
    #         random_rewards.append(reward)
    #     mean_reward: float = numpy.mean(random_rewards)
    #     self.assertAlmostEqual(-0.006, mean_reward, delta=5E-3)
        # print('random_reward', mean(random_rewards), mean(random_rewards) * 20)

    def test_consistentpolicy(self):
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        optimize_req['max_measure_steps'] = 20
        # self.assertEqual(20, optimize_req['max_measure_steps'])
        env = MacroRLEnv(optimize_req)
        env.set_weights(lambda_activated_measures_ratio=0,
                        lambda_death_ratio=0,
                        lambda_gdp_accumulated_loss_ratio=1,
                        lambda_ICU_saturation_ratio=0)
        done=False
        while not done:
            obs, reward, done, info = env.step(action=[1, 2, 2, 2, 1, 0, 0, 0, 0, 0])
        self.assertEqual(info['consistency_check'],5)


if __name__ == '__main__':
    unittest.main()
