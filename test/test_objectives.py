import json
import unittest

from src.model.policy_utils import decov_path, set_all_objectives_on
from src.model.policy_utils import all_objectives
from src.optim.objectives import Objectives


class ObjectivesTests(unittest.TestCase):

    def test_ctor_no_objectives(self):
        objectives = Objectives({'objectives': []})
        self.assertFalse(objectives.objectives)

    def test_ctor_bad_objectives(self):
        objectives = Objectives({'objectives': ['bad_objective', 'death_ratio']})
        self.assertFalse(objectives.objectives)

    def test_compute_nok(self):
        objectives = Objectives({'objectives': ['gdp_accumulated_loss_ratio']})
        self.assertIsNone(objectives.compute(False, {}, {}))

    def test_activated_measures_ratio(self):
        optimize_req = {
            'zones': ['z1', 'z2', 'z3', 'z4', 'z5'],
            'zone_label_names': ['rouge', 'vert'],
            'max_measure_steps': 3,
            'objectives': ['activated_measures_ratio'],
            'max_measure_values': {
                'forced_telecommuting': 0,
                'school_closures': 2,
                'social_containment_all': 0,
                'total_containment_high_risk': 0
            }
        }
        eval_policy_req = {'school_closures': [
            {'activated': [0, 1]},
            {'activated': [0, 2]},
            {'activated': [0, 0]}
        ]}
        objectives = Objectives(optimize_req)
        exp_rate = (1 + 2) / (2 * 3 * 2)  # max_week_measure_sum=2, max_measure_steps=3, nlabels=2
        self.assertEqual(objectives.compute(True, eval_policy_req, {}), [exp_rate])

    def test_death_ratio(self):
        optimize_req = {'population_size': [100] * 13,
                        'objectives': ['death_ratio']}
        eval_policy_res = {'deaths': [
            {'initial': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]},
            {'week': [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]},
            {'week': [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]}
        ]}
        objectives = Objectives(optimize_req)
        self.assertEqual(objectives.compute(True, {}, eval_policy_res), [4 / 100])

    def test_inactivity(self):
        optimize_req = {'zones': ['z1', 'z2', 'z3', 'z4', 'z5'],
                        'objectives': ['inactivity']}
        eval_policy_res = {'activity': [
            {'initial': [0, 0, 0, 0, 0]},
            {'week': [1, 1, 1, 1, 1]},
            {'week': [1.5, 1.5, 1.5, 1.5, 1.5]}
        ]}
        objectives = Objectives(optimize_req)
        self.assertEqual(objectives.compute(True, {}, eval_policy_res), [1 - 1.5 / (2 * 7)])

    def test_gdp_accumulated_loss_ratio(self):
        optimize_req = {'objectives': ['gdp_accumulated_loss_ratio']}
        eval_policy_res = {'gdp_accumulated_loss': [0, 1, 2, 3]}
        objectives = Objectives(optimize_req)
        self.assertEqual(objectives.compute(True, {}, eval_policy_res), [3 / 100])

    def test_ICU_saturation_ratio(self):
        optimize_req = {'zones': ['z1', 'z2', 'z3', 'z4', 'z5'],
                        'compartmental_model_parameters': {'common': {'max_days': 730}},
                        'ICU_beds': [10, 20, 60, 40, 10],
                        'objectives': ['ICU_saturation_ratio']}
        eval_policy_res = {'ICU_patients': [{'initial': [1, -140, 56, 0, 0]},
                                            {'week': [1, -140, 56, 50, 0]},
                                            {'week': [100, -140, 56, 50, 3]}]}
        objectives = Objectives(optimize_req)
        exp_saturation = (90 / 10 + 2 * (50 - 40) / 40) / (5 * 2)  # 5 zones, 2 weeks
        self.assertEqual(objectives.compute(True, {}, eval_policy_res), [exp_saturation])

    def test_all_objectives(self):
        optimize_req = {'zones': ['z1', 'z2', 'z3', 'z4', 'z5'],
                        'zone_label_names': ['rouge', 'vert'],
                        'population_size': [100] * 5,
                        'ICU_beds': [10, 20, 60, 40, 10],
                        'max_measure_values': {'school_closures': 2},
                        'compartmental_model_parameters': {'common': {'max_days': 730}},
                        'max_measure_steps': 2,
                        'objectives': all_objectives()}
        eval_policy_req = {'school_closures': [
            {'activated': [1, 2]},
            {'activated': [0, 0]}
        ]}
        eval_policy_res = {'gdp_accumulated_loss': [0, 3, 6],
                           'activity': [
                               {'initial': [0, 0, 0, 0, 0]},
                               {'week': [1, 1, 1, 1, 1]},
                               {'week': [1.5, 1.5, 1.5, 1.5, 1.5]}
                           ],
                           'deaths': [
                               {'initial': [1, 1, 1, 1, 1]},
                               {'week': [2, 2, 2, 2, 2]},
                               {'week': [5, 5, 5, 5, 5]}
                           ],
                           'ICU_patients': [
                               {'initial': [1, -140, 56, 0, 0]},
                               {'week': [1, -140, 56, 50, 0]},
                               {'week': [100, -140, 56, 50, 3]}
                           ]}
        objectives = Objectives(optimize_req)
        objs = all_objectives()
        m = objectives.compute(True, eval_policy_req, eval_policy_res)
        exp_rate = (1 + 2) / (2 * 2 * 2)  # max_week_measure_sum=2, max_measure_steps=2, nlabels=2
        self.assertEqual(m[objs.index('activated_measures_ratio')], exp_rate)
        self.assertEqual(m[objs.index('death_ratio')], 4 / 100)
        self.assertEqual(m[objs.index('gdp_accumulated_loss_ratio')], 6 / 100)
        exp_saturation = (90 / 10 + 2 * (50 - 40) / 40) / (5 * 2)  # 5 zones, 2 weeks
        self.assertEqual(m[objs.index('ICU_saturation_ratio')], exp_saturation)
        self.assertEqual(m[objs.index('inactivity')], 1 - 1.5 / (2 * 7))

    def test_json_doc(self):
        with open(decov_path + "/doc/optimize_req.json") as f:
            optimize_req = json.load(f)
        optimize_req["max_measure_values"] = {
            "contact_tracing": 0,
            "forced_telecommuting": 0,
            "school_closures": 2,
            "social_containment_all": 0,
            "total_containment_high_risk": 0
        }
        set_all_objectives_on(optimize_req)
        with open(decov_path + "/doc/eval_policy_req.json") as f:
            eval_policy_req = json.load(f)
        with open(decov_path + "/doc/eval_policy_res.json") as f:
            eval_policy_res = json.load(f)

        objectives = Objectives(optimize_req)
        m = objectives.compute(True, eval_policy_req, eval_policy_res)
        print(m)
        for idx, obj in enumerate(all_objectives()):
            if obj == 'activated_measures_ratio':
                exp_rate = (4 + 3) / (
                        2 * 5 * 2)  # max_week_measure_sum=2 (school_closures), max_measure_steps=5, nlabels=2
                nzones = 96
                self.assertEqual(m[idx], exp_rate)
            elif obj == 'death_ratio':
                self.assertEqual(m[idx], 3 * nzones / sum(optimize_req['population_size']))
            elif obj == 'gdp_accumulated_loss_ratio':
                self.assertEqual(m[idx], 3 / 100)
            elif obj == 'ICU_saturation_ratio':  # 27 beds in '2A'
                nweeks = 3
                ICU_sat_w1 = sum([max(0, 20 - b) / b for b in eval_policy_req['ICU_beds']])
                ICU_sat_w2 = sum([max(0, 30 - b) / b for b in eval_policy_req['ICU_beds']])
                ICU_sat_w3 = sum([max(0, 40 - b) / b for b in eval_policy_req['ICU_beds']])

                exp_saturation = (ICU_sat_w1 + ICU_sat_w2 + ICU_sat_w3) / (nzones * nweeks)
                self.assertAlmostEqual(m[idx], exp_saturation)
            elif obj == 'inactivity':
                self.assertEqual(m[idx], 1 - 3 / (3 * 7))  # nweeks = 3


if __name__ == '__main__':
    unittest.main()
