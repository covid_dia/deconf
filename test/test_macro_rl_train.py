import unittest

from stable_baselines import PPO2
from stable_baselines.common.env_checker import check_env
from stable_baselines.common.vec_env import DummyVecEnv, SubprocVecEnv

from src.model.macro_rl_env import MacroRLEnv
from src.model.policy_utils import default_optimize_req, set_all_measures_activable
from src.optim.macro_rl_train import train_pareto


class MacromodelGymTests(unittest.TestCase):
    def test_check_env(self):
        optimize_req = default_optimize_req()
        env = MacroRLEnv(optimize_req)
        # print(env.reset())
        check_env(env)

    def test_step(self):
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        env = MacroRLEnv(optimize_req)
        self.assertEqual([1 + 1, 1 + 1, 2 + 1, 2 + 1, 1 + 1,
                          1 + 1, 1 + 1, 2 + 1, 2 + 1, 1 + 1], env._action_list)
        nzones = len(optimize_req['zones'])
        if env._per_zone_observation:
            self.assertEqual((4 * len(optimize_req['zones']) * env._num_ratios,), env._observation_shape)
        else:
            self.assertEqual((2 * MacroRLEnv.per_label_nobs,), env._observation_shape)

        action = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        env.step(action=action)

    def test_stable_baseline_DummyVecEnv(self):
        # The algorithms require a vectorized environment to run
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        env = DummyVecEnv([lambda: MacroRLEnv(optimize_req)])
        model = PPO2('MlpPolicy', env, verbose=1)
        # mini-train
        model.learn(total_timesteps=5)
        # mini-eval
        obs = env.reset()
        for i in range(2):
            action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)
        self.assertEqual(done, False)

    def no_test_stable_baseline_SubprocVecEnv(self):
        """
        WARNING: This simple test is valid BUT HANGS FOREVER IN UNIT TESTS,
        because SubprocVecEnv cannot be called outside a main() context.
        Cf. https://github.com/hill-a/stable-baselines/issues/155
        and warning on https://stable-baselines.readthedocs.io/en/master/guide/vec_envs.html
        """

        def make_env():
            def _init():
                env = MacroRLEnv(optimize_req=default_optimize_req(), per_zone_observation=False)
                return env
            return _init

        num_cpu = 2
        vec_env = SubprocVecEnv([make_env() for _ in range(num_cpu)])
        model = PPO2('MlpPolicy', vec_env, verbose=1)
        # mini-train
        model.learn(total_timesteps=5)
        # mini-eval
        env = make_env()()  # () to call _init function
        obs = env.reset()
        for i in range(2):
            action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)
        self.assertEqual(done, False)

    def test_train_pareto(self):
        optimize_req = default_optimize_req()
        set_all_measures_activable(optimize_req)
        # set_all_objectives_on(optimize_req)
        optimize_req['objectives'] = ['activated_measures_ratio', 'death_ratio']
        weight_combinations = train_pareto(optimize_req=optimize_req, per_zone_observation=False,
                                           total_timesteps=50, policy='MlpPolicy')
        # 1,1  1,0  0,1  1,.8  .8,1
        # self.assertEqual(5, len(weight_combinations))
        # 1,1  1,0  0,1
        self.assertEqual(3, len(weight_combinations))


if __name__ == '__main__':
    unittest.main()
