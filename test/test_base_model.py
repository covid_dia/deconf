import json
import string
import unittest

import requests

from src.model.policy_utils import decov_path
from src.model.policy_utils import set_one_zone


class BaseModelServerTests(unittest.TestCase):
    def check_res_fields(self, res):
        # check all expected fields are here
        with open(decov_path + '/doc/eval_policy_res.json', 'rb') as json2_data:
            res_template = json.load(json2_data)
            keys = sorted(list(res.keys()))
            template_keys = sorted(list(res_template.keys()))
            # self.assertListEqual(template_keys, keys)
            # assert each key in template_keys is in keys (but it may contain other keys)
            for k in template_keys:
                self.assertIn(k, keys)

    # https://github.com/slok/prometheus-python/blob/master/prometheus/test_pusher.py
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _test_wrong_path(self):
        URL = 'http://localhost:8000/foobar'
        with open(decov_path + '/doc/eval_policy_req.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertFalse(resp['success'])

    def _test_empty(self):
        URL = 'http://localhost:8000/eval_policy'
        with open(decov_path + '/doc/empty.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertFalse(resp['success'])

    def _test_one_zone(self, **kwargs):
        URL = 'http://localhost:8000/eval_policy'
        with open(decov_path + '/doc/eval_policy_req.json', 'rb') as json_data:
            data = json.load(json_data)
            for k, v in kwargs.items():
                if k in data:
                    data[k] = v
            set_one_zone(data)
            response = requests.post(URL, data=json.dumps(data))
            self.assertTrue(response.ok)
            res = response.json()
            self.assertTrue(res['success'])
            self.check_res_fields(res)
            nweeks, nzones = 2 * 52 + 1, 1
            self.assertEqual(nweeks, len(res['gdp_accumulated_loss']))
            self.assertEqual(nweeks, len(res['cases']))
            self.assertEqual(nzones, len(res['cases'][0]['initial']))
            self.assertEqual(nzones, len(res['cases'][-1]['week']))
            self.assertEqual(nzones, len(res['cases'][-1]['week']))

    def _test_ok(self, check_SEAIR_values=True,
                 max_days=730,
                 StopWhenEpidemyFinished=False, **kwargs):

        URL = 'http://localhost:8000/eval_policy'
        with open(decov_path + '/doc/eval_policy_req.json', 'rb') as json_data:
            # with open(decov_path + '/src/frontend/static/eval_policy_req_blank.json', 'rb') as json_data:
            eval_policy_req = json.load(json_data)
            for k, v in kwargs.items():
                if k in eval_policy_req:
                    eval_policy_req[k] = v

            # ensure no early stopping
            eval_policy_req['compartmental_model_parameters']['common']['max_days'] = max_days
            eval_policy_req['compartmental_model_parameters']['micro'][
                'StopWhenEpidemyFinished'] = StopWhenEpidemyFinished
            response = requests.post(URL, data=json.dumps(eval_policy_req))
            self.assertTrue(response.ok)
            eval_policy_res = response.json()
            self.assertTrue(eval_policy_res['success'])
            self.check_res_fields(eval_policy_res)
            self.assertListEqual(eval_policy_res['zones'],
                                 ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14',
                                  '15', '16', '17', '18', '19', '21', '22', '23', '24', '25', '26', '27', '28', '29',
                                  '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43',
                                  '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57',
                                  '58', '59', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '70', '71',
                                  '72', '73', '74', '75', '76', '77', '78', '79', '80', '81', '82', '83', '84', '85',
                                  '86', '87', '88', '89', '90', '91', '92', '93', '94', '95', '2A', '2B'])
            self.assertEqual(eval_policy_req['compartmental_model'], eval_policy_res['compartmental_model'])

            nweeks, nzones = max_days // 7 + 1, 96  # max_days=730 -> nweeks=105 (including initial)
            if StopWhenEpidemyFinished:
                self.assertGreater(max_days, eval_policy_res['max_days'])
                nweeks = eval_policy_res['max_days'] // 7 + 1
            else:
                self.assertEqual(max_days, eval_policy_res['max_days'])
            self.assertEqual(nweeks, len(eval_policy_res['gdp_accumulated_loss']))
            self.assertEqual(nweeks, len(eval_policy_res['cases']))
            self.assertEqual(nzones, len(eval_policy_res['cases'][0]['initial']))
            self.assertEqual(nzones, len(eval_policy_res['cases'][-1]['week']))
            self.assertEqual(nweeks, len(eval_policy_res['hospital_patients']))
            self.assertEqual(nweeks, len(eval_policy_res['ICU_patients']))
            self.assertEqual(nweeks, len(eval_policy_res['deaths']))
            # check for week 0 (initial conditions) of FR-BFC (model #1)
            I2begin = eval_policy_res['hospital_patients'][0]['initial'][1]
            I1begin = eval_policy_res['cases'][0]['initial'][1] - I2begin
            if check_SEAIR_values:
                popAisne = 526050
                self.assertEqual(popAisne, eval_policy_req['population_size'][1])
                self.assertAlmostEqual(popAisne * 0.00038340904641798716, I1begin, delta=1)
                self.assertAlmostEqual(popAisne * 0.00033412794281286926, I2begin, delta=1)
            I2end = eval_policy_res['hospital_patients'][-1]['week'][1]
            I1end = eval_policy_res['cases'][-1]['week'][1] - I2end
            Mend = sum(eval_policy_res['deaths'][-1]['week'])
            compartmental_model: string = None
            if 'compartmental_model' in kwargs:
                compartmental_model = kwargs["compartmental_model"]
            if compartmental_model == "Alizon20":
                self.assertLess(I1end, 1)  # nobody sick anymore
                self.assertLess(I2end, 1)  # nobody sick anymore
            elif compartmental_model == "Salje20":
                self.assertLess(I1end, 1.5)  # nobody sick anymore
                self.assertLess(I2end, 1.25)  # nobody sick anymore
            self.assertGreater(Mend, 0)
