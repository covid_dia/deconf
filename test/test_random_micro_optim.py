import threading
import unittest

from src.model.micromodel import MicroModelServer, MicroModelRequestHandler
from src.optim.base_optim import BaseOptimServer
from src.optim.random_optim import RandomOptimRequestHandler
from test.test_base_optim import BaseOptimServerTests


class RandomOptimServerTests(BaseOptimServerTests):
    def setUp(self):
        # Start a ModelServer and an OptimServer
        self.model_server = MicroModelServer(('localhost', 8000), MicroModelRequestHandler)
        draw = None
        # draw = ['ICU_saturation_ratio', 'gdp_accumulated_loss_ratio']
        self.optim_server = BaseOptimServer(('localhost', 8001), RandomOptimRequestHandler, draw=draw)
        self.optim_server.ntries = 2  # not good but very fast
        if draw:
            self.optim_server.ntries = 15  # that will be cute but slow
        model_thread = threading.Thread(target=self.model_server.serve_forever)
        model_thread.start()
        optim_thread = threading.Thread(target=self.optim_server.serve_forever)
        optim_thread.start()

    def tearDown(self):
        self.model_server.shutdown()
        self.model_server.socket.close()
        self.optim_server.shutdown()
        self.optim_server.socket.close()

    def test_optimize_empty(self):
        self._test_optimize_empty()

    def test_optimize_bad_measure(self):
        self._test_optimize_bad_measure()

    # cannot work, we need all zones
    # def test_optimize_1z_1c_1m_1max_1dur(self):
    #     self._test_optimize_generic(nzones=1, ncolors=1, nmeasures=1, maxsteps=1, step_duration=1)

    # test passes OK, commented for speed
    # def test_optimize_allz_allc_1m_4max_3dur(self):
    #     self._test_optimize_generic(nzones=None, ncolors=None, nmeasures=1, maxsteps=4, step_duration=3)

    def test_optimize_allz_allc_allm_4max_3dur(self):
        self._test_optimize_generic(nzones=None, ncolors=None, nmeasures=None, maxsteps=4, step_duration=3)


if __name__ == '__main__':
    unittest.main()
