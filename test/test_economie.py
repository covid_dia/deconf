import string
import unittest

from src.model.economie import Economie
from src.model.policy_utils import decov_path
from src.model.policy_utils import default_eval_policy_req


def m2j(zone: string, ft: int, sch: int, soc: int, hr: int):
    return {'zones': [zone],
            'zone_labels': [0],
            'forced_telecommuting': [ft],
            'school_closures': [sch],
            'social_containment_all': [soc],
            'total_containment_high_risk': [hr]
            }


class EcoTest(unittest.TestCase):
    def test_economie(self):
        eco = Economie()

        self.assertAlmostEqual(0.04, eco.impact['agriculture']['part'])
        self.assertAlmostEqual(2450, eco.emploi['2A']['agriculture'])

        sum_agr = sum([eco.emploi[z]['agriculture'] for z in eco.part_pib_region.keys()])
        self.assertAlmostEqual(sum_agr, 629900)  # obtained by importing in LibreOffice and summing there
        sum_ter = sum([eco.emploi[z]['autre tertiaire'] for z in eco.part_pib_region.keys()])
        self.assertAlmostEqual(sum_ter, 8391600)  # obtained by importing in LibreOffice and summing there

        # control values for test_brute_force.cpp
        # print(eco.part_pib_region['2A']['agriculture'])
        # print(eco.part_pib_region['2A']['industrie'])
        # print(eco.part_pib_region['2A']['construction'])
        # print(eco.part_pib_region['2A']['tertiaire marchand'])
        # print(eco.part_pib_region['2A']['autre tertiaire'])

        self.assertAlmostEqual(0.04 * 2450 / sum_agr, eco.part_pib_region['2A']['agriculture'])
        self.assertAlmostEqual(0.22, eco.impact['autre tertiaire']['part'])
        self.assertAlmostEqual(0.22 * 22450 / sum_ter, eco.part_pib_region['2B']['autre tertiaire'])

        filename = decov_path + "/doc/eval_policy_req.json"
        eco_results = eco.evaluate(filename, 10)
        self.assertEqual(1 + 10, len(eco_results))  # includes week 0
        self.assertEqual(0, eco_results[0])  # week 0
        self.assertTrue(0 <= eco_results[-1] <= 5)

    def test_evaluate_one(self):
        eco = Economie()
        # max: ft=2, sch=3, sco=3, hr=1
        hrvcr = 0.8
        self.assertAlmostEqual(0, eco.evaluate_one(m2j('2A', 0, 0, 0, 0), hrvcr))
        self.assertAlmostEqual(.00020381222496003163, eco.evaluate_one(m2j('2A', ft=2, sch=0, soc=0, hr=0), hrvcr))
        self.assertAlmostEqual(.00015392366540270124, eco.evaluate_one(m2j('2A', ft=0, sch=3, soc=0, hr=0), hrvcr))
        self.assertAlmostEqual(.0009874545630529064, eco.evaluate_one(m2j('2A', ft=0, sch=0, soc=3, hr=0), hrvcr))
        self.assertAlmostEqual(.0003444834210984901, eco.evaluate_one(m2j('2A', ft=0, sch=0, soc=0, hr=1), hrvcr))
        self.assertAlmostEqual(.0010184014112908377, eco.evaluate_one(m2j('2A', ft=0, sch=0, soc=3, hr=1), hrvcr))
        self.assertAlmostEqual(.0004984070865011913, eco.evaluate_one(m2j('2A', ft=0, sch=3, soc=0, hr=1), hrvcr))
        self.assertAlmostEqual(0.001345190453415639, eco.evaluate_one(m2j('2A', ft=2, sch=3, soc=3, hr=1), hrvcr))

    def test_evaluate_one_all_zones(self):
        eco = Economie()
        eval_policy_req = default_eval_policy_req()
        eval_policy_req['forced_telecommuting'] = [2, 2]
        eval_policy_req['school_closures'] = [3, 3]
        eval_policy_req['social_containment_all'] = [3, 3]
        eval_policy_req['total_containment_high_risk'] = [1, 1]
        hrvcr = eval_policy_req['compartmental_model_parameters']['common']['high_risk_voluntary_containment_rate']
        self.assertAlmostEqual(0.6039230769230788, eco.evaluate_one(eval_policy_req, hrvcr=hrvcr))


if __name__ == '__main__':
    unittest.main()
