import json
import unittest

import requests

from src.model.policy_utils import decov_path
from src.model.policy_utils import set_n_zones, set_n_colors, \
    set_n_measures


class BaseOptimServerTests(unittest.TestCase):
    def check_res_fields(self, res):
        # check all expected fields are here
        with open(decov_path + '/doc/optimize_res.json', 'rb') as json2_data:
            res_template = json.load(json2_data)
            self.assertListEqual(list(res.keys()), list(res_template.keys()))
            for k in ['policies']:
                # check policy
                self.assertGreater(len(res[k]), 0)
                self.assertGreater(len(res_template[k]), 0)
                keys = sorted(list(res[k][0].keys()))
                keys_template = sorted(list(res_template[k][0].keys()))
                self.assertListEqual(keys, keys_template)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _test_optimize_empty(self):
        URL = 'http://localhost:8001/optimize'
        with open(decov_path + '/doc/empty.json', 'rb') as data:
            response = requests.post(URL, data=data)
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertFalse(resp['success'])

    def _test_optimize_bad_measure(self):
        URL = 'http://localhost:8001/optimize'
        with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['max_measure_values'] = {'bad_measure': 3}
            response = requests.post(URL, data=json.dumps(data))
            self.assertTrue(response.ok)
            resp = response.json()
            self.assertFalse(resp['success'])

    def _test_optimize_generic(self, nzones, ncolors, nmeasures, maxsteps, step_duration):
        """ Generic test for optimizers

        :param nzones: number of zones (None means all zones)
        :param ncolors: number of colors (None means all colors)
        :param nmeasures: number of measures (None means all measures)
        :param maxsteps: max number of steps (None means default max)
        :param step_duration: duration of a step (None means default duration of step)
        :return:
        """
        URL = 'http://localhost:8001/optimize'
        with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            if nzones is None:
                nzones = len(data['zones'])
            if ncolors is None:
                ncolors = len(data['zone_label_names'])
            if nmeasures is None:
                nmeasures = len(data['max_measure_values'])

            self.assertLessEqual(nzones, len(data['zones']))
            self.assertGreaterEqual(nzones, 1)
            self.assertLessEqual(ncolors, len(data['zone_label_names']))
            self.assertGreaterEqual(ncolors, 1)
            self.assertLessEqual(nmeasures, len(data['max_measure_values']))
            self.assertGreaterEqual(nmeasures, 1)
            if maxsteps is not None:
                data['max_measure_steps'] = maxsteps
            else:
                maxsteps = data['max_measure_steps']
            self.assertGreaterEqual(maxsteps, 1)
            if step_duration is not None:
                data['measure_step_duration'] = step_duration
            else:
                step_duration = data['measure_step_duration']
            self.assertGreaterEqual(step_duration, 1)
            set_n_zones(data, nzones)
            set_n_colors(data, ncolors)
            set_n_measures(data, nmeasures)

            response = requests.post(URL, data=json.dumps(data))

            self.assertTrue(response.ok)
            res = response.json()
            # check results
            self.assertTrue(res['success'])
            self.check_res_fields(res)
            self.assertLessEqual(1, len(res['policies']))
            for p in res['policies']:
                self.assertTrue(2 <= len(p['measure_weeks']) <= maxsteps + 1)
                # must start at week 0 and end at maxweeks
                self.assertEqual(0, p['measure_weeks'][0])
                self.assertEqual(maxsteps * step_duration, p['measure_weeks'][-1])
                for x in p['measure_weeks']:
                    self.assertTrue(x % step_duration == 0)
                # some measures are None
                counter = 0
                for measure in data['max_measure_values'].keys():
                    m_max = data['max_measure_values'][measure]
                    if counter < nmeasures:
                        self.assertEqual(len(p['measure_weeks']), len(p[measure]))
                        for measure_week in p[measure]:
                            self.assertEqual(len(measure_week['activated']), ncolors)
                            for x in measure_week['activated']:
                                self.assertTrue(0 <= x <= m_max)
                        self.assertEqual([0 for _ in range(ncolors)], p[measure][-1]['activated'])
                        counter += 1
                    else:
                        self.assertIsNone(p[measure])
