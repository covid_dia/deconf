import threading
import unittest

from src.model.macromodel import MacroModelServer, MacroModelRequestHandler
from src.optim.base_optim import BaseOptimServer
from src.optim.brute_force_optim import BruteForceOptimRequestHandler
from test.test_base_optim import BaseOptimServerTests


class BruteForceServerTests(BaseOptimServerTests):
    def setUp(self):
        # Start an BruteForceServer and a ModelServer
        self.model_server = MacroModelServer(('localhost', 8000), MacroModelRequestHandler)
        self.optim_server = BaseOptimServer(('localhost', 8001), BruteForceOptimRequestHandler)
        model_thread = threading.Thread(target=self.model_server.serve_forever)
        model_thread.start()
        optim_thread = threading.Thread(target=self.optim_server.serve_forever)
        optim_thread.start()

    def tearDown(self):
        self.model_server.shutdown()
        self.model_server.socket.close()
        self.optim_server.shutdown()
        self.optim_server.socket.close()

    def test_optimize_empty(self):
        self._test_optimize_empty()

    def test_optimize_bad_measure(self):
        self._test_optimize_bad_measure()

    # def test_optimize_1z_allc_4m_1max_1dur(self):
    #     self._test_optimize_generic(nzones=1, ncolors=None, nmeasures=None, maxsteps=1, step_duration=1)
    #
    # def test_optimize_allz_allc_4m_1max_1dur(self):
    #     self._test_optimize_generic(nzones=None, ncolors=None, nmeasures=None, maxsteps=1, step_duration=1)


if __name__ == '__main__':
    unittest.main()
