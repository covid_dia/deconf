import unittest

from src.model.policy_utils import simplify_policy_eval_req_except_last_week, pretty_string


class PolicyUtilsTests(unittest.TestCase):

    def test_simplify_policy_eval_req_except_last_week_None(self):
        policy = None
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_not_dict(self):
        policy = [1,2,3]
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_measure_weeks_absent(self):
        policy = {"school_closures": None,
                  "forced_telecommuting": [{"activated": [1, 2]}, {"activated": [1, 2]}, {"activated": [1, 2]},
                                           {"activated": [1, 3]}, {"activated": [1, 3]}, {"activated": [1, 3]},
                                           {"activated": [1, 3]}],
                  "containment_high_risk": [{"activated": [3, 4]}, {"activated": [3, 4]}, {"activated": [1, 4]},
                                            {"activated": [1, 4]}, {"activated": [1, 4]}, {"activated": [1, 4]},
                                            {"activated": [1, 4]}]}
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_empty(self):
        policy = {"measure_weeks": []}
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_one_week(self):
        policy = {"measure_weeks": [0], "school_closures": None, "forced_telecommuting": [{"activated": [1, 2]}],
                  "containment_high_risk": [{"activated": [3, 4]}]}
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_two_different_weeks(self):
        policy = {"measure_weeks": [0, 1], "school_closures": None,
                  "forced_telecommuting": [{"activated": [1, 2]}, {"activated": [1, 3]}],
                  "containment_high_risk": [{"activated": [3, 4]}, {"activated": [3, 4]}]}
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_two_same_weeks(self):
        policy = {"measure_weeks": [0, 1], "school_closures": None,
                  "forced_telecommuting": [{"activated": [1, 2]}, {"activated": [1, 2]}],
                  "containment_high_risk": [{"activated": [3, 4]}, {"activated": [3, 4]}]}
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_three_same_weeks(self):
        policy = {"measure_weeks": [0, 1, 2], "school_closures": None,
                  "forced_telecommuting": [{"activated": [1, 2]}, {"activated": [1, 2]}, {"activated": [1, 2]}],
                  "containment_high_risk": [{"activated": [3, 4]}, {"activated": [3, 4]}, {"activated": [3, 4]}]}
        policy2 = simplify_policy_eval_req_except_last_week(policy)
        self.assertEqual(policy2["measure_weeks"], [0, 2])
        self.assertEqual(policy2["school_closures"], None)
        self.assertEqual(policy2["forced_telecommuting"], [{"activated": [1, 2]}, {"activated": [1, 2]}])
        self.assertEqual(policy2["containment_high_risk"], [{"activated": [3, 4]}, {"activated": [3, 4]}])

    def test_simplify_policy_eval_req_except_last_week_three_different_weeks(self):
        policy = {"measure_weeks": [0, 1, 2], "school_closures": None,
                  "forced_telecommuting": [{"activated": [1, 2]}, {"activated": [1, 3]}, {"activated": [1, 2]}],
                  "containment_high_risk": [{"activated": [3, 4]}, {"activated": [3, 4]}, {"activated": [3, 4]}]}
        self.assertEqual(policy, simplify_policy_eval_req_except_last_week(policy))

    def test_simplify_policy_eval_req_except_last_week_general(self):
        policy = {"measure_weeks": [0, 1, 2, 3, 4, 5, 6], "school_closures": None,
                  "forced_telecommuting": [{"activated": [1, 2]}, {"activated": [1, 2]}, {"activated": [1, 2]},
                                           {"activated": [1, 3]}, {"activated": [1, 3]}, {"activated": [1, 3]},
                                           {"activated": [1, 3]}],
                  "containment_high_risk": [{"activated": [3, 4]}, {"activated": [3, 4]}, {"activated": [1, 4]},
                                            {"activated": [1, 4]}, {"activated": [1, 4]}, {"activated": [1, 4]},
                                            {"activated": [1, 4]}]}
        policy2 = simplify_policy_eval_req_except_last_week(policy)
        self.assertEqual(policy2["measure_weeks"], [0, 2, 3, 6])
        self.assertEqual(policy2["school_closures"], None)
        self.assertEqual(policy2["forced_telecommuting"], [policy["forced_telecommuting"][i] for i in policy2["measure_weeks"]])
        self.assertEqual(policy2["containment_high_risk"], [policy["containment_high_risk"][i] for i in policy2["measure_weeks"]])

    def test_pretty_string(self):
        foo = {'intlist': [0, 1, 2, 3],
               'stringlist': ['A', 'B', 'C', 'D', 'E', 'F'],
               'stringlist2': ['1', '2'],
               'foo': {'bar': [1, 2], 'bar2': ['A', 'B']},
               'end': 0}
        pretty = pretty_string(foo)
        self.assertEqual(pretty, '''{
  "intlist": [ 0, 1, 2, 3 ],
  "stringlist": [ "A", "B", "C", "D", "E", "F" ],
  "stringlist2": [ "1", "2" ],
  "foo": {
    "bar": [ 1, 2 ],
    "bar2": [ "A", "B" ]
  },
  "end": 0
}''')

if __name__ == '__main__':
    unittest.main()
