import threading
import unittest
from time import time

from stable_baselines import PPO2

from src.model.macromodel import MacroModelServer, MacroModelRequestHandler
from src.optim.base_optim import BaseOptimServer
from src.optim.macro_rl_optim import RLOptimRequestHandler
from src.optim.macro_rl_train import models_folder
from test.test_base_optim import BaseOptimServerTests


class LoadSpeedTests(unittest.TestCase):
    def _test_load(self):
        model_filename = models_folder + '/00001-0;0;1;0.zip'
        ntries = 3
        model = None

        start = time()
        for _ in range(3):
            model = PPO2.load(model_filename)
        time_load = (time() - start) / ntries
        # print('time_load:', time_load)
        self.assertLess(time_load, 5)

        start = time()
        for _ in range(ntries):
            model.load_parameters(load_path_or_dict=model_filename)
        time_load_parameters = (time() - start) / ntries
        # print('time_load_parameters:', time_load_parameters)
        self.assertLess(time_load_parameters, 0.2)


class RLOptimServerTests(BaseOptimServerTests):
    def setUp(self):
        # Start a ModelServer and an OptimServer
        self.model_server = MacroModelServer(('localhost', 8000), MacroModelRequestHandler)
        draw = None
        # draw = ['ICU_saturation_ratio', 'gdp_accumulated_loss_ratio']
        self.optim_server = BaseOptimServer(('localhost', 8001), RLOptimRequestHandler, draw=draw)
        model_thread = threading.Thread(target=self.model_server.serve_forever)
        model_thread.start()
        optim_thread = threading.Thread(target=self.optim_server.serve_forever)
        optim_thread.start()

    def tearDown(self):
        self.model_server.shutdown()
        self.model_server.socket.close()
        self.optim_server.shutdown()
        self.optim_server.socket.close()

    def test_optimize_empty(self):
        self._test_optimize_empty()

    def test_optimize_bad_measure(self):
        self._test_optimize_bad_measure()

    # NOK: one color=all red, while all RL models trained on two colors
    # def test_optimize_allz_1c_1m_4max_3dur(self):
    #     self._test_optimize_generic(nzones=None, ncolors=1, nmeasures=1, maxsteps=4, step_duration=3)

    # NOK: one measure=10000=contact tracing only and macromodel does not handle it
    # def test_optimize_1z_allc_1m_4max_3dur(self):
    #     self._test_optimize_generic(nzones=1, ncolors=None, nmeasures=1, maxsteps=4, step_duration=3)

    # NOK: one measure=10000=contact tracing only and macromodel does not handle it
    # def test_optimize_allz_allc_1m_4max_3dur(self):
    #     self._test_optimize_generic(nzones=None, ncolors=None, nmeasures=1, maxsteps=4, step_duration=3)

    # NOK: one zone while RL trained of multiple Zones, weird this test passed in the firt place
    # def test_optimize_1z_allc_allm_4max_3dur(self):
    #    self._test_optimize_generic(nzones=1, ncolors=None, nmeasures=None, maxsteps=4, step_duration=3)

    def test_optimize_allz_allc_allm_4max_3dur(self):
        self._test_optimize_generic(nzones=None, ncolors=None, nmeasures=None, maxsteps=4, step_duration=3)


if __name__ == '__main__':
    unittest.main()
