import json
import unittest

from src.model.policy_utils import decov_path
from src.model.policy_utils import measure_vec2policy, policy2measure_vec
from src.optim.branch_and_bound.branch_and_bound_utils import extend_policy, insert_in_policy, update_best_policies, \
    generate_possible_policies_except_max, color_strictness_filter


class BBUnitTests(unittest.TestCase):
    def test_extend_policy(self):
        URL = 'http://localhost:8001/optimize'
        with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['zones'] = ['FR-BFC']
            data['population_size'] = [2795301]
            data['ICU_beds'] = [198]
            data['max_measure_steps'] = 10
            data['measure_weeks'] = [0, 3, 7]
            data['school_closures'] = [{'activated': [True]}, {'activated': [False]}, {'activated': [True]}]
            data['forced_telecommuting'] = None
            new_max_measure_steps = [0, 1, 3, 4, 5, 7, 8]

            new_data = extend_policy(data, new_max_measure_steps, ['school_closures'])
            for k in new_data.keys():
                if k == 'school_closures':
                    self.assertEqual(new_data['school_closures'],
                                     [{'activated': [True]}, {'activated': [True]}, {'activated': [False]},
                                      {'activated': [False]}, {'activated': [False]}, {'activated': [True]},
                                      {'activated': [True]}])
                else:
                    self.assertEqual(data[k], new_data[k])

    def test_insert_in_policy_1region(self):
        URL = 'http://localhost:8001/optimize'
        T, F = True, False
        with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['zones'] = ['FR-BFC']
            data['population_size'] = [2795301]
            data['measure_weeks'] = [0, 3, 7]
            data['school_closures'] = measure_vec2policy([[T], [F], [T]])
            data['social_containment_all'] = measure_vec2policy([[T], [T], [T]])
            data['forced_telecommuting'] = None
            activable_measures_dict = ['school_closures', 'social_containment_all']

            insert_in_policy(data, 'school_closures', activable_measures_dict, 7, {'activated': [F]})
            self.assertEqual(data['measure_weeks'], [0, 3, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[T], [F], [F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T], [T], [T]])

            insert_in_policy(data, 'school_closures', activable_measures_dict, 5, {'activated': [T]})
            self.assertEqual(data['measure_weeks'], [0, 3, 5, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[T], [F], [T], [F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T], [T], [T], [T]])

            insert_in_policy(data, 'school_closures', activable_measures_dict, 3, {'activated': [T]})
            self.assertEqual(data['measure_weeks'], [0, 3, 5, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[T], [T], [T], [F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T], [T], [T], [T]])

            insert_in_policy(data, 'school_closures', activable_measures_dict, 0, {'activated': [F]})
            self.assertEqual(data['measure_weeks'], [0, 3, 5, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[F], [T], [T], [F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T], [T], [T], [T]])

    def test_insert_in_policy_2regions(self):
        URL = 'http://localhost:8001/optimize'
        T, F = True, False
        with open(decov_path + '/doc/optimize_req.json', 'rb') as json_data:
            data = json.load(json_data)
            data['zones'] = ['2A', '2B']
            data['population_size'] = [8026685, 2795301]
            data['measure_weeks'] = [0, 3, 7]
            data['school_closures'] = measure_vec2policy([[T, F], [F, F], [T, F]])
            data['social_containment_all'] = measure_vec2policy([[T, F], [T, F], [T, F]])
            data['forced_telecommuting'] = None
            activable_measures_dict = ['school_closures', 'social_containment_all']

            insert_in_policy(data, 'school_closures', activable_measures_dict, 7, {'activated': [F, F]})
            self.assertEqual(data['measure_weeks'], [0, 3, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[T, F], [F, F], [F, F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T, F], [T, F], [T, F]])

            insert_in_policy(data, 'school_closures', activable_measures_dict, 5, {'activated': [T, F]})
            self.assertEqual(data['measure_weeks'], [0, 3, 5, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[T, F], [F, F], [T, F], [F, F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T, F], [T, F], [T, F], [T, F]])

            insert_in_policy(data, 'school_closures', activable_measures_dict, 3, {'activated': [T, F]})
            self.assertEqual(data['measure_weeks'], [0, 3, 5, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[T, F], [T, F], [T, F], [F, F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T, F], [T, F], [T, F], [T, F]])

            insert_in_policy(data, 'school_closures', activable_measures_dict, 0, {'activated': [F, F]})
            self.assertEqual(data['measure_weeks'], [0, 3, 5, 7])
            self.assertEqual(policy2measure_vec(data, 'school_closures'), [[F, F], [T, F], [T, F], [F, F]])
            self.assertEqual(policy2measure_vec(data, 'social_containment_all'), [[T, F], [T, F], [T, F], [T, F]])

    @staticmethod
    def generate_policy_of_death_rate(death_rate_wanted):
        policy = {}
        policy['deaths'] = [{'initial' : 0}, {'week' : 2 * death_rate_wanted}]
        policy['population_size'] = 1
        return policy

    def test_update_best_policies(self):
        list_policies = [None, None, None, None]
        death_threasholds = [1,1.5,2,2.5]
        policy1 = self.generate_policy_of_death_rate(3)
        update_best_policies(list_policies, death_threasholds, 1, 3, True, {}, policy1)
        self.assertEqual(list_policies, [None, None, None, None])
        policy2 = self.generate_policy_of_death_rate(2.5)
        update_best_policies(list_policies, death_threasholds, 1, 2.5, True, {}, policy2)
        self.assertEqual(list_policies, [None, None, None, (1, policy2, True, {})])
        policy3 = self.generate_policy_of_death_rate(2.4)
        update_best_policies(list_policies, death_threasholds, 0.9, 2.4, True, {}, policy3)
        self.assertEqual(list_policies, [None, None, None, (0.9, policy3, True, {})])
        policy4 = self.generate_policy_of_death_rate(0.9)
        update_best_policies(list_policies, death_threasholds, 2, 0.9, True, {}, policy4)
        self.assertEqual(list_policies, [(2, policy4, True, {}), (2, policy4, True, {}), (2, policy4, True, {}), (0.9, policy3, True, {})])
        policy5 = self.generate_policy_of_death_rate(1.1)
        update_best_policies(list_policies, death_threasholds, 1, 1.1, True, {}, policy5)
        self.assertEqual(list_policies, [(2, policy4, True, {}), (1, policy5, True, {}), (1, policy5, True, {}), (0.9, policy3, True, {})])

    def test_generate_possible_policies_except_max(self):
        a = 1
        b = 2
        c = 2
        d = 1
        ncolors = 2
        available_measures = {"school_closing": a, 'forced_telecommuting' : b, 'social_containment_all': c, 'measure' : d}
        l = generate_possible_policies_except_max(available_measures, ncolors)
        self.assertEqual(len(l),36*36 - 1)

    def test_color_strictness_filter(self):
        policy = {}
        policy['measure_weeks'] = [0,3,4]
        policy['zone_label_names'] = ['rouge', 'vert']
        policy['measure1'] = None
        policy['measure2'] = [{'activated' : [1,0]}, {'activated' : [1,0]}, {'activated' : [1,1]}]
        policy['measure3'] = [{'activated' : [1,0]}, {'activated' : [1,0]}, {'activated' : [1,0]}]
        self.assertTrue(color_strictness_filter(policy))
        policy['measure2'] = [{'activated': [1, 2]}, {'activated': [1, 0]}, {'activated': [1, 1]}]
        self.assertFalse(color_strictness_filter(policy))
        policy['measure2'] = [{'activated': [1, 0]}, {'activated': [1, 2]}, {'activated': [1, 1]}]
        self.assertFalse(color_strictness_filter(policy))
        policy['measure2'] = [{'activated': [1, 0]}, {'activated': [1, 0]}, {'activated': [1, 2]}]
        self.assertFalse(color_strictness_filter(policy))