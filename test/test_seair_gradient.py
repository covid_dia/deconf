import copy
import json
from copy import deepcopy

import numpy as np
from termcolor import colored

from src.model.model_seair_alizon import SEAIRModelAlizon as SEAIRModel, SEAIR_alizon_ratios2y0 as SEAIR_ratios2y0, covid_ode_alizon as covid_ode
from src.optim.model_seair_gradient import SEAIRGradientOptimizer


def print_compare_mat(Tr,Est):
    print("\nEstimated :")
    for i in range(Est.shape[0]):
        for j in range(Est.shape[1]):
            print("%.8f"%Est[i,j],end=' \t')
        print()
        
    print("\nTheoretical :")
    for i in range(Tr.shape[0]):
        for j in range(Tr.shape[1]):
            print("%.8f"%Tr[i,j],end=' \t')
        print()

    errors = (np.logical_or(np.absolute(Tr)>1e-8, np.absolute(Est)>1e-8))*2*np.absolute(Tr-Est)/np.absolute(Tr+Est+1e-10)
    
    print("\nError:")
    for i in range(Tr.shape[0]):
        for j in range(Tr.shape[1]):
            print("%.5f"%errors[i,j],end=' \t')
        print()
    return errors

def get_new_model():
    with open('data/eval_policy_req_blank.json') as fp:
        eval_policy_req = json.load(fp)
    zone_idx = 0
    zone = eval_policy_req['zones'][0]
    zone_params = deepcopy(eval_policy_req['compartmental_model_parameters']['common'])
    for k,v in eval_policy_req['compartmental_model_parameters']['macro'].items():
        zone_params[k] = v
    zone_params['i_sat'] = eval_policy_req['ICU_beds'][zone_idx]
    ratios = eval_policy_req['initial_ratios'][zone]
    
    npop = int(eval_policy_req['population_size'][zone_idx])
    ratios = eval_policy_req['initial_ratios'][zone]
    y0 = SEAIR_ratios2y0(npop, *ratios, zone_params)
    
    model = SEAIRGradientOptimizer(zone_params)
    return model

def get_new_model_ref():
    with open('data/eval_policy_req_blank.json') as fp:
        eval_policy_req = json.load(fp)

    zone_idx = 0
    zone = eval_policy_req['zones'][0]
    zone_params = deepcopy(eval_policy_req['compartmental_model_parameters']['common'])
    for k,v in eval_policy_req['compartmental_model_parameters']['macro'].items():
        zone_params[k] = v
    zone_params['i_sat'] = eval_policy_req['ICU_beds'][zone_idx]
    ratios = eval_policy_req['initial_ratios'][zone]
    
    npop = int(eval_policy_req['population_size'][zone_idx])
    ratios = eval_policy_req['initial_ratios'][zone]
    y0 = SEAIR_ratios2y0(npop, *ratios, zone_params)
    model_ref = SEAIRModel(alizon20_parameters=zone_params, y0=y0)
    return model_ref


def test_model():
    print("Testing if model evaluation in seair_gradient matches the one in model_seair")

    for nw_opt in [1,3, 50]:
        nw = 2*nw_opt
        model = get_new_model()
        c = {"forced_telecommuting": 0.3,
         "school_closures": 0,
         "social_containment_all": 0,
         "total_containment_high_risk": 0
        }
        model.parameters["c"] = [c]*(nw_opt+1)
        model.parameters['diff_mu'] = True
        model.parameters['rtol'] = 1e-6
        model.parameters['atol'] = 1e-6
        model.parameters['mxstep'] = 5000
        model_ref = get_new_model_ref()
        model_ref.parameters['diff_mu'] = True

        c = [c] * (nw_opt + 1)
        time_intervals = list(range(0, 7 * nw_opt + 1, 7))
        time_intervals.append(7 * nw)
        results_ref = model_ref.compute(c_list= c, time_intervals=time_intervals, rtol=1e-6, atol=1e-6)
    
        policies = np.zeros((nw_opt,4))
        results = model.oracle(policies, nw , 0, debug_model=True)
        print("Ref : ")
        print(results_ref[-1,:])
        print("Model : ")
        print(results[-1,:])
        
        dist = np.sqrt(np.sum((results[-1,:].flatten()-results_ref[-1,:].flatten())**2))/np.sqrt(np.sum(results_ref[-1,:].flatten()**2))
        print("Normalized error for %d weeks:%g --------- %s"%(nw, dist, colored("Passed","green") if dist < 1e-10 else colored("Failed",'red')))
        

def test_jacobian():
    print("\n\n\nNumerically testing the Jacobian")

    model = get_new_model()
    policies = np.zeros((1,4))
    results = model.oracle(policies,4 ,0, debug_model=True)
    J = model.compute_J(results, 0.3)
  
    #approximate df_i/dy_j and check that it isnt too far from the corresponding entry in J
    epsilon = 1e-3
    y = results[-1,:]
    problems = []
    model.parameters['diff_mu'] = True
    Df = np.zeros((10,10))
    for j in range(10):
        ydy = y.copy()
        ydy[j] += epsilon
        fdy = np.array(covid_ode(0,ydy, model.parameters))
        fy = np.array(covid_ode(0,y, model.parameters))
        dfdyj = (fdy - fy)/epsilon
        Df[:,j] = dfdyj
    
    errors = print_compare_mat(J[-1,:,:],Df)
    for i in range(10):
        for j in range(10):
            if errors[i,j]>0.001: problems.append((i,j))
    print("Problematic values: %d --------- %s"%(len(problems), colored("Passed",'green') if len(problems)==0 else colored("Failed",'red')))
    if len(problems)>0: print("Problematic cases : ", problems)

def test_diffC():
    ps = [np.array([0.0,0.0,0.0,0.0]),np.array([0.5,0.5,0.5,0.5]),np.array([1.0,1.0,1.0,1.0])]
    model = get_new_model()
    model.parameters['diff_mu'] = True
    epsilon = 1e-3
    
    for k,p in enumerate(ps):
        c = model.sanitary_impact(p)
        results  = model.oracle(p.reshape((1,-1)), 4, 0, debug_model=True)
        y = results[-1,:]
        model.parameters['c'] = c
        fp = np.array(covid_ode(0,y, model.parameters))
        X = model.compute_diffC(results,p)
        Df = np.zeros((10,4))
        for j in range(4):
            pdp = copy.copy(p)
            pdp[j] += epsilon
            
            cdp = model.sanitary_impact(pdp)
            model.parameters['c'] = cdp
            fdp = np.array(covid_ode(0,results[-1,:], model.parameters)).flatten()
            dfdp = (fdp - fp)/epsilon
            Df[:,j] = dfdp
            
        errors = print_compare_mat(X[-1,:,:],Df)
        problems=[]
        for i in range(10):
            for j in range(4):
                if errors[i,j]>0.0001: problems.append((i,j))
           
        print("Problematic values: %d --------- %s"%(len(problems), colored("Passed",'green') if len(problems)==0 else colored("Failed",'red')))
        if len(problems)>0: print("Problematic cases : ", problems)

def test_ode_diff():
    print('\n\nTesting that ode_diff finds the gradient of the solution')
    model = get_new_model()
    nw = 1
    nw_opt = 1
    y0 = model.parameters['y0'].copy()
    policies = np.zeros((nw_opt,4))
    epsilon=1e-3
    model_results = model.oracle(policies, nw, 0, debug_diff_ode = True)
    y = model_results[-1]['y'][-1,:]
    As, Xs = model.compute_diff_final(model_results,policies, nw)
    est_A = np.zeros((10,10))
    for j in range(10):
        y1 = y0.copy()
        y1[j] += epsilon
        model2 = get_new_model()
        model2.parameters['y0'] = y1
        ydp = model2.oracle(policies, nw, 0, debug_diff_ode = True)[-1]['y'][-1,:]
      
        est_A[:,j] = (ydp-y)/epsilon
        
    the_A = As[0]
    for i in range(1,nw_opt):
        the_A = np.dot(As[i], the_A)
    if nw > nw_opt:
        the_A = np.dot(As[-1], the_A)
        
    problems=[]
    errors = print_compare_mat(the_A,est_A)
    for i in range(10):
        for j in range(10):
            if errors[i,j]>0.01: problems.append((i,j))
            
    print("Problematic values: %d --------- %s"%(len(problems), colored("Passed",'green') if len(problems)==0 else colored("Failed",'red')))
    if len(problems)>0: print("Problematic cases : ", problems)

def test_Psi():
    model = get_new_model()
    nw = 4
    nw_opt = 2
    y0 = model.parameters['y0'].copy()
    policies = np.zeros((nw_opt,4))
    epsilon=1e-3
    model_results = model.oracle(policies, nw, 0, debug_diff_ode = True)
    y = model_results[-1]['y'][-1,:]
    As, Xs = model.compute_diff_final(model_results,policies, nw)
    est_Psi = np.zeros((10,4*nw_opt))
    for j in range(4):
        for i in range(nw_opt):
            policies2 = policies.copy()
            policies2[i,j] += epsilon
            model2 = get_new_model()
            results = model2.oracle(policies2, nw, 0, debug_diff_ode = True)
            ydp = results[-1]['y'][-1,:]
            est_Psi[:,4*i+j] = (ydp-y)/epsilon

    Psi = np.zeros((10, nw_opt, 4))
    Psi[:,0,:] = Xs[0]
    print("Starting with Xs[0]")
    print(len(As), len(Xs))
    for j in range(1,nw_opt):
        print("multiplying by As[%d]"%j)
        Psi = np.dot(As[j], Psi.reshape(10,-1)).reshape(Psi.shape)
        print("adding Xs[%d]"%j)
        Psi[:,j,:] += Xs[j]
    if nw_opt<nw:
        Psi = np.dot(As[nw_opt], Psi.reshape(10,-1)).reshape(Psi.shape)
        print("multyplying by As[%d]"%nw_opt)

    problems=[]
    errors = print_compare_mat(Psi.reshape(10,-1),est_Psi)
    for i in range(10):
        for j in range(4*nw_opt):
            if errors[i,j]>0.01: problems.append((i,j))

    print("Problematic values: %d --------- %s"%(len(problems), colored("Passed",'green') if len(problems)==0 else colored("Failed",'red')))
    if len(problems)>0: print("Problematic cases : ", problems)


def test_gradient():
    print('\n\nTesting the gradient of the global objective')

    model = get_new_model()
    
    model.parameters['reg_lambda']=0
    model.parameters['weight_econ']=1e5

    nw = 5
    nw_opt = 2
    policies = np.zeros((nw_opt,4))
    epsilon=1e-5
    grad_est = np.zeros((nw_opt,4))
    obj, grad,death, econ = model.oracle(policies, nw, 0)
    grad = grad.reshape(grad_est.shape)
    for i in range(nw_opt):
        for j in range(4):
            policies2 = policies.copy()
            policies2[i,j] += epsilon
            model2 = get_new_model()
            model2.parameters['reg_lambda'] = model.parameters['reg_lambda']
            model2.parameters['weight_econ'] = model.parameters['weight_econ']
            objdp, grad2, death2, econ2 = model2.oracle(policies2, nw, 0)
            print('current objective: ', objdp)
            grad_est[i,j] = (objdp-obj)/epsilon
    
    problems=[]
    errors = print_compare_mat(grad, grad_est)
    for i in range(nw_opt):
        for j in range(4):
            if errors[i,j]>0.01: problems.append((i,j))

    print("\n\nProblematic values: %d --------- %s"%(len(problems), colored("Passed",'green') if len(problems)==0 else colored("Failed",'red')))
    if len(problems)>0: print("Problematic cases : ", problems)
    

if __name__=="__main__":
    test_model()
    test_jacobian()
    test_diffC()
    test_ode_diff()
    test_Psi()
    test_gradient()
  
    ##################################################################################
    print('\n\nTesting one optimization (no ref)')

    model = get_new_model()
    model.parameters['i_sat'] = 1e-5
    for nweeks_optim in [50]:
        print("----------------------------------")
        print(" Optimizing for %d week policies "%nweeks_optim)
        print("----------------------------------")
        model.parameters['verbose']=True
        model.parameters['reg_lambda']=0
        model.parameters['weight_econ']=1e5
        model.parameters['niter'] = 50
        policies, stats = model.find_best_policy(100,nweeks_optim, 0)
        print()
        print(policies)
