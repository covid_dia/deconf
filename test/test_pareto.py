import unittest

import numpy as np
from numpy.testing import assert_equal, assert_almost_equal

from src.optim.pareto import ParetoFront, is_pareto_efficient_simple


class ParetoTests(unittest.TestCase):
    @staticmethod
    def assertNPEqual(first, second):
        assert_equal(first, second)

    @staticmethod
    def assertNPAlmostEqual(first, second, decimal=7):
        assert_almost_equal(first, second, decimal)

    def _test_vs_stack(self, dim=2, npts=1000):
        mu, sigma = 0, 0.1  # mean and standard deviation
        costs = np.random.normal(mu, sigma, (npts, dim))
        f = ParetoFront()
        for c in costs:
            # print(c)
            f.update(None, None, c)
        is_efficient = is_pareto_efficient_simple(costs)
        self.assertEqual(np.count_nonzero(is_efficient), len(f.metrics))
        for idx, c in enumerate(costs):
            if is_efficient[idx]:
                is_equal = np.all(f.metrics == c, axis=1)
                ok = np.count_nonzero(is_equal) == 1
                self.assertTrue(ok)

    def test_empty(self):
        f = ParetoFront()
        self.assertEqual(0, len(f.metrics))

    def test_dim1(self):
        f = ParetoFront()
        for i in range(1000):
            update = f.update(None, None, [i])
            self.assertTrue((i == 0 and update) or not update)
        self.assertTrue(1, len(f.metrics))
        self.assertNPAlmostEqual([0], f.metrics[0])

    def test_dim1_reversed(self):
        f = ParetoFront()
        for i in reversed(range(1000)):
            self.assertTrue(f.update(None, None, [i]))
        self.assertTrue(1, len(f.metrics))
        self.assertNPAlmostEqual([0], f.metrics[0])

    def test_dim1_vs_stack(self):
        self._test_vs_stack(1)

    def test_dim2(self):
        f = ParetoFront()
        self.assertTrue(f.update('ac', None, [1, 3]))
        self.assertFalse(f.update(None, None, [1, 3]))

        self.assertTrue(f.update('ca', None, [3, 1]))
        self.assertNPAlmostEqual([[1, 3], [3, 1]], f.metrics)

        self.assertTrue(f.update('ab', None, [1, 2]))
        self.assertNPAlmostEqual([[3, 1], [1, 2]], f.metrics)

        self.assertFalse(f.update(None, None, [1, 2]))
        self.assertTrue(f.update('ba', None, [2, 1]))
        self.assertNPAlmostEqual([[1, 2], [2, 1]], f.metrics)
        self.assertNPEqual(['ab', 'ba'], f.eval_policy_reqs)

        self.assertTrue(f.update('aa', None, [1, 1]))
        self.assertNPAlmostEqual([[1, 1]], f.metrics)
        self.assertNPEqual(['aa'], f.eval_policy_reqs)

    def test_dim2_vs_stack(self):
        self._test_vs_stack(2)

    def test_dim3(self):
        f = ParetoFront()
        self.assertTrue(f.update('acc', None, [1, 3, 3]))
        self.assertFalse(f.update(None, None, [1, 3, 3]))
        self.assertNPEqual(['acc'], f.eval_policy_reqs)

        self.assertTrue(f.update('cca', None, [3, 3, 1]))
        self.assertNPEqual(['acc', 'cca'], f.eval_policy_reqs)

        self.assertTrue(f.update('cba', None, [3, 2, 1]))
        self.assertNPEqual(['acc', 'cba'], f.eval_policy_reqs)

        self.assertTrue(f.update('bca', None, [2, 3, 1]))
        self.assertNPEqual(['acc', 'cba', 'bca'], f.eval_policy_reqs)

        self.assertTrue(f.update('bba', None, [2, 2, 1]))
        self.assertNPEqual(['acc', 'bba'], f.eval_policy_reqs)

        self.assertTrue(f.update('aaa', None, [1, 1, 1]))
        self.assertNPEqual(['aaa'], f.eval_policy_reqs)

    def test_dim3_vs_stack(self):
        self._test_vs_stack(3)

    def test_dim20_vs_stack(self):
        self._test_vs_stack(20, npts=200)


if __name__ == '__main__':
    unittest.main()
