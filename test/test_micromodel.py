import json
import os
import pathlib
import threading
import unittest
from subprocess import check_call, check_output

import numpy
import numpy as np

from src.model.macromodel import MacroModelServer
from src.model.micromodel import MicroModelRequestHandler
from src.model.policy_utils import pretty_write
from test.test_base_model import BaseModelServerTests


class MicroModelServerTests(BaseModelServerTests):

    def setUp(self):
        self.server = MacroModelServer(('localhost', 8000), MicroModelRequestHandler)
        thread = threading.Thread(target=self.server.serve_forever)
        thread.start()

    def tearDown(self):
        self.server.shutdown()
        self.server.socket.close()

    def test_wrong_path(self):
        self._test_wrong_path()

    def test_empty(self):
        self._test_empty()

    # micromodel does not work on a single region
    # def test_one_zone(self):
    #     self._test_one_zone()

    def test_ok730_no_StopWhenEpidemyFinished(self):
        self._test_ok(check_SEAIR_values=False, StopWhenEpidemyFinished=False)

    def test_ok730_StopWhenEpidemyFinished(self):
        self._test_ok(check_SEAIR_values=False, StopWhenEpidemyFinished=True)


class MicroModelFunctionalTest(unittest.TestCase):

    @staticmethod
    def get_path(rel_path):
        decov_path = str(pathlib.Path(__file__).parent.parent)
        return os.path.join(decov_path, rel_path)

    def build_model(self):
        # We use g++-9 because the provided version of g++ in docker image is too old
        # (warning issues with pragma omp capture)
        check_call("make CXX=g++-9 DUMP_STATS=1 -C " + self.get_path("src/model/decov"), shell=True)

    def overwrite_json(self, dictionary, key, value):
        self.assertIn(key, dictionary)
        dictionary[key] = value

    def test_release(self):
        # We use g++-9 again.
        check_call("make CXX=g++-9 test -C " + self.get_path("src/model/decov"), shell=True)

    def test_debug(self):
        # We use g++-9 again.
        check_call("make CXX=g++-9 OPTIM=0 DUMP_STATS=1 test -C " + self.get_path("src/model/decov"), shell=True)

    def run_micro_model(self, input_req, test_name):

        bin_path = self.get_path('src/model/decov/build/RELEASE_STATS/decov')
        self.assertTrue(os.path.isfile(bin_path))

        # Write input to file to make later debugging simpler

        input_path = self.get_path("out/eval_policy_req." + test_name + ".json")
        os.makedirs(os.path.dirname(input_path), exist_ok=True)
        with open(input_path, "w") as tmp:
            json.dump(input_req, tmp)

        args = [bin_path,
                '-parameters', input_path,
                '-report', 'stdout',
                '-verbose', 'false',
                '-cm', self.get_path('out/matrices.json'),
                '-data', self.get_path('src/model/decov/data.json')]

        print("Run: ", " ".join(args))
        output = check_output(args)
        output = json.loads(output)
        pretty_write(output, self.get_path('out/eval_policy_res.' + test_name + '.json'))
        return output

    def test_activity_rate_no_covid(self):
        # Check that activity rate is 1 per day in absence of virus
        self.build_model()

        with open(self.get_path("doc/eval_policy_req.json")) as f:
            input_req = json.load(f)

        init = [0] * len(input_req["compartments_Alizon20"])
        init[0] = 1
        for dep in input_req["zones"]:
            self.overwrite_json(input_req["initial_ratios_Alizon20"], dep, init)

        self.overwrite_json(input_req, "measure_weeks", [0, 19])
        self.overwrite_json(input_req, "forced_telecommuting", None)
        self.overwrite_json(input_req, "contact_tracing", None)
        self.overwrite_json(input_req, "school_closures", None)
        self.overwrite_json(input_req, "social_containment_all", None)
        self.overwrite_json(input_req, "total_containment_high_risk", None)
        self.overwrite_json(input_req["compartmental_model_parameters"]["micro"], "StopWhenEpidemyFinished", False)
        self.overwrite_json(input_req["compartmental_model_parameters"]["common"], "max_days", 7)

        output = self.run_micro_model(input_req, "ar_nocovid")

        for a in output["activity"][1]["week"]:
            self.assertEqual(a, 7)

    def test_activity_rate_min(self):
        # Check that activity rate is 0 when everybody's died
        self.build_model()

        with open(self.get_path("doc/eval_policy_req.json")) as f:
            input_req = json.load(f)

        init = [0] * len(input_req["compartments_Alizon20"])
        init[-1] = 1
        for dep in input_req["zones"]:
            self.overwrite_json(input_req["initial_ratios_Alizon20"], dep, init)

        self.overwrite_json(input_req["compartmental_model_parameters"]["micro"], "StopWhenEpidemyFinished", False)
        self.overwrite_json(input_req["compartmental_model_parameters"]["common"], "max_days", 7)

        output = self.run_micro_model(input_req, "ar_all_dead")

        for a in output["activity"][1]["week"]:
            self.assertEqual(a, 0)

    def test_activity_rate_measures(self):
        # Check that activity rate is lowered wy political measures
        self.build_model()

        with open(self.get_path("doc/eval_policy_req.json")) as f:
            input_req = json.load(f)

        self.overwrite_json(input_req, "measure_weeks", [0])
        self.overwrite_json(input_req, "forced_telecommuting", [{"activated": [2, 2]}])
        self.overwrite_json(input_req, "contact_tracing", [{"activated": [2, 2]}])
        self.overwrite_json(input_req, "school_closures", [{"activated": [3, 3]}])
        self.overwrite_json(input_req, "social_containment_all", [{"activated": [3, 3]}])
        self.overwrite_json(input_req, "total_containment_high_risk", [{"activated": [1, 1]}])

        self.overwrite_json(input_req["compartmental_model_parameters"]["micro"], "StopWhenEpidemyFinished",
                            False)
        self.overwrite_json(input_req["compartmental_model_parameters"]["common"], "max_days", 7 * 4)

        output = self.run_micro_model(input_req, "ar_covid")

        mean = np.mean(output["activity"][4]["week"]) / (4 * 7)
        print("Mean activity:", mean)
        self.assertLess(mean, .8)

    def test_contact_tracing(self):
        self.build_model()

        with open(self.get_path("doc/eval_policy_req.json")) as f:
            input_req = json.load(f)

        # Custom measures for test
        self.overwrite_json(input_req, "measure_weeks", [0, 2, 10])
        self.overwrite_json(input_req, "contact_tracing",
                            [{"activated": [0, 0]}, {"activated": [1, 1]}, {"activated": [0, 0]}])
        self.overwrite_json(input_req, "forced_telecommuting", None)
        self.overwrite_json(input_req, "school_closures", None)
        self.overwrite_json(input_req, "social_containment_all",
                            [{"activated": [0, 0]}, {"activated": [2, 2]}, {"activated": [0, 0]}])
        self.overwrite_json(input_req, "total_containment_high_risk", None)
        self.overwrite_json(input_req["compartmental_model_parameters"]["common"], "max_days", 70)

        output = self.run_micro_model(input_req, "ct")

        num_zones = len(output["zones"])
        contacts = []
        week_with_contact_tracing = {}
        for z in range(num_zones):
            previous_tests = 0
            previous_traced = 0

            for week, (d_tests, d_traced) in enumerate(zip(output["tests"], output["traced_contacts"])):
                try:
                    l_tests = d_tests["week"]
                    l_traced = d_traced["week"]
                except KeyError:
                    l_tests = d_tests["initial"]
                    l_traced = d_traced["initial"]

                tests = l_tests[z]
                traced = l_traced[z]
                delta_tests = tests - previous_tests
                delta_traced = traced - previous_traced
                previous_tests = tests
                previous_traced = traced

                if delta_tests == 0:
                    continue

                self.assertTrue(week > 0)
                week_with_contact_tracing[week - 1] = True

                contacts_per_tests = delta_traced / delta_tests
                contacts.append(contacts_per_tests)

        print("Averaged CT weeks per zones:", len(contacts) / num_zones)
        print("Weeks with CT:", len(week_with_contact_tracing), ":\n", week_with_contact_tracing)
        mean_contacts = np.mean(contacts)
        print("Mean contacts:", mean_contacts)

        self.assertDictEqual({2: True, 3: True, 4: True, 5: True, 6: True, 7: True, 8: True, 9: True},
                             week_with_contact_tracing)
        self.assertGreater(mean_contacts, 15)
        self.assertLess(mean_contacts, 35)

        self.now_test_check_matrices()

    @staticmethod
    def json_matrix2array(day_matrix):
        mat = numpy.zeros((8, 8))
        # merge 0..20, and 80..90
        key_mapping = {'0': 0, '10': 0, '20': 1, '30': 2, '40': 3, '50': 4, '60': 5, '70': 6, '80': 7, '90': 7}
        index_mapping = {0: 0, 1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7, 9: 7}
        for isrc in day_matrix.keys():
            for jsrc in range(10):
                idst, jdst = key_mapping[isrc], index_mapping[jsrc]
                mat[idst][jdst] += day_matrix[isrc][jsrc]
        return mat

    def now_test_check_matrices(self):
        # check contact matrices
        with open(self.get_path('out/matrices.json'), 'r') as f:
            matrices = json.load(f)
            pretty_write(matrices, self.get_path('out/matrices_pretty.json'))
        # convert in Mattis format
        prev_mat = None
        for day, day_data in enumerate(matrices['days']):
            day_mat = self.json_matrix2array(day_data['contacts'])
            diff_mat = day_mat - prev_mat if prev_mat is not None else day_mat
            prev_mat = day_mat
            with open(self.get_path('out/matrix{}.csv'.format(day)), 'w') as f:
                f.write('"","[0,20)","[20,30)","[30,40)","[40,50)","[50,60)","[60,70)","[70,80)","80+"\n')
                for i in range(8):
                    line = '"{}",'.format(i + 1) + ','.join(str(v) for v in diff_mat[i])
                    f.write(line + '\n')


if __name__ == '__main__':
    unittest.main()
