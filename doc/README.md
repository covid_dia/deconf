Google Docs
===========

DECOV - suivi:
  https://docs.google.com/document/d/1t58MrZ14Zx85fldT-tsAOGAxLozPwDQ3t-wNv_lfjPY/edit

20200403-DECOV (planches):
  https://docs.google.com/presentation/d/118k-1x6WZHXHojByJyP410MhgAa32Q7DpT7kkOuHibY

Synthèse de la collecte de données (tâche 3):
  https://docs.google.com/document/d/1CCFBmgBWcEmYY2Qb3OTEbovx1RJ9m2-9wsXIXpiYV2c

20200403-Decov - sujet
  https://docs.google.com/document/d/1a1JHwZaI1KksPU8hZ4qlzPnLPpCOBstf_0x3G0EM6DM

Modèle bas niveau:
  https://docs.google.com/document/d/1MWZmgwKVidsEBURqzIC8UDvLWQ8OX-7u-BWsql7xc4Y

Politique - Mesures de luttes à modéliser
  https://docs.google.com/document/d/1C4YTv8UoQ5OxSAb4JNAnF1Wp9Eymhb5vAm0uFYYkFW8

Simulateurs SMA (tâche 5)
  https://docs.google.com/document/d/1G3h3bHNCx6rzR7qlUi8DeVR9-K5ZnUZxvpdeyvKk7iU/edit

Sources data - Data.json(Alexis/Amelie)
  https://docs.google.com/document/d/1Cs3QL5hsfElF5dEs2owZxLWuRewdSMc_S-wQA2foz0M/edit#heading=h.gpt2b9jvqveb

Presentation decov ()
  https://docs.google.com/presentation/d/1_9Nz73TgSo2kbjmhBH4lfNRdwXCaa8xJ66LBfLHvUpE/edit#slide=id.g7fe5a4cc20_1_39