#!/bin/sh
# stop at first error
set -e
# Docker cannot read a file outside of the "docker" folder 
# (Forbidden path outside the build context)
# so make a hard copy here
cp -f ../requirements.txt .
# build image
docker build -t registry.gitlab.com/covid_dia/deconf .
# push it to registry
docker push registry.gitlab.com/covid_dia/deconf
# cleanup
rm ./requirements.txt
