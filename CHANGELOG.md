DECOV

08/06/2020 - V1.0.1
===================

https://gitlab.com/covid_dia/deconf/-/releases#V1.0.1

	- Frontend: ajout d'un raccourci pour régler R0 et le ratio de lits de soins intensifs


29/05/2020 - V1.0
=================

https://gitlab.com/covid_dia/deconf/-/releases#V1.0

	- Modèles épidémiologiques : paramètres et valeurs initiales fittées sur les données SIVIC
	- Modèles macro Alizon et Salje : implémentation en c++ pour gain de rapidité (facteur 6-7)
	- Frontend : ajout des légendes pour les régions et ajout du calque avec les tests
	- Ajout de la mesure "inactivité" qui fonctionne uniquement avec le modèle micro
	- RL : mise à jour des modèles pour améliorer l'optimalité des solutions
	- L'optimisation des politiques impose désormais que vert < rouge
	- Quelques réglages des paramètres par défaut


22/05/2020 - V0.5
=================

https://gitlab.com/covid_dia/deconf/-/releases#V0.5

	- Optimisation: version beta par Reinforcement Learning (algorithme PPO2 avec différentes pondérations des 4 objectifs)
	- Modèle macro [Alizon] et [Salje]: fit des conditions initiales sur les données observables
	- Modèle micro: accélération par chargement d’une population précalculée (au lieu de génération à chaque évaluation) et passage au compilateur G++-9
	- Frontend: bascule entre mode d'affichage sur la carte : différentes métriques visualisées en heatmap
	- Intégration continue par Docker auto-hébergé
	- Intitulés exacts des publications dans les références
	- Correction de bugs divers

15/05/2020 - V0.4
=================

https://gitlab.com/covid_dia/deconf/-/releases#V0.4

	- Ajout d'un "résumé graphique" des solutions d'optimisation dans l'onglet "liste"
	- Optimisation du modele micro 10 fois plus rapide par défaut (ajustement du facteur de réduction à 1000)
	- Passage de l'écran d'optimisation en "GoldenLayout" (onglets réagençables)
	- Ajout de tooltips d'aide
	- Correction du bug d'invisibilité des icones de mesures lorsque le tab est trop petit
	- Correction de bugs divers

13/05/2020 - V0.3.1
===================

https://gitlab.com/covid_dia/deconf/-/releases#V0.3.1

	- Calcul et affichage du nombre de tests PCR par semaine lors de l'utilisation du modele micro
	- Ajout d'un manuel utilisateur
	- Changement de la méthode de saisie des mesures
	- NSGAX renvoie maintenant plus de solutions (au prix d'un temps de calcul plus conséquent).
	- Correction d'un bug qui augmentait significativement le nombre de lits en ICU
	- Désactivation de Branch and Bound (trop lent : risque de saturation du serveur).

09/05/2020 - V0.3
=================

https://gitlab.com/covid_dia/deconf/-/releases#V0.3

	- Ajout de la date du lundi dans l'affichage de la semaine courante
	- Passage à une carte au niveau départemental, avec optimisation par "couleur"
	- possibilité de choisir le simulateur micro/macro
	- possibilité de choisir la routine d'optimisation
	- indicateur d'attente pour les actions longues (affichage de la carte, simulation, optimisation)
	- visualisation des agents (mode contact ou mode lieux)
	- visualisation de la capacité de tests par département
	- ajout de la mesure de contact-tracing à trois valeurs
	(0: pas de contact-tracing, 1: contact-tracing réaliste, 2: contact-tracing avec adhésion complète)
	- ajout d'une authentification basique (et moche)
	- migration vers decov.kitia.fr


01/05/2020 - V0.2
=================

https://gitlab.com/covid_dia/deconf/-/releases#V0.2

	- Généralisation des 4 mesures à n>2 valeurs
	(télétravail: 3, fermeture des établissements scolaires: 4, confinement social: 4, confinement des personnes à risque: 2)
	- Numérotation des semaines à partir de la semaine 18
	- La première semaine est désormais la S20
	- Utilisation d'onglets
	- Ajout de boutons pour contrôler la semaine courante et le nombre max de semaines affichées
	- Ajout de l'information de population à risque
	- La répartition par secteur est désormais un camembert
	- Ajout de la visualisation dans la timeline du nombre de cas en soins intensifs
	- Conditons de départ au 11 mai estimées par best fit du modèle SEAIR sur les grandeurs observées
	- Ajout d'un lien dans l'IHM vers ce CHANGELOG

21/04/2020 - V0.1
=================

https://gitlab.com/covid_dia/deconf/-/releases#V0.1

	- Première version déployée.
	- 4 mesures binaires (télétravail, fermeture des établissements scolaires, confinement social, confinement des personnes à risque).
	- Optimisation par Monte-Carlo.
