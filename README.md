DECOV:

an Artificial Intelligence-based tool to search and optimize tradeoffs
between sanitary and economy criteria of the COVID-19-lockdown measures
liftings.

[![pipeline status](https://gitlab.com/covid_dia/deconf/badges/master/pipeline.svg)](https://gitlab.com/covid_dia/deconf/-/commits/master)


DECOV is a software tool
for helping political leaders to make the best decisions
regarding the lifting of lockdown measures after the COVID-19 crisis.
It uses differents methods of Artificial Intelligence
and Operational Research
to search and optimize tradeoffs
between sanitary and economy objectives.

The project has been initiated by engineers of the French MoD
[Ministère des Armées](https://www.defense.gouv.fr/)
and supported by the
[Agence de l'innovation de défense](https://www.defense.gouv.fr/aid).
As a state-based initiative,  it does not
seek to make a profit from the resulting project software

The full documentation is available here (in French):
[Google Docs](https://docs.google.com/document/d/1MCOTYJwZxRkf51xdP9sfNsCIeN5VvYG9DuNM3KC2T-w/).


License
=======

Cf. LICENSE.md .


Third-parties and credits
=========================

DECOV relies on different open-source third-party libraries
that are listed here:

  - [deap](https://deap.readthedocs.io/en/master/)
  - [Flask](https://flask.palletsprojects.com/en/)
  - [gym](https://gym.openai.com/)
  - [leaflet](https://leafletjs.com/)
  - [matplotlib](https://matplotlib.org/) - Copyright (c) 2012-2013 Matplotlib Development Team; All Rights Reserved
  - [mpi4py](https://mpi4py.readthedocs.io/en/stable/)
  - [numpy](https://numpy.org/)
  - [pandas](https://pandas.pydata.org/)
  - [plotly](https://plotly.com/)
  - [pymoo](https://pymoo.org/)
  - [requests](https://requests.readthedocs.io/)
  - [SALib](https://salib.readthedocs.io/en/latest/)
  - [scipy](https://www.scipy.org/)
  - [scoop](https://github.com/soravux/scoop)
  - [scikit-learn](https://scikit-learn.org/stable/)
  - [stable-baselines](https://stable-baselines.readthedocs.io/en/master/)
  - [tensorflow](https://www.tensorflow.org/)
  - [termcolor](https://pypi.org/project/termcolor/)


Install
=======

Dependencies:

Ubuntu 18.04:
```bash
# PPA for g++-9
$ sudo add-apt-repository ppa:ubuntu-toolchain-r/test
$ sudo apt install  virtualenv  python3-pip  python-matplotlib  cmake  g++-9  libboost-dev  libopenmpi-dev
```

Ubuntu 20.04:
```bash
$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt-get update
$ sudo apt-get install python3.6 python3.6-dev
$ sudo apt install virtualenv cmake g++-9 libboost-dev libopenmpi-dev
```

Clone and update submodules:

```bash
$ git clone <projet_URL>
$ cd deconf
$ git submodule init
$ git submodule update
```

C++:

```bash
$ cd <project_root>
$ mkdir build ; cd build
  Option 1, use g++-9 compiler, default compiling options (no assert, no trace):
  $ CXX=g++-9 cmake ..
  Option 2, use g++ compiler, default compiling options (no assert, no trace)
  $ cmake ..
  Option 3, configure compiling options with CCMake (press c, toggle options with Return, press c, g):
  $ ccmake ..
$ make
```

Python:

```bash
$ cd <project_root>
$ virtualenv -p python3.6 venv/
$ source venv/bin/activate
$ pip install -r requirements.txt
# for Python bindings of C++ Alizon and Salje models:
$ pip install src/model
```

Unit tests
----------

Python:

```bash
$ cd test
$ PYTHONPATH+=.. python -m unittest  --verbose
```

C++:

```bash
$ cd build
$ ./tests
$ ./test_brute_force_optim
```

The projet uses Continuous Integration.
After every commit, unit tests are run on Gitlab:
https://gitlab.com/covid_dia/deconf/-/jobs
Check § "Docker notes" at the end of this file.



Use
===

You must run separately the model server (macromodel or micromodel) and the optimizer server.

Macromodel server (default port 8000)
-----------------------------

Run server:

```bash
$ cd <project_root>
$ source venv/bin/activate
$ PYTHONPATH+=. python src/model/macromodel.py
```
or for custom port configuration:

```bash
$ cd <project_root>
$ bash run/run_model.sh run/config.txt
```

Test with CURL & Post:

```bash
T2$ curl --data @doc/eval_policy_req.json http://localhost:8000/eval_policy > /tmp/eval_policy.json
# use jq to reformat cleanly the file (easier to read)
T2$ jq . /tmp/eval_policy.json | less
<you should get something similar to doc/eval_policy_res.json>
```

Micromodel server (default port 8000)
-------------------------------------

This is a WIP.

```bash
$ cd <project_root>/build
$ ./decov -p ../doc/eval_policy_req.json  -d ../src/model/decov/data.json
```

To annotate with stamps

```bash
$ ./decov -p ../doc/eval_policy_req.json  -d ../src/model/decov/data.json 2>&1 | ts "%H:%M:%.S"
```

BSON tests:

```bash
$ ./decov -p ../doc/eval_policy_req.json  -d ../src/model/decov/data.json -save f.bson
$ ./decov -p ../doc/eval_policy_req.json  -d ../src/model/decov/data.json -load f.bson 2>&1 | ts "%H:%M:%.S"
```

Optimizer server (default port 8001)
----------------------------

We also run macromodel server in the following snippet:

```bash
$ cd <project_root>
$ source venv/bin/activate
T1$ PYTHONPATH+=. python src/model/macromodel.py
T2$ PYTHONPATH+=. python src/optim/random_optim.py
```

or for custom port configuration:

```bash
$ cd <project_root>
T1$ bash run/run_model.sh run/config.txt
T2$ bash run/run_optim.sh run/config.txt
```

Using the in-house scheduler: there is no need to start the model
server, ie no T1 step. In order to allow the scheduler to seamlessly
spawn jobs without asking for a password, we recommend to use SSH keys
([how to generate keys](https://docs.gitlab.com/ee/ssh/#ed25519-ssh-keys))
and start an agent.

```bash
T2$ eval `ssh-agent`
T2$ ssh-add ~/.ssh/id_ed25519
T2$ PYTHONPATH+=. python src/optim/random_optim.py --enablescheduler --computingresources src/scheduler/resources_4cores --singlerequest
```

[More information on using the
scheduler](https://gitlab.com/covid_dia/deconf/-/tree/master/src/scheduler).


Tests:

```bash
T3$ curl --data @doc/optimize_req.json http://localhost:8001/optimize > /tmp/optimize.json
T3$ jq . /tmp/optimize.json | less
<you should get something similar to doc/optimize_res.json>
```

Frontend
--------

Run:

```bash
$ cd <project_root>
T1$ python run/run_all_models_and_optimizers.py run/config.json
T2$ bash run/run_frontend.sh run/config.json
T3$ firefox  http://localhost:8080/
```

Util to visualize contact matrices
(see `doc/france_contact_matrix.csv` for format):

Run the frontend steps, then:

```bash
$ cd <project_root>
$ cp yourmatrix.csv src/frontend/static
$ firefox localhost:8080/visualize_contact.html
```

and change the name of your matrix in the input.


Message Passing Interface (MPI)
-------------------------------

Documentation is available on this page:
[https://gitlab.com/covid_dia/deconf/-/tree/master/src/mpi](https://gitlab.com/covid_dia/deconf/-/tree/master/src/mpi)


Contrib
=======

Measures
--------

The supported activable measures per week are listed here :
https://docs.google.com/spreadsheets/d/12rbLql-R7eG06RTnvZSW2W8PQW3bPYUfxyuCzih4u9A/edit#gid=1699577903
(tab "Mesures")

Optimizers
----------

Optimizers are based on two classes:
  - BaseOptimServer: inherits a HTTPServer, will wait for "optim_req" requests,
    when one is received will spawn a BaseOptimRequestHandler
  - BaseOptimRequestHandler: will make the effective optimization

To create a new optimizer, you need to create a class that inherits BaseOptimRequestHandler.
This class must overload
`def optimize(self, optimize_req, policy_common_values, activable_measures_dict)`.
Complete doc is in `base_optim.py`.

Train the RL models
-------------------

To re-train the RL models, do:

```bash
T1$ source venv/bin/activate
T1$ PYTHONPATH=. python src/optim/macro_rl_train.py
Optional, monitor with TensorBoard:
T2$ source venv/bin/activate
T2$ tensorboard --logdir /tmp/log_tb/
```

Docker notes
============

To check CI minutes quota (2000 free per month) :
https://gitlab.com/groups/covid_dia/-/usage_quotas

To build the docker image locally:

```bash
$ sudo apt install docker.io
$ docker build .
```

To build the docker image for Gitlab CI:
https://gitlab.com/covid_dia/deconf/container_registry


```bash
$ sudo apt  install docker.io
$ docker login registry.gitlab.com
$ docker build -t registry.gitlab.com/covid_dia/deconf .
$ docker push registry.gitlab.com/covid_dia/deconf
One liner:
docker build -t registry.gitlab.com/covid_dia/deconf . && docker push registry.gitlab.com/covid_dia/deconf
```

Troubleshooting
---------------

Bug:
"ERROR: Could not install packages due to an EnvironmentError: [Errno 28] No space left on device"

Cause: partition `/var/lib/docker` is full on local filesystem.

Fix : move docker to another partition, e.g. `/home`
(from https://www.guguweb.com/2019/02/07/how-to-move-docker-data-directory-to-another-location-on-ubuntu/ ):

```bash
$ docker system prune -a  # clean the current partition before leaving
$ sudo service docker stop
$ sudo rm /var/lib/docker -rf  # clean, more brutal
$ sudo mkdir /home/docker
$ sudo nano /etc/docker/daemon.json
{
"graph": "/home/docker"
}
$ sudo service docker start
```
